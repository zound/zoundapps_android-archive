package com.apps_lib.multiroom.setup.presets;

import android.app.Activity;
import android.os.AsyncTask;

import com.apps_lib.multiroom.util.LocationDecoder;
import com.apps_lib.multiroom.presets.BlobEncoderFactory;
import com.apps_lib.multiroom.presets.IBlobEncoder;
import com.apps_lib.multiroom.presets.PresetItemModel;
import com.apps_lib.multiroom.presets.PresetSubType;
import com.apps_lib.multiroom.presets.PresetTypeNames;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by nbalazs on 03/10/2016.
 */
public class DefaultIRPresetListCreatorTask extends AsyncTask<Void, Void, List<PresetItemModel>> {

    private Activity mActivity;
    private boolean mGetFileFromURL;
    private IPresetListReadyCallback mReadyCallback;

    private static final String STATION_ID = "stationId";
    private static final String STATION_NAME = "stationName";
    private static final String STATION_ARTWORK_URL = "artworkUrl";
    private static final String DEFAULT_COUNTRY = "default";
    private static final String GROUP_OF_COUNTRIES = "countries";

    private static String PRESETS_SERVER_URL = "";
    private static int DEFAULT_PRESETS_RAW_FILE_ID = -1;

    public interface IPresetListReadyCallback {
        void onIRPresetListReady(List<PresetItemModel> presetItemModelList);

        void onIRPresetReady(PresetItemModel presetItemModel);
    }

    public static void init(String serverURL, int rawFileID) {
        PRESETS_SERVER_URL = serverURL;
        DEFAULT_PRESETS_RAW_FILE_ID = rawFileID;
    }

    public DefaultIRPresetListCreatorTask(Activity activity, IPresetListReadyCallback readyCallback, boolean getFileFromURL) {
        mActivity = activity;
        mReadyCallback = readyCallback;
        mGetFileFromURL = getFileFromURL;
    }

    @Override
    protected List<PresetItemModel> doInBackground(Void... params) {
        JSONObject jsonCountryDefaults = null;
        String iso2Country = LocationDecoder.getCountryCodeSync();
        if (mGetFileFromURL) {
            jsonCountryDefaults = readURL(PRESETS_SERVER_URL);
        }
        if (jsonCountryDefaults == null) {
            jsonCountryDefaults = getDefaultsFromLocalJsonFile();
        }

        return getDefaultPresetListForCountry(jsonCountryDefaults, iso2Country);
    }

    @Override
    protected void onPostExecute(List<PresetItemModel> list) {
        mReadyCallback.onIRPresetListReady(list);
    }

    private List<PresetItemModel> getDefaultPresetListForCountry(JSONObject jsonObject, String iso2Country) {
        List<PresetItemModel> presetItemModelList = null;
        JSONArray jsonPresetList = null;
        try {
            jsonPresetList = jsonObject.getJSONObject(GROUP_OF_COUNTRIES).getJSONArray(iso2Country);
        } catch (JSONException e) {
            try {
                // If the country is not available in the default json file, use the "default" country stations
                jsonPresetList = jsonObject.getJSONArray(DEFAULT_COUNTRY);
            } catch (JSONException e1) {
                e1.printStackTrace();
            }
        }
        if (jsonPresetList != null) {
            try {
                presetItemModelList = new ArrayList<>();
                IBlobEncoder blobEncoder = BlobEncoderFactory.getNewBlobEncoder(PresetTypeNames.IR);
                for (int i = 0; i < jsonPresetList.length(); i++) {
                    JSONObject jsonPresetStation = jsonPresetList.getJSONObject(i);
                    final PresetItemModel presetItemModel = new PresetItemModel(jsonPresetStation.getString(STATION_NAME),
                            PresetTypeNames.IR, blobEncoder.encodeToString(jsonPresetStation.getString(STATION_ID)),
                            jsonPresetStation.getString(STATION_ARTWORK_URL), PresetSubType.InternetRadio, i);
                    presetItemModelList.add(presetItemModel);

                    notifyNewStation(presetItemModel);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return presetItemModelList;
    }

    private JSONObject readURL(String httpLink) {
        StringBuilder stringBuilder = new StringBuilder();
        HttpURLConnection urlConnection = null;
        InputStream inputStream = null;
        JSONObject jsonObject = null;


        try {
            URL url = new URL(httpLink);
            urlConnection = (HttpURLConnection) url.openConnection();
            inputStream = new BufferedInputStream(urlConnection.getInputStream());
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            String line;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (inputStream != null) {
                    inputStream.close();
                }
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        try {
            jsonObject = new JSONObject(stringBuilder.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject; // Success = jsonObject != null ? true : false
    }

    private void notifyNewStation(final PresetItemModel presetItemModel) {
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mReadyCallback.onIRPresetReady(presetItemModel);
            }
        });
    }

    private JSONObject getDefaultsFromLocalJsonFile() {
        JSONObject jsonObject = null;
        InputStream inputStream = null;
        try {
            inputStream = mActivity.getResources().openRawResource(DEFAULT_PRESETS_RAW_FILE_ID);
            int count;
            byte[] bytes = new byte[1000];
            StringBuilder stringBuilder = new StringBuilder();
            while ((count = inputStream.read(bytes, 0, 1000)) > 0) {
                stringBuilder.append(new String(bytes, 0, count));
            }
            String radioInputList = stringBuilder.toString();
            jsonObject = new JSONObject(radioInputList);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return jsonObject;
    }
}
