package com.apps_lib.multiroom.setup.normalSetup;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.apps_lib.multiroom.R;
import com.apps_lib.multiroom.NavigationHelper;
import com.apps_lib.multiroom.UEActivityBase;
import com.apps_lib.multiroom.databinding.ActivityHiddenSsidBinding;
import com.apps_lib.multiroom.widgets.EditTextWithBackEvent;

/**
 * Created by nbalazs on 13/10/2016.
 */

public class HiddenSSIDActivity extends UEActivityBase implements EditTextWithBackEvent.EditTextImeBackListener, View.OnFocusChangeListener, IConfigNetworkListener {

    private ActivityHiddenSsidBinding mBinding;
    public static final int REQUEST_CODE_AUTH = 43534;
    public static final int REQUEST_CODE_ENC = 5435;
    private HiddenSSIDViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View rootView = LayoutInflater.from(this).inflate(R.layout.activity_hidden_ssid, null);
        mBinding = DataBindingUtil.bind(rootView);
        setContentView(rootView);

        setupAppBar();
        enableUpNavigation();
        setTitle(R.string.ssid_other_network);

        viewModel = new HiddenSSIDViewModel(this, mBinding);
        mBinding.setViewModel(viewModel);

        initCallbacks();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mBinding != null) {
            mBinding.buttonConnect.setVisibility(View.VISIBLE);
            mBinding.focusableLayout.requestFocus();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    void initCallbacks() {
        mBinding.titleTextView.setOnFocusChangeListener(this);

        mBinding.networkName.setOnEditTextImeBackListener(this);
        mBinding.networkName.setOnFocusChangeListener(this);

        mBinding.authType.setOnEditTextImeBackListener(this);
        mBinding.authType.setOnFocusChangeListener(this);

        mBinding.encType.setOnEditTextImeBackListener(this);
        mBinding.encType.setOnFocusChangeListener(this);

        mBinding.passwordText.setOnEditTextImeBackListener(this);
        mBinding.passwordText.setOnFocusChangeListener(this);

        mBinding.ipAddress.setOnFocusChangeListener(this);
        mBinding.ipAddress.setOnEditTextImeBackListener(this);

        mBinding.subnetMask.setOnFocusChangeListener(this);
        mBinding.subnetMask.setOnEditTextImeBackListener(this);

        mBinding.gatewayAddress.setOnFocusChangeListener(this);
        mBinding.gatewayAddress.setOnEditTextImeBackListener(this);

        mBinding.primaryAddress.setOnFocusChangeListener(this);
        mBinding.primaryAddress.setOnEditTextImeBackListener(this);

        mBinding.secondaryAddress.setOnFocusChangeListener(this);
        mBinding.secondaryAddress.setOnEditTextImeBackListener(this);

        TextView.OnEditorActionListener editorActionListener = new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (event == null) {
                    if (v.getId() == mBinding.ipAddress.getId()) {
                        mBinding.subnetMask.requestFocus();
                        mBinding.subnetMask.setSelection(mBinding.subnetMask.getText().length());
                    } else if (v.getId() == mBinding.subnetMask.getId()) {
                        mBinding.gatewayAddress.requestFocus();
                        mBinding.gatewayAddress.setSelection(mBinding.gatewayAddress.getText().length());
                    } else if (v.getId() == mBinding.gatewayAddress.getId()) {
                        mBinding.primaryAddress.requestFocus();
                        mBinding.primaryAddress.setSelection(mBinding.primaryAddress.getText().length());
                    } else if (v.getId() == mBinding.primaryAddress.getId()) {
                        mBinding.secondaryAddress.requestFocus();
                        mBinding.secondaryAddress.setSelection(mBinding.secondaryAddress.getText().length());
                    } else {
                        mBinding.buttonConnect.setVisibility(View.VISIBLE);
                        mBinding.focusableLayout.requestFocus();
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    }
                    return true;
                }
                return false;
            }
        };

        mBinding.networkName.setOnEditorActionListener(editorActionListener);
        mBinding.passwordText.setOnEditorActionListener(editorActionListener);
        mBinding.ipAddress.setOnEditorActionListener(editorActionListener);
        mBinding.subnetMask.setOnEditorActionListener(editorActionListener);
        mBinding.gatewayAddress.setOnEditorActionListener(editorActionListener);
        mBinding.primaryAddress.setOnEditorActionListener(editorActionListener);
        mBinding.secondaryAddress.setOnEditorActionListener(editorActionListener);
    }

    @Override
    public void onImeBack(EditTextWithBackEvent ctrl, String text) {
        mBinding.focusableLayout.requestFocus();
    }

    @Override
    public void onFocusChange(View view, boolean hasFocus) {
        final int viewId = view.getId();
        final int networkNameId = mBinding.networkName.getId();
        final int authenticationTypeId = mBinding.authType.getId();
        final int encodingTypeId = mBinding.encType.getId();
        final int passwordTextId = mBinding.passwordText.getId();
        final int ipAddressTextId = mBinding.ipAddress.getId();
        final int subnetMaskTextId = mBinding.subnetMask.getId();
        final int gatewayAddressTextId = mBinding.gatewayAddress.getId();
        final int primaryAddressTextId = mBinding.primaryAddress.getId();
        final int secondaryAddressTextId = mBinding.secondaryAddress.getId();

        if (viewId == networkNameId || viewId == authenticationTypeId || viewId == passwordTextId || viewId == encodingTypeId || viewId == ipAddressTextId ||
                viewId == subnetMaskTextId || viewId == gatewayAddressTextId || viewId == primaryAddressTextId || viewId == secondaryAddressTextId) {
            if (hasFocus) {
                mBinding.buttonConnect.setVisibility(View.GONE);
            } else {
                mBinding.buttonConnect.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_AUTH) {
            handleAuthRespons(resultCode, data);
        } else if (requestCode == REQUEST_CODE_ENC) {
            handleEncResponse(resultCode, data);
        }
    }

    private void handleAuthRespons(int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            String result = data.getStringExtra("AUTH_TYPE_AS_STRING");
            viewModel.mAuthType.set(result);
        }
    }

    private void handleEncResponse(int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            String result = data.getStringExtra("ENC_TYPE_AS_STRING");
            viewModel.mEncType.set(result);
        }
    }

    @Override
    public void onSubmitFriendlyName() {

    }

    @Override
    public void onConfigReadyToCommit() {
        goToFinalisingPage();
    }

    private void goToFinalisingPage() {
        NavigationHelper.goToActivity(HiddenSSIDActivity.this, SetupFinalizingActivity.class, NavigationHelper.AnimationType.SlideToLeft);
    }
}
