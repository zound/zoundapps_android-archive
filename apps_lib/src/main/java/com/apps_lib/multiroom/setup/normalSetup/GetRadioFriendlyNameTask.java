package com.apps_lib.multiroom.setup.normalSetup;

import android.os.AsyncTask;

import com.frontier_silicon.NetRemoteLib.Node.NodeSysInfoFriendlyName;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;

import java.util.ArrayList;
import java.util.List;

class GetRadioFriendlyNameTask extends AsyncTask<Void, Integer, String> {
    private List<SetupManager.IGetFriendlyNameAsync> mGetFriendlyNameInterfaces = new ArrayList<>();

    GetRadioFriendlyNameTask(SetupManager.IGetFriendlyNameAsync getFriendlyNameInterface) {
        setInterfaceListener(getFriendlyNameInterface);
    }

    @Override
    protected String doInBackground(Void... params) {
        return getRadioFriendlyNameFromDeviceSync();
    }

    @Override
    protected void onPostExecute(String friendlyName) {
        SetupManager.getInstance().setFriendlyName(friendlyName);

        for (int i = 0; i < mGetFriendlyNameInterfaces.size(); i++) {
            SetupManager.IGetFriendlyNameAsync friendlyNameInterface = mGetFriendlyNameInterfaces.get(i);
            friendlyNameInterface.onFriendlyNameRetrieved(friendlyName);
        }

        mGetFriendlyNameInterfaces.clear();
        SetupManager.getInstance().removeFriendlyNameTask();
    }

    void setInterfaceListener(SetupManager.IGetFriendlyNameAsync getFriendlyNameInterface) {
        if (getFriendlyNameInterface != null)
            mGetFriendlyNameInterfaces.add(getFriendlyNameInterface);
    }

     String getRadioFriendlyNameFromDeviceSync() {
        String radioName = "";
        Radio radio = SetupManager.getInstance().getCurrentRadio();
        if (radio != null) {
            NodeSysInfoFriendlyName nameNode = (NodeSysInfoFriendlyName) radio.getNodeSyncGetter(NodeSysInfoFriendlyName.class, true).get();

            if (nameNode != null)
                radioName = nameNode.getValue();
        }

        return radioName;
    }
}
