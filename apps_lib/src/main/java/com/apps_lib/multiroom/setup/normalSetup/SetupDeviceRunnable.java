package com.apps_lib.multiroom.setup.normalSetup;

import android.text.TextUtils;

import com.frontier_silicon.NetRemoteLib.AccessPointUtil;
import com.frontier_silicon.NetRemoteLib.Discovery.ping.PingUtil;
import com.frontier_silicon.NetRemoteLib.Discovery.scanner.IScanOnDemandListener;
import com.frontier_silicon.NetRemoteLib.NetRemote;
import com.frontier_silicon.NetRemoteLib.Node.BaseSysClockTimeZone;
import com.frontier_silicon.NetRemoteLib.Node.NodeCastTos;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysCfgIrAutoPlayFlag;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysInfoFriendlyName;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.components.common.CommonPreferences;
import com.frontier_silicon.components.common.FsComponentsConfig;
import com.frontier_silicon.components.common.RadioNodeUtil;
import com.frontier_silicon.components.NetRemoteManager;
import com.frontier_silicon.components.setup.RadioNetworkConfig.EConnectionType;
import com.frontier_silicon.components.setup.RadioNetworkConfig.RadioNetworkConfigParams;
import com.frontier_silicon.components.setup.RadioNetworkConfig.RadioWiFiSetup;
import com.frontier_silicon.loggerlib.FsLogger;
import com.apps_lib.multiroom.setup.dateTimeConfig.DateTimeNodesSender;
import com.apps_lib.multiroom.setup.dateTimeConfig.DateTimeParams;

import java.util.List;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;

class SetupDeviceRunnable implements Runnable {
    private final Object mLock = new Object();
    private IStateOfCheckingHeadlessSetupListener mStateListener;
    private int mCurrentProgress = 0;
    private int mSecondsToAchieve = 0;
    private volatile boolean mShouldStopTheTimer = false;
    private Timer mProgressTimer;

    SetupDeviceRunnable(IStateOfCheckingHeadlessSetupListener stateListener) {
        setListener(stateListener);
    }

    @Override
    public void run() {
        setupDeviceImpl();
    }

    void setShouldStopTheTimer(boolean shouldStopTheTimer) {
        mShouldStopTheTimer = shouldStopTheTimer;
    }

    synchronized private void setupDeviceImpl() {
        SetupManager setupManager = SetupManager.getInstance();

        RadioNetworkConfigParams configParams = setupManager.getConfigParams();
        boolean isConfiguredOK = false;

        if (mStateListener == null || configParams == null)
            return;

        FsLogger.log("SetupManager:setupNetworkForRadio keyId= " + configParams.mSelectedAPKeyId + " region= " + configParams.mRegionId /*+ " pass= " + configParams.mWiFiPasscode*/);

        sendConfiguringMessageDependingOnConfigParams(configParams);

        Radio radio = setupManager.getCurrentRadio();
        if (radio != null) {
            setRadioFriendlyName();

            setAutoPlayNode();

            setCastToSNode(radio);

            setTimeZone(radio);

            DateTimeParams dateTimeParams = setupManager.getDateTimeParams();
            DateTimeNodesSender dateTimeNodesSender = new DateTimeNodesSender(radio);
            dateTimeNodesSender.setup(dateTimeParams);

            GoogleCastSetupSetter castSetupSetter = new GoogleCastSetupSetter(radio);
            castSetupSetter.setCastAcceptance();

            RadioWiFiSetup radioWiFiSetup = new RadioWiFiSetup(radio);
            isConfiguredOK = radioWiFiSetup.setupNetwork(configParams);
            isConfiguredOK = radioWiFiSetup.closeSetupNetwork();
        }

        setupManager.closeRadioConnection();

        if (!isConfiguredOK) {
            setStateOfSetup(EStateOfCheckingHeadlessSetup.NodesWereNotSetCorrectly, setupManager);
            setupManager.cleanNetRemote();

        } else if (configParams.mConnectionType == EConnectionType.WPS) {
            setStateOfSetup(EStateOfCheckingHeadlessSetup.WaitOnConnectingWithWPS, setupManager);
            updateProgress(FsComponentsConfig.MAX_SECONDS_TO_CONNECT_WITH_WPS);

            int seconds = 0;
            do {
                RadioNodeUtil.waitMs(1000);
                seconds++;

                if (mShouldStopTheTimer) {
                    mShouldStopTheTimer = false;
                    break;
                }
            } while (seconds < FsComponentsConfig.MAX_SECONDS_TO_CONNECT_WITH_WPS);

            sendSetupComplete();
            setupManager.cleanNetRemote();

        } else if (configParams.mConnectionType == EConnectionType.Ethernet) {
            sendSetupComplete();
            setupManager.cleanNetRemote();

        } else if (configParams.mConnectionType == EConnectionType.WiFi) {

            setStateOfSetup(EStateOfCheckingHeadlessSetup.ConnectingToSelectedWiFi, setupManager);
            updateProgress(FsComponentsConfig.MAX_SECONDS_TO_WAIT_FOR_CONNECTING_TO_AP);

            boolean connectedToAP = connectToAPSelectedByUser(configParams);

            //Check if the phone is connected to the selected Wi-Fi. If not, try to reconnect.
            if(!connectedToAP) {
                updateProgress(FsComponentsConfig.MAX_SECONDS_TO_WAIT_FOR_CONNECTING_TO_AP);
                connectedToAP =  connectToAPSelectedByUser(configParams);
            }

            if (!connectedToAP) {
                setStateOfSetup(EStateOfCheckingHeadlessSetup.CouldntConnectToWiFi, setupManager);
                setupManager.cleanNetRemote();
                return;
            }

            setStateOfSetup(EStateOfCheckingHeadlessSetup.SearchingTheAudioDevice, setupManager);
            updateProgress(FsComponentsConfig.MAX_SECONDS_TO_LOOK_FOR_CONFIGURED_RADIO);

            Radio foundRadio = searchDeviceWithDiscovery(FsComponentsConfig.MAX_SECONDS_TO_LOOK_FOR_CONFIGURED_RADIO);
            if (foundRadio == null) {
                setStateOfSetup(EStateOfCheckingHeadlessSetup.CouldntFindDeviceWithSSDP, setupManager);
                setupManager.cleanNetRemote();
                return;
            }

            sendSetupComplete();
            setupManager.cleanNetRemote();
            setupManager.setFoundRadioAfterSetup(foundRadio);
        }
    }

    private void setCastToSNode(Radio radio) {
        boolean castAccepted = CommonPreferences.getInstance().getCastTosAcceptance();

        NodeCastTos.Ord ord = castAccepted ? NodeCastTos.Ord.ACTIVE : NodeCastTos.Ord.INACTIVE;
        radio.setNode(new NodeCastTos(ord), true);
        RadioNodeUtil.waitDefaultMs();
    }

    private void setTimeZone(Radio radio) {
        TimeZone tz = TimeZone.getDefault();
        BaseSysClockTimeZone baseSysClockTimeZone = new BaseSysClockTimeZone(tz.getID());
        radio.setNode(baseSysClockTimeZone, true);
    }

    private void sendConfiguringMessageDependingOnConfigParams(RadioNetworkConfigParams configParams) {
        SetupManager setupManager = SetupManager.getInstance();
        EStateOfCheckingHeadlessSetup stateOfSetup;

        if (configParams.mConnectionType == EConnectionType.WPS) {
            stateOfSetup = EStateOfCheckingHeadlessSetup.ConfiguringWithWPS;
            setStateOfSetup(stateOfSetup, setupManager);
            updateProgress(FsComponentsConfig.MAX_SECOND_TO_CONFIG_WITH_WPS);
        }
        else {
            stateOfSetup = EStateOfCheckingHeadlessSetup.ConfiguringAudioDevice;
            setStateOfSetup(stateOfSetup, setupManager);
            updateProgress(FsComponentsConfig.MAX_SECONDS_TO_CONFIGURE_THE_DEVICE);
        }
    }

    private void setStateOfSetup(EStateOfCheckingHeadlessSetup stateOfSetup, SetupManager setupManager) {
        setupManager.setStateOfSetup(stateOfSetup);
        sendNewState(stateOfSetup);
    }

    private void setRadioFriendlyName() {
        SetupManager setupManager = SetupManager.getInstance();

        String friendlyName = setupManager.getFriendlyName();
        if (!TextUtils.isEmpty(friendlyName)) {
            setupManager.getCurrentRadio().setNode(new NodeSysInfoFriendlyName(friendlyName), true);
            RadioNodeUtil.waitDefaultMs();
        }
    }

    private void setAutoPlayNode() {
        SetupManager.getInstance().getCurrentRadio().setNode(new NodeSysCfgIrAutoPlayFlag(NodeSysCfgIrAutoPlayFlag.Ord.AUTOPLAY_ON), true);

        RadioNodeUtil.waitDefaultMs();
    }

    synchronized private boolean connectToAPSelectedByUser(RadioNetworkConfigParams configParams) {
        int seconds = 0;
        FsLogger.log("Connecting to " + configParams.mSSID /*+ " with Pass " + configParams.mWiFiPasscode*/);

        AccessPointUtil.connectToAccessPoint(configParams.mSSID, configParams.mWiFiPasscode);

        while (!AccessPointUtil.isConnectedToGivenAP(configParams.mSSID) && seconds < FsComponentsConfig.MAX_SECONDS_TO_WAIT_FOR_CONNECTING_TO_AP) {
            seconds++;
            RadioNodeUtil.waitMs(1000);
        }

        return AccessPointUtil.isConnectedToGivenAP(configParams.mSSID);
    }

    private List<Radio> mFoundRadios = null;
    synchronized private Radio searchDeviceWithDiscovery(int secondsAvailable) {
        SetupManager setupManager = SetupManager.getInstance();


        mFoundRadios = null;
        String friendlyName = setupManager.getFriendlyName();
        FsLogger.log("Starting SSDP scan for " + friendlyName);

        if (TextUtils.isEmpty(friendlyName) || secondsAvailable == 0)
            return null;

        NetRemote netRemote = NetRemoteManager.getInstance().getNetRemote();
        if (netRemote == null)
            return null;

        netRemote.getDiscoveryService().singleScanOnDemand(false, new IScanOnDemandListener() {
            @Override
            public void onScanResults(List<Radio> allRadios) {
                mFoundRadios = allRadios;
            }
        });

        while (mFoundRadios == null && secondsAvailable > 0) {
            secondsAvailable--;
            RadioNodeUtil.waitMs(1000);

            if (mShouldStopTheTimer) {
                mShouldStopTheTimer = false;
                return null;
            }
        }

        if (mFoundRadios != null) {
            Radio foundRadio = radioIsInScanResults(mFoundRadios, friendlyName);
            if (foundRadio != null) {
                FsLogger.log("Found with Discovery and PING OK " + friendlyName);
                return foundRadio;
            }
        }

        if (secondsAvailable > 0) {
            return searchDeviceWithDiscovery(secondsAvailable);
        } else {
            return null;
        }
    }

    private Radio radioIsInScanResults(List<Radio> foundRadios, String searchedFriendlyName) {
        Radio foundRadio = null;

        for (int i = 0; i < foundRadios.size(); i++) {
            Radio radio = foundRadios.get(i);
            String friendlyName = radio.getFriendlyName();

            if (searchedFriendlyName.contentEquals(friendlyName)) {
                FsLogger.log("Found with Discovery: " + friendlyName + " pinging radio " + radio);
                PingUtil pingUtil = new PingUtil();
                boolean radioFoundAndAvailable = pingUtil.isRadioAvailable(radio);
                if (radioFoundAndAvailable) {
                    foundRadio = radio;
                }
                break;
            }
        }

        return foundRadio;
    }

    private void sendSetupComplete() {
        sendNewState(EStateOfCheckingHeadlessSetup.SuccesfullSetup);
    }

    private void updateProgress(int maxDuration) {
        if (mProgressTimer != null) {
            mProgressTimer.cancel();
            mCurrentProgress = mSecondsToAchieve;
            sendProgress(mSecondsToAchieve);
        }

        mSecondsToAchieve += maxDuration;

        mProgressTimer = new Timer();
        mProgressTimer.schedule(new ProgressTimerTask(), 1000, 1000);
    }

    private class ProgressTimerTask extends TimerTask {

        @Override
        public void run() {
            if (mCurrentProgress < mSecondsToAchieve) {
                mCurrentProgress++;
                sendProgress(mCurrentProgress);
            }
            else {
                sendProgress(mSecondsToAchieve);
                mProgressTimer.cancel();
            }
        }
    }

    private void sendNewState(EStateOfCheckingHeadlessSetup state) {
        synchronized (mLock) {
            if (mStateListener != null)
                mStateListener.onStateOfCheckingHeadlessSetupChanged(state);
        }
    }

    private void sendProgress(int progress) {
        synchronized (mLock) {
            if (mStateListener != null)
                mStateListener.onProgress(progress);
        }
    }

    void cleanTimer() {
        if (mProgressTimer != null) {
            mProgressTimer.cancel();
            mProgressTimer.purge();
            mProgressTimer = null;
        }
    }

    public void dispose() {
        setShouldStopTheTimer(false);
        cleanTimer();

        synchronized (mLock) {
            mStateListener = null;
        }
    }

    public void setListener(IStateOfCheckingHeadlessSetupListener stateListener) {
        synchronized (mLock) {
            mStateListener = stateListener;
        }
    }
}
