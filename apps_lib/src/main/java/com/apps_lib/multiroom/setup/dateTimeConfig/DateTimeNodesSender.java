package com.apps_lib.multiroom.setup.dateTimeConfig;

import com.frontier_silicon.NetRemoteLib.Node.NodeListItem;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysClockDst;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysClockLocalDate;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysClockLocalTime;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysClockMode;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysClockSource;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysClockUtcOffset;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.components.common.RadioNodeUtil;

/**
 * Created by lsuhov on 5/13/2015.
 */
public class DateTimeNodesSender {

    final private Radio mRadio;
    private final boolean mWaitAfterNodeSet;

    public DateTimeNodesSender(Radio radio) {
        this(radio, true);
    }

    DateTimeNodesSender(Radio radio, boolean waitAfterNodeSet) {
        mRadio = radio;
        mWaitAfterNodeSet = waitAfterNodeSet;
    }

    public boolean setup(DateTimeParams dateTimeParams) {

        try {
            if (dateTimeParams == null)
                return false;

            mRadio.setNode(new NodeSysClockMode(dateTimeParams.mHourFormat), false);
            if (mWaitAfterNodeSet) {
                RadioNodeUtil.waitDefaultMs();
            }

            NodeSysClockSource.Ord clockSource = getClockSourceForSendingToDevice(dateTimeParams.mClockSource);

            mRadio.setNode(new NodeSysClockSource(clockSource), false);
            if (mWaitAfterNodeSet) {
                RadioNodeUtil.waitDefaultMs();
            }

            switch (clockSource) {
                case SNTP:
                    sendSntpNodes(dateTimeParams);
                    break;
                case MANUAL:
                    sendManualDateTimeNodes(dateTimeParams);
                    break;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    private void sendSntpNodes(DateTimeParams dateTimeParams) {
        mRadio.setNode(new NodeSysClockUtcOffset(dateTimeParams.mUtcOffsetSeconds), false);
        if (mWaitAfterNodeSet) {
            RadioNodeUtil.waitDefaultMs();
        }

        Long dst = (dateTimeParams.mDaylightSaving != null && dateTimeParams.mDaylightSaving) ? 1L : 0;
        mRadio.setNode(new NodeSysClockDst(dst), false);
        if (mWaitAfterNodeSet) {
            RadioNodeUtil.waitDefaultMs();
        }
    }

    private void sendManualDateTimeNodes(DateTimeParams dateTimeParams) {
        mRadio.setNode(new NodeSysClockLocalDate(dateTimeParams.getDateString()), false);
        if (mWaitAfterNodeSet) {
            RadioNodeUtil.waitDefaultMs();
        }

        mRadio.setNode(new NodeSysClockLocalTime(dateTimeParams.getTimeString()), false);
        if (mWaitAfterNodeSet) {
            RadioNodeUtil.waitDefaultMs();
        }
    }

    private NodeSysClockSource.Ord getClockSourceForSendingToDevice(NodeListItem.FieldSource fieldSource) {
        return NodeSysClockSource.Ord.values()[fieldSource.ordinal()];
    }
}
