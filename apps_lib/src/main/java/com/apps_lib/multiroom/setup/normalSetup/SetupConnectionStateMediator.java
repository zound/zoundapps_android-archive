package com.apps_lib.multiroom.setup.normalSetup;

import android.app.Activity;
import android.os.Bundle;

import com.apps_lib.multiroom.factory.ActivityFactory;
import com.frontier_silicon.components.connection.ConnectionState;
import com.frontier_silicon.loggerlib.FsLogger;
import com.frontier_silicon.components.connection.ConnectionStateUtil;
import com.frontier_silicon.components.connection.IConnectionStateListener;
import com.apps_lib.multiroom.NavigationHelper;

/**
 * Created by lsuhov on 10/05/16.
 */
public class SetupConnectionStateMediator implements IConnectionStateListener {

    Activity mActivity;

    public SetupConnectionStateMediator(Activity activity) {
        mActivity = activity;
    }

    public void onResume(Activity activity) {
        mActivity = activity;
        ConnectionStateUtil.getInstance().addListener(this, true);
    }

    public void onPause() {
        removeListener();
    }

    @Override
    public void onStateUpdate(ConnectionState newState) {
        switch (newState) {
            case NO_WIFI_OR_ETHERNET:
                NavigationHelper.goToActivity(mActivity, ActivityFactory.getActivityFactory().getSetupActivity(),
                        NavigationHelper.AnimationType.SlideToRight, true);
                dispose();
                break;
            case DISCONNECTED:
            case INVALID_SESSION:
            case NOT_CONNECTED_TO_RADIO:
                navigateToConnectionLost();
                break;
        }
    }

    private void removeListener() {
        ConnectionStateUtil.getInstance().removeListener(this);
    }

    public void navigateToConnectionLost() {
        Bundle args = new Bundle();
        args.putBoolean(NavigationHelper.CONNECTION_LOST_ARG, true);

        FsLogger.log("SETUP: navigating back to first page");

        NavigationHelper.goToActivity(mActivity, ActivityFactory.getActivityFactory().getSetupActivity(),
                NavigationHelper.AnimationType.SlideToRight, true, args);

        dispose();
    }

    private void dispose() {
        mActivity = null;
        removeListener();
    }
}
