package com.apps_lib.multiroom.setup.normalSetup;

import com.frontier_silicon.NetRemoteLib.Node.NodeSysNetWlanScanList;

import java.util.ArrayList;

public interface IScanAvailableWifiNetworksListener {
	void onReceiveWiFiNetworks(ArrayList<NodeSysNetWlanScanList.ListItem> mAPList);
	void onProgress(int progress);
}
