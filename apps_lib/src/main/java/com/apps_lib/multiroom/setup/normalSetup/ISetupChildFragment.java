package com.apps_lib.multiroom.setup.normalSetup;

/**
 * Created by viftime on 16/03/16.
 */
public interface ISetupChildFragment {

    void refreshContent();
}
