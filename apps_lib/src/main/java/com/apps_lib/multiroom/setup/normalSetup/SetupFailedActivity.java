package com.apps_lib.multiroom.setup.normalSetup;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;

import com.apps_lib.multiroom.R;
import com.apps_lib.multiroom.databinding.ActivitySetupFailedBinding;
import com.apps_lib.multiroom.NavigationHelper;
import com.apps_lib.multiroom.UEActivityBase;

/**
 * Created by lsuhov on 13/04/16.
 */
public class SetupFailedActivity extends UEActivityBase {

    private ActivitySetupFailedBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View rootView = LayoutInflater.from(this).inflate(R.layout.activity_setup_failed, null);
        setContentView(rootView);
        mBinding = DataBindingUtil.bind(rootView);

        setupAppBar();
        setTitle(R.string.empty_string);


    }

    @Override
    protected void onStart() {
        setupControls();

        super.onStart();
    }

    @Override
    public void onBackPressed() {
        //Don't let to go back
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavigationHelper.goToHomeAndReconnectToInitialWiFi(this);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setupControls() {
        mBinding.exitSetup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavigationHelper.goToHomeAndReconnectToInitialWiFi(SetupFailedActivity.this);
            }
        });
    }
}
