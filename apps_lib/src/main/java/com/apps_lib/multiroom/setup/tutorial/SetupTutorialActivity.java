package com.apps_lib.multiroom.setup.tutorial;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.apps_lib.multiroom.R;
import com.apps_lib.multiroom.NavigationHelper;
import com.apps_lib.multiroom.UEActivityBase;
import com.apps_lib.multiroom.setup.presets.ChoosePresetsTypeActivity;
import com.apps_lib.multiroom.widgets.CircleViewPagerIndicator;

/**
 * Created by lsuhov on 14/04/16.
 */
public class SetupTutorialActivity extends UEActivityBase implements INavigationListener {

    private boolean isActivityStarted = false;
    private Button mButtonSkip;
    private Button mButtonNext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_setup_tutorial);

        setupAppBar();

        setupControls();
    }

    private void setupControls() {
        setupSkipButton();
        setupNextButton();

        initViewPager();
    }

    private void setupSkipButton() {
        mButtonSkip = (Button) findViewById(R.id.buttonSkip);
        if (mButtonSkip == null) {
            return;
        }

        mButtonSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavigationHelper.goToActivity(SetupTutorialActivity.this, ChoosePresetsTypeActivity.class,
                        NavigationHelper.AnimationType.SlideToLeft);
            }
        });
    }

    private void setupNextButton() {
        mButtonNext = (Button)findViewById(R.id.buttonNext);
        if (mButtonNext == null) {
            return;
        }
        mButtonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavigationHelper.goToActivity(SetupTutorialActivity.this, ChoosePresetsTypeActivity.class,
                        NavigationHelper.AnimationType.SlideToLeft);
            }
        });
    }

    private void initViewPager() {
        SetupTutorialPagerAdapter mAdapter = new SetupTutorialPagerAdapter(getSupportFragmentManager());
        ViewPager mViewPager = (ViewPager) findViewById(R.id.viewPagerSetupTutorial);
        CircleViewPagerIndicator pagerIndicator = (CircleViewPagerIndicator) findViewById(R.id.viewPagerIndicator);

        mViewPager.setAdapter(mAdapter);
        pagerIndicator.setViewPager(mViewPager);

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                if (position == 3 && positionOffset > 0.300000 && !isActivityStarted) {
                    NavigationHelper.goToActivity(SetupTutorialActivity.this, ChoosePresetsTypeActivity.class,
                            NavigationHelper.AnimationType.SlideToLeft);
                    isActivityStarted = true;

                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {}

            @Override
            public void onPageSelected(int position) {
                int titleId = getTitleIdForPagePosition(position);
                setTitle(titleId);
                if (position == 3) {
                    mButtonNext.setVisibility(View.VISIBLE);
                    mButtonSkip.setVisibility(View.GONE);
                } else {
                    mButtonNext.setVisibility(View.GONE);
                    mButtonSkip.setVisibility(View.VISIBLE);
                }
            }
        });

        mViewPager.setOffscreenPageLimit(5);

        int pagePosition = mViewPager.getCurrentItem();
        setTitle(getTitleIdForPagePosition(pagePosition));
    }

    private int getTitleIdForPagePosition(int position) {
        int titleId = -1;
        switch (position) {
            case 0:
                titleId = R.string.setup_cloud_play;
                break;
            case 1:
                titleId = R.string.setup_internet_radio;
                break;
            case 2:
                titleId = R.string.multi_mode;
                break;
            case 3:
                titleId = R.string.common_chromecast_built_in;
                break;
        }

        return titleId;
    }

    @Override
    public void goToNextPage() {

    }

    @Override
    public void goToPreviousPage() {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavigationHelper.goToHomeAndClearActivityStack(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private class SetupTutorialPagerAdapter extends FragmentPagerAdapter {

        private static final int NUM_FRAGMENTS = 4;

        public SetupTutorialPagerAdapter(FragmentManager supportFragmentManager) {
            super(supportFragmentManager);
        }

        @Override
        public Fragment getItem(int position) {

            switch (position) {
                case 0:
                    return new CloudTutorialFirstPageFragment();
                case 1:
                    return new InternetRadioTutorialFragment();
                case 2:
                    return new SoloMultiTutorialFragment();
                case 3:
                    return new GoogleCastTutorialFragment();

            }

            return null;
        }

        @Override
        public int getCount() {
            return NUM_FRAGMENTS;
        }

    }

}
