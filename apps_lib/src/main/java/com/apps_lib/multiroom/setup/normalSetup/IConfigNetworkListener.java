package com.apps_lib.multiroom.setup.normalSetup;

/**
 * Created by nbalazs on 17/10/2016.
 */
public interface IConfigNetworkListener {
    void onSubmitFriendlyName();

    void onConfigReadyToCommit();
}
