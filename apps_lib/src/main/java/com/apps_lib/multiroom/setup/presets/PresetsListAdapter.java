package com.apps_lib.multiroom.setup.presets;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.apps_lib.multiroom.R;
import com.apps_lib.multiroom.presets.PresetItemModel;
import com.apps_lib.multiroom.presets.PresetSubType;

import java.util.List;

/**
 * Created by lsuhov on 24/04/16.
 */
public class PresetsListAdapter extends ArrayAdapter<PresetItemModel> {

    private int mLayoutId;

    public PresetsListAdapter(Context context, int resource, List<PresetItemModel> list) {
        super(context, resource, list);

        mLayoutId = resource;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        View v = convertView;
        Context context = getContext();

        if (v == null) {
            LayoutInflater li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = li.inflate(mLayoutId, null);
        }

        PresetItemModel presetItemModel = getItem(position);

        if (presetItemModel == null) {
            return v;
        }

        boolean isEmpty = TextUtils.isEmpty(presetItemModel.presetName.get()) && presetItemModel.presetType == null;

        final ImageView presetImageView = (ImageView) v.findViewById(R.id.presetImageView);
        presetImageView.setVisibility(isEmpty ? View.INVISIBLE : View.VISIBLE);
        if (URLUtil.isValidUrl(presetItemModel.artworkUrl.get())) {
            Glide.with(context)
                    .load(presetItemModel.artworkUrl.get())
                    .into(presetImageView);
        }

        TextView presetIndexTextView = (TextView)v.findViewById(R.id.indexOfPresetTextView);
        presetIndexTextView.setText(String.valueOf(presetItemModel.getFriendlyPresetIndex()));

        TextView presetNameTextView = (TextView) v.findViewById(R.id.presetNameTextView);
        presetNameTextView.setText(isEmpty ? context.getString(R.string.setup_empty_caps) : presetItemModel.presetName.get());

        TextView presetTypeTextView = (TextView)v.findViewById(R.id.presetTypeTextView);
        presetTypeTextView.setVisibility(isEmpty ? View.GONE : View.VISIBLE);
        if (!isEmpty) {
            presetTypeTextView.setText(PresetSubType.getFriendlyNameFromSubType(
                    presetItemModel.presetSubType.get(), context));
        }

        return v;
    }
}
