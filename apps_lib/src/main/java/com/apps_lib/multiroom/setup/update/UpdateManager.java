package com.apps_lib.multiroom.setup.update;

import android.databinding.ObservableField;

import com.frontier_silicon.NetRemoteLib.Discovery.ping.PingUtil;
import com.frontier_silicon.NetRemoteLib.Discovery.scanner.IScanOnDemandListener;
import com.frontier_silicon.NetRemoteLib.NetRemote;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysIsuControl;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysIsuSoftwareUpdateProgress;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.components.common.FsComponentsConfig;
import com.frontier_silicon.components.common.RadioNodeUtil;
import com.frontier_silicon.loggerlib.FsLogger;
import com.frontier_silicon.components.NetRemoteManager;
import com.apps_lib.multiroom.setup.normalSetup.SetupManager;

import java.util.List;

/**
 * Created by lsuhov on 10/08/16.
 */
public class UpdateManager {
    private static UpdateManager ourInstance;

    public ObservableField<EUpdateState> updateState = new ObservableField<>(EUpdateState.None);
    private Thread mUpdateThread;
    private UpdateRunnable mUpdateRunnable;
    ObservableField<String> updatePercent = new ObservableField<>();

    public static UpdateManager getInstance() {
        if (ourInstance == null) {
            ourInstance = new UpdateManager();
        }
        return ourInstance;
    }

    private UpdateManager() {}

    void startOrAttachToUpdate() {

        if (mUpdateThread == null || !mUpdateThread.isAlive() ||
                mUpdateThread.isInterrupted()) {

            if (mUpdateRunnable != null) {
                mUpdateRunnable.dispose();
            }

            mUpdateRunnable = new UpdateRunnable();
            mUpdateThread = new Thread(mUpdateRunnable);
            mUpdateThread.start();
        }
    }

    public void updateRadio(final Radio radio) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (radio == null) {
                    return;
                }
                boolean result = radio.getNodeSyncSetter(new NodeSysIsuControl(NodeSysIsuControl.Ord.UPDATE_FIRMWARE)).set();
                if (!result) {
                    // TODO: Discuss with zound for any kind of error msg
                }
            }
        }).start();
    }

    private synchronized void setUpdateState(EUpdateState updateState) {
        this.updateState.set(updateState);
    }

    private synchronized void setUpdatePercent(String updatePercent) {
        this.updatePercent.set(updatePercent);
    }

    private class UpdateRunnable implements Runnable {

        String mRadioSN;

        @Override
        public void run() {
            setUpdateState(EUpdateState.CheckingUpdate);

            final Radio radio = getRadio();
            if (radio == null) {
                setUpdateState(EUpdateState.Error);
                return;
            }
            mRadioSN = radio.getSN();

            CheckForUpdateRunnable checkRunnable = new CheckForUpdateRunnable(radio, new ICheckForUpdateListener() {

                @Override
                public void onAvailableUpdate() {
                    setUpdateState(EUpdateState.DownloadingUpdate);
                    startUpdateProcess(radio);
                }

                @Override
                public void onNoAvailableUpdate() {
                    setUpdateState(EUpdateState.Finished);
                }

                @Override
                public void onCheckFailed() {
                    setUpdateState(EUpdateState.Error);
                }
            });
            checkRunnable.run();
        }

        private void startUpdateProcess(Radio radio) {
            boolean result = radio.getNodeSyncSetter(new NodeSysIsuControl(NodeSysIsuControl.Ord.UPDATE_FIRMWARE)).set();

            if (!result) {
                setUpdateState(EUpdateState.Error);
                return;
            }

            setUpdatePercent("0");

            waitUpdatePercentToBeGreaterThanZero(radio);

            checkUpdateProgress(radio);
        }

        private void waitUpdatePercentToBeGreaterThanZero(Radio radio) {
            int totalRetries = 60;
            int currentRetries = 0;
            boolean shouldRetry = true;

            while (shouldRetry) {
                RadioNodeUtil.waitDefaultMs();

                NodeSysIsuSoftwareUpdateProgress nodeIsuProgress = (NodeSysIsuSoftwareUpdateProgress) radio.getNodeSyncGetter(NodeSysIsuSoftwareUpdateProgress.class).get();

                if (nodeIsuProgress != null) {
                    Integer updatePercent = nodeIsuProgress.getValue().intValue();
                    setUpdatePercent(updatePercent.toString());

                    shouldRetry = (updatePercent == 0) && (++currentRetries <= totalRetries);

                    FsLogger.log("Current retries: " + currentRetries);
                } else {
                    shouldRetry = false;
                }
            }
        }

        private void checkUpdateProgress(Radio radio) {
            boolean shouldCheckProgress = true;

            FsLogger.log("Start checkUpdateProgress function");

            while (shouldCheckProgress) {
                RadioNodeUtil.waitDefaultMs();

                NodeSysIsuSoftwareUpdateProgress nodeIsuProgress = (NodeSysIsuSoftwareUpdateProgress) radio.getNodeSyncGetter(NodeSysIsuSoftwareUpdateProgress.class).get();

                if (nodeIsuProgress != null) {
                    Integer updatePercent = nodeIsuProgress.getValue().intValue();
                    setUpdatePercent(updatePercent.toString());

                    if (updatePercent == 0 || updatePercent == 100) {
                        shouldCheckProgress = false;

                        startWaitForRealUpdate();
                    }
                } else {
                    shouldCheckProgress = false;
                    //Assume that real update and reboot started
                    startWaitForRealUpdate();
                }
            }
        }

        private void startWaitForRealUpdate() {
            FsLogger.log("startWaitForRealUpdate function");

            setUpdateState(EUpdateState.Updating);
            RadioNodeUtil.waitMs(90_000);

            Radio foundRadio = searchDeviceWithDiscovery(FsComponentsConfig.MAX_SECONDS_TO_LOOK_FOR_CONFIGURED_RADIO);

            if (foundRadio != null) {
                SetupManager.getInstance().setFoundRadioAfterSetup(foundRadio);
                setUpdateState(EUpdateState.Finished);
            } else {
                setUpdateState(EUpdateState.Error);
            }
        }

        List<Radio> mFoundRadios = null;
        synchronized private Radio searchDeviceWithDiscovery(int secondsAvailable) {

            mFoundRadios = null;

            if (secondsAvailable == 0) {
                FsLogger.log("Couldn't find " + mRadioSN + " in the allocated time");
                return null;
            }

            FsLogger.log("Starting scan for " + mRadioSN);

            NetRemote netRemote = NetRemoteManager.getInstance().getNetRemote();
            if (netRemote == null)
                return null;

            netRemote.getDiscoveryService().singleScanOnDemand(false, new IScanOnDemandListener() {
                @Override
                public void onScanResults(List<Radio> allRadios) {
                    mFoundRadios = allRadios;
                }
            });

            while (mFoundRadios == null && secondsAvailable > 0) {
                secondsAvailable--;
                RadioNodeUtil.waitMs(1000);
            }

            if (mFoundRadios != null) {
                Radio foundRadio = radioIsInScanResults(mFoundRadios);
                if (foundRadio != null) {
                    FsLogger.log("Found with Discovery and PING OK " + mRadioSN);
                    return foundRadio;
                }
            }

            if (secondsAvailable > 0) {
                return searchDeviceWithDiscovery(secondsAvailable);
            } else {
                return null;
            }
        }

        private Radio radioIsInScanResults(List<Radio> foundRadios) {
            Radio foundRadio = null;

            for (int i = 0; i < foundRadios.size(); i++) {
                Radio radio = foundRadios.get(i);
                String sn = radio.getSN();

                if (mRadioSN.contentEquals(sn)) {
                    FsLogger.log("Found with Discovery: " + sn + " pinging radio " + radio);
                    PingUtil pingUtil = new PingUtil();
                    boolean radioFoundAndAvailable = pingUtil.isRadioAvailable(radio);
                    if (radioFoundAndAvailable) {
                        foundRadio = radio;
                    }
                    break;
                }
            }

            return foundRadio;
        }

        private Radio getRadio() {
            Radio radio = SetupManager.getInstance().getFoundRadioAfterSetup();

            //needed for the add presets shortcut
            if (radio == null) {
                radio = NetRemoteManager.getInstance().getCurrentRadio();
            }

            return radio;
        }

        public void dispose() {

        }
    }
}
