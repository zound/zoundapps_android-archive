package com.apps_lib.multiroom.setup.normalSetup;

public interface IStateOfCheckingHeadlessSetupListener {
	void onStateOfCheckingHeadlessSetupChanged(EStateOfCheckingHeadlessSetup state);
	void onProgress(int progress);
}
