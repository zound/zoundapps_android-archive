package com.apps_lib.multiroom.setup.normalSetup;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.frontier_silicon.NetRemoteLib.Node.NodeSysNetWlanScanList;
import com.apps_lib.multiroom.R;
import com.frontier_silicon.components.common.RadioNodeUtil;


import java.util.List;

public class WiFiForRadioArrayAdapter extends ArrayAdapter<NodeSysNetWlanScanList.ListItem> {
	Context mContext = null;

	public WiFiForRadioArrayAdapter(Context context, int resource, List<NodeSysNetWlanScanList.ListItem> objects) {
		super(context, resource, objects);

		mContext = context;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        LayoutInflater li = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (v == null)
            v = li.inflate(R.layout.list_item_wifi_network, null);

        String text = "";
        ImageView imageView = (ImageView)v.findViewById(R.id.wifiIcon);
        TextView tv = (TextView) v.findViewById(R.id.text1);
        if (position < getCount() - 1) {
            NodeSysNetWlanScanList.ListItem scanItem = getItem(position);
            text = RadioNodeUtil.getHumanReadableSSID(scanItem.getSSID());
            imageView.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_wifi));
        } else if (position == getCount() - 1){
            imageView.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_add_other_network_icon));
            text = getContext().getString(R.string.ssid_other_networks);
        }
        tv.setText(text);
        
        return v;
	}

    @Override
    public int getCount() {
        return super.getCount() + 1;
    }
}
