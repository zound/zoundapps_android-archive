package com.apps_lib.multiroom.setup.normalSetup;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.apps_lib.multiroom.R;

import java.util.List;

/**
 * Created by nbalazs on 13/10/2016.
 */

public class HiddenSSIDTextAdapter extends ArrayAdapter<String> {

    public HiddenSSIDTextAdapter(Context context, int resource, List<String> objects) {
        super(context, resource, objects);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = null;
        if (convertView == null) {
            rowView = LayoutInflater.from(getContext()).inflate(R.layout.auth_enc_list_item, null);
            TextView item = (TextView) rowView.findViewById(R.id.securityName);
            item.setText(getItem(position));
        } else {
            rowView = convertView;
        }
        
        return rowView;
    }
}
