package com.apps_lib.multiroom.setup.normalSetup;

import android.os.AsyncTask;

import com.frontier_silicon.NetRemoteLib.Node.BaseSysNetWlanScanList;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysNetWlanRegion;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysNetWlanRegionFcc;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysNetWlanScanList;
import com.frontier_silicon.components.common.RadioNodeUtil;
import com.frontier_silicon.components.setup.RadioNetworkConfig.RadioWiFiNetworkScanner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

class ScanAvailableWiFiNetworksTask extends AsyncTask<Void, Integer, List<NodeSysNetWlanScanList.ListItem>> {
    private final Object mLock = new Object();
    private IScanAvailableWifiNetworksListener mScanWifiNetworksListener = null;
    private int mCurrentProgress = -1;
    private Timer mProgressTimer;

    ScanAvailableWiFiNetworksTask(IScanAvailableWifiNetworksListener scanWifiNetworksListener) {
        mScanWifiNetworksListener = scanWifiNetworksListener;

        mProgressTimer = new Timer();
        mProgressTimer.schedule(new ScanWiFiTimerTask(), 0, 1000);
    }

    @Override
    protected List<NodeSysNetWlanScanList.ListItem> doInBackground(Void... arg0) {
        SetupManager setupManager = SetupManager.getInstance();

        retrieveWlanRegion();

        RadioWiFiNetworkScanner networkScanner = new RadioWiFiNetworkScanner();
        List<NodeSysNetWlanScanList.ListItem> scanList = networkScanner.scanAvailableWiFiNetworks(
                setupManager.getCurrentRadio(), setupManager.getRegionId(), this);

        Collections.sort(scanList, new Comparator<NodeSysNetWlanScanList.ListItem>() {
            @Override
            public int compare(BaseSysNetWlanScanList.ListItem lhs, BaseSysNetWlanScanList.ListItem rhs) {
                String leftSSID = RadioNodeUtil.getHumanReadableSSID(lhs.getSSID());
                String rightSSID = RadioNodeUtil.getHumanReadableSSID(rhs.getSSID());
                return leftSSID.compareToIgnoreCase(rightSSID);
            }
        });

        return scanList;
    }

    private void retrieveWlanRegion() {
        SetupManager setupManager = SetupManager.getInstance();

        if (setupManager.getCurrentRadio() == null) {
            return;
        }

        NodeSysNetWlanRegionFcc regionFcc = (NodeSysNetWlanRegionFcc) setupManager.getCurrentRadio().getNodeSyncGetter(NodeSysNetWlanRegionFcc.class).get();

        // 0 - EU/ANY/FULL and 1 - USA
        int regionId;
        if (regionFcc == null || regionFcc.getValue() == 0)
            regionId = NodeSysNetWlanRegion.Ord.EUROPE.ordinal();
        else
            regionId = NodeSysNetWlanRegion.Ord.USA.ordinal();

        setupManager.setRegionId(regionId);
    }

    @Override
    protected void onPostExecute(List<NodeSysNetWlanScanList.ListItem> scanList) {
        SetupManager setupManager = SetupManager.getInstance();
        mProgressTimer.cancel();

        synchronized (mLock) {
            setupManager.setScannedListOfWiFis(new ArrayList<>(scanList));

            if (mScanWifiNetworksListener != null) {
                mScanWifiNetworksListener.onReceiveWiFiNetworks(new ArrayList<>(scanList));
                mScanWifiNetworksListener = null;
            }
        }

        setupManager.removeScanWiFiTask();
    }

    private void sendProgress(int progress) {
        synchronized (mLock) {
            if (mScanWifiNetworksListener != null)
                mScanWifiNetworksListener.onProgress(progress);
        }
    }

    private class ScanWiFiTimerTask extends TimerTask {

        @Override
        public void run() {
            if (mCurrentProgress < RadioWiFiNetworkScanner.MAX_SECONDS_TO_SCAN) {
                mCurrentProgress++;
                sendProgress(mCurrentProgress);
            }
            else {
                mProgressTimer.cancel();
            }
        }
    }

    void setScanWiFiListener(IScanAvailableWifiNetworksListener scanWifiNetworksListener) {
        synchronized (mLock) {
            mScanWifiNetworksListener = scanWifiNetworksListener;
        }
    }
}
