package com.apps_lib.multiroom.setup.presets;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;

import com.apps_lib.multiroom.NavigationHelper;
import com.apps_lib.multiroom.UEActivityBase;
import com.apps_lib.multiroom.databinding.ActivitySetupPresetsFinalBinding;
import com.apps_lib.multiroom.factory.ActivityFactory;

import com.apps_lib.multiroom.R;

/**
 * Created by lsuhov on 20/04/16.
 */
public class PresetsFinalActivity extends UEActivityBase {

    private ActivitySetupPresetsFinalBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_setup_presets_final);

        setupAppBar();

        setTitle(R.string.easy_peasy);

        setupControls();
    }

    private void setupControls() {
        mBinding.buttonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavigationHelper.goToActivity(PresetsFinalActivity.this, ActivityFactory.getActivityFactory().getFinalSetupActivity(),
                        NavigationHelper.AnimationType.SlideToLeft);
            }
        });
    }

    @Override
    public void onBackPressed() {
    }
}
