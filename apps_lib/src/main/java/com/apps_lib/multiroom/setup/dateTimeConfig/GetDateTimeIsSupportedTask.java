package com.apps_lib.multiroom.setup.dateTimeConfig;

import android.os.AsyncTask;

import com.frontier_silicon.NetRemoteLib.Node.NodeListItem;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysCapsClockSourceList;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysCapsUtcSettingsList;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.components.common.RadioNodeUtil;
import com.frontier_silicon.components.common.nodeCommunication.IListNodeListener;
import com.apps_lib.multiroom.setup.normalSetup.SetupManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lsuhov on 5/12/2015.
 */
public class GetDateTimeIsSupportedTask extends AsyncTask<Void, Integer, Boolean> {

    private List<IDateTimeCapsListener> mListeners = new ArrayList<>();
    private Radio mRadio;
    private List<NodeSysCapsUtcSettingsList.ListItem> mUTCList;

    public GetDateTimeIsSupportedTask(Radio radio, IDateTimeCapsListener dateTimeCapsListener) {
        setInterfaceListener(dateTimeCapsListener);
        mRadio = radio;
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        boolean isDateTimeSupported = false;

        if (mRadio != null) {
            NodeSysCapsClockSourceList clockSourceList = (NodeSysCapsClockSourceList) mRadio.getNodeSyncGetter(NodeSysCapsClockSourceList.class, true).get();

            RadioNodeUtil.getListNodeItems(mRadio, NodeSysCapsUtcSettingsList.class, true, new IListNodeListener<NodeListItem>() {
                @Override
                public void onListNodeResult(List resultList, boolean isListComplete) {
                    mUTCList = resultList;
                }
            });

            if (clockSourceList != null) {
                notifyOnDateTimeCapsUpdated(clockSourceList, mUTCList);
                isDateTimeSupported = true;
            }
        }

        return isDateTimeSupported;
    }

    @Override
    protected void onPostExecute(Boolean dateTimeIsSupported) {
        notifyOnDateTimeSupported(dateTimeIsSupported);

        mListeners.clear();
        mRadio = null;
        SetupManager.getInstance().removeDateAndTimeIsSupportedTask();
    }

    private void notifyOnDateTimeSupported(Boolean dateTimeIsSupported) {
        for (int i = 0; i < mListeners.size(); i++) {
            IDateTimeCapsListener listener = mListeners.get(i);
            listener.onDateTimeSupportUpdate(dateTimeIsSupported);
        }
    }

    private void notifyOnDateTimeCapsUpdated(NodeSysCapsClockSourceList clockSourceList, List<NodeSysCapsUtcSettingsList.ListItem> utcTimeZoneList) {
        for (int i = 0; i < mListeners.size(); i++) {
            IDateTimeCapsListener listener = mListeners.get(i);
            listener.onDateTimeCapsUpdate(clockSourceList, utcTimeZoneList);
        }
    }

    public void setInterfaceListener(IDateTimeCapsListener dateTimeCapsListener) {
        if (dateTimeCapsListener != null)
            mListeners.add(dateTimeCapsListener);
    }
}
