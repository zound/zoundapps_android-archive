package com.apps_lib.multiroom.setup.normalSetup;

import android.content.Context;
import android.content.res.Resources;
import android.net.wifi.ScanResult;
import android.os.AsyncTask;
import android.text.TextUtils;

import com.frontier_silicon.NetRemoteLib.AccessPointUtil;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysCapsClockSourceList;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysCapsUtcSettingsList;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysNetWlanRegion;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysNetWlanScanList;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.NetRemoteLib.Radio.RadioHttp;
import com.apps_lib.multiroom.R;
import com.frontier_silicon.components.NetRemoteManager;
import com.frontier_silicon.components.common.FsComponentsConfig;
import com.frontier_silicon.components.common.TaskHelper;
import com.frontier_silicon.components.setup.RadioNetworkConfig.GetEthernetIsSupportedTask;
import com.frontier_silicon.components.setup.RadioNetworkConfig.IGetEthernetIsSupportedListener;
import com.frontier_silicon.components.setup.RadioNetworkConfig.RadioNetworkConfigParams;
import com.frontier_silicon.loggerlib.FsLogger;
import com.apps_lib.multiroom.setup.dateTimeConfig.DateTimeParams;
import com.apps_lib.multiroom.setup.dateTimeConfig.GetDateTimeIsSupportedTask;
import com.apps_lib.multiroom.setup.dateTimeConfig.IDateTimeCapsListener;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SetupManager {
	
	private static SetupManager mInstance = null;

	private Radio mCurrentRadio = null;
	private  Radio radioForLongClickedPresetSetup = null;

	private Date mTimeOfLastApScan = null; 
	
	private boolean mIsConnectingToHeadlessAP = false;
	private String mConnectingToSSID = "";
	private boolean mIsConnectingToRadio = false;
	private String mSSIDOfCurrentRadioConnection = "";

	private String mRadioFriendlyName = "";
    private Boolean mDateTimeIsSupported = null;
    private Boolean mEthernetIsSupported = null;
    private NodeSysCapsClockSourceList mClockSourceList = null;
    private ArrayList<NodeSysCapsUtcSettingsList.ListItem> mUtcTimeZoneList = null;
    private DateTimeParams mDateTimeParams = null;

	private int mRegionId = NodeSysNetWlanRegion.Ord.EUROPE.ordinal();
	private RadioNetworkConfigParams mConfigParams;

	private int mWiFiNetworkId = -1;

	private ScanAvailableWiFiNetworksTask mScanWiFiTask = null;
	private ArrayList<NodeSysNetWlanScanList.ListItem> mScannedWiFis = null;
	
	private GetRadioFriendlyNameTask mFriendlyNameTask = null;
    private GetDateTimeIsSupportedTask mDateTimeIsSuportedTask = null;
	
	private Thread mCommitingNetworkConfigThread;
	private SetupDeviceRunnable mSetupDeviceRunnable;
	private volatile EStateOfCheckingHeadlessSetup mStateOfSettingUpHeadlessSetup = EStateOfCheckingHeadlessSetup.None;
	public boolean mShouldClearListOfWiFis = false;
    private GetEthernetIsSupportedTask mGetEthernetIsSupportedTask;
    private Radio mFoundRadioAfterSetup;
	private AccessPointModel mLastSelectecAccessPoint = null;

    public static SetupManager getInstance() {
		if (mInstance == null)
			mInstance = new SetupManager();
		
		return mInstance; 
	}
	
	private SetupManager() {}

    public void saveCurrentWiFiNetworkId() {
        mWiFiNetworkId  = AccessPointUtil.getCurrentWiFiNetworkId();
    }

    public void setSelectedAccessPoint(AccessPointModel apModel) {
		mLastSelectecAccessPoint = apModel;
	}

	public AccessPointModel getLastSelectedAccessPoint() {
		return mLastSelectecAccessPoint;
	}


	public void cleanNetRemote() {
		closeRadioConnection();
	}

	public Radio getCurrentRadio() {
		return mCurrentRadio; 
	}


	public void connectToMicroAPRadio(String radioIPAddr) {
		closeRadio();

		resetConnectedToHeadlessAP();

		mIsConnectingToRadio = true;
		mSSIDOfCurrentRadioConnection = AccessPointUtil.getSSIDOfCurrentConnection();
		FsLogger.log("Connecting to NetRemote - " + mSSIDOfCurrentRadioConnection);

		mCurrentRadio = getStaticRadio(radioIPAddr, mSSIDOfCurrentRadioConnection);

		NetRemoteManager.getInstance().connectToRadio(mCurrentRadio, false, false, false);
	}

	private Radio getStaticRadio(String radioIPAddr, String ssidOfCurrentRadioConnection) {
		Radio radio = NetRemoteManager.getInstance().getNetRemote().createStaticRadioHTTP(radioIPAddr, ssidOfCurrentRadioConnection, "udn");


		((RadioHttp) radio).setPIN(NetRemoteManager.RADIO_DEFAULT_PIN);

		return radio;
	}

	public boolean isConnectedToRadio() {
		return mCurrentRadio != null && mCurrentRadio.isConnectionOk();
	}	
	
	public boolean isConnecting(ScanResult scanResult) {
		if (!isConnectingToGivenAP(scanResult)) {
			return (mIsConnectingToRadio && isConnectedToGivenAP(scanResult));
		} else {
			return true;
		}
	}
	
	public boolean isConnectedToGivenRadio(ScanResult scanResult) {
		if (!isConnectedToRadio())
			return false;
		
		if (!scanResult.SSID.contentEquals(mSSIDOfCurrentRadioConnection))
			return false;
		
		return isConnectedToGivenAP(scanResult);
	}
	
	public boolean isConnectedToAnotherRadioConnection(String ssid) {
		//in case there is no radio connection we ignore  
		if (!isConnectedToRadio())
			return false;
		
		return !ssid.contentEquals(mSSIDOfCurrentRadioConnection);
	}
	
	public boolean isConnectedToADifferentWiFiThanRadiosConnection() {
		String currentSSID = AccessPointUtil.getSSIDOfCurrentConnection();
		
		return isConnectedToAnotherRadioConnection(currentSSID);
	}
	
	public boolean isConnectingToRadio() {
		return mIsConnectingToRadio;
	}

	public void setConnectingToRadio(boolean isConnectingToRadio) {
		mIsConnectingToRadio = isConnectingToRadio;
	}
	
	public boolean canConnectToAP(ScanResult scanResult) {

		return scanResult != null && !isConnectedToGivenRadio(scanResult);
	}
	
	public List<AccessPointModel> getCurrentAccessPoints(Context context) {
		List<ScanResult> scanResults = AccessPointUtil.getScanResults();
		
		List<AccessPointModel> suggestedDevices = new ArrayList<>();
		List<AccessPointModel> otherDevices = new ArrayList<>();

		sortAccessPointsInAppropriateLists(context, scanResults, suggestedDevices, otherDevices);
		
		List<AccessPointModel> allDevices = new ArrayList<>();
		allDevices.addAll(suggestedDevices);

		return allDevices; 
	}
	
	private void sortAccessPointsInAppropriateLists(Context context, List<ScanResult> scanResults, List<AccessPointModel> suggestedDevices,
			List<AccessPointModel> otherDevices) {
		
		String[] namePartsForSuggestedDevices = getNamePartsForSuggestedDevices(context);
		
		if (scanResults != null) {
			for (ScanResult scanResult: scanResults) {
				AccessPointModel apModel = new AccessPointModel(scanResult);
				
				if (nameSeemsToBeHeadless(apModel, namePartsForSuggestedDevices))
					suggestedDevices.add(apModel);
				else
					otherDevices.add(apModel);
			}
		}
	}

	private boolean nameSeemsToBeHeadless(AccessPointModel accessPointModel, String[] namePartsForSuggestedDevices) {
		if (accessPointModel.getScanResult() == null) {
			return false;
		}

		String name = accessPointModel.getScanResult().SSID;

		if (name == null)
			return false;

		name = name.toLowerCase();

		for (String huiName : namePartsForSuggestedDevices) {
			String huiNameLowerCase = huiName.toLowerCase();
			if (name.contains(huiNameLowerCase)) {
                if (name.contains("minuet")) {
                    accessPointModel.setSpeakerName(name);
                } else {
                    accessPointModel.setSpeakerName(huiName);
                }
				return true;
			}
		}

		return false;
	}

	private boolean nameSeemsToBeHeadless(String name, String[] namePartsForSuggestedDevices) {
		if (name == null)
			return false;
		
		name = name.toLowerCase();
		
		for (String huiName : namePartsForSuggestedDevices) {
			huiName = huiName.toLowerCase();			
			if (name.contains(huiName))
				return true;
		}
		
		return false;
	}

    /**
     * Extracting each time these strings, in case there will be
     * differences between translations
     *
     * Crash #513 happened here. I've split the code in two, to try figure out which part crashed.
     */
	private String[] getNamePartsForSuggestedDevices(Context context) {
        Resources resources = context.getResources();

        return resources.getStringArray(R.array.suggested_devices_name_parts_array);
	}
	
	public boolean shoudStartNewAccessPointsScan() {
		if (mTimeOfLastApScan == null)
			return true;
		
		Date currentTimeStamp = new Date();
		long timeDifference = currentTimeStamp.getTime() - mTimeOfLastApScan.getTime();

		return (timeDifference/1000.0) > FsComponentsConfig.SECONDS_TO_WAIT_BEFORE_MAKING_NEW_AP_SCAN_WHEN_REQUESTED;
	}
	
	public boolean startWiFiAccessPointScan() {
		mTimeOfLastApScan = new Date();
		return AccessPointUtil.startAccesPointsScan();
	}
	
	public void connectToAccessPoint(ScanResult accessPoint) {
		closeRadioConnection();
		
		mIsConnectingToHeadlessAP = true;
		mIsConnectingToRadio = true;
		mConnectingToSSID = accessPoint.SSID;
		
		AccessPointUtil.connectToAccessPoint(accessPoint);
	}
	
	public boolean isConnectedToAnyAP() {			
    	return AccessPointUtil.isConnectedToWiFi();
	}

	public void resetConnectedToHeadlessAP() {
		mIsConnectingToHeadlessAP = false;
		mConnectingToSSID = "";
		mRadioFriendlyName = "";
        mIsConnectingToRadio = false;
    }
	
	public boolean isConnectedToGivenAP(ScanResult scanResult) {
		return AccessPointUtil.isConnectedToGivenAP(scanResult);
	}
	
	public boolean isConnectingToGivenAP(ScanResult scanResult) {
		if (!mIsConnectingToHeadlessAP)			
			return false;

		return scanResult.SSID.contentEquals(mConnectingToSSID);
	}
	
	public boolean isConnectingToHeadlessAP() {
		return mIsConnectingToHeadlessAP;
	}
	
	public String getSSIDOfConnectingAP() {
		return mConnectingToSSID;
	}
	
	public boolean isConnectedToHeadlessAP(Context context) {
		if (!isConnectedToAnyAP())
			return false;
		
		String currentSSID = AccessPointUtil.getSSIDOfCurrentConnection();
		String[] namePartsForSuggestedDevices = getNamePartsForSuggestedDevices(context);

		return nameSeemsToBeHeadless(currentSSID, namePartsForSuggestedDevices);
	}

    public void setEthernetIsSupported(boolean isSupported) {
        mEthernetIsSupported = isSupported;
    }

    public Boolean getEthernetIsSupported() {
        return mEthernetIsSupported;
    }

    public void setFoundRadioAfterSetup(Radio foundRadio) {
        mFoundRadioAfterSetup = foundRadio;
    }

    public Radio getFoundRadioAfterSetup() {
        return mFoundRadioAfterSetup;
    }

    public interface IGetFriendlyNameAsync {
		void onFriendlyNameRetrieved(String friendlyName);
	}
	
	public void getRadioFriendlyNameAsync(IGetFriendlyNameAsync getFriendlyNameInterface) {
		if (getFriendlyNameInterface == null)
			return;
		
		if (!TextUtils.isEmpty(mRadioFriendlyName)) {
			getFriendlyNameInterface.onFriendlyNameRetrieved(mRadioFriendlyName);
		}
		else {
			if (mFriendlyNameTask == null) {
				mFriendlyNameTask = new GetRadioFriendlyNameTask(getFriendlyNameInterface);
				TaskHelper.execute(mFriendlyNameTask);
			}
			else
				mFriendlyNameTask.setInterfaceListener(getFriendlyNameInterface);
		}
	}

    public void getDateTimeIsSupportedAsync(IDateTimeCapsListener dateTimeCapsListener) {
        if (dateTimeCapsListener == null)
            return;

        if (mDateTimeIsSupported != null) {
			dateTimeCapsListener.onDateTimeSupportUpdate(mDateTimeIsSupported);
        } else {
            if (mDateTimeIsSuportedTask == null) {
                mDateTimeIsSuportedTask = new GetDateTimeIsSupportedTask(getCurrentRadio(), dateTimeCapsListener);
                TaskHelper.execute(mDateTimeIsSuportedTask);
            } else {
                mDateTimeIsSuportedTask.setInterfaceListener(dateTimeCapsListener);
            }
        }
    }

    public void getEthernetIsSupportedAsync(IGetEthernetIsSupportedListener listener) {
        if (listener == null)
            return;

        if (mEthernetIsSupported != null) {
            listener.onEthernetSupported(mEthernetIsSupported);
        } else {
            if (mGetEthernetIsSupportedTask == null || mGetEthernetIsSupportedTask.getStatus() == AsyncTask.Status.FINISHED) {
                mGetEthernetIsSupportedTask = new GetEthernetIsSupportedTask(getCurrentRadio(), listener);
                TaskHelper.execute(mGetEthernetIsSupportedTask);
            } else {
                mGetEthernetIsSupportedTask.setInterfaceListener(listener);
            }
        }
    }

	public void scanAvailableWiFiNetworks(IScanAvailableWifiNetworksListener scanWifiNetworksListener) {
		if (scanWifiNetworksListener != null && mScannedWiFis != null) {
			// we consider that we just entered page SetupPage.SetWirelessAP and we should return the results 
			//from the scan ordered in page SetupPage.SelectName.
			scanWifiNetworksListener.onReceiveWiFiNetworks(mScannedWiFis);
			return;
		}
		
		if (mScanWiFiTask == null) {
			mScanWiFiTask = new ScanAvailableWiFiNetworksTask(scanWifiNetworksListener);
			TaskHelper.execute(mScanWiFiTask);
		}
		else {
			// In two cases it could enter here:
			// - on the first enter in page SetupPage.SetWirelessAP, the scan ordered from page SetupPage.SelectName 
			// didn't finish yet
			// - user presses repeatedly refresh button(this should not happen because we 
			// check in higher level code).
			mScanWiFiTask.setScanWiFiListener(scanWifiNetworksListener);
		}
	}

	public void setupDevice(IStateOfCheckingHeadlessSetupListener stateListener) {
		if (stateListener != null) {
			stateListener.onStateOfCheckingHeadlessSetupChanged(mStateOfSettingUpHeadlessSetup);
			
			if (isFinishedState(mStateOfSettingUpHeadlessSetup))
				return;
		}
		
		if (!setupDeviceThreadIsRunning()) {
			if (mSetupDeviceRunnable != null)
				mSetupDeviceRunnable.dispose();

			mSetupDeviceRunnable = new SetupDeviceRunnable(stateListener);
			mCommitingNetworkConfigThread = new Thread(mSetupDeviceRunnable);
			mCommitingNetworkConfigThread.start();
		}
		else
			mSetupDeviceRunnable.setListener(stateListener);
	}
	
	public boolean setupDeviceThreadIsRunning() {
		return mCommitingNetworkConfigThread != null && mCommitingNetworkConfigThread.isAlive() &&
				!mCommitingNetworkConfigThread.isInterrupted();
	}
	
	public void skipSearchingForConfiguredDevice() {
		if (mSetupDeviceRunnable != null)
			mSetupDeviceRunnable.setShouldStopTheTimer(true);
	}

	private boolean isFinishedState(EStateOfCheckingHeadlessSetup stateOfSettingUpHeadlessSetup) {
		return stateOfSettingUpHeadlessSetup == EStateOfCheckingHeadlessSetup.CouldntConnectToWiFi ||
				stateOfSettingUpHeadlessSetup == EStateOfCheckingHeadlessSetup.CouldntFindDeviceWithSSDP ||
				stateOfSettingUpHeadlessSetup == EStateOfCheckingHeadlessSetup.NodesWereNotSetCorrectly ||
				stateOfSettingUpHeadlessSetup == EStateOfCheckingHeadlessSetup.SuccesfullSetup ||
				stateOfSettingUpHeadlessSetup == EStateOfCheckingHeadlessSetup.WaitOnConnectingWithWPS;
	}

	public EStateOfCheckingHeadlessSetup getStateOfSetup() {
		return mStateOfSettingUpHeadlessSetup;
	}

	public void setStateOfSetup(EStateOfCheckingHeadlessSetup stateOfSetup) {
		mStateOfSettingUpHeadlessSetup = stateOfSetup;
	}
	
	public void resetStateOfSetup() {
		mStateOfSettingUpHeadlessSetup = EStateOfCheckingHeadlessSetup.None;
		if (mSetupDeviceRunnable != null) {
			mSetupDeviceRunnable.setShouldStopTheTimer(false);
			mSetupDeviceRunnable.cleanTimer();
		}
	}

	public void setFriendlyName(String friendlyName) {
		mRadioFriendlyName = friendlyName;
	}
	
	public String getFriendlyName() {
		return mRadioFriendlyName;
	}

	public void setNetworkConfigParams(RadioNetworkConfigParams configParams) {
		mConfigParams = configParams;
		mConfigParams.mRegionId = mRegionId;
	}
	
	public RadioNetworkConfigParams getConfigParams() {
		return mConfigParams;
	}

	public int getRegionId() {
		return mRegionId;
	}

	public void setRegionId(int regionId) {
		mRegionId = regionId;
	}
	
	public boolean reconnectToInitialWiFiNetwork() {
		return mWiFiNetworkId != -1 && AccessPointUtil.connectToNetworkId(mWiFiNetworkId);
	}

	public boolean tryReconnectToInitialWiFiNetwork() {
		return mWiFiNetworkId != -1 && AccessPointUtil.tryConnectToNetworkId(mWiFiNetworkId);
	}

	public void setScannedListOfWiFis(ArrayList<NodeSysNetWlanScanList.ListItem> scanList) {
		mScannedWiFis = scanList;
	}

	public void removeScannedListOfWiFis() {
		mScannedWiFis = null;
	}

    public Boolean getDateAndTimeSetupIsSupported() {
        return mDateTimeIsSupported;
    }

	public void setDateAndTimeSetupIsSupported(boolean isSupported) {
		mDateTimeIsSupported = isSupported;
	}

    public void setClockSourceList(NodeSysCapsClockSourceList clockSourceList) {
        mClockSourceList = clockSourceList;
    }

    public NodeSysCapsClockSourceList getClockSourceList() {
        return mClockSourceList;
    }

    public DateTimeParams getDateTimeParams() {
        return mDateTimeParams;
    }

    public void setDateTimeParams(DateTimeParams dateTimeParams) {
        mDateTimeParams = dateTimeParams;
    }

    public void setUtcTimeZoneList(ArrayList<NodeSysCapsUtcSettingsList.ListItem> utcTimeZoneList) {
        mUtcTimeZoneList = utcTimeZoneList;
    }

    public ArrayList<NodeSysCapsUtcSettingsList.ListItem> getUtcTimeZoneList() {
        return mUtcTimeZoneList;
    }

    public String getSSIDOfConfiguredRadio() {
        return mSSIDOfCurrentRadioConnection;
    }

	public void removeFriendlyNameTask() {
		if (mFriendlyNameTask != null) {
			mFriendlyNameTask.cancel(true);
			mFriendlyNameTask = null;
		}
	}

    public void removeDateAndTimeIsSupportedTask() {
        if (mDateTimeIsSuportedTask != null) {
            mDateTimeIsSuportedTask.cancel(true);
            mDateTimeIsSuportedTask = null;
        }
    }

	public void removeScanWiFiTask() {
		if (mScanWiFiTask != null) {
			mScanWiFiTask.cancel(true);
			mScanWiFiTask = null;
		}
	}

    public void removeEthernetIsSupportedTask() {
        if (mGetEthernetIsSupportedTask != null) {
            mGetEthernetIsSupportedTask.cancel(true);
            mGetEthernetIsSupportedTask = null;
        }
    }
	
	public void closeRadioConnection() {
        removeFriendlyNameTask();
		removeDateAndTimeIsSupportedTask();
		removeScanWiFiTask();
        removeEthernetIsSupportedTask();
		
		closeRadio();

        mDateTimeIsSupported = null;
        mEthernetIsSupported = null;
		mShouldClearListOfWiFis = true;
        mClockSourceList = null;
        mDateTimeParams = null;
        mUtcTimeZoneList = null;
        mFoundRadioAfterSetup = null;
		removeScannedListOfWiFis();
	}

    private void closeRadio() {
        if (mCurrentRadio != null) {
            mCurrentRadio.close();
            mCurrentRadio = null;
        }
    }

    public void setRadioForPresets(Radio radioForPresets) {
		radioForLongClickedPresetSetup = radioForPresets;
	}

	public Radio getRadioForPresets() {
		return radioForLongClickedPresetSetup;
	}

}
