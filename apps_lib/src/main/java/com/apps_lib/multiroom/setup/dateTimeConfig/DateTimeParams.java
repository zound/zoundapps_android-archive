package com.apps_lib.multiroom.setup.dateTimeConfig;

import android.content.Context;
import android.text.TextUtils;
import android.text.format.DateFormat;

import com.frontier_silicon.NetRemoteLib.Node.BaseSysClockMode;
import com.frontier_silicon.NetRemoteLib.Node.NodeListItem;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysCapsUtcSettingsList;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by lsuhov on 5/12/2015.
 */
public class DateTimeParams {
    private static final int DATE_STRING_SIZE = 8;
    private static final int TIME_STRING_SIZE = 6;

    public BaseSysClockMode.Ord mHourFormat;
    public NodeListItem.FieldSource mClockSource;
    private NodeSysCapsUtcSettingsList.ListItem mUtcOffset;
    public long mUtcOffsetSeconds = -1;
    public Boolean mDaylightSaving;
    public int mDateYear, mDateMonth, mDateDay;
    public int mHour, mMinute, mSecond;
    public TimeZone mPhoneTimeZone;

    public DateTimeParams(Context context) {
        if (DateFormat.is24HourFormat(context)) {
            mHourFormat = BaseSysClockMode.Ord._24_HOUR;
        } else {
            mHourFormat = BaseSysClockMode.Ord._12_HOUR;
        }

        mClockSource = NodeListItem.FieldSource.SNTP;

        Calendar c = Calendar.getInstance();
        mDateYear = c.get(Calendar.YEAR);
        mDateMonth = c.get(Calendar.MONTH) + 1;
        mDateDay = c.get(Calendar.DAY_OF_MONTH);

        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);
        mSecond = 0;
        mPhoneTimeZone = c.getTimeZone();
        mDaylightSaving = mPhoneTimeZone.inDaylightTime(new Date());
    }

    public String getDateString() {
        String dayStr = "" + mDateDay;
        if (mDateDay < 10) {
            dayStr = "0" + mDateDay;
        }

        String monthStr = "" + mDateMonth;
        if (mDateMonth < 10) {
            monthStr = "0" + mDateMonth;
        }

        // YYYYMMDD
        return mDateYear + monthStr + dayStr;
    }

    public String getTimeString() {

        String minuteStr = "" + mMinute;
        if (mMinute < 10) {
            minuteStr = "0" + mMinute;
        }

        String hourStr = "" + mHour;
        if (mHour < 10) {
            hourStr = "0" + mHour;
        }

        String secondStr = "" + mSecond;
        if (mSecond < 10) {
            secondStr = "0" + mSecond;
        }

        return hourStr + minuteStr + secondStr;
    }

    public void setUTCOffsetNode(NodeSysCapsUtcSettingsList.ListItem utcOffsetNode) {
        mUtcOffset = utcOffsetNode;
        mUtcOffsetSeconds = utcOffsetNode.getTime();
    }

    public NodeSysCapsUtcSettingsList.ListItem getUTCOffsetNode() {
        return mUtcOffset;
    }
    
    public void setDateFromString(String dateString) {
        // YYYYMMDD format
        final int DATE_YEAR_START_IDX = 0;
        final int DATE_YEAR_END_IDX = 4;
        final int DATE_MONTH_START_IDX = 4;
        final int DATE_MONTH_END_IDX = 6;
        final int DATE_DAY_START_IDX = 6;
        final int DATE_DAY_END_IDX = 8;

        if (!TextUtils.isEmpty(dateString) && dateString.length() == DATE_STRING_SIZE) {
            try {
                mDateYear = Integer.valueOf(dateString.substring(DATE_YEAR_START_IDX, DATE_YEAR_END_IDX));
                mDateMonth = Integer.valueOf(dateString.substring(DATE_MONTH_START_IDX, DATE_MONTH_END_IDX));
                mDateDay = Integer.valueOf(dateString.substring(DATE_DAY_START_IDX, DATE_DAY_END_IDX));
            } catch (Exception e) {
            }
        }
    }

    public void setTimeFromString(String timeString) {
        // HHMMSS format 24h
        final int TIME_HOUR_START_IDX = 0;
        final int TIME_HOUR_END_IDX = 2;
        final int TIME_MINUTE_START_IDX = 2;
        final int TIME_MINUTE_END_IDX = 4;
        final int TIME_SECOND_START_IDX = 4;
        final int TIME_SECOND_END_IDX = 6;

        if (!TextUtils.isEmpty(timeString) && timeString.length() == TIME_STRING_SIZE) {
            try {
                mHour = Integer.valueOf(timeString.substring(TIME_HOUR_START_IDX, TIME_HOUR_END_IDX));
                mMinute = Integer.valueOf(timeString.substring(TIME_MINUTE_START_IDX, TIME_MINUTE_END_IDX));
                mSecond = Integer.valueOf(timeString.substring(TIME_SECOND_START_IDX, TIME_SECOND_END_IDX));
            } catch (Exception e) {
            }
        }
    }

}
