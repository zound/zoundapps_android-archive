package com.apps_lib.multiroom.setup.normalSetup;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import com.apps_lib.multiroom.databinding.ActivitySetupWifiPasswordBinding;
import com.frontier_silicon.components.common.GuiUtils;
import com.frontier_silicon.components.setup.RadioNetworkConfig.RadioNetworkConfigParams;
import com.apps_lib.multiroom.NavigationHelper;
import com.apps_lib.multiroom.R;
import com.apps_lib.multiroom.UEActivityBase;
import com.apps_lib.multiroom.widgets.EditTextWithBackEvent;

/**
 * Created by lsuhov on 11/04/16.
 */
public class ConfigureWiFiPasswordActivity extends UEActivityBase {

    private ActivitySetupWifiPasswordBinding mBinding;
    private SetupManager mSetupManager;
    private SetupConnectionStateMediator mSetupConnectionStateMediator;
    private boolean passwordIsTooShort = true;
    private final int WPA_PASSWORD_MINIMUM_LENGTH = 8;
    private final int WPA_PASSWORD_MINIMUM_LENGTH_TO_SHOW_WORNING = 6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        View view = LayoutInflater.from(this).inflate(R.layout.activity_setup_wifi_password, null);
        mBinding = DataBindingUtil.bind(view);
        setContentView(view);

        setupAppBar();
        enableUpNavigation();
        setTitle(R.string.setup_android_wifi_password_title);

        mSetupConnectionStateMediator = new SetupConnectionStateMediator(this);

        setupControls();

    }

    @Override
    protected void onResume() {
        super.onResume();

        mSetupConnectionStateMediator.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();

        mSetupConnectionStateMediator.onPause();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavigationHelper.goToHomeAndReconnectToInitialWiFi(this);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setupControls() {
        mSetupManager = SetupManager.getInstance();
        RadioNetworkConfigParams configParams = mSetupManager.getConfigParams();

        if (configParams == null) {
            return;
        }

        mBinding.nameOfWiFiTextView.setText(configParams.mSSID);

        mBinding.checkBoxShowPassword.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    mBinding.passwordEditText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                else
                    mBinding.passwordEditText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);

                //move cursor to end
                int textEndPos = mBinding.passwordEditText.getText().length();
                mBinding.passwordEditText.setSelection(textEndPos, textEndPos);
            }
        });
        mBinding.checkBoxShowPassword.setChecked(false);
        mBinding.passwordEditText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);

        mBinding.buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                savePasswordInConfigParams();
                goToFinalizingPage();
            }
        });

        mBinding.passwordEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                GuiUtils.hideKeyboard(mBinding.passwordEditText, ConfigureWiFiPasswordActivity.this);
                String content = mBinding.passwordEditText.getText().toString();
                if (content.length() >= WPA_PASSWORD_MINIMUM_LENGTH) {
                    if (actionId == EditorInfo.IME_ACTION_DONE && !passwordIsTooShort) {
                        GuiUtils.hideKeyboard(mBinding.passwordEditText, ConfigureWiFiPasswordActivity.this);
                        savePasswordInConfigParams();
                        goToFinalizingPage();
                    }
                } else {
                    mBinding.passwordEditText.setError(getString(R.string.wifi_password_too_short));
                }

                return false;
            }
        });

        mBinding.passwordEditText.requestFocus();
        mBinding.passwordEditText.setOnEditTextImeBackListener(new EditTextWithBackEvent.EditTextImeBackListener() {
            @Override
            public void onImeBack(EditTextWithBackEvent ctrl, String text) {
                String content = mBinding.passwordEditText.getText().toString();
                if (content.length() < WPA_PASSWORD_MINIMUM_LENGTH) {
                    mBinding.passwordEditText.setError(getString(R.string.wifi_password_too_short));
                }
                mBinding.focusableLayout.requestFocus();
            }
        });

        addTextWatcher(mBinding.passwordEditText);
    }

    private void savePasswordInConfigParams() {
        RadioNetworkConfigParams configParams = mSetupManager.getConfigParams();
        configParams.mWiFiPasscode = mBinding.passwordEditText.getText().toString();
    }

    private void addTextWatcher(final EditText editText) {
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // Nothing to do here
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // Nothing to do here
            }

            @Override
            public void afterTextChanged(Editable s) {
                String content = editText.getText().toString().trim();
                if (content.length() < WPA_PASSWORD_MINIMUM_LENGTH) {
                    enableNextButton(false);
                } else {
                    passwordIsTooShort = false;
                    enableNextButton(true);
                }
            }
        });
    }

    private void goToFinalizingPage() {
        NavigationHelper.goToActivity(ConfigureWiFiPasswordActivity.this, SetupFinalizingActivity.class, NavigationHelper.AnimationType.SlideToLeft);
    }

    private void enableNextButton(boolean isEnabled) {
        mBinding.buttonNext.setVisibility(isEnabled ? View.VISIBLE : View.INVISIBLE);
        mBinding.buttonNext.setEnabled(isEnabled);
    }
}
