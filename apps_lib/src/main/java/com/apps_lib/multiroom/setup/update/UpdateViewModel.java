package com.apps_lib.multiroom.setup.update;

import android.app.Activity;
import android.databinding.Observable;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;

import com.apps_lib.multiroom.R;
import com.apps_lib.multiroom.NavigationHelper;
import com.apps_lib.multiroom.setup.normalSetup.SetupSuccessActivity;

/**
 * Created by lsuhov on 09/08/16.
 */

public class UpdateViewModel {
    Activity mActivity;

    public ObservableField<String> updatePercent = new ObservableField<>();
    public ObservableBoolean updatePercentIsVisible = new ObservableBoolean();
    public ObservableField<String> updateDescription = new ObservableField<>();

    private Observable.OnPropertyChangedCallback mPropertyChangedCallback;

    public void init(Activity activity) {
        mActivity = activity;


    }

    public void onResume() {
        if (UpdateManager.getInstance().updateState.get() == EUpdateState.Finished) {
            goToNextActivity();
            return;
        }

        if (mPropertyChangedCallback == null) {
            mPropertyChangedCallback = new Observable.OnPropertyChangedCallback() {
                @Override
                public void onPropertyChanged(Observable observable, int i) {
                    if (observable == UpdateManager.getInstance().updateState) {
                        updateState();
                    } else if (observable == UpdateManager.getInstance().updatePercent) {
                        updateThePercent();
                    }
                }
            };
        }
        UpdateManager.getInstance().updateState.addOnPropertyChangedCallback(mPropertyChangedCallback);
        UpdateManager.getInstance().updatePercent.addOnPropertyChangedCallback(mPropertyChangedCallback);

        UpdateManager.getInstance().startOrAttachToUpdate();
        updateState();
    }

    public void onPause() {
        UpdateManager.getInstance().updateState.removeOnPropertyChangedCallback(mPropertyChangedCallback);
        UpdateManager.getInstance().updatePercent.removeOnPropertyChangedCallback(mPropertyChangedCallback);
    }


    private void updateState() {
        if (mActivity == null) {
            return;
        }

        EUpdateState eUpdateState = UpdateManager.getInstance().updateState.get();

        switch (eUpdateState) {

            case None:
                //goToNextActivity();
                break;
            case CheckingUpdate:
                updateDescription.set(mActivity.getString(R.string.checking_for_updates));
                updatePercentIsVisible.set(false);
                break;
            case DownloadingUpdate:
                updateDescription.set(mActivity.getString(R.string.speaker_is_updating));
                updatePercentIsVisible.set(true);
                break;
            case Updating:
                updateDescription.set(mActivity.getString(R.string.speaker_is_updating));
                updatePercentIsVisible.set(false);
                break;
            case Finished:
                goToNextActivity();
                break;
            case Error:
                goToNextActivity();
                break;
        }
    }

    private void updateThePercent() {
        if (mActivity == null) {
            return;
        }

        updatePercent.set(mActivity.getString(R.string.percent, UpdateManager.getInstance().updatePercent.get()));
    }

    private void goToNextActivity() {
        NavigationHelper.goToActivity(mActivity, SetupSuccessActivity.class, NavigationHelper.AnimationType.SlideToLeft);
    }


    public void dispose() {
        mActivity = null;
        mPropertyChangedCallback = null;
    }
}
