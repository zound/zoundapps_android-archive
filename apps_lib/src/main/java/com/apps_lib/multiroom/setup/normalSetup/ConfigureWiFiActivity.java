package com.apps_lib.multiroom.setup.normalSetup;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.frontier_silicon.NetRemoteLib.Node.NodeSysNetWlanScanList;
import com.apps_lib.multiroom.R;
import com.apps_lib.multiroom.databinding.ActivitySetupWifiBinding;
import com.frontier_silicon.components.common.GuiUtils;
import com.frontier_silicon.components.common.RadioNodeUtil;
import com.frontier_silicon.components.setup.RadioNetworkConfig.EConnectionType;
import com.frontier_silicon.components.setup.RadioNetworkConfig.RadioNetworkConfigParams;
import com.frontier_silicon.components.setup.RadioNetworkConfig.RadioWiFiNetworkScanner;
import com.frontier_silicon.loggerlib.FsLogger;
import com.apps_lib.multiroom.NavigationHelper;
import com.apps_lib.multiroom.UEActivityBase;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ConfigureWiFiActivity extends UEActivityBase{

    public static final String FRAGMENT_TAG = "TAG_SETUP_NEW_SPEAKER_FRAGMENT";

    private ActivitySetupWifiBinding mBinding;
    private WiFiForRadioArrayAdapter mListAdapter;
    private SetupConnectionStateMediator mSetupConnectionStateMediator;

    private List<NodeSysNetWlanScanList.ListItem> mScanList = new ArrayList<>();
    private ConfigureWiFiActivity.ScanAvailableWiFiNetworksCallback mScanWiFiListener;
    private NodeSysNetWlanScanList.ListItem mSelectedAP;
    private Date mTimeStampOfStartedScanListener;
    private ProgressBar mScanningProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        View view = LayoutInflater.from(this).inflate(R.layout.activity_setup_wifi, null);
        mBinding = DataBindingUtil.bind(view);
        setContentView(view);

        setupAppBar();
        enableUpNavigation();

        setTitle(R.string.setup_android_choose_wifi_title);

        mSetupConnectionStateMediator = new SetupConnectionStateMediator(this);

        setupControls();
    }

    @Override
    public void onResume() {
        super.onResume();

        addWiFiNetworksToAdapter();

        mSetupConnectionStateMediator.onResume(this);
    }

    @Override
    public void onPause() {
        super.onPause();

        mSetupConnectionStateMediator.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_refresh, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int i = item.getItemId();
        if (i == android.R.id.home) {
            NavigationHelper.goToHomeAndReconnectToInitialWiFi(this);
            return true;
        } else if (i == R.id.action_rescan) {
            rescanWiFi();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setupControls() {

        View listHeaderView = LayoutInflater.from(this).inflate(R.layout.list_item_header, null);
        View listFooterView = LayoutInflater.from(this).inflate(R.layout.list_item_footer, null);
        mBinding.listViewWiFiNetworks.addHeaderView(listHeaderView, null, false);
        mBinding.listViewWiFiNetworks.addFooterView(listFooterView, null, false);
        mScanningProgress = mBinding.progressScanning;
        mScanningProgress.setMax(RadioWiFiNetworkScanner.MAX_SECONDS_TO_SCAN_AND_RETRIEVE);
        updateScanningControls();

        mListAdapter = new WiFiForRadioArrayAdapter(this, android.R.layout.simple_list_item_1, mScanList);
        mBinding.listViewWiFiNetworks.setAdapter(mListAdapter);
        mBinding.listViewWiFiNetworks.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position < mListAdapter.getCount()) {
                    mSelectedAP = (NodeSysNetWlanScanList.ListItem) parent.getItemAtPosition(position);
                    if (mSelectedAP != null) {
                        setConfigNetworkParams();
                        if (mSelectedAP.getPrivacy()) {
                            goToPasswordPage();
                        } else {
                            goToFinalisingPage();
                        }
                    }
                } else if (position == mListAdapter.getCount()) {
                    // Manual setup of the WIFI address
                    goToManualSetup();
                }
            }
        });

    }

    private void addWiFiNetworksToAdapter() {
        if (SetupManager.getInstance().isConnectedToRadio()) {
            if (!scanListenerIsValid()) {

                mSelectedAP = null;
                mScanWiFiListener = new ConfigureWiFiActivity.ScanAvailableWiFiNetworksCallback();
                mTimeStampOfStartedScanListener = new Date();

                updateScanningControls();

                SetupManager.getInstance().scanAvailableWiFiNetworks(mScanWiFiListener);
            } else {
                // It's possible that user goes to previous page, and quickly returns back
                SetupManager.getInstance().scanAvailableWiFiNetworks(mScanWiFiListener);
            }
        } else {
            mSetupConnectionStateMediator.navigateToConnectionLost();
        }
    }

    private void updateScanningControls() {
        boolean mScanListenerIsValid = scanListenerIsValid();
        mScanningProgress.setProgress(0);
        mBinding.listViewWiFiNetworks.setVisibility(mScanListenerIsValid ? View.GONE : View.VISIBLE);
        mScanningProgress.setVisibility(mScanListenerIsValid ? View.VISIBLE : View.GONE);
    }

    private boolean scanListenerIsValid() {
        if (mScanWiFiListener == null)
            return false;

        if (mTimeStampOfStartedScanListener != null) {
            Date currentTimeStamp = new Date();
            long timeDifference = currentTimeStamp.getTime() - mTimeStampOfStartedScanListener.getTime();
            if ((timeDifference / 1000.0) > RadioWiFiNetworkScanner.MAX_SECONDS_TO_SCAN_AND_RETRIEVE)
                return false;
        }
        return true;
    }

    protected void rescanWiFi() {
        if (mListAdapter != null) {
            mListAdapter.clear();
            mListAdapter.notifyDataSetChanged();
            SetupManager.getInstance().removeScannedListOfWiFis();

            addWiFiNetworksToAdapter();
        }
    }

    private void setConfigNetworkParams() {
        RadioNetworkConfigParams configParams = new RadioNetworkConfigParams();
        configParams.mConnectionType = EConnectionType.WiFi;
        if (mSelectedAP == null) {
            FsLogger.log("mSelectedAP was null when the user tried to commit wifi settings");
            Toast.makeText(this.getApplicationContext(), R.string.select_wifi_again, Toast.LENGTH_SHORT).show();
            return;
        }

        configParams.mSelectedAPKeyId = mSelectedAP.getKey();
        configParams.mSSID = RadioNodeUtil.getHumanReadableSSID(mSelectedAP.getSSID());

        SetupManager.getInstance().setNetworkConfigParams(configParams);
    }

    private class ScanAvailableWiFiNetworksCallback implements IScanAvailableWifiNetworksListener {

        public void onReceiveWiFiNetworks(ArrayList<NodeSysNetWlanScanList.ListItem> mAPList) {

            GuiUtils.replaceContentsOfList(mScanList, mAPList);

            mListAdapter.notifyDataSetChanged();
            mScanWiFiListener = null;
            updateScanningControls();
        }

        @Override
        public void onProgress(final int progress) {
            Activity view = ConfigureWiFiActivity.this;
            view.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    onProgressChanged(progress);
                }
            });
        }
    }

    private void onProgressChanged(int progress) {
        mScanningProgress.setProgress(progress);
    }

    private void goToFinalisingPage() {
        NavigationHelper.goToActivity(ConfigureWiFiActivity.this, SetupFinalizingActivity.class, NavigationHelper.AnimationType.SlideToLeft);
    }

    private void goToPasswordPage() {
        NavigationHelper.goToActivity(ConfigureWiFiActivity.this, ConfigureWiFiPasswordActivity.class, NavigationHelper.AnimationType.SlideToLeft);
    }

    private void goToManualSetup() {
        NavigationHelper.goToActivity(ConfigureWiFiActivity.this, HiddenSSIDActivity.class, NavigationHelper.AnimationType.SlideToLeft);
    }
}
