package com.apps_lib.multiroom.setup.normalSetup;

/**
 * Created by lsuhov on 10/04/16.
 */
public interface INoNewSpeakerFoundListener {
    void goToNextPage();
    void cancelSetup();
    void scanNewSpeakers();
}
