package com.apps_lib.multiroom.setup.normalSetup;

import android.net.wifi.ScanResult;

import java.util.Comparator;

public class AccessPointModel {
    private boolean mIsGroupHeader = false;
    private ScanResult mScanResult = null;
    private String mTitle = null;
    private String mSpeakerName; // Speaker type, like Baggen / Minuet / ...

    public AccessPointModel(String title) {
        mTitle = title;
        mIsGroupHeader = true;
    }

    AccessPointModel(ScanResult scanResult) {
        mScanResult = scanResult;
        mIsGroupHeader = false;
    }

    public AccessPointModel(String title, boolean fakeItem) {
        mTitle = title;
        mIsGroupHeader = false;
    }

    boolean getIsGroupHeader() {
        return this.mIsGroupHeader;
    }

    public String getTitle() {
        return this.mTitle;
    }

    public ScanResult getScanResult() {
        return this.mScanResult;
    }

    void setSpeakerName(String speakerName) {
        mSpeakerName = speakerName;
    }

    public String getSpeakerName() {
        return mSpeakerName;
    }

    public static class NameComparator implements Comparator<AccessPointModel> {

        @Override
        public int compare(AccessPointModel o1, AccessPointModel o2) {
            int retValue;

            String o1FriendlyName = o1.getSpeakerName();
            String o2FriendlyName = o2.getSpeakerName();
            retValue = o1FriendlyName.compareTo(o2FriendlyName);

            return retValue;
        }
    }
}
