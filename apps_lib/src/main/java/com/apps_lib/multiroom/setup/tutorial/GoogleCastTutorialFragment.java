package com.apps_lib.multiroom.setup.tutorial;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.apps_lib.multiroom.R;
import com.apps_lib.multiroom.databinding.FragmentSetupTutorialGoogleCastBinding;

/**
 * Created by lsuhov on 14/04/16.
 */
public class GoogleCastTutorialFragment extends Fragment {

    private FragmentSetupTutorialGoogleCastBinding mBinding;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_setup_tutorial_google_cast, container, false);

        return mBinding.getRoot();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setupControls();
    }

    private void setupControls() {

    }
}
