package com.apps_lib.multiroom.setup.normalSetup;

import java.util.List;

/**
 * Created by viftime on 28/03/16.
 */
public interface INewSpeakersFoundListener {

    void onNewSpeakersFound(List<AccessPointModel> deviceList);

}
