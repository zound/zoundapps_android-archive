package com.apps_lib.multiroom.setup.normalSetup;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;

import com.apps_lib.multiroom.NavigationHelper;
import com.apps_lib.multiroom.R;
import com.apps_lib.multiroom.UEActivityBase;
import com.apps_lib.multiroom.databinding.ActivitySetupFriendlyNameBinding;
import com.apps_lib.multiroom.speakerImages.ESpeakerImageSizeType;
import com.apps_lib.multiroom.speakerImages.SpeakerManager;
import com.apps_lib.multiroom.util.MessageHandlerUtil;
import com.apps_lib.multiroom.widgets.EditTextWithBackEvent;
import com.frontier_silicon.components.common.ErrorSanitizerMessageType;
import com.frontier_silicon.components.common.FriendlyNameSanitizer;
import com.frontier_silicon.components.common.FsComponentsConfig;
import com.frontier_silicon.components.common.GuiUtils;

/**
 * Created by lsuhov on 11/04/16.
 */
public class ConfigureFriendlyNameActivity extends UEActivityBase {

    private ActivitySetupFriendlyNameBinding mBinding;
    private FriendlyNameSanitizer mFriendlyNameSanitizer;
    private SetupConnectionStateMediator mSetupConnectionStateMediator;
    private boolean isActivityRestarted = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        View rootView = LayoutInflater.from(this).inflate(R.layout.activity_setup_friendly_name, null);
        setContentView(rootView);
        mBinding = DataBindingUtil.bind(rootView);

        setupAppBar();

        setTitle(R.string.setup_android_friendly_name_title);
        enableUpNavigation();

        SetupManager.getInstance().resetStateOfSetup();

        setupControls();
    }

    @Override
    protected void onStart() {
        mSetupConnectionStateMediator = new SetupConnectionStateMediator(this);
        super.onStart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        isActivityRestarted = true;
    }

    @Override
    protected void onResume() {
        initEditText();
        mSetupConnectionStateMediator.onResume(this);
        super.onResume();
    }

    @Override
    protected void onPause() {
        mSetupConnectionStateMediator.onPause();
        super.onPause();
    }

    @Override
    protected void onStop() {
        if (mFriendlyNameSanitizer != null) {
            mFriendlyNameSanitizer.hideError();
        }
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        if (mFriendlyNameSanitizer != null) {
            mFriendlyNameSanitizer.dispose();
            mFriendlyNameSanitizer = null;
        }
        super.onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavigationHelper.goToHomeAndReconnectToInitialWiFi(this);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setupControls() {
        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        display.getMetrics(displayMetrics);
        if (displayMetrics.ydpi < 225.0f) {
            mBinding.editTextFriendlyName.setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
        } else {
            mBinding.editTextFriendlyName.setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
        }

        mBinding.editTextFriendlyName.setSaveEnabled(false);

        mFriendlyNameSanitizer = new FriendlyNameSanitizer(FsComponentsConfig.MAX_LENGTH_OF_FRIENDLY_NAME_WHEN_GOOGLE_CAST_IS_PRESENT);
        mFriendlyNameSanitizer.appendSanitizerToEditText(mBinding.editTextFriendlyName, this, new FriendlyNameSanitizer.IFriendlyNameSanitizerListener() {

            @Override
            public void onSubmitFriendlyName(String friendlyName) {
                goToNextPage();
            }

            @Override
            public void onNewFriendlyName(String friendlyName) {
                SetupManager.getInstance().setFriendlyName(friendlyName);
            }

            @Override
            public void onBadFriendlyName(ErrorSanitizerMessageType errorMessageType) {
                MessageHandlerUtil.handleMessageErrorType(ConfigureFriendlyNameActivity.this, errorMessageType, mBinding.editTextFriendlyName);
            }
        });

        mBinding.buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToNextPage();
            }
        });

        int imageId = SpeakerManager.getInstance().getImageIdForSSID(SetupManager.getInstance().getSSIDOfConfiguredRadio(),
                ESpeakerImageSizeType.Large);
        mBinding.speakerImageView.setImageResource(imageId);
    }

    private void initEditText() {
        if (mBinding.editTextFriendlyName.length() > 0) {
            mFriendlyNameSanitizer.mIsManualChange = true;
            mBinding.editTextFriendlyName.setText("");
        }

        mBinding.editTextFriendlyName.setEnabled(false);
        mBinding.progressBarRetrievingFriendlyName.setVisibility(View.VISIBLE);
        mBinding.textViewRetrievingFriendlyName.setVisibility(View.VISIBLE);
        mBinding.buttonNext.setVisibility(View.GONE);

        SetupManager.getInstance().getRadioFriendlyNameAsync(new SetupManager.IGetFriendlyNameAsync() {

            @Override
            public void onFriendlyNameRetrieved(String friendlyName) {
                setFriendlyName(friendlyName);
                mBinding.editTextFriendlyName.setEnabled(true);
                mBinding.editTextFriendlyName.requestFocus();
                mBinding.editTextFriendlyName.selectAll();
                mBinding.editTextFriendlyName.setOnEditTextImeBackListener(new EditTextWithBackEvent.EditTextImeBackListener() {
                    @Override
                    public void onImeBack(EditTextWithBackEvent ctrl, String text) {
                        mBinding.focusableLayout.requestFocus();
                    }
                });
                mBinding.editTextFriendlyName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View view, boolean hasFocus) {
                        if (view.getId() == mBinding.editTextFriendlyName.getId()) {
                            updateNextButtonState(hasFocus);
                        }
                    }
                });

                mBinding.progressBarRetrievingFriendlyName.setVisibility(View.GONE);
                mBinding.textViewRetrievingFriendlyName.setVisibility(View.GONE);

                if (isActivityRestarted) {
                    mBinding.focusableLayout.requestFocus();
                } else {
                    GuiUtils.showKeyboardForGivenEditText(mBinding.editTextFriendlyName, ConfigureFriendlyNameActivity.this);
                }
            }
        });
    }

    private void setFriendlyName(String friendlyName) {
        String[] array = friendlyName.split(" ");
        int length = array.length;
        if (length == 3 && array[1].equalsIgnoreCase("Gold") && array[2].equalsIgnoreCase("Fish")) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(array[0])
                    .append(" ")
                    .append(array[1])
                    .append(array[2].toLowerCase());
            mBinding.editTextFriendlyName.setText(stringBuilder.toString());
        } else {
            mBinding.editTextFriendlyName.setText(friendlyName);
        }
    }

    private void updateNextButtonState(boolean hasFocus) {
        String friendlyName = mBinding.editTextFriendlyName.getText().toString().trim();
        boolean enableNavigation = !TextUtils.isEmpty(friendlyName);
        mBinding.buttonNext.setVisibility((enableNavigation && !hasFocus) ? View.VISIBLE : View.GONE);
    }

    private void goToNextPage() {
        mBinding.focusableLayout.requestFocus();
        NavigationHelper.goToActivity(ConfigureFriendlyNameActivity.this, ConfigureWiFiActivity.class, NavigationHelper.AnimationType.SlideToLeft);
    }
}
