package com.apps_lib.multiroom.setup.dateTimeConfig;

import com.frontier_silicon.NetRemoteLib.Node.NodeSysCapsClockSourceList;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysCapsUtcSettingsList;

import java.util.List;

public interface IDateTimeCapsListener {

    void onDateTimeSupportUpdate(boolean isDateTimeSupported);
    void onDateTimeCapsUpdate(NodeSysCapsClockSourceList clockSourceList, List<NodeSysCapsUtcSettingsList.ListItem> utcTimeZoneList);

}
