package com.apps_lib.multiroom.setup.normalSetup;

import android.content.Context;
import android.net.wifi.ScanResult;

import com.frontier_silicon.NetRemoteLib.AccessPointUtil;
import com.apps_lib.multiroom.R;
import com.frontier_silicon.components.common.FsComponentsConfig;
import com.frontier_silicon.components.common.GuiUtils;
import com.frontier_silicon.loggerlib.FsLogger;
import com.apps_lib.multiroom.util.DisposableTimerTask;

import java.util.Timer;

public class ConnectToHeadlessAPHelper  {
	private static ConnectToHeadlessAPHelper mInstance;

	private SetupManager mSetupManager = null;

	private IConnectToHeadlessAPListener mConnectToAPListener = null;
	private Timer mConnectToAPTimer = null;
    private DisposableTimerTask mTimerTask;
	
	private ConnectToHeadlessAPHelper() {
		mSetupManager = SetupManager.getInstance();
	}
	
	public static ConnectToHeadlessAPHelper getInstance() {
		if (mInstance == null)
			mInstance = new ConnectToHeadlessAPHelper();
		
		return mInstance;
	}

	public void setConnectToHeadlessAPListener(IConnectToHeadlessAPListener listener) {
		mConnectToAPListener = listener;
	}

	public void removeConnectToHeadlessAPListener() {
		mConnectToAPListener = null;
	}

	private void onConnectedWiFi(Context context) {
//		cleanConnectToAPTimer();
//        AccessPointUtil.bindWifiNetworkToProcess();
//
//		String currentSSID = AccessPointUtil.getSSIDOfCurrentConnection();
//		FsLogger.log("Connected to Wi-Fi" + currentSSID);
//
//	    if (isTryingToConnectToAnAPChosenByUser(currentSSID)) {
//	    	mSetupManager.connectToCurrentRadio(context);
//	    }
//	    else if (isConnectedToHeadlessAPButDifferentRadioConnection(context, currentSSID)) {
//	    	mSetupManager.closeRadioConnection();
//	    	mSetupManager.connectToCurrentRadio(context);
//	    }
//	    else if (!mSetupManager.isConnectedToHeadlessAP(context)) {
//	    	if (mSetupManager.isConnectingToHeadlessAP())
//	    		notifyOnConnectFailed();
//	    	mSetupManager.closeRadioConnection();
//	    }
	}

	private void notifyOnConnectFailed() {
		if (mConnectToAPListener != null) {
			mConnectToAPListener.onConnectToApFailed();
		}
	}

	public boolean isTryingToConnectToAnAPChosenByUser(String ssid) {
		return mSetupManager.isConnectingToHeadlessAP() && ssid.equals(mSetupManager.getSSIDOfConnectingAP());
	}

	public boolean isConnectedToHeadlessAPButDifferentRadioConnection(Context context, String ssid) {
		return mSetupManager.isConnectedToHeadlessAP(context) && mSetupManager.isConnectedToAnotherRadioConnection(ssid);
	}

	private class ConnectToAPFailedTimerTask extends DisposableTimerTask {
		
		@Override
		public void run() {
            if (mIsDisposed) {
                cancel();
                return;
            }

			cleanConnectToAPTimer();
			mSetupManager.reconnectToInitialWiFiNetwork();

			notifyOnConnectFailed();
		}
	}

	private void startConnectToAPTimer() {
		cleanConnectToAPTimer();
        mTimerTask = new ConnectToAPFailedTimerTask();
		mConnectToAPTimer = new Timer();
		long delay = 1000 * FsComponentsConfig.SECONDS_TO_WAIT_UNTIL_CURRENTLY_CONNECTING_AP_SHOULD_BE_DISMISSED;

		mConnectToAPTimer.schedule(mTimerTask, delay);
	}

	public void cleanConnectToAPTimer() {
		if (mConnectToAPTimer != null) {
			mConnectToAPTimer.cancel();
			mConnectToAPTimer.purge();
			mConnectToAPTimer = null;
		}
        if (mTimerTask != null) {
            mTimerTask.cancel();
            mTimerTask.dispose();
            mTimerTask = null;
        }
	}

    public void tryConnectToAP(Context context, AccessPointModel apModel) {
        if (apModel == null || apModel.getIsGroupHeader()) {
            return;
        }

        ScanResult scanResult = apModel.getScanResult();

        if (!checkIfShouldTryConnectToAP(context, scanResult)) {
            return;
        }

        mSetupManager.connectToAccessPoint(scanResult);
        FsLogger.log("Connecting to AP - " + scanResult.SSID);
        startConnectToAPTimer();
    }

    private boolean checkIfShouldTryConnectToAP(Context context, ScanResult scanResult) {
        if (!mSetupManager.canConnectToAP(scanResult))
            return false;

        //ensuring that the selected AP isn't encrypted
        if (!AccessPointUtil.isOpenAP(scanResult)) {
            CharSequence text = context.getString(R.string.cannot_connect_to_encrypted_ap);
            GuiUtils.showToast(text, context);
            return false;
        }

        return true;
    }

}
