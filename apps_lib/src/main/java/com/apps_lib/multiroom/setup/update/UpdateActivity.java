package com.apps_lib.multiroom.setup.update;

import android.databinding.DataBindingUtil;
import android.os.Bundle;

import com.apps_lib.multiroom.R;
import com.apps_lib.multiroom.UEActivityBase;
import com.apps_lib.multiroom.databinding.ActivitySetupUpdateBinding;

/**
 * Created by lsuhov on 09/08/16.
 */

public class UpdateActivity extends UEActivityBase {

    private ActivitySetupUpdateBinding mBinding;
    private UpdateViewModel mViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_setup_update);

        mViewModel = new UpdateViewModel();
        mViewModel.init(this);

        mBinding.setViewModel(mViewModel);

        setupControls();
    }

    private void setupControls() {

    }

    @Override
    protected void onResume() {
        super.onResume();

        mViewModel.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();

        mViewModel.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        mViewModel.dispose();
        mViewModel = null;
    }
}
