package com.apps_lib.multiroom.nowPlaying;

import com.frontier_silicon.NetRemoteLib.Node.NodeCastAppName;
import com.frontier_silicon.NetRemoteLib.Node.NodeInfo;
import com.frontier_silicon.NetRemoteLib.Node.NodeMultiroomGroupState;
import com.frontier_silicon.NetRemoteLib.Node.NodePlayCaps;
import com.frontier_silicon.NetRemoteLib.Node.NodePlayControl;
import com.frontier_silicon.NetRemoteLib.Node.NodePlayErrorStr;
import com.frontier_silicon.NetRemoteLib.Node.NodePlayInfoAlbum;
import com.frontier_silicon.NetRemoteLib.Node.NodePlayInfoArtist;
import com.frontier_silicon.NetRemoteLib.Node.NodePlayInfoDuration;
import com.frontier_silicon.NetRemoteLib.Node.NodePlayInfoName;
import com.frontier_silicon.NetRemoteLib.Node.NodePlayInfoText;
import com.frontier_silicon.NetRemoteLib.Node.NodePlayPosition;
import com.frontier_silicon.NetRemoteLib.Node.NodePlayRepeat;
import com.frontier_silicon.NetRemoteLib.Node.NodePlayShuffle;
import com.frontier_silicon.NetRemoteLib.Node.NodePlayShuffleStatus;
import com.frontier_silicon.NetRemoteLib.Node.NodePlayStatus;
import com.frontier_silicon.NetRemoteLib.Node.NodeSpotifyPlaylistName;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysCapsValidModes;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysMode;
import com.frontier_silicon.NetRemoteLib.Radio.INodeNotificationCallback;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.components.common.RadioNodeUtil;

import org.greenrobot.eventbus.EventBus;

import java.util.Map;

/**
 * Created by lsuhov on 23/05/16.
 */
public class NowPlayingManager implements INodeNotificationCallback {
    private static NowPlayingManager mInstance;

    private Radio mRadio;
    private NowPlayingDataModel mNowPlayingDataModel = new NowPlayingDataModel();

    private NowPlayingManager() {
    }

    public static NowPlayingManager getInstance() {
        if (mInstance == null) {
            mInstance = new NowPlayingManager();
        }

        return mInstance;
    }

    public void bindToRadio(Radio radio) {
        unbindFromPreviousRadio();

        if (radio == null) {
            return;
        }

        mRadio = radio;
        mRadio.addNotificationListener(this);

        updateTrackInformation(true, false);
        updatePlaybackState(true);
    }

    public NowPlayingDataModel getNowPlayingDataModel() {
        return mNowPlayingDataModel;
    }

    private void unbindFromPreviousRadio() {
        if (mRadio != null) {
            mRadio.removeNotificationListener(this);
        }
    }

    @Override
    public void onNodeUpdated(NodeInfo node) {
        if (node instanceof NodePlayInfoName ||
                node instanceof NodePlayInfoText) {
            updateTrackInformation(false, false);

        } else if (node instanceof NodePlayStatus ||
                node instanceof NodePlayRepeat ||
                node instanceof NodePlayShuffle ||
                node instanceof NodePlayShuffleStatus) {
            updatePlaybackState(false);

        } else if (node instanceof NodeSysMode) {
            updateTrackInformation(false, false);
            updatePlaybackState(false);

            EventBus.getDefault().post(new ModeChangedEvent((NodeSysMode) node));
        } else if (node instanceof NodeMultiroomGroupState) {
            mNowPlayingDataModel.multiState.set(((NodeMultiroomGroupState) node).getValueEnum());
        }
    }

    private void updateTrackInformation(final boolean sync, boolean forceUncached) {
        RadioNodeUtil.getNodesFromRadio(mRadio, new Class[]{NodePlayInfoName.class, NodePlayInfoText.class, NodePlayInfoArtist.class,
                        NodePlayInfoAlbum.class, NodePlayErrorStr.class, NodePlayInfoDuration.class, NodeCastAppName.class, NodeSpotifyPlaylistName.class},
                sync, forceUncached, new RadioNodeUtil.INodesResultListener() {
                    @Override
                    public void onNodesResult(Map<Class, NodeInfo> nodes) {

                        NodePlayInfoName infoNameNode = (NodePlayInfoName) nodes.get(NodePlayInfoName.class);
                        if (infoNameNode != null) {
                            mNowPlayingDataModel.playInfoName.set(infoNameNode.getValue().trim());
                        }

                        NodePlayInfoText infoTextNode = (NodePlayInfoText) nodes.get(NodePlayInfoText.class);
                        if (infoTextNode != null) {
                            mNowPlayingDataModel.playInfoText.set(infoTextNode.getValue());
                        }

                        NodePlayInfoArtist infoArtistNode = (NodePlayInfoArtist) nodes.get(NodePlayInfoArtist.class);
                        if (infoArtistNode != null) {
                            mNowPlayingDataModel.artist.set(infoArtistNode.getValue());
                        }

                        NodePlayInfoAlbum infoAlbumNode = (NodePlayInfoAlbum) nodes.get(NodePlayInfoAlbum.class);
                        if (infoAlbumNode != null) {
                            mNowPlayingDataModel.album.set(infoAlbumNode.getValue());
                        }

                        NodePlayErrorStr errorStrNode = (NodePlayErrorStr) nodes.get(NodePlayErrorStr.class);
                        if (errorStrNode != null) {
                            mNowPlayingDataModel.playError.set(errorStrNode.getValue());
                        }

                        NodePlayInfoDuration durationNode = (NodePlayInfoDuration) nodes.get(NodePlayInfoDuration.class);
                        if (durationNode != null) {
                            mNowPlayingDataModel.duration.set(durationNode.getValue());
                        }

                        NodeCastAppName castAppNameNode = (NodeCastAppName) nodes.get(NodeCastAppName.class);
                        if (castAppNameNode != null) {
                            mNowPlayingDataModel.castingApp.set(castAppNameNode.getValue());
                        }

                        NodeSpotifyPlaylistName spotifyPlaylistNameNode = (NodeSpotifyPlaylistName) nodes.get(NodeSpotifyPlaylistName.class);
                        if (spotifyPlaylistNameNode != null) {
                            mNowPlayingDataModel.spotifyPlaylistName.set(spotifyPlaylistNameNode.getValue());
                        }
                        if (sync) {
                            mNowPlayingDataModel.setAlbumCover(null);
                        }
                    }

                });

        if (sync) {
            NodeSysCapsValidModes.Mode currentMode = mRadio.getModeSync();
            resetArtworkIfNeeded(currentMode);
            mNowPlayingDataModel.currentMode.set(currentMode);
        } else {
            mRadio.getMode(new Radio.IRadioModeCallback() {
                @Override
                public void getCurrentMode(NodeSysCapsValidModes.Mode currentMode) {
                    resetArtworkIfNeeded(currentMode);
                    mNowPlayingDataModel.currentMode.set(currentMode);
                }
            });
        }
    }

    private void resetArtworkIfNeeded(NodeSysCapsValidModes.Mode currentMode) {
        if (currentMode != mNowPlayingDataModel.currentMode.get()) {
            mNowPlayingDataModel.mAlbumCoverBitmap = null;
        }
    }

    private void updatePlaybackState(boolean sync) {

        boolean forceUncached = false;
        RadioNodeUtil.getNodesFromRadio(mRadio, new Class[]{NodePlayCaps.class, NodePlayStatus.class,
                        NodePlayPosition.class, NodePlayRepeat.class, NodePlayShuffle.class,
                        NodePlayShuffleStatus.class, NodePlayInfoDuration.class},
                sync, forceUncached, new RadioNodeUtil.INodesResultListener() {
                    @Override
                    public void onNodesResult(Map<Class, NodeInfo> nodes) {

                        NodePlayCaps playCapsNode = (NodePlayCaps) nodes.get(NodePlayCaps.class);
                        mNowPlayingDataModel.playCaps.set(playCapsNode);

                        NodePlayStatus nodePlayStatus = (NodePlayStatus) nodes.get(NodePlayStatus.class);
                        mNowPlayingDataModel.playStatus.set(nodePlayStatus);

                        NodePlayPosition nodePlayPosition = (NodePlayPosition) nodes.get(NodePlayPosition.class);
                        updatePlayPositionInternal(nodePlayPosition);

                        NodePlayRepeat nodePlayRepeat = (NodePlayRepeat) nodes.get(NodePlayRepeat.class);
                        mNowPlayingDataModel.isRepeat.set(nodePlayRepeat != null && nodePlayRepeat.getValueEnum() == NodePlayRepeat.Ord.ON);

                        NodePlayShuffle nodePlayShuffle = (NodePlayShuffle) nodes.get(NodePlayShuffle.class);
                        mNowPlayingDataModel.isShuffle.set(nodePlayShuffle != null && nodePlayShuffle.getValueEnum() == NodePlayShuffle.Ord.ON);

                        NodePlayShuffleStatus nodePlayShuffleStatus = (NodePlayShuffleStatus) nodes.get(NodePlayShuffleStatus.class);
                        mNowPlayingDataModel.playShuffleStatus.set(nodePlayShuffleStatus);
                    }
                });
    }

    private void updatePlayPositionInternal(NodePlayPosition nodePlayPosition) {
        if (nodePlayPosition != null) {
            if (mNowPlayingDataModel.isPlaying() || mNowPlayingDataModel.isPaused()) {
                mNowPlayingDataModel.playPosition.set(nodePlayPosition.getValue());
            }
        } else {
            mNowPlayingDataModel.playPosition.set(NowPlayingDataModel.INVALID_VALUE);
        }
    }

    public void updatePlayPositionAsync() {
        RadioNodeUtil.getNodeFromRadioAsync(mRadio, NodePlayPosition.class, new RadioNodeUtil.INodeResultListener() {
            @Override
            public void onNodeResult(NodeInfo node) {
                NodePlayPosition playPosNode = (NodePlayPosition) node;
                updatePlayPositionInternal(playPosNode);
            }
        });
    }

    public void updatePlayPosition() {
        NodePlayPosition playPosNode = (NodePlayPosition) mRadio.getNodeSyncGetter(NodePlayPosition.class).get();
        updatePlayPositionInternal(playPosNode);
    }

    public void play() {
        NodePlayControl.Ord playOrd;
        if (mNowPlayingDataModel.currentMode.get() == NodeSysCapsValidModes.Mode.IR) {
            playOrd = NodePlayControl.Ord.STOP;
        } else {
            playOrd = NodePlayControl.Ord.PLAY;
        }

        NodePlayControl playNode = new NodePlayControl(playOrd);
        mRadio.setNode(playNode, false);
    }

    public void pause() {
        if (mNowPlayingDataModel.currentMode.get() == NodeSysCapsValidModes.Mode.IR &&
                mNowPlayingDataModel.isStopAvailable()) {
            stop();
        } else {
            pauseImpl();
        }
    }

    private void pauseImpl() {
        NodePlayControl playNode = new NodePlayControl(NodePlayControl.Ord.PAUSE);
        mRadio.setNode(playNode, false);
    }

    public void stop() {
        NodePlayControl playNode = new NodePlayControl(NodePlayControl.Ord.STOP);
        mRadio.setNode(playNode, false);
    }

    public void next() {
        NodePlayControl playNode = new NodePlayControl(NodePlayControl.Ord.NEXT);
        mRadio.setNode(playNode, false);
    }

    public void previous() {
        NodePlayControl playNode = new NodePlayControl(NodePlayControl.Ord.PREVIOUS);
        mRadio.setNode(playNode, false);
    }

    public void setRepeat(boolean enable) {
        if (enable != mNowPlayingDataModel.isRepeat.get()) {
            mNowPlayingDataModel.isRepeat.set(enable);

            NodePlayRepeat.Ord repeatOrd = enable ? NodePlayRepeat.Ord.ON : NodePlayRepeat.Ord.OFF;
            mRadio.setNode(new NodePlayRepeat(repeatOrd), false);
        }
    }

    public void setShuffle(boolean enable) {
        if (enable != mNowPlayingDataModel.isShuffle.get()) {
            mNowPlayingDataModel.isShuffle.set(enable);

            NodePlayShuffle.Ord shuffleOrd = enable ? NodePlayShuffle.Ord.ON : NodePlayShuffle.Ord.OFF;
            mRadio.setNode(new NodePlayShuffle(shuffleOrd), false);
        }
    }

    public void seek(long seekPosition) {
        mRadio.setNode(new NodePlayPosition(seekPosition), false);
    }
}
