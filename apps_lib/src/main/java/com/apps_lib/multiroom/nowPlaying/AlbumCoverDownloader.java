package com.apps_lib.multiroom.nowPlaying;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.frontier_silicon.NetRemoteLib.NetRemote;
import com.frontier_silicon.NetRemoteLib.Node.NodeInfo;
import com.frontier_silicon.NetRemoteLib.Node.NodePlayInfoGraphicUri;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.components.common.RadioNodeUtil;
import com.frontier_silicon.loggerlib.FsLogger;
import com.frontier_silicon.loggerlib.LogLevel;

public class AlbumCoverDownloader {

    public void getAlbumArtwork(Radio radio, final IAlbumCoverListener listener, final Context context) {

        if (radio == null) {
            FsLogger.log("getAlbumArtwork NULL radio ERROR", LogLevel.Error);
            listener.onAlbumCoverUpdate(null);
        }

        //get artwork from radio
        RadioNodeUtil.getNodeFromRadioAsync(radio, NodePlayInfoGraphicUri.class, new RadioNodeUtil.INodeResultListener() {
            @Override
            public void onNodeResult(NodeInfo node) {
                final NodePlayInfoGraphicUri graphicUriNode = (NodePlayInfoGraphicUri) node;

                NetRemote.getMainThreadExecutor().executeOnMainThread(new Runnable() {
                    @Override
                    public void run() {
                        if (graphicUriNode != null) {
                            String artworkURL = graphicUriNode.getValue();
                            getArtworkBitmapFromUrl(artworkURL, listener, context);
                        }
                    }
                }, true);
            }
        });
    }

    private void getArtworkBitmapFromUrl(final String artworkURL, final IAlbumCoverListener listener,
                                        final Context context) {

        if (!TextUtils.isEmpty(artworkURL)) {
            Glide.with(context)
                    .load(artworkURL)
                    .asBitmap()
                    .into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(Bitmap bitmap, GlideAnimation glideAnimation) {
                            FsLogger.log("getAlbumArtwork RESPONSE for " + artworkURL);
                            NowPlayingManager.getInstance().getNowPlayingDataModel().setAlbumCover(bitmap);
                            if (listener != null) {
                                listener.onAlbumCoverUpdate(bitmap);
                            }
                        }

                        @Override
                        public void onLoadFailed(Exception e, Drawable errorDrawable) {
                            FsLogger.log("getAlbumArtwork fail for " + artworkURL, LogLevel.Error);
                            if (listener != null) {
                                listener.onAlbumCoverUpdate(null);
                            }
                        }
                    });
        } else {
            listener.onAlbumCoverUpdate(null);
        }
    }
}
