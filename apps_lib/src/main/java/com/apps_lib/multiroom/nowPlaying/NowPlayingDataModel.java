package com.apps_lib.multiroom.nowPlaying;

import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.databinding.ObservableLong;
import android.graphics.Bitmap;

import com.frontier_silicon.NetRemoteLib.Node.NodeMultiroomGroupState;
import com.frontier_silicon.NetRemoteLib.Node.NodePlayCaps;
import com.frontier_silicon.NetRemoteLib.Node.NodePlayShuffleStatus;
import com.frontier_silicon.NetRemoteLib.Node.NodePlayStatus;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysCapsValidModes;

/**
 * Created by lsuhov on 24/05/16.
 */
public class NowPlayingDataModel {

    public static final long INVALID_VALUE = -1;

    public ObservableInt indexOfCurrentlyPlayingPreset = new ObservableInt(-1);
    public ObservableField<String> playInfoName = new ObservableField<>();
    public ObservableField<String> playInfoText = new ObservableField<>();
    public ObservableField<String> artist = new ObservableField<>();
    public ObservableField<String> album = new ObservableField<>();
    public ObservableField<String> playError = new ObservableField<>();
    public ObservableField<String> castingApp = new ObservableField<>();
    public ObservableLong duration = new ObservableLong();
    public ObservableField<NodeSysCapsValidModes.Mode> currentMode = new ObservableField<>(NodeSysCapsValidModes.Mode.UnknownMode);
    public ObservableField<NodePlayCaps> playCaps = new ObservableField<>();
    public ObservableField<NodePlayStatus> playStatus = new ObservableField<>();
    public ObservableLong playPosition = new ObservableLong();
    public ObservableBoolean isRepeat = new ObservableBoolean();
    public ObservableBoolean isShuffle = new ObservableBoolean();
    public ObservableField<NodePlayShuffleStatus> playShuffleStatus = new ObservableField<>();
    public ObservableField<String> spotifyPlaylistName = new ObservableField<>();
    public ObservableField<NodeMultiroomGroupState.Ord> multiState = new ObservableField<>();

    public Bitmap mAlbumCoverBitmap;


    public boolean isPlaying() {
        boolean isPlaying = false;
        if (playStatus.get() != null) {
            isPlaying = (playStatus.get().getValueEnum() == NodePlayStatus.Ord.PLAYING);
        }

        return isPlaying;
    }

    public boolean isPaused() {
        boolean isPaused = false;
        if (playStatus.get() != null) {
            isPaused = (playStatus.get().getValueEnum() == NodePlayStatus.Ord.PAUSED);
        }

        return isPaused;
    }

    public boolean isBuffering() {
        boolean isBuffering = false;
        if (playStatus.get() != null) {
            isBuffering = (playStatus.get().getValueEnum() == NodePlayStatus.Ord.BUFFERING ||
                    playStatus.get().getValueEnum() == NodePlayStatus.Ord.REBUFFERING);
        }

        return isBuffering;
    }

    public boolean isError() {
        boolean isError = false;
        if (playStatus.get() != null) {
            isError = (playStatus.get().getValueEnum() == NodePlayStatus.Ord.ERROR);
        }

        return isError;
    }

    public boolean isIdle() {
        boolean isIdle = true;
        if (playStatus.get() != null) {
            isIdle = (playStatus.get().getValueEnum() == NodePlayStatus.Ord.IDLE);
        }

        return isIdle;
    }

    public boolean isStopped() {
        boolean isStopped = false;
        if (playStatus.get() != null) {
            isStopped = (playStatus.get().getValueEnum() == NodePlayStatus.Ord.STOPPED);
        }

        return isStopped;
    }

    public boolean isStopAvailable() {
        boolean isAvailable = false;
        if (playCaps.get() != null) {
            isAvailable = (playCaps.get().DoesSupport(NodePlayCaps.Operation.Stop));
        }

        return isAvailable;
    }

    public boolean isRepeatAvailable() {
        boolean isAvailable = false;
        if (playCaps.get() != null) {
            isAvailable = playCaps.get().DoesSupport(NodePlayCaps.Operation.Repeat);
        }

        return isAvailable;
    }

    public boolean isShuffleAvailable() {
        boolean isAvailable = false;
        if (playCaps.get() != null) {
            isAvailable = playCaps.get().DoesSupport(NodePlayCaps.Operation.Shuffle);
        }

        return isAvailable;
    }

    public boolean isSeekBarAvailable() {
        boolean isAvailable = false;
        if (playCaps.get() != null) {
            isAvailable = playCaps.get().DoesSupport(NodePlayCaps.Operation.Seek);
        }

        return isAvailable;
    }

    public boolean isNextButtonAvailable() {
        boolean isAvailable = false;
        if (playCaps.get() != null) {
            isAvailable = playCaps.get().DoesSupport(NodePlayCaps.Operation.SkipNext);
        }

        return isAvailable;
    }

    public boolean isPrevButtonAvailable() {
        boolean isAvailable = false;
        if (playCaps.get() != null) {
            isAvailable = playCaps.get().DoesSupport(NodePlayCaps.Operation.SkipPrevious);
        }

        return isAvailable;
    }

    public boolean addToPresetIsAllowed() {
        boolean isAllowed = false;
        if (playCaps.get() != null) {
            isAllowed = playCaps.get().DoesSupport(NodePlayCaps.Operation.AddPreset);
        }

        return isAllowed;
    }

    public boolean isSpotifyMode() {
        return currentMode.get() == NodeSysCapsValidModes.Mode.Spotify;
    }

    public boolean isBluetoothMode() {
        return currentMode.get() == NodeSysCapsValidModes.Mode.Bluetooth;
    }

    public boolean isInternetRadioMode() {
        return currentMode.get() == NodeSysCapsValidModes.Mode.IR;
    }

    public boolean isAuxInMode() {
        return currentMode.get() == NodeSysCapsValidModes.Mode.AuxIn;
    }

    public boolean isRCAMode() {
        return currentMode.get() == NodeSysCapsValidModes.Mode.RCA;
    }

    public Bitmap getAlbumCover() {
        return mAlbumCoverBitmap;
    }

    public void setAlbumCover(Bitmap albumCover) {
        mAlbumCoverBitmap = albumCover;
    }
}
