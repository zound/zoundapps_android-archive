package com.apps_lib.multiroom.nowPlaying;

import com.frontier_silicon.NetRemoteLib.Node.NodeSysMode;

/**
 * Created by lsuhov on 13/01/2017.
 */

public class ModeChangedEvent {
    public NodeSysMode modeNode;

    public ModeChangedEvent(NodeSysMode modeNode) {
        this.modeNode = modeNode;
    }
}
