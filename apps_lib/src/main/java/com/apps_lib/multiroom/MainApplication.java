package com.apps_lib.multiroom;

import android.app.Application;
import android.os.StrictMode;

import com.bumptech.glide.Glide;
import com.bumptech.glide.GlideBuilder;
import com.bumptech.glide.load.DecodeFormat;
import com.crashlytics.android.Crashlytics;
import com.frontier_silicon.NetRemoteLib.AccessPointUtil;
import com.frontier_silicon.NetRemoteLib.NetRemote;
import com.frontier_silicon.components.NetRemoteManager;
import com.frontier_silicon.components.common.CommonPreferences;
import com.frontier_silicon.components.common.FsComponentsConfig;
import com.frontier_silicon.components.common.TaskHelper;
import com.frontier_silicon.components.connection.ConnectionStateUtil;
import com.frontier_silicon.components.multiroom.MultiroomGroupManager;
import com.frontier_silicon.loggerlib.FsLogger;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import io.fabric.sdk.android.Fabric;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

//import io.fabric.sdk.android.Fabric;

/**
 * Created by viftime on 11/03/16.
 */
public abstract class MainApplication extends Application {
    private static final boolean ENABLE_LOGGING = true;

    private ActivityLifecycleHandler activityLifecycleHandler;
    private boolean mAppWasInBackground = false;

    public enum ECurrentApplication {
        URBEANEARS_APP,
        MARSHALL_APP
    }

    public abstract ECurrentApplication getCurrentApplication();

    @Override
    public void onCreate() {
        //setStrictMode(true);

        super.onCreate();

        activityLifecycleHandler = new ActivityLifecycleHandler();
        registerActivityLifecycleCallbacks(activityLifecycleHandler);

        Fabric.with(this, new Crashlytics());

        enableLogging(ENABLE_LOGGING);

        TaskHelper.initRejectedExecutionHandler();

        initFonts();

        AccessPointUtil.init(getApplicationContext());

        bindWiFiToApplication();

        initGlide();

        initNetRemoteLib();

        initPrefs();

        EventBus.getDefault().register(this);

        PowerManager.getINSTANCE().init(this);

//        AnalyticsSender.getInstance().startListeningToConnectionStateEvents();
    }

    private void setStrictMode(boolean enabled) {
        if (enabled) {
            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
                    .detectAll()
                    .penaltyLog()
                    .build());
            StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
                    .detectAll()
                    .penaltyLog()
//                    .penaltyDeath()
                    .build());
        }
    }

    private void initGlide() {
        GlideBuilder glideBuilder = (new GlideBuilder(this))
                .setDecodeFormat(DecodeFormat.PREFER_ARGB_8888);

        //In Glide 4.0 this function will disappear
        Glide.setup(glideBuilder);
    }

    private void initFonts() {
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/AGNextRegular.otf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
    }

    private void initPrefs() {
        CommonPreferences.getInstance().init(getApplicationContext());
        Preferences.getInstance().init(getApplicationContext());

        FsComponentsConfig.MAX_NAV_LIST_ITEMS = 20;
        FsComponentsConfig.MILLISECONDS_TO_WAIT_BEFORE_NAV_STATUS_CHECK = 250;
    }

    private void bindWiFiToApplication() {
        if (AccessPointUtil.isConnectedToWiFi()) {
            AccessPointUtil.bindWifiNetworkToProcess();
        }
    }

    private void initNetRemoteLib() {

        int scannersFlag = NetRemote.SCANNER_BONJOUR;
        boolean cacheRadiosToFile = true;
        boolean useGetMultipleForDeviceDetailsRetrieval = true;
        boolean showAlsoMissingSpeakers = false;

        NetRemoteManager.getInstance().initNetRemote(getApplicationContext(),
                getResources().getStringArray(R.array.vendor_id_array),
                getResources().getStringArray(R.array.firmware_blacklist),
                getResources().getStringArray(R.array.bonjour_service_type_array),
                scannersFlag, cacheRadiosToFile, useGetMultipleForDeviceDetailsRetrieval,
                showAlsoMissingSpeakers);

        MultiroomGroupManager.getInstance().init(NetRemoteManager.getInstance());

        ConnectionStateUtil.getInstance().init();
    }

    public void enableLogging(boolean enable) {
        if (enable) {
            FsLogger.initNewLogFile(getApplicationContext());
            NetRemote.setFileLogging(FsLogger.getFileLogger());
        }
    }

    @Subscribe
    public void onAppInBackground(AppInBackgroundEvent event) {
        setAppWasInBackground(true);
    }

    public boolean getAppWasInBackground() {
        return mAppWasInBackground;
    }

    public void setAppWasInBackground(boolean wasInBackground) {
        mAppWasInBackground = wasInBackground;
    }
}
