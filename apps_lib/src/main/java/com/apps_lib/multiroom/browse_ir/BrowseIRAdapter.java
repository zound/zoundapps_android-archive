package com.apps_lib.multiroom.browse_ir;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.apps_lib.multiroom.R;
import com.apps_lib.multiroom.browse_ir.model.BrowseIRManager;
import com.apps_lib.multiroom.databinding.ListItemBrowseIrBinding;

/**
 * Created by lsuhov on 28/06/16.
 */

public class BrowseIRAdapter extends RecyclerView.Adapter<BrowseIRAdapter.BrowseItemViewHolder> {

    private final Context mContext;
    private String mLoadingText;

    public class BrowseItemViewHolder extends RecyclerView.ViewHolder {

        ListItemBrowseIrBinding mBinding;

        public BrowseItemViewHolder(ListItemBrowseIrBinding binding) {
            super(binding.getRoot());

            mBinding = binding;
        }
    }

    public BrowseIRAdapter(Context context) {
        mContext = context;
        mLoadingText = context.getString(R.string.loading_list_item);
    }

    @Override
    public BrowseItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        ListItemBrowseIrBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.list_item_browse_ir,
                parent, false);

        BrowseItemViewHolder browseItemViewHolder = new BrowseItemViewHolder(binding);
        return browseItemViewHolder;
    }

    @Override
    public void onBindViewHolder(final BrowseItemViewHolder holder, int position) {
        ListItemBrowseIrBinding binding = holder.mBinding;

        BrowseListItemViewModel viewModel = new BrowseListItemViewModel(mLoadingText, position, mContext);
        binding.setViewModel(viewModel);
    }

    @Override
    public void onViewRecycled(BrowseItemViewHolder holder) {
        BrowseListItemViewModel viewModel = holder.mBinding.getViewModel();
        if (viewModel != null) {
            viewModel.dispose();
            holder.mBinding.setViewModel(null);
        }
    }

    @Override
    public int getItemCount() {
        return BrowseIRManager.getInstance().getTotalCountForCurrentList();
    }
}
