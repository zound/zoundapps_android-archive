package com.apps_lib.multiroom.browse_ir.model;

/**
 * Created by lsuhov on 29/06/16.
 */

public interface IIRNavigationListener {
    void onNavigationCompleted(EIRNavigationResult result);
}
