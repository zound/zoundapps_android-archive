package com.apps_lib.multiroom.browse_ir.model;

import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.components.common.RadioNavigationUtil;

/**
 * Created by lsuhov on 02/07/16.
 */

public class NavigateRunnable implements Runnable {

    private long mKey;
    private Radio mRadio;
    private IIRNavigationListener mListener;

    public NavigateRunnable(long key, Radio radio, IIRNavigationListener listener) {
        mKey = key;
        mRadio = radio;
        mListener = listener;
    }

    @Override
    public void run() {

        boolean result;
        if (mKey == RadioNavigationUtil.INVALID_INT32_KEY) {
           result = RadioNavigationUtil.getInstance().navigateUp(mRadio);
        } else {
            result = RadioNavigationUtil.getInstance().navigateNavItem(mRadio, mKey);
        }

        if (!result) {
            notifyError();
        }

        BrowseIRManager.getInstance().retrieveFirstPageSync(mRadio);

        result = BrowseIRManager.getInstance().retrieveTotalNumberOfItemsFromCurrentListSync(mRadio);

        if (mListener != null) {
            mListener.onNavigationCompleted(result ? EIRNavigationResult.Success : EIRNavigationResult.TotalItemsNotRetrieved);
        }

        dispose();
    }

    private void dispose() {
        mRadio = null;
        mListener = null;
    }

    private void notifyError() {
        if (mListener != null) {
            mListener.onNavigationCompleted(EIRNavigationResult.NavigationFailed);
        }
    }
}
