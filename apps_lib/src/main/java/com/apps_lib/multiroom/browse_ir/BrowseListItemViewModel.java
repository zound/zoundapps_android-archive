package com.apps_lib.multiroom.browse_ir;

import android.content.Context;
import android.databinding.ObservableField;
import android.view.View;

import com.apps_lib.multiroom.browse_ir.model.BrowseIRItemModel;
import com.apps_lib.multiroom.browse_ir.model.BrowseIRManager;
import com.apps_lib.multiroom.browse_ir.model.IBrowseItemModelRetrieveListener;

/**
 * Created by lsuhov on 28/06/16.
 */

public class BrowseListItemViewModel implements IBrowseItemModelRetrieveListener {

    private String mLoadingMessage;
    private int mPosition;
    private Context mContext;
    private BrowseIRItemModel mBrowseIRItemModel;

    public ObservableField<String> name = new ObservableField<>();

    BrowseListItemViewModel(String loadingMessage, int position, Context context) {
        mLoadingMessage = loadingMessage;
        mPosition = position;
        mContext = context;

        init();
    }

    private void init() {
        name.set(mLoadingMessage);

        BrowseIRManager.getInstance().getBrowseItemModelAsync(mPosition, this);
    }

    @Override
    public void onBrowseItemModelRetrieved(BrowseIRItemModel itemModel) {
        mBrowseIRItemModel = itemModel;

        if (mBrowseIRItemModel != null) {
            name.set(mBrowseIRItemModel.getName());
        }
    }

    public void onListItemClicked(View view) {
        if (mBrowseIRItemModel == null) {
            return;
        }

        BrowseIRManager.getInstance().navigateToBrowseItem(mBrowseIRItemModel);
    }

    public void dispose() {
        mLoadingMessage = null;
        mPosition = -1;
        mContext = null;
        mBrowseIRItemModel = null;
    }

}
