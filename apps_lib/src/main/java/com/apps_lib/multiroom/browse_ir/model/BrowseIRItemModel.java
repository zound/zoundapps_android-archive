package com.apps_lib.multiroom.browse_ir.model;

import com.frontier_silicon.NetRemoteLib.Node.NodeNavList;

/**
 * Created by lsuhov on 29/06/16.
 */

public class BrowseIRItemModel {

    private NodeNavList.ListItem mNavListItem = null;

    public BrowseIRItemModel(NodeNavList.ListItem navListItem) {
        mNavListItem = navListItem;
    }

    public String getName() {
        if (mNavListItem != null) {
            return mNavListItem.getName();
        }
        return "";
    }

    public Long getKey() {
        if (mNavListItem != null) {
            return mNavListItem.getKey();
        }
        return -1L;
    }

    public boolean isDirectory() {
        return (mNavListItem != null && mNavListItem.getType() == NodeNavList.ListItem.FieldType.Directory);
    }

    public boolean isPlayable() {
        return (mNavListItem != null && mNavListItem.getType() == NodeNavList.ListItem.FieldType.PlayableItem);
    }

    public boolean isSearch() {
        return (mNavListItem != null && mNavListItem.getType() == NodeNavList.ListItem.FieldType.SearchDirectory);
    }
}
