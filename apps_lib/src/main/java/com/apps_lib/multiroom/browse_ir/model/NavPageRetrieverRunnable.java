package com.apps_lib.multiroom.browse_ir.model;

import com.frontier_silicon.NetRemoteLib.Node.BaseNavList;
import com.frontier_silicon.NetRemoteLib.Node.BaseNavStatus;
import com.frontier_silicon.NetRemoteLib.Node.NodeNavList;
import com.frontier_silicon.NetRemoteLib.Node.NodeNavStatus;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.components.common.RadioNavigationUtil;
import com.frontier_silicon.loggerlib.FsLogger;
import com.frontier_silicon.loggerlib.LogLevel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lsuhov on 30/06/16.
 */

class NavPageRetrieverRunnable implements Runnable {

    interface INavPageRetrieverListener {
        void onNavPageRetrieved(List<NodeNavList.ListItem> listItems);
    }

    private Radio mRadio;
    private int mStartIndex;
    private INavPageRetrieverListener mListener;

    NavPageRetrieverRunnable(int startIndex, Radio radio, INavPageRetrieverListener listener) {
        mStartIndex = startIndex;
        mRadio = radio;
        mListener = listener;
    }

    @Override
    public void run() {
        FsLogger.log("navigation runnable started " + mStartIndex, LogLevel.Info);

        ArrayList<BaseNavList.ListItem> navList = new ArrayList<>();

        NodeNavStatus statusNode = RadioNavigationUtil.enableNavigation(mRadio);

        if (statusNode == null || statusNode.getValueEnum() == BaseNavStatus.Ord.FAIL ||
                statusNode.getValueEnum() == BaseNavStatus.Ord.FATAL_ERR) {
            sendNavListToListener(navList);
            dispose();
            return;
        }

        String startKey = "" + mStartIndex;
        List<NodeNavList.ListItem> returnedNavList = RadioNavigationUtil.getInstance().getNavList(mRadio, startKey, true);

        if (returnedNavList != null) {
            navList.addAll(returnedNavList);
        }

        sendNavListToListener(navList);

        dispose();
    }

    private void sendNavListToListener(List<NodeNavList.ListItem> listItems) {
        if (mListener != null) {
            mListener.onNavPageRetrieved(listItems);
        }
    }

    private void dispose() {
        mRadio = null;
        mListener = null;
    }
}
