package com.apps_lib.multiroom.factory;

import android.support.v4.app.Fragment;

/**
 * Created by nbalazs on 10/05/2017.
 */

public class FragmentFactory {

    private static FragmentFactory INSTANCE;

    private FragmentCreator mFragmentCreator;

    public static void init(FragmentCreator fragmentCreator) {
        INSTANCE = new FragmentFactory();
        INSTANCE.mFragmentCreator = fragmentCreator;
    }

    public static FragmentFactory getInstanece() {
        return INSTANCE;
    }

    public java.lang.Class getScanExistingSpeakersFragment() { return mFragmentCreator.getScanExistingSpeakersFragment(); }

    public java.lang.Class getHomeFragment() {
        return mFragmentCreator.getHomeFragment();
    }

    public java.lang.Class getConnectionLostFragment() { return mFragmentCreator.getConnectionLostFragment(); }

    public java.lang.Class getNoWiFiFragment() { return mFragmentCreator.getNoWiFiFragment(); }

    public java.lang.Class getScanNewSpeakerFragment() {return mFragmentCreator.getScanNewSpeakerFragment(); }

    public static Fragment getFragment(EHomeFragmentTag tag) {

        // TODO: finish him!

        switch (tag) {
            case ScanExistingSpeakers:
                return createFragmentFromClass(INSTANCE.getScanExistingSpeakersFragment());
            case HomeFragment:
                return createFragmentFromClass(INSTANCE.getHomeFragment());
            case ConnectionLostFragment:
                return createFragmentFromClass(INSTANCE.getConnectionLostFragment());
            case NoWiFiFragment:
                return createFragmentFromClass(INSTANCE.getNoWiFiFragment());
        }

        return null;


    }

    private static Fragment createFragmentFromClass(java.lang.Class theClass) {
        Fragment fragment = null;
        try {
            fragment = (Fragment) theClass.newInstance();
        }catch (Exception e) {
            e.printStackTrace();
        }

        return fragment;
    }

    public  java.lang.Class getNoSpeakerFoundFragmentClass(int tag) {
        return mFragmentCreator.getNoSpeakerFoundFragment(tag);
    }
}
