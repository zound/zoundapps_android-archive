package com.apps_lib.multiroom.factory;

/**
 * Created by nbalazs on 12/05/2017.
 */

public enum EHomeFragmentTag {
    ScanExistingSpeakers,
    HomeFragment,
    ConnectionLostFragment,
    NoWiFiFragment
}
