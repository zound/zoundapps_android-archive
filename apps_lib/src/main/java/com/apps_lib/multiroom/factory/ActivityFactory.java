package com.apps_lib.multiroom.factory;

/**
 * Created by nbalazs on 09/05/2017.
 */

public class ActivityFactory {

    private static ActivityFactory INSTANCE;

    private ActivityCreator mActivityCreator;

    public final static String SPEAKER_JUST_CONNECTED = "SPEAKER_JUST_CONNECTED";

    public static void init(ActivityCreator activityCreator) {
        INSTANCE = new ActivityFactory();
        INSTANCE.mActivityCreator = activityCreator;
    }

    private ActivityFactory() {}

    public static ActivityFactory getActivityFactory() {
        return INSTANCE;
    }

    public java.lang.Class getHomeActivity() {
        return  mActivityCreator.getHomeActivity();
    }

    public java.lang.Class getSetupActivity() {
        return mActivityCreator.getSetupActivity();
    }

    public java.lang.Class getVolumeActivity() { return  mActivityCreator.getVolumeActivity(); }

    public java.lang.Class getSourcesActivity() { return mActivityCreator.getSourcesActivity(); }

    public java.lang.Class getSettingsActivity() { return mActivityCreator.getSettingsActivity(); }

    public java.lang.Class getAboutScreenActivity() { return mActivityCreator.getAboutScreenActivity(); }

    public java.lang.Class getCastTosActivity() { return mActivityCreator.getCastTosActivity(); }

    public java.lang.Class getPresetAddingFailedActivity() { return mActivityCreator.getPresetAddingFailedActivity(); }

    public java.lang.Class getSetPresetsActivity() { return mActivityCreator.getSetPresetsActivity(); }

    public java.lang.Class getFinalSetupActivity() { return mActivityCreator.getFinalSetupActivity(); }

    public java.lang.Class getSpotifyLinkingSuccessActivity() { return mActivityCreator.getSpotifyLinkingSuccessActivity(); }

    public java.lang.Class getSpotifyAuthenticationActivity() { return mActivityCreator.getSpotifyAuthenticationActivity(); }
}