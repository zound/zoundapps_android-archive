package com.apps_lib.multiroom.webview;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.apps_lib.multiroom.R;
import com.apps_lib.multiroom.databinding.ActivityEmbeddedWebViewBinding;

/**
 * Created by nbalazs on 07/02/2017.
 */

public class EmbeddedWebViewActivity extends Activity {

    public static final String EMBEDDED_URL = "EMBEDDED_URL";

    private ActivityEmbeddedWebViewBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        View view = LayoutInflater.from(this).inflate(R.layout.activity_embedded_web_view, null);
        mBinding = DataBindingUtil.bind(view);
        setContentView(view);

        setTitle(R.string.app_name);

        setupControls();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            switch (keyCode) {
                case KeyEvent.KEYCODE_BACK:
                    if (mBinding.webView.canGoBack()) {
                        mBinding.webView.goBack();
                    } else {
                        finish();
                    }
                    return true;
            }

        }
        return super.onKeyDown(keyCode, event);
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void setupControls() {
        Intent intent = getIntent();
        String url = null;

        if (intent != null) {
            url = intent.getStringExtra(EMBEDDED_URL);
        }

        if (url == null) {
            Toast.makeText(this, getResources().getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
            finish();
        }

        WebSettings webSettings = mBinding.webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setUseWideViewPort(true);
        mBinding.webView.setBackgroundColor(Color.TRANSPARENT);

        mBinding.webView.setWebViewClient(new EmbeddedWebViewClient());
        mBinding.webView.setWebChromeClient(new EmbeddedWebChromeClient());
        mBinding.webView.loadUrl(url);
    }

    @SuppressWarnings("all")
    private class EmbeddedWebViewClient extends WebViewClient {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            return false;
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            mBinding.progressLoading.setProgress(0);
            mBinding.progressLoading.setVisibility(View.VISIBLE);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            mBinding.progressLoading.setVisibility(View.INVISIBLE);
        }
    }

    private class EmbeddedWebChromeClient extends WebChromeClient {
        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            super.onProgressChanged(view, newProgress);
            mBinding.progressLoading.setProgress(newProgress);
        }
    }
}
