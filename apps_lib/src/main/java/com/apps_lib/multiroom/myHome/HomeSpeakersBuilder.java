package com.apps_lib.multiroom.myHome;

import com.apps_lib.multiroom.myHome.speakers.SpeakersBuilder;

/**
 * Created by lsuhov on 13/06/16.
 */
public class HomeSpeakersBuilder extends SpeakersBuilder {


    public HomeSpeakersBuilder() {
        super(true);
    }

    @Override
    public void addOtherMultiModelsAfterHeader() {}
}
