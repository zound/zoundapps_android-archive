package com.apps_lib.multiroom.myHome;

import com.apps_lib.multiroom.myHome.speakers.SpeakerModel;

/**
 * Created by cvladu on 04/10/16.
 */

public class SwitchSpeakerBetweenSoloMultiEvent {
    public SpeakerModel mSpeakerModel;
    public boolean mIsChecked;

  public SwitchSpeakerBetweenSoloMultiEvent(SpeakerModel speakerModel, boolean isChecked) {
      this.mSpeakerModel = speakerModel;
      this.mIsChecked = isChecked;
  }
}
