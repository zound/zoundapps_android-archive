package com.apps_lib.multiroom.myHome;

/**
 * Created by lsuhov on 15/07/16.
 */
public interface ISwitchCastToMultiroomListener {
    void onResult(boolean switchToMultiroom);
}
