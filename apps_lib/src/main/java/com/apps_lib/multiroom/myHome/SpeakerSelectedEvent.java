package com.apps_lib.multiroom.myHome;

import com.frontier_silicon.components.multiroom.MultiroomDeviceModel;

/**
 * Created by lsuhov on 08/05/16.
 */
public class SpeakerSelectedEvent {
    public enum EventType {
        SPEAKER_CONNECT_TO_CONTROL,
        SPEAKER_CONNECT_TO_SHOW_SETTINGS
    }

    public final MultiroomDeviceModel multiroomDeviceModel;
    public final EventType eventType;

    public SpeakerSelectedEvent(MultiroomDeviceModel multiroomDeviceModel) {
        this.multiroomDeviceModel = multiroomDeviceModel;
        this.eventType = EventType.SPEAKER_CONNECT_TO_CONTROL;
    }

    public SpeakerSelectedEvent(MultiroomDeviceModel multiroomDeviceModel, EventType type) {
        this.multiroomDeviceModel = multiroomDeviceModel;
        this.eventType = type;
    }
}

