package com.apps_lib.multiroom.myHome;

import java.util.Locale;

/**
 * Created by lsuhov on 31/05/16.
 */
public class NavDrawerItem {
    public String mTitle;
    public int mIcon;

    public NavDrawerItem(String title, int icon) {
        capitalizeFirstLetterOfEachWord(title);
        mIcon = icon;
    }

    private void capitalizeFirstLetterOfEachWord(String title) {
      title = title.toLowerCase(Locale.getDefault());
      String[] stringArray = title.split(" ");
      StringBuilder builder = new StringBuilder();
        for (String s : stringArray) {
            String cap = s.substring(0,1).toUpperCase(Locale.getDefault()) + s.substring(1);
            builder.append(cap + " ");
        }

        mTitle = builder.toString();
    }
}
