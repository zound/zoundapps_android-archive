package com.apps_lib.multiroom.myHome;

import com.frontier_silicon.NetRemoteLib.Discovery.IRadioDiscoveryListener;
import com.frontier_silicon.NetRemoteLib.NetRemote;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.loggerlib.FsLogger;
import com.frontier_silicon.components.NetRemoteManager;
import com.apps_lib.multiroom.AppInBackgroundEvent;
import com.apps_lib.multiroom.multiroom.MultiroomRebuildGroups;
import com.apps_lib.multiroom.util.DisposableTimerTask;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.Timer;

/**
 * Created by lsuhov on 09/07/16.
 */
public class SpeakersDiscoveryManager implements IRadioDiscoveryListener {
    private static SpeakersDiscoveryManager mInstance;
    private final int mRebuildPeriod = 10 * 1000;
    private Timer mTimer;
    private RebuildTimerTask mTimerTask;

    public static SpeakersDiscoveryManager getInstance() {
        if (mInstance == null) {
            mInstance = new SpeakersDiscoveryManager();
        }
        return mInstance;
    }

    private SpeakersDiscoveryManager() {
        EventBus.getDefault().register(this);
    }

    public void tryStartDiscovery() {
        NetRemoteManager.getInstance().addRadioDiscoveryListener(this);

        if (NetRemoteManager.getInstance().isDiscoveryScanActive()) {
            return;
        }

        NetRemoteManager.getInstance().startBackgroundRadioScan(false);

        startRebuildTimer();
    }

    public void stopDiscovery() {
        NetRemoteManager.getInstance().removeRadioDiscoveryListener(this);
        NetRemoteManager.getInstance().stopBackgroundRadioScan();

        stopRebuildTimer();
    }

    public void rescan() {
        if (NetRemoteManager.getInstance().isDiscoveryScanActive()) {
            NetRemoteManager.getInstance().rescan();
        } else {
            tryStartDiscovery();
        }
    }

    @Override
    public void onRadioFound(Radio radio) {
        FsLogger.log("DiscoveryManager: onRadioFound "  + radio);
        rebuildGroupsAndDevices();
    }

    @Override
    public void onRadioLost(Radio radio) {
        FsLogger.log("DiscoveryManager: onRadioLost "  + radio);
        rebuildGroupsAndDevices();
    }

    @Override
    public void onRadioUpdated(Radio radio) {
        FsLogger.log("DiscoveryManager: onRadioUpdated "  + radio);
        rebuildGroupsAndDevices();
    }


    private void rebuildGroupsAndDevices() {
        NetRemote.getMainThreadExecutor().executeOnMainThread(new Runnable() {
            @Override
            public void run() {
                MultiroomRebuildGroups.getInstance().rebuildMultiroomGroupsAndDevices();
            }
        }, true);
    }

    @Subscribe
    public void onSwapHomeSpeakerEvent(SwitchSpeakerBetweenSoloMultiEvent switchSpeakerBetweenSoloMultiEvent) {
        startRebuildTimer();
        MultiroomRebuildGroups.getInstance().clearQueue();
    }

    @Subscribe
    public void onAppNotVisible(AppInBackgroundEvent event) {
        stopDiscovery();
    }

    private void startRebuildTimer() {
        stopRebuildTimer();

        mTimerTask = new RebuildTimerTask();
        mTimer = new Timer();
        mTimer.schedule(mTimerTask, mRebuildPeriod, mRebuildPeriod);
    }

    private void stopRebuildTimer() {
        if (mTimer != null) {
            mTimer.cancel();
            mTimer.purge();
            mTimer = null;
        }
        if (mTimerTask != null) {
            mTimerTask.cancel();
            mTimerTask.dispose();
            mTimerTask = null;
        }
    }

    private class RebuildTimerTask extends DisposableTimerTask {

        @Override
        public void run() {
            if (mIsDisposed) {
                cancel();
                return;
            }

            rebuildGroupsAndDevices();
        }
    }
}
