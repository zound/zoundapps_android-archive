package com.apps_lib.multiroom.myHome;

import com.apps_lib.multiroom.factory.EHomeFragmentTag;

/**
 * Created by lsuhov on 04/05/16.
 */
public interface IFragmentSwitcher {
    void switchFragment(EHomeFragmentTag fragmentTag);
}
