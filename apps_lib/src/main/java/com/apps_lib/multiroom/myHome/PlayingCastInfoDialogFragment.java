package com.apps_lib.multiroom.myHome;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.apps_lib.multiroom.R;
import com.frontier_silicon.components.common.ExternalAppsOpener;
import com.apps_lib.multiroom.AnimatedDialogFragmentBase;
import com.apps_lib.multiroom.NavigationHelper;

/**
 * Created by nbalazs on 28/11/2016.
 */

public class PlayingCastInfoDialogFragment extends AnimatedDialogFragmentBase {

    public static DialogFragment newInstance() {
        return new PlayingCastInfoDialogFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_playing_cast_info, container, false);

        setupControls(view);

        return view;
    }

    private void setupControls(View view) {
        (view.findViewById(R.id.buttonClose)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        (view.findViewById(R.id.buttonGetGoogleCastApp)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                ExternalAppsOpener.openGoogleCast(getActivity());
            }
        });

        (view.findViewById(R.id.buttonLearnMore)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                NavigationHelper.openWebViewActivityForURL(getActivity(), NavigationHelper.AnimationType.Normal, getActivity().getString(R.string.google_cast_learn_cast_multiroom));
            }
        });
    }
}
