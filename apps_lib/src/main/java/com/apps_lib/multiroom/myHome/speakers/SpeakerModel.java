package com.apps_lib.multiroom.myHome.speakers;

import android.support.annotation.NonNull;

import com.apps_lib.multiroom.speakerImages.SpeakerManager;
import com.frontier_silicon.components.multiroom.MultiroomDeviceModel;

/**
 * Created by lsuhov on 12/06/16.
 */
public class SpeakerModel implements Comparable{
    public MultiroomDeviceModel deviceModel;
    public SpeakerType type;
    public String name;

    public boolean isInMultiMode;

    @Override
    public int compareTo(@NonNull Object o) {
        int retValue = 0;
        if (o instanceof SpeakerModel && o != this) {
            SpeakerModel otherSpeaker = (SpeakerModel) o;

            // 0. Condition: Is master?
            retValue = deviceModel.isServer() ? -1 : otherSpeaker.deviceModel.isServer() ? 1 : 0;

            // 1. condition: Friendly Name
            if (retValue == 0) {
                retValue = this.deviceModel.mRadio.getFriendlyName().compareToIgnoreCase(otherSpeaker.deviceModel.mRadio.getFriendlyName());
            }

            // 2. condition: Speaker Model
            if (retValue == 0) {
                String otherModelName = SpeakerManager.getInstance().getSpeakerModelFromModelName(otherSpeaker.deviceModel.mRadio.getModelName()).name();
                String thisModelName = SpeakerManager.getInstance().getSpeakerModelFromModelName(this.deviceModel.mRadio.getModelName()).name();
                retValue = thisModelName.compareToIgnoreCase(otherModelName);
            }

            // 3. condition: Internal Friendly Name
            if (retValue == 0) {
                String thisInternalName = SpeakerNameManager.getInstance().getNameForRadio(this.deviceModel.mRadio);
                String otherInternalName = SpeakerNameManager.getInstance().getNameForRadio(otherSpeaker.deviceModel.mRadio);
                if (thisInternalName != null && otherInternalName != null)
                retValue = thisInternalName.compareToIgnoreCase(otherInternalName);
            }

            // Fallback: udn -> should never happen
            if (retValue == 0) {
                retValue = otherSpeaker.deviceModel.mRadio.getUDN().compareToIgnoreCase(this.deviceModel.mRadio.getUDN());
            }
        }

        return retValue;
    }
}
