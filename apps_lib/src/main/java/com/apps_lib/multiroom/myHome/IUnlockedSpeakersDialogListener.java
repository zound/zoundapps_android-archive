package com.apps_lib.multiroom.myHome;

/**
 * Created by cvladu on 06/10/2017.
 */

public interface IUnlockedSpeakersDialogListener {
    void onDialogFinished();
}
