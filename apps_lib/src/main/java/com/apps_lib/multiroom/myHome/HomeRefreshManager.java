package com.apps_lib.multiroom.myHome;

/**
 * Created by cvladu on 25/07/2017.
 */

public class HomeRefreshManager {
    private static boolean mIsManuallyRefresh = false;


    public static boolean isIsManuallyRefresh() {
        return mIsManuallyRefresh;
    }

    public static void setIsManuallyRefresh(boolean isManuallyRefresh ) {
        mIsManuallyRefresh = isManuallyRefresh;
    }
}
