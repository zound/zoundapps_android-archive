package com.apps_lib.multiroom.myHome;

import android.content.Context;

import com.frontier_silicon.NetRemoteLib.Node.BasePlayStatus;
import com.frontier_silicon.NetRemoteLib.Node.BaseSysPower;
import com.frontier_silicon.NetRemoteLib.Node.NodeInfo;
import com.frontier_silicon.NetRemoteLib.Node.NodeNavPresetCurrentPreset;
import com.frontier_silicon.NetRemoteLib.Node.NodePlayStatus;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysCapsValidModes;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysPower;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.apps_lib.multiroom.R;
import com.frontier_silicon.components.common.RadioNodeUtil;
import com.frontier_silicon.components.multiroom.MultiroomDeviceModel;
import com.frontier_silicon.components.multiroom.MultiroomGroupManager;

import java.util.Map;

/**
 * Created by lsuhov on 15/06/16.
 */
public class HomeSpeakerPlayInformationProvider {

    public interface IHomeSpeakerPlayInformationListener {
        void onPlayInfoRetrieved(HomeSpeakerPlayModel homeSpeakerPlayModel);
    }

    public static void retrieveSpeakerPlayingInfo(final MultiroomDeviceModel deviceModel, final Context context,
                                                  final IHomeSpeakerPlayInformationListener listener) {
        final HomeSpeakerPlayModel playModel = new HomeSpeakerPlayModel();

        final Radio serverRadio = MultiroomGroupManager.getInstance().getServerOrUngroupedRadio(deviceModel);
        if (serverRadio == null) {
            listener.onPlayInfoRetrieved(playModel);
            return;
        }

        final Class[] nodeClasses = new Class[] {NodePlayStatus.class, NodeNavPresetCurrentPreset.class,
                NodeSysPower.class};

        RadioNodeUtil.getNodesFromRadioAsync(serverRadio, nodeClasses, false, new RadioNodeUtil.INodesResultListener() {
            @Override
            public void onNodesResult(Map<Class, NodeInfo> nodes) {

                NodePlayStatus nodePlayStatus = (NodePlayStatus)nodes.get(NodePlayStatus.class);
                NodeSysPower nodeSysPower = (NodeSysPower)nodes.get(NodeSysPower.class);

                if (nodePlayStatus == null || nodeSysPower == null || !isPlayingOrRelated(nodePlayStatus) ||
                        nodeSysPower.getValueEnum() == BaseSysPower.Ord.OFF) {
                    listener.onPlayInfoRetrieved(playModel);
                    return;
                }

                playModel.isPlaying = true;

                retrievePlayInformation(deviceModel, context, serverRadio, playModel, nodes, listener);
            }
        });
    }

    private static void retrievePlayInformation(final MultiroomDeviceModel deviceModel, final Context context,
                                                final Radio serverRadio, final HomeSpeakerPlayModel playModel,
                                                Map<Class, NodeInfo> nodes,
                                                final IHomeSpeakerPlayInformationListener listener) {
        Radio speakerRadio = deviceModel.mRadio;
        if (serverRadio.equals(speakerRadio)) {
            playModel.isPlayingAndServer = true;

            NodeNavPresetCurrentPreset nodeNavPresetCurrentPreset = (NodeNavPresetCurrentPreset)nodes.get(NodeNavPresetCurrentPreset.class);

            if (nodeNavPresetCurrentPreset != null &&
                    nodeNavPresetCurrentPreset.getValue().intValue() != (-1 & 0xffffffff)) {
                playModel.playingInformation = context.getString(R.string.playing_preset_number,
                        nodeNavPresetCurrentPreset.getValue().intValue() + 1);

                listener.onPlayInfoRetrieved(playModel);
            } else {
                serverRadio.getMode(new Radio.IRadioModeCallback() {
                    @Override
                    public void getCurrentMode(NodeSysCapsValidModes.Mode currentMode) {

                        if (currentMode == NodeSysCapsValidModes.Mode.Cast) {
//                            retrieveCastInformation(serverRadio, playModel, context, currentMode, listener);
                            retriveCastInformation(playModel, context, listener);
                            return;

                        } else if (currentMode == NodeSysCapsValidModes.Mode.Standby ||
                                currentMode == NodeSysCapsValidModes.Mode.Audsync ||
                                currentMode == NodeSysCapsValidModes.Mode.UnknownMode ||
                                currentMode == NodeSysCapsValidModes.Mode.None) {
                            playModel.playingInformation = null;
                        } else {
                            playModel.playingInformation = getModeForUEApp(context, currentMode);
                        }

                        listener.onPlayInfoRetrieved(playModel);
                    }
                });
            }
        } else {
            playModel.playingInformation = context.getString(R.string.playing_from_radio_name,
                    serverRadio.getFriendlyName());

            listener.onPlayInfoRetrieved(playModel);
        }
    }

    private static String getModeForUEApp(Context context, NodeSysCapsValidModes.Mode mode) {
        String displayMode;
        switch (mode) {
            case AuxIn:
                displayMode = context.getString(R.string.carousel_aux_caps);
                break;
            default:
                displayMode = mode.toString();
        }

        return displayMode;
    }

    private static void retriveCastInformation(final HomeSpeakerPlayModel playModel, final Context context, final IHomeSpeakerPlayInformationListener listener) {
        playModel.playingInformation = context.getString(R.string.playing_chromecast_built_in);
        playModel.isPlayingCast = true;
        listener.onPlayInfoRetrieved(playModel);
    }

//    private static void retrieveCastInformation(Radio serverRadio, final HomeSpeakerPlayModel playModel,
//                                                final Context context, final NodeSysCapsValidModes.Mode currentMode,
//                                                final IHomeSpeakerPlayInformationListener listener) {
//
//        RadioNodeUtil.getNodeFromRadioAsync(serverRadio, NodeCastAppName.class, new RadioNodeUtil.INodeResultListener() {
//            @Override
//            public void onNodeResult(NodeInfo node) {
//                NodeCastAppName nodeCastAppName = (NodeCastAppName)node;
//
//                playModel.isPlayingCast = true;
//
//                if (nodeCastAppName != null) {
//                    playModel.playingInformation = context.getString(R.string.playing_chromecast_built_in_via_app,
//                            nodeCastAppName.getValue());
//                } else {
//                    playModel.playingInformation = context.getString(R.string.playing_mode_name,
//                            currentMode.toString());
//                }
//
//                listener.onPlayInfoRetrieved(playModel);
//            }
//        });
//    }


    private static boolean isPlayingOrRelated(NodePlayStatus nodePlayStatus) {
        return nodePlayStatus.getValueEnum() == BasePlayStatus.Ord.PLAYING ||
                nodePlayStatus.getValueEnum() == BasePlayStatus.Ord.BUFFERING ||
                nodePlayStatus.getValueEnum() == BasePlayStatus.Ord.REBUFFERING;
    }

}
