package com.apps_lib.multiroom.myHome.speakers;

/**
 * Created by lsuhov on 12/06/16.
 */
public enum SpeakerType {
    /**
     * Radio
     */
    Speaker,
    /**
     * Multi or Solo mode header
     */
    ModeHeader,
    /**
     * Message that appears in case there is no speaker in one of the modes
     */
    NoSpeakersHeader,
    /**
     * Divider between multi and solo modes
     */
    Divider,
    /**
     * Master volume
     */
    MasterVolume
}
