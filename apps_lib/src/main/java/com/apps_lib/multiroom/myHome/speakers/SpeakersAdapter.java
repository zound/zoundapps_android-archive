package com.apps_lib.multiroom.myHome.speakers;

import android.app.Activity;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.apps_lib.multiroom.R;
import com.apps_lib.multiroom.browse_ir.events.ShowLoadingProgressEvent;
import com.apps_lib.multiroom.myHome.HomeRefreshManager;
import com.frontier_silicon.loggerlib.FsLogger;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lsuhov on 22/06/16.
 */
public abstract class SpeakersAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    protected Activity mActivity;
    protected RecyclerView mRecyclerView;
    protected boolean mMasterIsPresent = true;

    public List<SpeakerModel> mSpeakerModels = new ArrayList<>();

    public class DividerViewHolder extends RecyclerView.ViewHolder {
        public DividerViewHolder(View view) {
            super(view);
        }
    }

    public class ModeHeaderViewHolder extends RecyclerView.ViewHolder {
        TextView textView;

        public ModeHeaderViewHolder(View view) {
            super(view);
            textView = (TextView)view.findViewById(R.id.text);
        }
    }

    public class NoSpeakersHeaderViewHolder extends RecyclerView.ViewHolder {
        TextView textView;

        public NoSpeakersHeaderViewHolder(View view) {
            super(view);
            textView = (TextView)view.findViewById(R.id.text);
        }
    }

    public SpeakersAdapter(Activity activity) {
        mActivity = activity;
    }

    public void swapItems(List<SpeakerModel> newList) {
        SpeakerModelDiffCallback diffCallback = new SpeakerModelDiffCallback(mSpeakerModels, newList);
        DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffCallback);

        mSpeakerModels.clear();
        mSpeakerModels.addAll(newList);

        diffResult.dispatchUpdatesTo(this);
        if (HomeRefreshManager.isIsManuallyRefresh()) {
            EventBus.getDefault().post(new ShowLoadingProgressEvent());
        }
    }

    @Override
    public int getItemViewType(int position) {
        SpeakerModel homeSpeakerModel = mSpeakerModels.get(position);
        int viewType = homeSpeakerModel.type.ordinal();

        return viewType;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        SpeakerType speakerType = SpeakerType.values()[viewType];
        switch (speakerType) {
            case ModeHeader:
                View modeHeaderLayout = LayoutInflater.from(parent.getContext()).inflate(
                        R.layout.list_item_home_speaker_mode_header, parent, false);
                ModeHeaderViewHolder modeHeaderViewHolder = new ModeHeaderViewHolder(modeHeaderLayout);

                return modeHeaderViewHolder;

            case NoSpeakersHeader:
                View noSpeakersLayout = LayoutInflater.from(parent.getContext()).inflate(
                        R.layout.list_item_home_speaker_no_speaker_header, parent, false);
                NoSpeakersHeaderViewHolder noSpeakersHeaderViewHolder = new NoSpeakersHeaderViewHolder(noSpeakersLayout);

                return noSpeakersHeaderViewHolder;
        }
        return null;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        SpeakerModel homeSpeakerModel = mSpeakerModels.get(position);
        switch (homeSpeakerModel.type) {
            case ModeHeader:
                ModeHeaderViewHolder modeHeaderViewHolder = (ModeHeaderViewHolder)holder;
                modeHeaderViewHolder.textView.setText(homeSpeakerModel.name);
                break;
            case NoSpeakersHeader:
                NoSpeakersHeaderViewHolder noSpeakersHeaderViewHolder = (NoSpeakersHeaderViewHolder)holder;
                noSpeakersHeaderViewHolder.textView.setText(homeSpeakerModel.name);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return mSpeakerModels.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);

        mRecyclerView = recyclerView;
    }

    @Override
    public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
        super.onDetachedFromRecyclerView(recyclerView);

        mRecyclerView = null;
    }


    public void dispose() {
        FsLogger.log("SpeakersAdapter: dispose");

        mActivity = null;
        mRecyclerView = null;
        mSpeakerModels.clear();
    }

    protected void checkIfMasterIsPresent(List<SpeakerModel> newList) {
        for (SpeakerModel speakerModel : newList) {
            if (speakerModel.deviceModel != null) {
                mMasterIsPresent |= speakerModel.deviceModel.isServer();
            }
        }
    }

    protected boolean isGroupPresent(List<SpeakerModel> newList) {
        for (SpeakerModel speakerModel : newList) {
            if (speakerModel.deviceModel != null && (speakerModel.deviceModel.isClient() || speakerModel.deviceModel.isServer())) {
                return true;
            }
        }
        return false;
    }
}
