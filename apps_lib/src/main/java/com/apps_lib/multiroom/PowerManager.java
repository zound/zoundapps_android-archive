package com.apps_lib.multiroom;

import android.content.Context;
import android.util.Log;

/**
 * Created by nbalazs on 10/10/2017.
 */

public class PowerManager {

    private static PowerManager INSTANCE;

    private Context mContext;

    android.os.PowerManager.WakeLock mWakeLock;

    private boolean mWakeLockAckuired = false;

    public static PowerManager getINSTANCE() {
        if (INSTANCE == null) {
            INSTANCE = new PowerManager();
        }

        return INSTANCE;
    }

    public final void init(Context context){
        mContext = context;
    }

    public final void keepScreenAlive() {
        android.os.PowerManager powerManager = (android.os.PowerManager) mContext.getSystemService(Context.POWER_SERVICE);
        if (powerManager != null) {
            mWakeLock = powerManager.newWakeLock(android.os.PowerManager.FULL_WAKE_LOCK, "Zound");
            mWakeLock.acquire();
            mWakeLockAckuired = true;
            Log.i("Norbi", "keepScreenAlive: +");
        }
    }

    public final void removeLock() {
        if (mWakeLockAckuired) {
            mWakeLockAckuired = false;
            mWakeLock.release();
            mWakeLock = null;
            Log.i("Norbi", "removeLock: -");
        }
    }
}
