package com.apps_lib.multiroom.util;

import android.app.Activity;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;

import com.apps_lib.multiroom.R;

/**
 * Created by nbalazs on 10/05/2017.
 */

public class ScreenshootManager {

    private static Drawable backgroundDrawable = null;

    public static void prepareScreenshootBackground(Activity activity) {
        float density = activity.getResources().getDisplayMetrics().density;
        float blurFactor = 0.05f;

        if (density < 1.75f) {
            blurFactor = 0.125f;
        }

        backgroundDrawable = BlurBuilder.getInstance().getBlurredScreenshotOfActivity(activity, blurFactor);

        backgroundDrawable.setColorFilter(ContextCompat.getColor(activity, R.color.volume_activity_color_filter), PorterDuff.Mode.OVERLAY);
    }

    public static Drawable getLastScreenshoot() {
        return backgroundDrawable;
    }
}
