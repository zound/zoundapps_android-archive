package com.apps_lib.multiroom.util;

import java.util.TimerTask;

/**
 * Created by lsuhov on 26/07/16.
 */

public abstract class DisposableTimerTask extends TimerTask {
    protected boolean mIsDisposed = false;

    public void dispose() {
        mIsDisposed = true;
    }
}
