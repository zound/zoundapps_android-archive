package com.apps_lib.multiroom.multiroom;

import com.frontier_silicon.components.multiroom.MultiroomGroupManager;
import com.frontier_silicon.loggerlib.FsLogger;
import com.frontier_silicon.loggerlib.LogLevel;
import com.apps_lib.multiroom.speakerImages.SpeakerModelMapper;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Created by lsuhov on 12/06/16.
 */
public class MultiroomRebuildGroups {

    private static MultiroomRebuildGroups mInstance;

    private final int corePoolSize = 0;
    private final int maxPoolSize = 1;
    // Sets the amount of time an idle thread waits before terminating
    private final int KEEP_ALIVE_TIME = 300;
    private final TimeUnit KEEP_ALIVE_TIME_UNIT = TimeUnit.MILLISECONDS;
    private BlockingQueue<Runnable> mRebuildRunnablesQueue = new LinkedBlockingQueue<>(2);
    private ExecutorService mThreadPoolExecutor;

    public static MultiroomRebuildGroups getInstance() {
        if (mInstance == null) {
            mInstance = new MultiroomRebuildGroups();
        }

        return mInstance;
    }

    synchronized public void rebuildMultiroomGroupsAndDevices() {
        createThreadPoolIfNeeded();

        if (mRebuildRunnablesQueue.size() > 1) {
            FsLogger.log("MultiroomRebuildGroups: rebuild queue is full", LogLevel.Info);
            return;
        }

        FsLogger.log("MultiroomRebuildGroups: adding task in rebuild queue", LogLevel.Info);
        mThreadPoolExecutor.submit(new Runnable() {
            @Override
            public void run() {
                SpeakerModelMapper.requestModelNameForAllSpeakersSync();

                MultiroomGroupManager.getInstance().recreateGroupsAndDevicesSync();
            }
        });
    }

    public void clearQueue() {
        mRebuildRunnablesQueue.clear();
    }

    private void createThreadPoolIfNeeded() {
        if (mThreadPoolExecutor != null) {
            return;
        }

        mThreadPoolExecutor = new ThreadPoolExecutor(corePoolSize, maxPoolSize, KEEP_ALIVE_TIME,
                KEEP_ALIVE_TIME_UNIT, mRebuildRunnablesQueue, new RejectedExecutionHandler() {
            @Override
            public void rejectedExecution(Runnable runnable, ThreadPoolExecutor threadPoolExecutor) {
                FsLogger.log("MultiroomRebuildGroups: thread execution was rejected", LogLevel.Error);
            }
        });
    }
}
