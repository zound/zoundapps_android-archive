package com.apps_lib.multiroom;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import android.os.Handler;

import com.apps_lib.multiroom.AppInBackgroundEvent;
import com.frontier_silicon.loggerlib.FsLogger;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by lsuhov on 09/07/16.
 */

public class ActivityLifecycleHandler implements Application.ActivityLifecycleCallbacks {
    private int resumed;
    private int paused;
    private int started;
    private int stopped;

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
    }

    @Override
    public void onActivityDestroyed(Activity activity) {
    }

    @Override
    public void onActivityResumed(Activity activity) {
        ++resumed;
    }

    @Override
    public void onActivityPaused(Activity activity) {
        ++paused;
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
    }

    @Override
    public void onActivityStarted(Activity activity) {
        ++started;
    }

    @Override
    public void onActivityStopped(Activity activity) {
        ++stopped;

        (new Handler()).postDelayed(new Runnable() {
            @Override
            public void run() {
                checkAndSignalIfAppIsNotVisible();
            }
        }, 1000);
        FsLogger.log("application is visible: " + (started > stopped));
    }

    private void checkAndSignalIfAppIsNotVisible() {
        if (!isApplicationVisible()) {
            EventBus.getDefault().post(new AppInBackgroundEvent());
        }
    }

    public boolean isApplicationVisible() {
        return started > stopped;
    }

    public boolean isApplicationInForeground() {
        return resumed > paused;
    }
}
