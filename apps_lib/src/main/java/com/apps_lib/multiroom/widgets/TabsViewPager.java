package com.apps_lib.multiroom.widgets;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

/**
 * Created by lsuhov on 15/05/16.
 */
public class TabsViewPager extends ViewPager {

    private ViewPager mBottomViewPager;
    private OnPageChangeListener mOnPageChangeOffBottomViewPagerListener;
    private OnPageChangeListener mOnPageChangeListener;

    public TabsViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);

        addListeners();
    }

    private void addListeners() {
        mOnPageChangeListener = new OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                mBottomViewPager.setCurrentItem(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        };
        addOnPageChangeListener(mOnPageChangeListener);

        EventBus.getDefault().register(this);
    }

    public void setBottomViewPager(ViewPager viewPager) {
        mBottomViewPager = viewPager;

        mOnPageChangeOffBottomViewPagerListener = new OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                setCurrentItem(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        };
        mBottomViewPager.addOnPageChangeListener(mOnPageChangeOffBottomViewPagerListener);
    }

    @Subscribe
    public void onTabClicked(TabSelectedEvent tabSelectedEvent) {
        this.setCurrentItem(tabSelectedEvent.position);
    }

    public void dispose() {
        if (mBottomViewPager != null) {
            mBottomViewPager.removeOnPageChangeListener(mOnPageChangeOffBottomViewPagerListener);
            mBottomViewPager = null;
            mOnPageChangeOffBottomViewPagerListener = null;
        }

        removeOnPageChangeListener(mOnPageChangeListener);
        mOnPageChangeListener = null;

        EventBus.getDefault().unregister(this);
    }
}
