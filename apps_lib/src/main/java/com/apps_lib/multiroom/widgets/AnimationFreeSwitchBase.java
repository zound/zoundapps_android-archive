package com.apps_lib.multiroom.widgets;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.widget.Switch;


import com.apps_lib.multiroom.R;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created by nbalazs on 26/09/2016.
 */
public abstract class AnimationFreeSwitchBase extends Switch {

    protected Paint mNotSelectedTextPaint;
    protected Paint mSelectedTextPaint;
    protected String mLeftString;
    protected String mRightString;

    public AnimationFreeSwitchBase(Context context) {
        super(context);
        initHack();
        mSelectedTextPaint = createSelectedTextPaint();
        mNotSelectedTextPaint = createNotSelectedTextPaint();
        mLeftString = context.getResources().getString(R.string.solo_caps);
        mRightString = context.getResources().getString(R.string.multi_caps);
    }

    public AnimationFreeSwitchBase(Context context, AttributeSet attrs) {
        super(context, attrs);
        initHack();
        mSelectedTextPaint = createSelectedTextPaint();
        mNotSelectedTextPaint = createNotSelectedTextPaint();
        mLeftString = context.getResources().getString(R.string.solo_caps);
        mRightString = context.getResources().getString(R.string.multi_caps);
    }

    public AnimationFreeSwitchBase(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initHack();
        mSelectedTextPaint = createSelectedTextPaint();
        mNotSelectedTextPaint = createNotSelectedTextPaint();
        mLeftString = context.getResources().getString(R.string.solo_caps);
        mRightString = context.getResources().getString(R.string.multi_caps);
    }

    private Method methodCancelPositionAnimator = null;
    private Method methodSetThumbPosition = null;

    private void initHack(){
        try {
            methodCancelPositionAnimator = Switch.class.getDeclaredMethod("cancelPositionAnimator");
            methodSetThumbPosition = Switch.class.getDeclaredMethod("setThumbPosition", float.class);
            methodCancelPositionAnimator.setAccessible(true);
            methodSetThumbPosition.setAccessible(true);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }

    public void setChecked(boolean checked, boolean animate){
        // Java does not support super.super.xxx calls, a call to the SwitchCompat default setChecked method is needed.
        super.setChecked(checked);
        if(!animate) {

            // See original SwitchCompat source:
            // Calling the super method may result in setChecked() getting called
            // recursively with a different value, so load the REAL value...
            checked = isChecked();

            // Cancel any running animations (started by super.setChecked()) and immediately move the thumb to the new position
            try {
                if(methodCancelPositionAnimator != null && methodSetThumbPosition != null) {
                    methodCancelPositionAnimator.invoke(this, (Object[]) null);
                    methodSetThumbPosition.invoke(this, checked ? 1 : 0);
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        }
    }

    protected abstract Paint createSelectedTextPaint();

    protected abstract Paint createNotSelectedTextPaint();

    protected abstract float getFixedSize();
}
