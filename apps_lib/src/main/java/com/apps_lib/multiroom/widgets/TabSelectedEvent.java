package com.apps_lib.multiroom.widgets;

/**
 * Created by lsuhov on 15/05/16.
 */
public class TabSelectedEvent {
    public final int position;

    public TabSelectedEvent(int position) {
        this.position = position;
    }
}
