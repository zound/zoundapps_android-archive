package com.apps_lib.multiroom.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;

/**
 * Created by nbalazs on 06/09/2016.
 */

public class EditTextWithBackEvent extends android.support.v7.widget.AppCompatEditText {

    public interface EditTextImeBackListener {
        public void onImeBack(EditTextWithBackEvent ctrl, String text);
    }

    private EditTextImeBackListener mOnImeBack;


    public EditTextWithBackEvent(Context context) {
        super(context);
    }

    public EditTextWithBackEvent(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public EditTextWithBackEvent(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public boolean onKeyPreIme(int keyCode, KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK &&
                event.getAction() == KeyEvent.ACTION_UP) {
            if (mOnImeBack != null)
                mOnImeBack.onImeBack(this, this.getText().toString());
        }
        return super.dispatchKeyEvent(event);
    }

    public void setOnEditTextImeBackListener(EditTextImeBackListener listener) {
        mOnImeBack = listener;
    }

}
