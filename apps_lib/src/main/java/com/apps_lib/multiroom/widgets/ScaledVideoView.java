package com.apps_lib.multiroom.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.VideoView;

/**
 * Created by nbalazs on 13/03/2017.
 */

public class ScaledVideoView extends VideoView {

    public ScaledVideoView(Context context) {
        super(context);
    }

    public ScaledVideoView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ScaledVideoView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int width = getDefaultSize(0, widthMeasureSpec);
        int height = getDefaultSize(0, heightMeasureSpec);

        setMeasuredDimension(width, height);
    }
}
