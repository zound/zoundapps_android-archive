package com.apps_lib.multiroom.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.v4.view.VelocityTrackerCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.widget.SeekBar;

import com.apps_lib.multiroom.R;

/**
 * Created by cvladu on 25/07/16.
 */

public class NoJumpSeekBar extends SeekBar {
    protected static  int DELTA_PROGRESS_ON_TAP = 4;
    private static final int DELTA_PROGRESS_ON_FAKE_TAP = 2;
    private Drawable mThumb;
    private OnSeekBarChangeListener mOnSeekBarChangeListener;
    private IMotionEvent mMotionListener;
    private boolean extra_space_on_thumb;
    private IOnValueChangeListener mOnValueChangeListener;
    private VelocityTracker mVelocityTracker = null;

    public void setOnValueChangeListener(IOnValueChangeListener onValueChangeListener) {
        this.mOnValueChangeListener = onValueChangeListener;
    }

    public interface IOnValueChangeListener{
        void onValueChanged(int newValue);
    }

    public NoJumpSeekBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        checkIfThumbIsEnabled(context, attrs);
        getDeltaProgressOnTap(context, attrs);
    }

    public NoJumpSeekBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        checkIfThumbIsEnabled(context, attrs);
        getDeltaProgressOnTap(context, attrs);
    }

    private void getDeltaProgressOnTap(Context context, AttributeSet attributeSet) {
        TypedArray typedArray = context.obtainStyledAttributes(attributeSet, R.styleable.CircleViewPagerIndicator);
        DELTA_PROGRESS_ON_TAP = typedArray.getInt(R.styleable.CircleViewPagerIndicator_delta_progress_on_tap, 4);
    }

    public void checkIfThumbIsEnabled(Context context, AttributeSet attrs) {
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.CircleViewPagerIndicator);
        extra_space_on_thumb = typedArray.getBoolean(R.styleable.CircleViewPagerIndicator_extra_space_on_thumb, false);
    }

    public NoJumpSeekBar(Context context) {
        super(context);
    }

    @Override
    public void setThumb(Drawable thumb) {
        super.setThumb(thumb);
        mThumb = thumb;
    }

    @Override
    public void setOnSeekBarChangeListener(OnSeekBarChangeListener l) {
        mOnSeekBarChangeListener = l;

        super.setOnSeekBarChangeListener(l);
    }

    public void setMotionListener(IMotionEvent listenr) {
        mMotionListener = listenr;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if(event.getAction() == MotionEvent.ACTION_DOWN) {
            int paddingWidth = mThumb.getBounds().width();
            int paddingHeight = mThumb.getBounds().height();

            // if tapped outside of the thumb (+padding for playing nicely with big fingers)
            if ( event.getX() < (mThumb.getBounds().left - (extra_space_on_thumb ? paddingWidth : 0)) ||
                    event.getX() > (mThumb.getBounds().right + (extra_space_on_thumb ? paddingWidth : 0)) ||
                    event.getY() > (mThumb.getBounds().bottom + paddingHeight) ||
                    event.getY() < (mThumb.getBounds().top - paddingHeight) ) {

                int progressDiff;
                if (event.getX() < (mThumb.getBounds().left - (extra_space_on_thumb ? paddingWidth : 0))) {
                    progressDiff = -DELTA_PROGRESS_ON_TAP;
                } else {
                    progressDiff = DELTA_PROGRESS_ON_TAP;
                }

                setProgress(getProgress() + progressDiff);

                //force call on progresschanged with fromUser "true"
                if (mOnSeekBarChangeListener != null) {
                    boolean fromUser = true;
                    mOnSeekBarChangeListener.onStartTrackingTouch(this);
                    mOnSeekBarChangeListener.onProgressChanged(NoJumpSeekBar.this, getProgress(), fromUser);
                    mOnSeekBarChangeListener.onStopTrackingTouch(this);
                }

                return false;
            }
        }  if (event.getAction() == MotionEvent.ACTION_MOVE && event.getAction() !=MotionEvent.ACTION_OUTSIDE && mMotionListener != null) {
                if (mVelocityTracker == null) {
                    mVelocityTracker = VelocityTracker.obtain();
                } else {
                    mVelocityTracker.clear();
                }
            int index = event.getActionIndex();
            int pointerId = event.getPointerId(index);
                mVelocityTracker.addMovement(event);
                mVelocityTracker.computeCurrentVelocity(10);

            double distance = VelocityTrackerCompat.getXVelocity(mVelocityTracker, pointerId);

                if (Math.abs(distance) > 1) {
                    mMotionListener.onMoving(this);
                }
        }  if (event.getAction() == MotionEvent.ACTION_UP && mMotionListener != null) {
            mMotionListener.onFinishMoving(this);
        }



        return super.onTouchEvent(event);
    }

    public void increase(boolean increase) {
        if (increase) {
            setProgress(getProgress() + DELTA_PROGRESS_ON_FAKE_TAP);
        } else {
            setProgress(getProgress() - DELTA_PROGRESS_ON_FAKE_TAP);
        }

        if (mOnSeekBarChangeListener != null) {
            boolean fromUser = true;
            mOnSeekBarChangeListener.onStartTrackingTouch(this);
            mOnSeekBarChangeListener.onProgressChanged(NoJumpSeekBar.this, getProgress(), fromUser);
            mOnSeekBarChangeListener.onStopTrackingTouch(this);
        }
        if (mOnValueChangeListener != null) {
            mOnValueChangeListener.onValueChanged(getProgress());
        }
    }

    public interface IMotionEvent {
        void onMoving(SeekBar seekBar);
        void onFinishMoving(SeekBar seekBar);
    }
}
