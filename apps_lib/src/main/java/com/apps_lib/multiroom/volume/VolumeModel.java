package com.apps_lib.multiroom.volume;

import android.databinding.ObservableBoolean;
import android.databinding.ObservableInt;

/**
 * Created by lsuhov on 23/06/16.
 */
public class VolumeModel {
    public ObservableInt steps = new ObservableInt(33);
    public ObservableInt value = new ObservableInt(0);
    public ObservableBoolean muted = new ObservableBoolean(false);
    public boolean stepsRetrieved = false;
}
