package com.apps_lib.multiroom.volume;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.apps_lib.multiroom.R;
import com.apps_lib.multiroom.databinding.ListItemVolumeMasterBinding;
import com.apps_lib.multiroom.databinding.ListItemVolumeSpeakerBinding;
import com.frontier_silicon.loggerlib.FsLogger;
import com.apps_lib.multiroom.myHome.speakers.SpeakerModel;
import com.apps_lib.multiroom.myHome.speakers.SpeakerType;
import com.apps_lib.multiroom.myHome.speakers.SpeakersAdapter;

import java.util.List;

/**
 * Created by lsuhov on 22/06/16.
 */
public class VolumeSpeakersAdapter extends SpeakersAdapter {

    public class VolumeSpeakerViewHolder extends RecyclerView.ViewHolder {
        ListItemVolumeSpeakerBinding mBinding;

        public VolumeSpeakerViewHolder(ListItemVolumeSpeakerBinding listItemBinding) {
            super(listItemBinding.getRoot());

            mBinding = listItemBinding;
        }
    }

    public class MasterVolumeViewHolder extends RecyclerView.ViewHolder {
        ListItemVolumeMasterBinding mBinding;
        public MasterVolumeViewHolder(ListItemVolumeMasterBinding listItemBinding) {
            super(listItemBinding.getRoot());

            mBinding = listItemBinding;
        }
    }

    public VolumeSpeakersAdapter(Activity activity) {
        super(activity);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        SpeakerType speakerType = SpeakerType.values()[viewType];
        switch (speakerType) {
            case Speaker:
                ListItemVolumeSpeakerBinding listItemBinding = DataBindingUtil.inflate(
                        LayoutInflater.from(parent.getContext()),
                        R.layout.list_item_volume_speaker,
                        parent, false);

                VolumeSpeakerViewHolder volumeSpeakerViewHolder = new VolumeSpeakerViewHolder(listItemBinding);
                return volumeSpeakerViewHolder;

            case MasterVolume:
                ListItemVolumeMasterBinding listItemVolumeMasterBinding = DataBindingUtil.inflate(
                        LayoutInflater.from(parent.getContext()),
                        R.layout.list_item_volume_master,
                        parent, false);

                MasterVolumeViewHolder masterVolumeViewHolder = new MasterVolumeViewHolder(listItemVolumeMasterBinding);
                return masterVolumeViewHolder;

            case Divider:
                // This usecase should never happen
                View dividerLayout = LayoutInflater.from(parent.getContext()).inflate(
                        R.layout.list_item_volume_speaker_divider, parent, false);

                DividerViewHolder dividerViewHolder = new DividerViewHolder(dividerLayout);
                return dividerViewHolder;

            case ModeHeader:
            case NoSpeakersHeader:
                return super.onCreateViewHolder(parent, viewType);
        }

        return null;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {

        SpeakerModel speakerModel = mSpeakerModels.get(position);
        switch (speakerModel.type) {
            case Speaker: {
                VolumeSpeakerViewHolder volumeSpeakerViewHolder = (VolumeSpeakerViewHolder) holder;
                ListItemVolumeSpeakerBinding binding = volumeSpeakerViewHolder.mBinding;

                VolumeSpeakerViewModel viewModel = new VolumeSpeakerViewModel(mActivity, speakerModel, binding);
                binding.setViewModel(viewModel);
                binding.volumeSeekBar.setOnSeekBarChangeListener(viewModel);
                break;
            }
            case MasterVolume: {
                MasterVolumeViewHolder masterVolumeViewHolder = (MasterVolumeViewHolder) holder;
                ListItemVolumeMasterBinding masterBinding = masterVolumeViewHolder.mBinding;

                MasterVolumeViewModel masterVolumeViewModel = new MasterVolumeViewModel(speakerModel, mMasterIsPresent, masterBinding, mActivity);
                masterBinding.setViewModel(masterVolumeViewModel);
                masterBinding.masterVolumeSeekBar.setOnSeekBarChangeListener(masterVolumeViewModel);
                break;
            }
            case ModeHeader:
            case NoSpeakersHeader:
                super.onBindViewHolder(holder, position);
                break;
        }
    }

    @Override
    public void onViewRecycled(RecyclerView.ViewHolder holder) {
        int position = holder.getAdapterPosition();
        FsLogger.log("recycled from position " + String.valueOf(position));

        if (position == -1) {
            if (holder instanceof VolumeSpeakerViewHolder) {
                VolumeSpeakerViewHolder volumeSpeakerViewHolder = (VolumeSpeakerViewHolder)holder;
                disposeVolumeSpeakerViewHolder(volumeSpeakerViewHolder);

            } else if (holder instanceof MasterVolumeViewHolder) {
                MasterVolumeViewHolder masterVolumeViewHolder = (MasterVolumeViewHolder) holder;
                disposeMasterVolumeViewHolder(masterVolumeViewHolder);
            }

        } else {
            SpeakerModel speakerModel = mSpeakerModels.get(position);

            if (speakerModel.type == SpeakerType.Speaker) {
                VolumeSpeakerViewHolder homeSpeakerViewHolder = (VolumeSpeakerViewHolder) holder;
                disposeVolumeSpeakerViewHolder(homeSpeakerViewHolder);
            } else if (speakerModel.type == SpeakerType.MasterVolume) {
                MasterVolumeViewHolder masterVolumeViewHolder = (MasterVolumeViewHolder) holder;
                disposeMasterVolumeViewHolder(masterVolumeViewHolder);
            }
        }

        super.onViewRecycled(holder);
    }

    private void disposeMasterVolumeViewHolder(MasterVolumeViewHolder masterVolumeViewHolder) {
        MasterVolumeViewModel masterVolumeViewModel = masterVolumeViewHolder.mBinding.getViewModel();
        if (masterVolumeViewModel != null) {
            masterVolumeViewModel.dispose();
            masterVolumeViewHolder.mBinding.masterVolumeSeekBar.setOnSeekBarChangeListener(null);
            masterVolumeViewHolder.mBinding.setViewModel(null);
        }
    }

    private void disposeVolumeSpeakerViewHolder(VolumeSpeakerViewHolder volumeSpeakerViewHolder) {
        VolumeSpeakerViewModel volumeSpeakerViewModel = volumeSpeakerViewHolder.mBinding.getViewModel();
        if (volumeSpeakerViewModel != null) {
            volumeSpeakerViewModel.dispose();
            volumeSpeakerViewHolder.mBinding.volumeSeekBar.setOnSeekBarChangeListener(null);
            volumeSpeakerViewHolder.mBinding.setViewModel(null);
        }
    }

    @Override
    public void swapItems(List<SpeakerModel> newList) {
        boolean isGroupPresent = isGroupPresent(newList);
        if (isGroupPresent) {
            mMasterIsPresent = false;
            checkIfMasterIsPresent(newList);
        }

        super.swapItems(newList);
    }
}
