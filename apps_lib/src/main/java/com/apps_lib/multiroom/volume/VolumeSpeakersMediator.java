package com.apps_lib.multiroom.volume;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;

import com.apps_lib.multiroom.myHome.speakers.SpeakersMediator;

/**
 * Created by lsuhov on 22/06/16.
 */
public class VolumeSpeakersMediator extends SpeakersMediator {

    public void init(Activity activity, RecyclerView recyclerView) {
        mAdapter = new VolumeSpeakersAdapter(activity);
        mSpeakersBuilder = new VolumeSpeakersBuilder();

        super.init(activity, recyclerView);
    }
}
