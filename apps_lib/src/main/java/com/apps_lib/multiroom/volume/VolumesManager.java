package com.apps_lib.multiroom.volume;

import android.databinding.ObservableBoolean;

import com.frontier_silicon.NetRemoteLib.Node.BaseSysAudioMute;
import com.frontier_silicon.NetRemoteLib.Node.NodeInfo;
import com.frontier_silicon.NetRemoteLib.Node.NodeMultiroomClientVolume0;
import com.frontier_silicon.NetRemoteLib.Node.NodeMultiroomClientVolume1;
import com.frontier_silicon.NetRemoteLib.Node.NodeMultiroomClientVolume2;
import com.frontier_silicon.NetRemoteLib.Node.NodeMultiroomClientVolume3;
import com.frontier_silicon.NetRemoteLib.Node.NodeMultiroomGroupMasterVolume;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysAudioMute;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysAudioVolume;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysCapsVolumeSteps;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.components.common.RadioNodeUtil;
import com.frontier_silicon.components.multiroom.MultiroomDeviceModel;
import com.frontier_silicon.components.multiroom.MultiroomGroupManager;
import com.frontier_silicon.components.NetRemoteManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by lsuhov on 22/06/16.
 */
public class VolumesManager {

    public static final float UNMUTED_OPACITY = (float) 0.9;
    public static final float MUTED_OPACITY = (float) 0.3;
    private final int VOLUME_CHECK_PERIOD = 700;

    public String masterUdn;
    public VolumeModel masterVolumeModel = new VolumeModel();
    public ObservableBoolean allSpeakersMuted = new ObservableBoolean(false);
    private Map<String /*udn*/, VolumeModel> mVolumeModelsMap = new HashMap<>();
    private final Object mLock = new Object();
    private int mLastValueSent;
    private Timer mTimer;
    private UpdateVolumesTimerTask mTimerTask;
    private boolean mServerRequestInProgress = false;
    private boolean mNewVolumeShouldBeSent = false;
    private  NodeInfo mVolumeNode;


    private static VolumesManager mInstance;

    public static VolumesManager getInstance() {
        if (mInstance == null) {
            mInstance = new VolumesManager();
        }

        return mInstance;
    }

    public VolumeModel getVolumeModel(String udn) {
        synchronized (mLock) {
            if (!mVolumeModelsMap.containsKey(udn)) {
                mVolumeModelsMap.put(udn, new VolumeModel());
            }
            updateAllSpeakersMuted();
        }

        return mVolumeModelsMap.get(udn);
    }

    public void startVolumeTimer() {
        stopVolumeTimer();

        mTimerTask = new UpdateVolumesTimerTask();
        mTimer = new Timer();

        mTimer.schedule(mTimerTask, 400, VOLUME_CHECK_PERIOD);
    }

    public void stopVolumeTimer() {
        if (mTimer != null) {
            mTimer.cancel();
            mTimer.purge();
            mTimer = null;
        }

        if (mTimerTask != null) {
            mTimerTask.cancel();
            mTimerTask.dispose();
            mTimerTask = null;
        }
    }

    public void setMasterVolumeValueToRadio(Radio radio, int value) {
        if (radio == null) {
            return;
        }

        setVolumeForAll(radio, new NodeMultiroomGroupMasterVolume((long) value));

        //TODO lsuhov: remove this after mute bug is fixed on FW
        List<String> udnsOfGroup = getUDNsOfGroup();
        for (String udn : udnsOfGroup) {
            VolumeModel volumeModel = mVolumeModelsMap.get(udn);
            if (volumeModel != null && volumeModel.muted.get()) {
                Radio speakerRadio = NetRemoteManager.getInstance().getRadio(udn);
                RadioNodeUtil.setNodeToRadioAsync(speakerRadio, new NodeSysAudioMute(NodeSysAudioMute.Ord.NOT_MUTE), null);
            }
        }
    }

    public void setVolumeValueToRadio(final Radio radio, final int value) {
        if (radio == null) {
            return;
        }

        setVolumeForAll(radio, new NodeSysAudioVolume((long) value));
        String udn = radio.getUDN();
        VolumeModel volumeModel = mVolumeModelsMap.get(udn);
        if (volumeModel != null && volumeModel.muted.get()) {
            radio.setNode(new NodeSysAudioMute(NodeSysAudioMute.Ord.NOT_MUTE), false);
        }
    }

    public void setVolumeForAll(final Radio radio, final NodeInfo volumeNode) {
        mVolumeNode = volumeNode;

        if (mServerRequestInProgress) {
            mNewVolumeShouldBeSent = true;
            return;

        } else {
            mServerRequestInProgress = true;
            RadioNodeUtil.setNodeToRadioAsync(radio, mVolumeNode, new RadioNodeUtil.INodeSetResultListener() {
                @Override
                public void onNodeSetResult(boolean success) {
                    mServerRequestInProgress = false;
                    if (mNewVolumeShouldBeSent) {
                        mNewVolumeShouldBeSent = false;
                        setVolumeForAll(radio, mVolumeNode);
                    }
                }
            });

        }
    }

    public void setMuteToRadio(final Radio radio, final boolean mute) {
        if (radio == null) {
            return;
        }

        NodeSysAudioMute.Ord muteOrd = mute ? NodeSysAudioMute.Ord.MUTE : NodeSysAudioMute.Ord.NOT_MUTE;
        radio.setNode(new NodeSysAudioMute(muteOrd), false);
    }

    private void setVolumeValueInternal(String udn, int value) {
        VolumeModel volumeModel = getVolumeModel(udn);
        volumeModel.value.set(value);
    }

    private void setMuteValueInternal(String udn, boolean muted) {
        VolumeModel volumeModel = getVolumeModel(udn);
        if (volumeModel.muted.get() != muted) {
            volumeModel.muted.set(muted);

            updateMasterMute();
            updateAllSpeakersMuted();
        }
    }

    private void updateAllSpeakersMuted() {
        boolean isMuted = masterVolumeModel.muted.get();

        List<String> udns = getAllUdnsOfRegisteredVolumes();
        for (String udn : udns) {
            VolumeModel volumeModel = mVolumeModelsMap.get(udn);
            isMuted &= volumeModel.muted.get();
        }

        allSpeakersMuted.set(isMuted);
    }

    private void updateMasterMute() {
        List<String> udnsOfGroup = getUDNsOfGroup();

        boolean isMuted = true;
        for (String udnOfSpeaker : udnsOfGroup) {
            VolumeModel volumeModel = mVolumeModelsMap.get(udnOfSpeaker);
            if (volumeModel != null && !volumeModel.muted.get()) {
                isMuted = false;
                break;
            }
        }

        masterVolumeModel.muted.set(isMuted);
    }

    private List<String> getUDNsOfGroup() {
        List<String> udns = new ArrayList<>();
        MultiroomDeviceModel serverDeviceModel = MultiroomGroupManager.getInstance().getDeviceByUdn(masterUdn);
        if (serverDeviceModel == null) {
            return udns;
        }
        List<MultiroomDeviceModel> devicesFromGroup = MultiroomGroupManager.getInstance().getAllDevicesForGroupId(serverDeviceModel.mGroupId);

        for (MultiroomDeviceModel deviceModel : devicesFromGroup) {
            String udnOfSpeaker = deviceModel.mRadio.getUDN();
            udns.add(udnOfSpeaker);
        }

        return udns;
    }

    public void toggleMuteAll(boolean checked) {
        if (allSpeakersMuted.get() == checked) {
            return;
        }

        List<String> allUdns = getAllUdnsOfRegisteredVolumes();

        for (String udn : allUdns) {
            Radio radio = NetRemoteManager.getInstance().getRadio(udn);
            setMuteToRadio(radio, checked);
        }
    }

    public class UpdateVolumesTimerTask extends TimerTask {

        private boolean isCanceled = false;

        @Override
        public void run() {
            if (isCanceled) {
                return;
            }

            updateVolumes();
        }

        public void dispose() {
            isCanceled = true;
        }
    }

    private void updateVolumes() {
        updateMasterVolume();
        updateSpeakerVolumes();
    }

    private void updateSpeakerVolumes() {
        List<String> udns = getAllUdnsOfRegisteredVolumes();

        for (String udn : udns) {
            Radio radio = NetRemoteManager.getInstance().getRadio(udn);
            if (radio == null) {
                //Removing radios not found, to not alter results like allSpeakersMuted
                mVolumeModelsMap.remove(udn);
                continue;
            }

            updateVolumeForRadioAsync(radio);
        }
    }

    private List<String> getAllUdnsOfRegisteredVolumes() {
        List<String> udns;
        synchronized (mLock) {
            udns = new ArrayList<>(mVolumeModelsMap.keySet());
        }

        return udns;
    }

    private void updateVolumeForRadioAsync(Radio radio) {
        final String udn = radio.getUDN();

        RadioNodeUtil.getNodesFromRadioAsync(radio, new Class[] {NodeSysAudioVolume.class, NodeSysAudioMute.class},
                false, new RadioNodeUtil.INodesResultListener() {
            @Override
            public void onNodesResult(Map<Class, NodeInfo> nodes) {

                NodeSysAudioVolume volumeNode = (NodeSysAudioVolume)nodes.get(NodeSysAudioVolume.class);
                if (volumeNode != null && !mServerRequestInProgress) {
                    int value = volumeNode.getValue().intValue();
                    setVolumeValueInternal(udn, value);
                }

                NodeSysAudioMute muteNode = (NodeSysAudioMute)nodes.get(NodeSysAudioMute.class);
                if (muteNode != null) {
                    boolean muted = muteNode.getValueEnum() == BaseSysAudioMute.Ord.MUTE;
                    setMuteValueInternal(udn, muted);
                }
            }
        });
    }

    private void getMultiroomClinetsVolume(Radio radio) {
        if(radio != null && NetRemoteManager.getInstance().checkConnection(radio)) {
            Map<Class, NodeInfo> volumeNodes = radio.getNodesSyncGetter(new Class[] {NodeMultiroomClientVolume0.class,  NodeMultiroomClientVolume1.class, NodeMultiroomClientVolume2.class,  NodeMultiroomClientVolume3.class}).get();

                NodeMultiroomClientVolume0 volume0 = (NodeMultiroomClientVolume0) volumeNodes.get(NodeMultiroomClientVolume0.class);
                NodeMultiroomClientVolume1 volume1 = (NodeMultiroomClientVolume1) volumeNodes.get(NodeMultiroomClientVolume1.class);
                NodeMultiroomClientVolume2 volume2 = (NodeMultiroomClientVolume2) volumeNodes.get(NodeMultiroomClientVolume2.class);
                NodeMultiroomClientVolume3 volume3 = (NodeMultiroomClientVolume3) volumeNodes.get(NodeMultiroomClientVolume3.class);

                if(volume0 != null) {
                    int value = volume0.getValue().intValue();

                }

                if(volume1 != null) {
                    int value = volume1.getValue().intValue();
                }

                if(volume2 != null) {
                    int value = volume2.getValue().intValue();
                }

                if (volume3 != null) {
                    int value = volume3.getValue().intValue();
                }
            }
        }

    private void updateMasterVolume() {
        Radio serverRadio = NetRemoteManager.getInstance().getRadio(masterUdn);
        if (serverRadio != null) {
            RadioNodeUtil.getNodeFromRadioAsync(serverRadio, NodeMultiroomGroupMasterVolume.class, new RadioNodeUtil.INodeResultListener() {
                @Override
                public void onNodeResult(NodeInfo node) {
                    if (node == null) {
                        return;
                    }

                    NodeMultiroomGroupMasterVolume masterVolumeNode = (NodeMultiroomGroupMasterVolume)node;
                    masterVolumeModel.value.set(masterVolumeNode.getValue().intValue());
                }
            });
        }
    }

    public void retrieveVolumeSteps(MultiroomDeviceModel deviceModel, final VolumeModel volumeModel) {
        if (deviceModel == null || deviceModel.mRadio == null || volumeModel == null || volumeModel.stepsRetrieved) {
            return;
        }

        RadioNodeUtil.getNodeFromRadioAsync(deviceModel.mRadio, NodeSysCapsVolumeSteps.class, new RadioNodeUtil.INodeResultListener() {
            @Override
            public void onNodeResult(NodeInfo node) {
                if (node == null) {
                    return;
                }

                NodeSysCapsVolumeSteps nodeSysCapsVolumeSteps = (NodeSysCapsVolumeSteps)node;

                volumeModel.stepsRetrieved = true;
                volumeModel.steps.set(nodeSysCapsVolumeSteps.getValue().intValue());
            }
        });
    }
}
