package com.apps_lib.multiroom.volume;

import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableInt;
import android.widget.SeekBar;

import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.apps_lib.multiroom.R;
import com.frontier_silicon.components.multiroom.MultiroomDeviceModel;
import com.frontier_silicon.components.multiroom.MultiroomGroupManager;
import com.frontier_silicon.loggerlib.FsLogger;
import com.frontier_silicon.loggerlib.LogLevel;

/**
 * Created by cvladu on 10/11/16.
 */

public class VolumePopupViewModel extends BaseObservable implements SeekBar.OnSeekBarChangeListener {

    public static final String NO_GROUP_ID = "";
    private Context mContext;
    private MultiroomDeviceModel mMultiRoomDeviceModel;
    private VolumeModel mVolumeModel;
    private boolean isSeeking = false;

    public ObservableInt max = new ObservableInt(0);
    public ObservableInt seekValue = new ObservableInt(0);
    public ObservableBoolean isVisible = new ObservableBoolean(false);

    public VolumePopupViewModel(Context context, MultiroomDeviceModel multiroomDeviceModel) {
        mContext = context;
        mMultiRoomDeviceModel = multiroomDeviceModel;

        init();
    }

    private void init() {
        if (mMultiRoomDeviceModel == null || mMultiRoomDeviceModel.mRadio == null) {
            FsLogger.log("Device model or Radio from VolumeSpeakerViewModel is null", LogLevel.Error);
            return;
        }

        String udn = mMultiRoomDeviceModel.mRadio.getUDN();
        mVolumeModel = VolumesManager.getInstance().getVolumeModel(udn);


        updateAll();
    }


    private void updateAll() {
        updateVolumeSteps();
        updateVolumeValue();
    }

    private void updateVolumeSteps() {
        if (max.get() != mVolumeModel.steps.get()) {
            max.set(mVolumeModel.steps.get() - 1);
        }
    }

    private void updateVolumeValue() {
        if (isSeeking) {
            return;
        }

        if (seekValue.get() != mVolumeModel.value.get()) {
            seekValue.set(mVolumeModel.value.get());
        }
    }

    @Bindable
    public String getRadioName() {

        Radio radio = mMultiRoomDeviceModel.mRadio;

        String radioName;
        if (radio != null && mMultiRoomDeviceModel.mGroupId.equals(NO_GROUP_ID)) {
            radioName = radio.getFriendlyName();
        } else {
            radioName = mContext.getResources().getString(R.string.volume_dialog_multi);
        }

        return radioName;
    }


    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
        if (mVolumeModel.steps.get() != 0 && isSeeking) {
            MultiroomGroupManager.getInstance().setMainVolumeInSteps(i, true);
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        isSeeking = true;
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        isSeeking = false;
    }
}
