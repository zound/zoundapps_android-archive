package com.apps_lib.multiroom.source;

import android.graphics.drawable.Drawable;

/**
 * Created by nbalazs on 14/06/2017.
 */

public interface IRotationHandler {
    void startLeftToRightRotation(Drawable drawable);

    void startRightToLeftRotation(Drawable drawable);
}
