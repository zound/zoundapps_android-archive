package com.apps_lib.multiroom.connection;

import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.components.multiroom.MultiroomDeviceModel;

/**
 * Created by lsuhov on 02/02/2017.
 */

public interface IConnectToServerAndSelectedSpeakerListener {

    void onConnected(boolean success, Radio serverRadio,
                     boolean secondSpeakerWasConnected, MultiroomDeviceModel selectedSpeakerDeviceModel);
}
