package com.apps_lib.multiroom.connection;

import com.apps_lib.multiroom.browse_ir.model.BrowseIRManager;
import com.apps_lib.multiroom.nowPlaying.NowPlayingManager;
import com.apps_lib.multiroom.source.SourcesManager;
import com.frontier_silicon.NetRemoteLib.Radio.ConnectionErrorResponse;
import com.frontier_silicon.NetRemoteLib.Radio.IConnectionCallback;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.components.NetRemoteManager;
import com.frontier_silicon.components.common.BaseShowProgressDialogTask;
import com.frontier_silicon.components.connection.AfterConnectionNodesSetter;
import com.frontier_silicon.components.multiroom.MultiroomDeviceModel;
import com.frontier_silicon.components.multiroom.MultiroomGroupManager;

import java.util.concurrent.CountDownLatch;

/**
 * Created by lsuhov on 16/06/16.
 */
public class ConnectToServerAndSelectedSpeakerTask extends BaseShowProgressDialogTask<Boolean> {
    private MultiroomDeviceModel mMultiroomDeviceModel;
    private IConnectToServerAndSelectedSpeakerListener mListener;
    private Radio mServerRadio;
    private boolean mFailSilently;
    private boolean mConnectedToSecondSpeaker = false;
    private boolean mConnectedToAllSpeakers = false;
    final CountDownLatch mCountDownLatch = new CountDownLatch(1);
    final CountDownLatch mPresetsCountDownLatch = new CountDownLatch(1);

    public ConnectToServerAndSelectedSpeakerTask(MultiroomDeviceModel deviceModel, boolean failSilently,
                                                 IConnectToServerAndSelectedSpeakerListener listener) {
        mMultiroomDeviceModel = deviceModel;
        mFailSilently = failSilently;
        mListener = listener;
    }

    @Override
    protected void onPreExecute() {
    }

    @Override
    protected Boolean doInBackground(Integer... arg0) {

        mServerRadio = MultiroomGroupManager.getInstance().getServerOrUngroupedRadio(mMultiroomDeviceModel);
        if (mServerRadio != null) {
            Radio selectedRadio = mMultiroomDeviceModel.mRadio;
            connectToSecondSpeakerIfNeeded(selectedRadio);

            mConnectedToAllSpeakers = MultiroomGroupManager.getInstance().connectToRadio(mServerRadio, mFailSilently);

            try {
                mCountDownLatch.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            ConnectionManager connectionManager = ConnectionManager.getInstance();

            connectionManager.setServerRadio(mServerRadio);
            connectionManager.setSelectedRadio(mMultiroomDeviceModel.mRadio);

            if (mConnectedToAllSpeakers) {
                connectionManager.setLastSelectedDeviceSN(mMultiroomDeviceModel.mRadio.getSN());

                prepareForNewRadio();

                try {
                    mPresetsCountDownLatch.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            return mConnectedToAllSpeakers;
        } else {
            return false;
        }
    }

    private void connectToSecondSpeakerIfNeeded(Radio selectedRadio) {
        if (selectedRadio != null && !mServerRadio.equals(selectedRadio)) {
            selectedRadio.connect(false, new IConnectionCallback() {
                @Override
                public void onConnectionSuccess(Radio radio) {
                    mConnectedToSecondSpeaker = true;
                    mCountDownLatch.countDown();
                }

                @Override
                public void onConnectionError(Radio radio, ConnectionErrorResponse error) {
                    mConnectedToSecondSpeaker = false;
                    mConnectedToAllSpeakers = false;

                    NetRemoteManager.getInstance().notifyRadioConnectionError(radio, error);
                    mCountDownLatch.countDown();
                }

                @Override
                public void onDisconnected(Radio radio, ConnectionErrorResponse error) {
                    mConnectedToSecondSpeaker = false;
                    mConnectedToAllSpeakers = false;

                    NetRemoteManager.getInstance().notifyRadioConnectionLost(radio, error);
                    mCountDownLatch.countDown();
                }
            });
        } else {
            mCountDownLatch.countDown();
        }
    }

    private void prepareForNewRadio() {
        AfterConnectionNodesSetter.setCastAcceptanceToServerAndClientsSync(mServerRadio, mMultiroomDeviceModel.mGroupId);

        ConnectionManager.getInstance().saveLastConnectedDevice();

//        RadioNodeUtil.powerOnIfStandBy(mServerRadio, true);

        SourcesManager.getInstance().bindToRadio(new IPresetDownloaderAtConnection() {
            @Override
            public void onPresetDownloaderFinished() {
                mPresetsCountDownLatch.countDown();
            }
        });
        NowPlayingManager.getInstance().bindToRadio(mServerRadio);

        BrowseIRManager.getInstance().onSpeakerConnected(mServerRadio);
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);

        if (result == null) {
            if (mListener != null) {
                mListener.onConnected(false, mServerRadio, mConnectedToSecondSpeaker, mMultiroomDeviceModel);
            }
            dispose();
            return;
        }

        if (mListener != null) {
            mListener.onConnected(result, mServerRadio, mConnectedToSecondSpeaker, mMultiroomDeviceModel);
        }

        dispose();
    }

    protected void dispose() {
        mListener = null;
        mMultiroomDeviceModel = null;
        mServerRadio = null;

        super.dispose();
    }
}
