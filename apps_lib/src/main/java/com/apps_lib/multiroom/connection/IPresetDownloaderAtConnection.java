package com.apps_lib.multiroom.connection;

/**
 * Created by cvladu on 27/07/2017.
 */

public interface IPresetDownloaderAtConnection {

    public void onPresetDownloaderFinished();
}
