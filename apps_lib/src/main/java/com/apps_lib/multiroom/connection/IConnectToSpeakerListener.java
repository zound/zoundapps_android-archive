package com.apps_lib.multiroom.connection;

import com.frontier_silicon.NetRemoteLib.Radio.Radio;

/**
 * Created by lsuhov on 13/12/2016.
 */

public interface IConnectToSpeakerListener {
    void onConnectedToServerOrRadio(boolean connectedOk, Radio selectedRadio);
}
