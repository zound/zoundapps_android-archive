package com.apps_lib.multiroom.speakerImages;

import android.text.TextUtils;

import com.frontier_silicon.NetRemoteLib.Node.NodeInfo;
import com.frontier_silicon.NetRemoteLib.Node.NodePlatformOEMColorProduct;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.components.NetRemoteManager;
import com.frontier_silicon.components.common.CommonPreferences;
import com.frontier_silicon.components.common.RadioNodeUtil;

import java.util.List;

/**
 * Created by lsuhov on 04/08/16.
 */
public class SpeakerModelMapper {

    public static void getModelNameForRadio(final Radio radio, final IModelNameListener listener) {
        if (radio == null) {
            listener.onModelNameRetrieved("");
            return;
        }

        String modelName = CommonPreferences.getInstance().getString(radio.getSN());
        if (!TextUtils.isEmpty(modelName)) {
            listener.onModelNameRetrieved(modelName);
            return;
        }

        RadioNodeUtil.getNodeFromRadioAsync(radio, NodePlatformOEMColorProduct.class, new RadioNodeUtil.INodeResultListener() {
            @Override
            public void onNodeResult(NodeInfo node) {
                NodePlatformOEMColorProduct modelName = (NodePlatformOEMColorProduct)node;
                if (modelName != null) {
                    CommonPreferences.getInstance().putString(radio.getSN(), modelName.getValue());

                    listener.onModelNameRetrieved(modelName.getValue());
                } else {
                    listener.onModelNameRetrieved("");
                }
            }
        });
    }

    public static void requestModelNameForAllSpeakersSync() {
        List<Radio> radios = NetRemoteManager.getInstance().getRadios();
        for (int i = 0; i < radios.size(); i++) {
            radios.get(i).getNodeSyncGetter(NodePlatformOEMColorProduct.class).get();
        }
    }
}
