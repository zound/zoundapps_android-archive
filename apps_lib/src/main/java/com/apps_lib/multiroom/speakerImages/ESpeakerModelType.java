package com.apps_lib.multiroom.speakerImages;
/**
 * Created by lsuhov on 04/08/16.
 */

public enum  ESpeakerModelType {
    Unknown,
    StammenDirtyPink,
    StammenGoldFish,
    StammenIndigo,
    StammenConcreteGrey,
    StammenPlantGreen,
    StammenVinylBlack,
    BaggenVinylBlack,
    BaggenPlantGreen,
    BaggenConcreteGrey,
    BaggenIndigo,
    BaggenGoldFish,
    BaggenDirtyPink,
    LotsenDirtyPink,
    LotsenGoldFish,
    LotsenIndigo,
    LotsenConcreteGrey,
    LotsenPlantGreen,
    LotsenVinylBlack,
    ActonBlack,
    ActonCream,
    StanmoreBlack,
    StanmoreCream,
    WoburnBlack,
    WoburnCream,

    BaggenUnlocked,
    StammenUnlocked
}
