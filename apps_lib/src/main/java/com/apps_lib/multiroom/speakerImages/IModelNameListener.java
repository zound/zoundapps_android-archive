package com.apps_lib.multiroom.speakerImages;
/**
 * Created by lsuhov on 04/08/16.
 */

public interface IModelNameListener {
    void onModelNameRetrieved(String modelName);
}
