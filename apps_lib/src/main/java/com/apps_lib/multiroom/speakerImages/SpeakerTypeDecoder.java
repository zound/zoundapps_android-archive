package com.apps_lib.multiroom.speakerImages;

import static com.apps_lib.multiroom.speakerImages.ESpeakerModelType.*;

/**
 * Created by nbalazs on 09/05/2017.
 */

public abstract class SpeakerTypeDecoder {

    protected SpeakerTypeDecoder() {}

    protected abstract ESpeakerModelType getSpeakerModelFromSSID(String ssid);

    protected abstract ESpeakerModelType getSpeakerModelFromModelName(String modelName, String softwareVersion);

    public static ESpeakerModelType getUrbanearsSpeakerModel(String ssidOrModelName, String softwareVersion) {
        if (ssidOrModelName.contains("Baggen") && softwareVersion.contains("unlocked")) {
            return BaggenUnlocked;
        } else if (ssidOrModelName.contains("Stammen") && softwareVersion.contains("unlocked")) {
            return StammenUnlocked;
        } else if (ssidOrModelName.contains("Baggen") && ssidOrModelName.contains("Dirty") && ssidOrModelName.contains("Pink")) {
            return BaggenDirtyPink;
        } else if (ssidOrModelName.contains("Baggen") && ssidOrModelName.contains("Gold") && ssidOrModelName.contains("Fish")) {
            return BaggenGoldFish;
        } else if (ssidOrModelName.contains("Baggen") && ssidOrModelName.contains("Indigo")) {
            return BaggenIndigo;
        } else if (ssidOrModelName.contains("Baggen") && ssidOrModelName.contains("Concrete") && ssidOrModelName.contains("Grey")) {
            return BaggenConcreteGrey;
        } else if (ssidOrModelName.contains("Baggen") && ssidOrModelName.contains("Plant") && ssidOrModelName.contains("Green")) {
            return BaggenPlantGreen;
        } else if (ssidOrModelName.contains("Baggen") && ssidOrModelName.contains("Vinyl") && ssidOrModelName.contains("Black")) {
            return BaggenVinylBlack;
        } else if (ssidOrModelName.contains("Stammen") && ssidOrModelName.contains("Vinyl") && ssidOrModelName.contains("Black")) {
            return StammenVinylBlack;
        } else if (ssidOrModelName.contains("Stammen") && ssidOrModelName.contains("Plant") && ssidOrModelName.contains("Green")) {
            return StammenPlantGreen;
        } else if (ssidOrModelName.contains("Stammen") && ssidOrModelName.contains("Concrete") && ssidOrModelName.contains("Grey")) {
            return StammenConcreteGrey;
        } else if (ssidOrModelName.contains("Stammen") && ssidOrModelName.contains("Indigo")) {
            return StammenIndigo;
        } else if (ssidOrModelName.contains("Stammen") && ssidOrModelName.contains("Gold") && ssidOrModelName.contains("Fish")) {
            return StammenGoldFish;
        } else if (ssidOrModelName.contains("Stammen") && ssidOrModelName.contains("Dirty") && ssidOrModelName.contains("Pink")) {
            return StammenDirtyPink;
        } else if (ssidOrModelName.contains("Lotsen") && ssidOrModelName.contains("Dirty") && ssidOrModelName.contains("Pink")) {
            return LotsenDirtyPink;
        } else if (ssidOrModelName.contains("Lotsen") && ssidOrModelName.contains("Gold") && ssidOrModelName.contains("Fish")) {
            return LotsenGoldFish;
        } else if (ssidOrModelName.contains("Lotsen") && ssidOrModelName.contains("Indigo")) {
            return LotsenIndigo;
        } else if (ssidOrModelName.contains("Lotsen") && ssidOrModelName.contains("Concrete") && ssidOrModelName.contains("Grey")) {
            return LotsenConcreteGrey;
        } else if (ssidOrModelName.contains("Lotsen") && ssidOrModelName.contains("Plant") && ssidOrModelName.contains("Green")) {
            return LotsenPlantGreen;
        } else if (ssidOrModelName.contains("Lotsen") && ssidOrModelName.contains("Vinyl") && ssidOrModelName.contains("Black")) {
            return LotsenVinylBlack;
        }

        return Unknown;
    }

    public static ESpeakerModelType getMarshallSpeakerModel(String ssidOrModelName) {
        if (ssidOrModelName.contains("Acton") && ssidOrModelName.contains("Black")) {
            return ActonBlack;
        } else if (ssidOrModelName.contains("Acton") && ssidOrModelName.contains("Cream")) {
            return ActonCream;
        } else if (ssidOrModelName.contains("Stanmore") && ssidOrModelName.contains("Black")) {
            return StanmoreBlack;
        } else if (ssidOrModelName.contains("Stanmore") && ssidOrModelName.contains("Cream") ) {
            return StanmoreCream;
        } else if (ssidOrModelName.contains("Woburn") && ssidOrModelName.contains("Black")) {
            return WoburnBlack;
        } else if (ssidOrModelName.contains("Woburn") && ssidOrModelName.contains("Cream")) {
            return WoburnCream;
        }

        return Unknown;
    }
}
