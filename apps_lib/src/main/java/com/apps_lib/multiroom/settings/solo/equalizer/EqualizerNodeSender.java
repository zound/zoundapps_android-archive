package com.apps_lib.multiroom.settings.solo.equalizer;

import com.frontier_silicon.NetRemoteLib.Node.NodeInfo;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.components.common.RadioNodeUtil;

/**
 * Created by lsuhov on 20/07/16.
 */

public class EqualizerNodeSender {
    private boolean mIsSendingNode;
    private Radio mRadio;
    private NodeInfo mNode;

    public void sendNode(Radio radio, NodeInfo nodeInfo) {
        if (mIsSendingNode) {
            mNode = nodeInfo;
            this.mRadio = radio;
            return;
        } else {
            mNode = null;
            mRadio = null;
            mIsSendingNode = true;
        }

        RadioNodeUtil.setNodeToRadioAsync(radio, nodeInfo, new RadioNodeUtil.INodeSetResultListener() {
            @Override
            public void onNodeSetResult(boolean success) {
                mIsSendingNode = false;

                if (mNode != null) {
                    sendNode(mRadio, mNode);
                }
            }
        });
    }
}
