package com.apps_lib.multiroom.settings.solo.equalizer;

import com.frontier_silicon.NetRemoteLib.Node.NodeSysAudioEqCustomParam0;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysAudioEqCustomParam1;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;

/**
 * Created by lsuhov on 20/07/16.
 */

public class EqualizerNodesUtil {
    private static EqualizerNodeSender bassNodeSender = new EqualizerNodeSender();
    private static EqualizerNodeSender trebleNodeSender = new EqualizerNodeSender();

    public static void sendBassValue(Radio radio, long value) {

        NodeSysAudioEqCustomParam0 bassNode = new NodeSysAudioEqCustomParam0(value);
        bassNodeSender.sendNode(radio, bassNode);
    }

    public static void sendTrebleValue(Radio radio, long value) {
        NodeSysAudioEqCustomParam1 trebleNode = new NodeSysAudioEqCustomParam1(value);
        trebleNodeSender.sendNode(radio, trebleNode);
    }
}
