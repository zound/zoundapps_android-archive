package com.apps_lib.multiroom.settings.solo.equalizer;

import android.app.Activity;

import com.frontier_silicon.NetRemoteLib.Node.NodeInfo;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysAudioEqCustomParam0;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysAudioEqCustomParam1;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysCapsEqBands;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.components.common.BaseShowProgressDialogTask;
import com.frontier_silicon.components.common.DialogsHolder;
import com.frontier_silicon.components.common.RadioNodeUtil;
import com.frontier_silicon.components.common.nodeCommunication.IListNodeListener;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by lsuhov on 19/07/16.
 */

public class EqualizerNodesRetrievalTask extends BaseShowProgressDialogTask<Boolean> {

    private Radio mRadio;
    private IEqualizerNodesListener mListener;

    Map<Class, NodeInfo> mNodesMap = new HashMap<>();
    List<NodeSysCapsEqBands.ListItem> mEqBands;

    public EqualizerNodesRetrievalTask(Radio radio, Activity activity, IEqualizerNodesListener listener) {
        mRadio = radio;
        mContext = activity;
        mListener = listener;

        mDialogKey = "equalizer_retriever_dialog";
    }

    @Override
    protected void onPreExecute() {
        if (mContext != null && !DialogsHolder.isShowingOrActivityIsFinishing(mDialogKey, (Activity)mContext)) {
            initTimeoutTimer();
        }
    }

    @Override
    protected Boolean doInBackground(Integer... integers) {
        if (mRadio == null)
            return false;


        mNodesMap = mRadio.getNodesSyncGetter(new Class[]{NodeSysAudioEqCustomParam0.class, NodeSysAudioEqCustomParam1.class}).get();

        if (mRadio == null)
            return false;

        RadioNodeUtil.getListNodeItems(mRadio, NodeSysCapsEqBands.class, true, new IListNodeListener() {
            @Override
            public void onListNodeResult(List resultList, boolean isListComplete) {
                mEqBands = resultList;
            }
        });


        return true;
    }

    @Override
    protected void onPostExecute(Boolean result) {
        if (mListener != null) {
            mListener.onEqualizerNodesRetrieved(mNodesMap, mEqBands);
            mListener = null;
        }

        mRadio = null;
        mNodesMap = null;
        mEqBands = null;

        super.onPostExecute(result);
        dispose();
    }
}
