package com.apps_lib.multiroom.settings.solo.equalizer;

import com.frontier_silicon.NetRemoteLib.Node.NodeInfo;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysCapsEqBands;

import java.util.List;
import java.util.Map;

/**
 * Created by lsuhov on 19/07/16.
 */

interface IEqualizerNodesListener {
    void onEqualizerNodesRetrieved(Map<Class, NodeInfo> nodesMap, List<NodeSysCapsEqBands.ListItem> eqBands);
}
