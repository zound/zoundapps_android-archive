package com.apps_lib.multiroom.settings.solo.speaker;

/**
 * Created by lsuhov on 07/06/16.
 */
public enum SpeakerSettingType {
    About,
    Equalizer,
    ChangeName,
    TimeZone,
    GoogleCast,
    LedAdjuster,
    Sounds
}
