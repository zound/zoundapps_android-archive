package com.apps_lib.multiroom;

import com.apps_lib.multiroom.factory.EHomeFragmentTag;

/**
 * Created by lsuhov on 05/05/16.
 */
public interface IUEChildFragment {
    EHomeFragmentTag getStaticTag();
}
