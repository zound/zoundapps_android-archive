package com.apps_lib.multiroom.presets;

import com.frontier_silicon.NetRemoteLib.NetRemote;
import com.frontier_silicon.loggerlib.FsLogger;
import com.frontier_silicon.loggerlib.LogLevel;
import com.apps_lib.multiroom.presets.spotify.SpotifyBlobDecoder;

/**
 * Created by lsuhov on 03/06/16.
 */
public class BlobDecoderFactory {

    public static IBlobDecoder getBlobDecoder(String presetType) {
        if (presetType.contentEquals(PresetTypeNames.Spotify)) {
            return new SpotifyBlobDecoder();
        }

        FsLogger.log("No blob decoder found for " + presetType, LogLevel.Warning);
        return null;
    }
}
