package com.apps_lib.multiroom.presets;

/**
 * Created by lsuhov on 05/07/16.
 */

public interface IAddPresetDialogListener {
    void onAddPresetDialogResult(boolean addPreset);
}
