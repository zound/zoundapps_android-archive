package com.apps_lib.multiroom.presets;

/**
 * Created by nbalazs on 13/09/2016.
 */

public interface IPresetAnimationFinishedListener {

    void onPresetAnimationFinished();
}
