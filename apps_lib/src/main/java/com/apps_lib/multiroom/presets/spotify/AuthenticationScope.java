package com.apps_lib.multiroom.presets.spotify;

/**
 * Created by nbalazs on 24/11/2016.
 */
public enum AuthenticationScope {
    AUTHENTICATION_AT_THE_BEGINNING,
    AUTHENTICATION_AT_THE_END
}
