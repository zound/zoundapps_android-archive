package com.apps_lib.multiroom.presets;

import com.apps_lib.multiroom.presets.ir.BlobEncoder;

/**
 * Created by nbalazs on 04/10/2016.
 */
public class BlobEncoderFactory {

    public static IBlobEncoder getNewBlobEncoder(String presetType) {
        IBlobEncoder blobEncoder = null;
        if (presetType.contentEquals(PresetTypeNames.IR)) {
            blobEncoder =  new BlobEncoder();
        }

        return blobEncoder;
    }
}
