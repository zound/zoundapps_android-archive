package com.apps_lib.multiroom.presets.spotify;

import android.text.TextUtils;
import android.util.Base64;

import com.frontier_silicon.loggerlib.FsLogger;
import com.frontier_silicon.loggerlib.LogLevel;
import com.apps_lib.multiroom.presets.IBlobDecoder;
import com.apps_lib.multiroom.presets.PresetItemModel;
import com.apps_lib.multiroom.presets.PresetSubType;

/**
 * Created by lsuhov on 03/06/16.
 */
public class SpotifyBlobDecoder implements IBlobDecoder {

    @Override
    public void decodeBlob(PresetItemModel presetItemModel) {

        String encodedBlob = presetItemModel.blob;
        if (encodedBlob == null) {
            FsLogger.log("Spotify blob is null ", LogLevel.Error);
            return;
        }

        byte[] decodedBytes;

        try {
            decodedBytes = Base64.decode(encodedBlob, Base64.DEFAULT);
        } catch (IllegalArgumentException e) {
            FsLogger.log("Spotify blob not valid: " + presetItemModel.blob, LogLevel.Error);
            return;
        }

        if (decodedBytes.length < 2) {
            FsLogger.log("Spotify blob is corrupted. " + encodedBlob, LogLevel.Error);
            return;
        }

        int lengthOfUsefulData = decodedBytes[1];
        String rawString = null;
        try {
            rawString = new String(decodedBytes, 2, lengthOfUsefulData);
        } catch (Exception e) {
            FsLogger.log("Something went wrong on decoding spotify blob", LogLevel.Error);
            e.printStackTrace();
        }
        if (TextUtils.isEmpty(rawString)) {
            return;
        }
        String[] splitString = rawString.split(":");

        FsLogger.log(rawString, LogLevel.Info);

        presetItemModel.userId = getUserIdFromSplitBlob(splitString);
        presetItemModel.presetSubType.set(getSubType(splitString));

        if (presetItemModel.presetSubType.get() != null) {
            String presetSubTypeString = PresetSubType.stringFromPresetSubType(presetItemModel.presetSubType.get());
            presetItemModel.presetId = getSplitValueString(splitString, presetSubTypeString);
        }

        if (presetItemModel.presetSubType.get() != null && presetItemModel.presetSubType.get() == PresetSubType.SpotifyArtist) {
            presetItemModel.artistId = getArtistIdFromSplitBob(splitString);
        }
    }

    private PresetSubType getSubType(String[] splitBlob) {
        if (arrayContainsString(splitBlob, PresetSubType.SPOTIFY_PLAYLIST_SUBTYPE)) {
            return PresetSubType.presetSubTypeFromString(PresetSubType.SPOTIFY_PLAYLIST_SUBTYPE);
        } else if (arrayContainsString(splitBlob, PresetSubType.SPOTIFY_ALBUM_SUBTYPE)) {
            return PresetSubType.presetSubTypeFromString(PresetSubType.SPOTIFY_ALBUM_SUBTYPE);
        } else if (arrayContainsString(splitBlob, PresetSubType.SPOTIFY_ARTIST_SUBTYPE)) {
            return PresetSubType.presetSubTypeFromString(PresetSubType.SPOTIFY_ARTIST_SUBTYPE);
        }

        return null;
    }

    private String getUserIdFromSplitBlob(String[] splitBlob) {

        return  getSplitValueString(splitBlob, "user");
    }

    private String getArtistIdFromSplitBob(String [] splitBlob) {

        return getSplitValueString(splitBlob, "artist");
    }

    private String getSplitValueString(String[] splitBlob, String part) {
        String splitString = "";
        for (int i = 0; i < splitBlob.length; i++) {
            if (splitBlob[i].contentEquals(part)) {
                splitString = splitBlob[i + 1];

                return splitString;
            }
        }
        return "";
    }

    private boolean arrayContainsString(String[] splitBlob, String part) {
        for (int i = 0; i < splitBlob.length; i++) {
            if (splitBlob[i].contentEquals(part)) {
                return true;
            }
        }
        return false;
    }
}
