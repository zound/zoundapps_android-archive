package com.apps_lib.multiroom.presets;

import android.app.Activity;
import android.os.AsyncTask;

import com.frontier_silicon.NetRemoteLib.Node.NodeInfo;
import com.frontier_silicon.NetRemoteLib.Node.NodeNavActionSelectPreset;
import com.frontier_silicon.NetRemoteLib.Node.NodeNavPresetCurrentPreset;
import com.frontier_silicon.NetRemoteLib.Node.NodeNavPresetDelete;
import com.frontier_silicon.NetRemoteLib.Node.NodeNavStatus;
import com.frontier_silicon.NetRemoteLib.Node.NodePlayAddPreset;
import com.frontier_silicon.NetRemoteLib.Node.NodePlayAddPresetStatus;
import com.frontier_silicon.NetRemoteLib.Node.NodePlayCaps;
import com.frontier_silicon.NetRemoteLib.Radio.IGetNodeCallback;
import com.frontier_silicon.NetRemoteLib.Radio.NodeErrorResponse;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.components.common.RadioNavigationUtil;
import com.frontier_silicon.components.common.RadioNodeUtil;
import com.frontier_silicon.components.common.TaskHelper;
import com.frontier_silicon.loggerlib.FsLogger;
import com.frontier_silicon.loggerlib.LogLevel;
import com.apps_lib.multiroom.presets.ir.IRPresetsProvider;
import com.apps_lib.multiroom.presets.spotify.SpotifyManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lsuhov on 20/05/16.
 */
public class PresetsManager {

    private static PresetsManager mInstance;

    private PresetsManager() {}

    private PresetsUploaderRunnable mPresetsUploaderRunnable;
    private PresetsDownloaderTask mPresetsDownloaderTask;
    private Thread mUploadPresetsThread;

    public ESetupPresetsType setupPresetsType;
    public List<PresetItemModel> setupPresetItemModels = new ArrayList<>();
    private int mNumberOfSpotifyPresetsInMixedMode = 4;
    private int mNumberOfIRPresetsInMixedMode = 3;

    private int mCurrentTabIndex = 0;

    public static PresetsManager getInstance() {
        if (mInstance == null) {
            mInstance = new PresetsManager();
        }
        return mInstance;
    }

    public void retrievePresetsForUpload(IPresetsRetrieverListener listener, Activity activity) {
        setupPresetItemModels.clear();

        if (setupPresetsType == ESetupPresetsType.Spotify || setupPresetsType == ESetupPresetsType.Mixed) {
            retrieveSpotifyPresetsForUpload(listener, activity);
        } else if (setupPresetsType == ESetupPresetsType.IR) {
            retrieveIRPresetsForUpload(listener, activity);
        }
    }

    private void retrieveSpotifyPresetsForUpload(final IPresetsRetrieverListener listener, final Activity activity) {
        SpotifyManager.getInstance().getSpotifyRecommendedPlaylistsAsync(new IPresetsRetrieverListener() {
            @Override
            public void onPresetsRetrieved(List<PresetItemModel> presets) {
                if (setupPresetsType == ESetupPresetsType.Spotify) {
                    setupPresetItemModels = presets;

                    listener.onPresetsRetrieved(setupPresetItemModels);
                } else if (setupPresetsType == ESetupPresetsType.Mixed) {
                    if (presets.size() >= mNumberOfSpotifyPresetsInMixedMode) {
                        for (int i = 0; i < mNumberOfSpotifyPresetsInMixedMode; i++) {
                            setupPresetItemModels.add(presets.get(i));
                        }
                    }

                    retrieveIRPresetsForUpload(listener, activity);
                }
            }
        });
    }

    private void retrieveIRPresetsForUpload(final IPresetsRetrieverListener listener, Activity activity) {
        IRPresetsProvider.getInstance().retrieveIRPresetListForUpload(new IPresetsRetrieverListener() {
            @Override
            public void onPresetsRetrieved(List<PresetItemModel> presets) {
                if (setupPresetsType == ESetupPresetsType.IR) {
                    setupPresetItemModels = presets;
                } else if (setupPresetsType == ESetupPresetsType.Mixed) {
                    if (presets.size() >= mNumberOfIRPresetsInMixedMode) {
                        for (int i = 0; i < mNumberOfIRPresetsInMixedMode; i++) {
                            setupPresetItemModels.add(presets.get(i));
                            presets.get(i).presetIndex = i + mNumberOfSpotifyPresetsInMixedMode;
                        }
                    }
                }

                listener.onPresetsRetrieved(setupPresetItemModels);
            }
        }, activity);
    }

    public void uploadPresetToRadio(List<PresetItemModel> presetItemModelList, Radio radio, IPresetsUploadListener listener) {
        if (mUploadPresetsThread == null || !mUploadPresetsThread.isAlive() || mUploadPresetsThread.isInterrupted()) {
            mPresetsUploaderRunnable = new PresetsUploaderRunnable(presetItemModelList, radio, listener);

            mUploadPresetsThread = new Thread(mPresetsUploaderRunnable);
            mUploadPresetsThread.start();
        } else {
            mPresetsUploaderRunnable.setListener(listener);
        }
    }

    public void getPresetsFromRadio(List<PresetItemModel> presetItemModelList, Radio radio, IPresetsUpdateListener listener) {
        if (mPresetsDownloaderTask == null || mPresetsDownloaderTask.getStatus() == AsyncTask.Status.FINISHED ||
                mPresetsDownloaderTask.getRadio() != radio) {
            mPresetsDownloaderTask = new PresetsDownloaderTask(presetItemModelList, radio, listener);
            TaskHelper.execute(mPresetsDownloaderTask);
        } else {
            mPresetsDownloaderTask.setListener(listener);
        }
    }

    public void getIndexOfCurrentlyPlayingPreset(Radio radio, boolean sync, final RadioNodeUtil.INodeResultListener listener) {
        if (radio != null) {

            radio.getNode(NodeNavPresetCurrentPreset.class, sync, false, new IGetNodeCallback() {
                @Override
                public void getNodeResult(NodeInfo node) {
                    listener.onNodeResult(node);
                }

                @Override
                public void getNodeError(Class nodeType, NodeErrorResponse error) {
                    listener.onNodeResult(null);
                }
            });
        } else {
            FsLogger.log("Selected radio was null when trying to retrieve the currently playing preset index", LogLevel.Error);

            if (listener != null) {
                listener.onNodeResult(null);
            }
        }
    }

    public void selectPresetAsync(final int presetIndex, final Radio radio, final RadioNodeUtil.INodeSetResultListener listener) {
        if (radio != null) {
            RadioNavigationUtil.enableNavigationAsync(radio, new RadioNavigationUtil.IEnableRadioRequestStatusListener() {
                @Override
                public void onEnableFinished(NodeNavStatus nodeNavStatus) {
                    RadioNodeUtil.setNodeToRadioAsync(radio, new NodeNavActionSelectPreset((long)presetIndex), listener);
                }
            });
        } else {
            respondToSetNodeListenerWithFalse(listener);
        }
    }

    public void deletePreset(int presetIndex, Radio radio, RadioNodeUtil.INodeSetResultListener listener) {
        if (radio != null) {
            RadioNodeUtil.setNodeToRadioAsync(radio, new NodeNavPresetDelete((long)presetIndex), listener);
        } else {
            respondToSetNodeListenerWithFalse(listener);
        }
    }

    public void addCurrentlyPlayingSourceToPreset(final int presetIndex, final Radio radio, final RadioNodeUtil.INodeSetResultListener listener) {
        if (radio != null) {
            checkIfCurrentModeIsAllowingAddToPreset(radio, new IAddToPresetSupportedListener() {
                @Override
                public void onAddToListenerSupported(boolean isSupported) {
                    if (isSupported) {
                        RadioNodeUtil.setNodeToRadioAsync(radio, new NodePlayAddPreset((long) presetIndex), new RadioNodeUtil.INodeSetResultListener() {
                            @Override
                            public void onNodeSetResult(boolean success) {
                                if (!success) {
                                    respondToSetNodeListenerWithFalse(listener);
                                }
                            }
                        });
                    } else {
                        respondToSetNodeListenerWithFalse(listener);
                    }
                }
            });
        } else {
            respondToSetNodeListenerWithFalse(listener);
        }
    }

    private void checkIfCurrentModeIsAllowingAddToPreset(Radio radio, final IAddToPresetSupportedListener listener) {
        if (radio != null) {
            RadioNodeUtil.getNodeFromRadioAsync(radio, NodePlayCaps.class, new RadioNodeUtil.INodeResultListener() {
                @Override
                public void onNodeResult(NodeInfo node) {
                    NodePlayCaps playCaps = (NodePlayCaps) node;

                    boolean addPresetsSupported = playCaps != null && playCaps.DoesSupport(NodePlayCaps.Operation.AddPreset);

                    if (listener != null) {
                        listener.onAddToListenerSupported(addPresetsSupported);
                    }
                }
            });
        } else {
            if (listener != null) {
                listener.onAddToListenerSupported(false);
            }
        }
    }

    private void respondToSetNodeListenerWithFalse(RadioNodeUtil.INodeSetResultListener listener) {
        if (listener != null) {
            listener.onNodeSetResult(false);
        }
    }

    public int getCurrentTabIndex() {
        return mCurrentTabIndex;
    }

    public void setCurrentTabIndex(int mCurrentPresetIndex) {
        this.mCurrentTabIndex = mCurrentPresetIndex;
    }
}
