package com.apps_lib.multiroom.presets.spotify;

/**
 * Created by lsuhov on 01/06/16.
 */
public interface ISpotifyRefreshAuthorizationListener {
    void onAccessTokenRefreshed(boolean result);
}
