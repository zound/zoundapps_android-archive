package com.apps_lib.multiroom.presets.ir;

import android.util.Base64;

import com.apps_lib.multiroom.presets.IBlobEncoder;

import java.io.UnsupportedEncodingException;

/**
 * Created by nbalazs on 04/10/2016.
 */

public class BlobEncoder implements IBlobEncoder {

    public BlobEncoder() {};

    @Override
    public String encodeToString(byte[] dataToEncode) {
        return Base64.encodeToString(dataToEncode, Base64.NO_WRAP);
    }

    @Override
    public String encodeToString(String stringToEncode) {
        byte[] data = new byte[0];
        try {
            stringToEncode = stringToEncode.concat("\0");
            data = stringToEncode.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return encodeToString(data);
    }
}
