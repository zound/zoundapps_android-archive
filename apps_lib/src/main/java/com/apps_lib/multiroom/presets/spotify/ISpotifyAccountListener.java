package com.apps_lib.multiroom.presets.spotify;

/**
 * Created by lsuhov on 01/06/16.
 */
public interface ISpotifyAccountListener {
    void onUserAccountRetrieved(boolean result, boolean hasUserPremiumAccount);
}
