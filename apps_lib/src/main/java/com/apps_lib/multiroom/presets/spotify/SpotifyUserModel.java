package com.apps_lib.multiroom.presets.spotify;

import android.text.TextUtils;

import com.frontier_silicon.NetRemoteLib.NetRemote;
import com.frontier_silicon.loggerlib.FsLogger;
import com.frontier_silicon.loggerlib.LogLevel;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by lsuhov on 02/06/16.
 */
public class SpotifyUserModel {
    public enum ProductType {
        Premium,
        Free,
        Open
    }

    public String displayName;
    public String userId;
    public ProductType productType;

    public String getAccountName() {
        if (!TextUtils.isEmpty(displayName)) {
            return displayName;
        } else {
            return userId;
        }
    }

    public boolean hasUserPremiumAccount() {
        if (productType != null) {
            if (productType.equals(ProductType.Premium)) {
                return true;
            }
        }

        return false;
    }

    public boolean parseUserResponse(String response) {
        try {
            JSONObject jsonResp = new JSONObject(response);

            userId = jsonResp.getString("id");

            displayName = jsonResp.getString("display_name");
            displayName = displayName.contentEquals("null") ? null : displayName;

            String productTypeString = jsonResp.getString("product");
            if (!TextUtils.isEmpty(productTypeString)) {
                if (productTypeString.contentEquals("premium")) {
                    productType = ProductType.Premium;
                } else if (productTypeString.contentEquals("free")) {
                    productType = ProductType.Free;
                } else if (productTypeString.contentEquals("open")) {
                    productType = ProductType.Open;
                }
            }

            return true;
        } catch (JSONException e) {
            FsLogger.log(e.getMessage(), LogLevel.Error);
            e.printStackTrace();
            return false;
        }
    }
}
