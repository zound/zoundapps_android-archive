package com.apps_lib.multiroom.presets;

/**
 * Created by lsuhov on 24/05/16.
 */
public interface IAddToPresetSupportedListener {
    void onAddToListenerSupported(boolean isSupported);
}
