package com.apps_lib.multiroom.presets;

import android.content.Context;

import com.apps_lib.multiroom.R;


/**
 * Created by lsuhov on 03/06/16.
 */
public enum PresetSubType {
    SpotifyPlaylist,
    SpotifyAlbum,
    SpotifyArtist,
    InternetRadio,
    Empty;
    public static final String SPOTIFY_PLAYLIST_SUBTYPE = "playlist";
    public static final String SPOTIFY_ALBUM_SUBTYPE = "album";
    public static final String SPOTIFY_ARTIST_SUBTYPE = "artist";

    public static PresetSubType presetSubTypeFromString(String string) {
        if (string.contentEquals(SPOTIFY_PLAYLIST_SUBTYPE)) {
            return PresetSubType.SpotifyPlaylist;
        } else if (string.contentEquals(SPOTIFY_ALBUM_SUBTYPE)) {
            return PresetSubType.SpotifyAlbum;
        } else if (string.contentEquals(SPOTIFY_ARTIST_SUBTYPE)) {
            return PresetSubType.SpotifyArtist;
        }

        return null;
    }

    public static String stringFromPresetSubType(PresetSubType presetSubType) {
        if (presetSubType == SpotifyPlaylist) {
            return SPOTIFY_PLAYLIST_SUBTYPE;
        } else if (presetSubType == SpotifyAlbum) {
            return SPOTIFY_ALBUM_SUBTYPE;
        } else if (presetSubType == SpotifyArtist) {
            return SPOTIFY_ARTIST_SUBTYPE;
        }

        return null;
    }

    public static String getFriendlyNameFromSubType(PresetSubType presetSubType, Context context) {
        if (presetSubType == null) {
            return "";
        }

        int stringId = -1;
        switch (presetSubType) {
            case SpotifyPlaylist:
                stringId = R.string.setup_spotify_playlist;
                break;
            case SpotifyAlbum:
                stringId = R.string.setup_spotify_album;
                break;
            case SpotifyArtist:
                stringId = R.string.spotify_artist;
                break;
            case InternetRadio:
                stringId = R.string.setup_internet_radio;
                break;
            case Empty:
                stringId = R.string.empty_string;
                break;
        }

        return context.getString(stringId);
    }
}
