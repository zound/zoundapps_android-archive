package com.apps_lib.multiroom.presets;

import java.util.List;

/**
 * Created by lsuhov on 19/05/16.
 */
public interface IPresetsRetrieverListener {
    void onPresetsRetrieved(List<PresetItemModel> presets);
}
