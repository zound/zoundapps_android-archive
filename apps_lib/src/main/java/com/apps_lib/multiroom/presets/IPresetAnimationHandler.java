package com.apps_lib.multiroom.presets;

/**
 * Created by nbalazs on 12/09/2016.
 */

public interface IPresetAnimationHandler {

    void startAnimationForPresetPlaylistDelete(IPresetAnimationFinishedListener presetAnimationFinishedListener);

    void startAnimationForPresetRadioStationDelete(IPresetAnimationFinishedListener presetAnimationFinishedListener);

    void startAnimationForPresetPlaylistAdded(IPresetAnimationFinishedListener presetAnimationFinishedListener, boolean succeeded);

    void startAnimationForPresetRadioStationAdded(IPresetAnimationFinishedListener presetAnimationFinishedListener, boolean succeeded);

    void startAnimationForPlayingEmptyPreset(IPresetAnimationFinishedListener presetAnimationFinishedListener);

    void startAnimationForUndo(IPresetAnimationFinishedListener presetAnimationFinishedListener, boolean succeeded);

    void startAnimationForRedo(IPresetAnimationFinishedListener presetAnimationFinishedListener, boolean succeeded);

    void showUndoButton();

    void hideUndoButton();

    void showRedoButton();

    void hideRedoButton();
}
