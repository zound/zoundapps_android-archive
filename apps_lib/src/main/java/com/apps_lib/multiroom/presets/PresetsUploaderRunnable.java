package com.apps_lib.multiroom.presets;

import com.frontier_silicon.NetRemoteLib.Node.NodeInfo;
import com.frontier_silicon.NetRemoteLib.Node.NodeNavPresetUploadArtworkUrl;
import com.frontier_silicon.NetRemoteLib.Node.NodeNavPresetUploadBlob;
import com.frontier_silicon.NetRemoteLib.Node.NodeNavPresetUploadName;
import com.frontier_silicon.NetRemoteLib.Node.NodeNavPresetUploadType;
import com.frontier_silicon.NetRemoteLib.Node.NodeNavPresetUploadUpload;
import com.frontier_silicon.NetRemoteLib.Radio.ErrorResponse;
import com.frontier_silicon.NetRemoteLib.Radio.ISetNodesCallback;
import com.frontier_silicon.NetRemoteLib.Radio.NodeResponse;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.components.common.RadioNavigationUtil;
import com.frontier_silicon.loggerlib.FsLogger;
import com.frontier_silicon.loggerlib.LogLevel;

import java.util.List;
import java.util.Map;

/**
 * Created by lsuhov on 20/05/16.
 */
class PresetsUploaderRunnable implements Runnable {
    private List<PresetItemModel> mPresetItemModels;
    private Radio mRadio;
    private IPresetsUploadListener mListener;
    private boolean mSetNodeSuccess;
    private boolean mPreviousPreset = true;
    private boolean mCurrentPreset = true;

    PresetsUploaderRunnable(List<PresetItemModel> presetItemModelList, Radio radio,
                            IPresetsUploadListener listener) {
        mPresetItemModels = presetItemModelList;
        mRadio = radio;
        mListener = listener;
    }

    public synchronized void setListener(IPresetsUploadListener listener) {
        mListener = listener;
    }

    @Override
    public void run() {
        int size = mPresetItemModels.size();

        for (int i = 0; i < size; i++) {
            if (mRadio != null ) {
                RadioNavigationUtil.resetNavigationToRootIfNeeded(mRadio);
                if (mPresetItemModels != null) {
                    notifyAndUploadPreset(mPresetItemModels.get(i), i);
                }

            } else {
                if (mListener != null) {
                    mListener.onComplete(mPresetItemModels, false);
                    disposeAll();
                }
            }
        }

            if (mListener != null) {
                mListener.onComplete(mPresetItemModels, true);
                disposeAll();
            }
        }

    private void notifyAndUploadPreset(PresetItemModel presetItemModel, int position) {
        synchronized (this) {
            if (mListener != null) {
                mListener.onUploadingPreset(presetItemModel, position);
            }
        }

        NodeNavPresetUploadBlob nodeUploadBlob = new NodeNavPresetUploadBlob(presetItemModel.blob);
        NodeNavPresetUploadName nodeNavPresetUploadName = new NodeNavPresetUploadName(presetItemModel.presetName.get());
        NodeNavPresetUploadType nodeNavPresetUploadType = new NodeNavPresetUploadType(presetItemModel.presetType.get());
        NodeNavPresetUploadArtworkUrl nodeNavPresetUploadArtworkUrl = new NodeNavPresetUploadArtworkUrl(presetItemModel.artworkUrl.get());
        NodeNavPresetUploadUpload nodeNavPresetUploadUpload = new NodeNavPresetUploadUpload((long) position);

        NodeInfo[] nodes = new NodeInfo[]{nodeUploadBlob, nodeNavPresetUploadName,
                nodeNavPresetUploadType, nodeNavPresetUploadArtworkUrl, nodeNavPresetUploadUpload};

        setNodesInfo(mRadio, nodes, true);

    }

    public void setNodesInfo(Radio radio, NodeInfo[] nodes, boolean blocking) {
        mSetNodeSuccess = false;
        try {
            radio.setNodes(nodes, blocking, new ISetNodesCallback() {
                @Override
                public void onResult(Map<Class, NodeResponse> nodeResponses) {
                    mSetNodeSuccess = true;

                    for (NodeResponse nodeResp : nodeResponses.values()) {
                        if (nodeResp.mType == NodeResponse.NodeResponseType.NODE) {
                            FsLogger.log("SetNodesSuccess: " + nodeResp.mNodeInfo.getName() + " value= " + nodeResp.mNodeInfo.toString());
                        } else if (nodeResp.mError.getErrorCode() == ErrorResponse.ErrorCode.NetworkProblem ||
                                nodeResp.mError.getErrorCode() == ErrorResponse.ErrorCode.Error ||
                                nodeResp.mError.getErrorCode() == ErrorResponse.ErrorCode.NetworkTimeout) {

                            FsLogger.log("SetNodesError: " + nodeResp.mError.toString(), LogLevel.Error);
                            mSetNodeSuccess = false;
                        }
                    }

                    mCurrentPreset = mSetNodeSuccess;

                    if (!mPreviousPreset && !mCurrentPreset && mListener != null) {
                        mListener.onComplete(mPresetItemModels, false);
                        disposeAll();
                    }

                    mPreviousPreset = mCurrentPreset;
                }
            });
        } catch (NullPointerException e) {
            FsLogger.log("setNodesInfo: " + e, LogLevel.Error);
        }

    }

    private void disposeAll() {
        mListener = null;
        mRadio = null;
        mPresetItemModels = null;
    }
}
