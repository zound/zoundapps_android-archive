package com.apps_lib.multiroom.presets.ir;

import android.app.Activity;

import com.frontier_silicon.components.common.TaskHelper;
import com.apps_lib.multiroom.presets.IPresetsRetrieverListener;
import com.apps_lib.multiroom.presets.PresetItemModel;
import com.apps_lib.multiroom.setup.presets.DefaultIRPresetListCreatorTask;

import java.util.List;

/**
 * Created by lsuhov on 29/07/16.
 */

public class IRPresetsProvider {

    private static IRPresetsProvider mInstance;

    public static IRPresetsProvider getInstance() {
        if (mInstance == null) {
            mInstance = new IRPresetsProvider();
        }
        return mInstance;
    }

    private IRPresetsProvider() {}

    public void retrieveIRPresetListForUpload(final IPresetsRetrieverListener listener, Activity activity) {

        DefaultIRPresetListCreatorTask task = new DefaultIRPresetListCreatorTask(activity, new DefaultIRPresetListCreatorTask.IPresetListReadyCallback() {
            @Override
            public void onIRPresetListReady(List<PresetItemModel> presetItemModelList) {
                listener.onPresetsRetrieved(presetItemModelList);
            }

            @Override
            public void onIRPresetReady(PresetItemModel presetItemModel) {
                // Maybe for a better workaround / UI effects
            }
        }, true);

        TaskHelper.execute(task);
    }
}
