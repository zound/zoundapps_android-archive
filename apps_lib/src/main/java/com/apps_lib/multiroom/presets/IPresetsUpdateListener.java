package com.apps_lib.multiroom.presets;

/**
 * Created by lsuhov on 22/05/16.
 */
public interface IPresetsUpdateListener {
    void onComplete(boolean result);
}
