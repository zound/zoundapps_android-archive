package com.apps_lib.multiroom.presets;

/**
 * Created by lsuhov on 03/06/16.
 */
public interface IPresetTypeManager {
    void retrieveArtworkUrlAsync(PresetItemModel presetItemModel, IPresetArtworkUrlListener listener);
}
