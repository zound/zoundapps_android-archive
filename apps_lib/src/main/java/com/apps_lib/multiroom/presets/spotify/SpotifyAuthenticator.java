package com.apps_lib.multiroom.presets.spotify;

import android.app.Activity;
import android.content.Intent;
import android.text.TextUtils;

import com.frontier_silicon.NetRemoteLib.NetRemote;
import com.frontier_silicon.components.common.CommonPreferences;
import com.spotify.sdk.android.authentication.AuthenticationClient;
import com.spotify.sdk.android.authentication.AuthenticationRequest;
import com.spotify.sdk.android.authentication.AuthenticationResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by lsuhov on 01/06/16.
 */
public class SpotifyAuthenticator {
    private static final int ACTIVITY_REQUEST_RESULT_CODE = 4242;

    private String mRedirectURL;
    private String mClientID;
    private String mServerEndpoint;

    private String mAccessToken;
    private String mTokenType;
    private String mRefreshToken;
    private long mExpirationTime;

    private enum RefreshType {
        Swap,
        Refresh
    }

    /**
     * This method must be called in order to initialise the spotify Redirect URL, Client ID and Server Endpoint
     */
    public void init(String redirectURL, String clientID, String serverEndpoint) {

        mRedirectURL = redirectURL;
        mClientID = clientID;
        mServerEndpoint = serverEndpoint;

        mAccessToken = CommonPreferences.getInstance().getSpotifyAccessToken();
        mRefreshToken = CommonPreferences.getInstance().getSpotifyRefreshToken();
        mExpirationTime = CommonPreferences.getInstance().getSpotifyTokenExpirationTime();
        mTokenType = CommonPreferences.getInstance().getSpotifyTokenType();
    }

    public void loginSpotify(Activity activity) {
        AuthenticationRequest.Builder builder = new AuthenticationRequest.Builder(mClientID,
                AuthenticationResponse.Type.CODE, mRedirectURL);
        builder.setScopes(new String[]{"streaming", "user-read-private"});
        AuthenticationRequest request = builder.build();

        AuthenticationClient.openLoginActivity(activity, ACTIVITY_REQUEST_RESULT_CODE, request);

    }

    public void onSpotifyAuthenticationResponseReceived(int requestCode, int resultCode, Intent intent, ISpotifyRefreshAuthorizationListener listener) {
        if (requestCode == ACTIVITY_REQUEST_RESULT_CODE) {
            AuthenticationResponse response = AuthenticationClient.getResponse(resultCode, intent);
            switch (response.getType()) {
                case CODE:
                    swapAccessToken(response.getCode(), listener);
                    break;

                default:
                    listener.onAccessTokenRefreshed(false);
                    break;
            }
        }
    }

    public boolean isSpotifyAccountExpired() {
        return System.currentTimeMillis() > mExpirationTime;
    }

    public void refreshToken(ISpotifyRefreshAuthorizationListener listener) {
        requestRefreshToken(listener);
    }

    private void swapAccessToken(String code, final ISpotifyRefreshAuthorizationListener listener) {

        doRequest(RefreshType.Swap, code, listener);
    }

    void getAccessTokenAsync(final IAccessTokenListener listener) {
        refreshTokenIfNeeded(new ISpotifyRefreshAuthorizationListener() {

            @Override
            public void onAccessTokenRefreshed(boolean result) {
                listener.onAccessTokenRetrieved(mTokenType + " " + mAccessToken);
            }
        });
    }

    public boolean isSpotifyLinked() {
        return !TextUtils.isEmpty(mRefreshToken);
    }

    private void parseRefreshTokenResponse(String jsonResponse) {
        try {
            JSONObject jsonObject = new JSONObject(jsonResponse);

            String accessToken = jsonObject.getString("access_token");
            mAccessToken = accessToken;
            CommonPreferences.getInstance().setSpotifyAccessToken(accessToken);

            String tokenType = jsonObject.getString("token_type");
            mTokenType = tokenType;
            CommonPreferences.getInstance().setSpotifyTokenType(tokenType);

            if (jsonObject.has("refresh_token")) {
                String refreshToken = jsonObject.getString("refresh_token");
                mRefreshToken = refreshToken;
                CommonPreferences.getInstance().setSpotifyRefreshToken(refreshToken);
            }

            int expirationTime = jsonObject.getInt("expires_in");
            //removing eventual network delay
            expirationTime -= 30;
            long currentTime = System.currentTimeMillis();

            mExpirationTime = currentTime + expirationTime * 1000;
            CommonPreferences.getInstance().setSpotifyTokenExpirationTime(mExpirationTime);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void refreshTokenIfNeeded(ISpotifyRefreshAuthorizationListener listener) {
        long currentTime = System.currentTimeMillis();
        if (currentTime > mExpirationTime) {
            requestRefreshToken(listener);
        } else {
            listener.onAccessTokenRefreshed(true);
        }
    }

    private void requestRefreshToken(ISpotifyRefreshAuthorizationListener listener) {
        doRequest(RefreshType.Refresh, mRefreshToken, listener);
    }

    private void doRequest(RefreshType refreshType, String paramValue, final ISpotifyRefreshAuthorizationListener listener) {
        String url = getRefreshUrl(refreshType);
        RequestBody requestBody = getParamDependantToRefreshType(refreshType, paramValue);

        Request request = new Request.Builder()
                .url(url)
                .post(requestBody)
                .build();

        NetRemote.getOkHttpClient().newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                NetRemote.getMainThreadExecutor().executeOnMainThread(new Runnable() {
                    @Override
                    public void run() {
                        if (listener != null) {
                            listener.onAccessTokenRefreshed(false);
                        }
                    }
                }, true);
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                final String result = response.isSuccessful() ? response.body().string() : "";
                response.close();

                NetRemote.getMainThreadExecutor().executeOnMainThread(new Runnable() {
                    @Override
                    public void run() {

                        if (response.isSuccessful()) {
                            parseRefreshTokenResponse(result);

                            if (listener != null) {
                                listener.onAccessTokenRefreshed(true);
                            }
                        } else {
                            if (listener != null) {
                                listener.onAccessTokenRefreshed(false);
                            }
                        }
                    }
                }, true);
            }
        });
    }

    private RequestBody getParamDependantToRefreshType(RefreshType refreshType, String paramValue) {
        FormBody.Builder formBody = new FormBody.Builder();
        switch (refreshType) {
            case Swap:
                formBody.add("code", paramValue);
                break;
            case Refresh:
                formBody.add("refresh_token", paramValue);
                break;
        }

        return formBody.build();
    }

    private String getRefreshUrl(RefreshType refreshType) {
        switch (refreshType) {
            case Swap:
                return mServerEndpoint + "/swap";
            case Refresh:
                return mServerEndpoint + "/refresh";
            default:
                return null;
        }
    }

    String getAccessToken() {
        return mAccessToken;
    }

    public void dispose() {
        mAccessToken = null;
        mRefreshToken = null;
        mExpirationTime = 0;
        mTokenType = null;
        CommonPreferences.getInstance().setSpotifyAccessToken("");
        CommonPreferences.getInstance().setSpotifyTokenType("");
        CommonPreferences.getInstance().setSpotifyRefreshToken("");
    }
}
