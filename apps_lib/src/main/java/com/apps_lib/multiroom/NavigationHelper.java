package com.apps_lib.multiroom;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import com.apps_lib.multiroom.factory.ActivityFactory;
import com.apps_lib.multiroom.factory.EHomeFragmentTag;
import com.apps_lib.multiroom.myHome.IFragmentSwitcher;
import com.apps_lib.multiroom.myHome.SpeakersDiscoveryManager;
import com.apps_lib.multiroom.setup.normalSetup.SetupManager;
import com.apps_lib.multiroom.util.ScreenshootManager;
import com.apps_lib.multiroom.webview.EmbeddedWebViewActivity;

/**
 * Created by lsuhov on 10/04/16.
 */
public class NavigationHelper {

    public enum AnimationType {
        Normal,
        /**
         * Right to left
         */
        SlideToLeft,
        /**
         * Left to right
         */
        SlideToRight,
        /**
         * From down to up
         */
        SlideUp,
        /**
         * From up to down
         */
        SlideDown
    }

    public static final String CONNECTION_LOST_ARG = "ConnectionLost";
    public static final String RADIO_UDN_ARG = "radioUDN";
    public static final String EXTERNAL_SPOTIFY_LINKING = "EXTERNAL_SPOTIFY_LINKING";

    public static void goToHome(Activity activity) {
        if (activity == null || activity.getClass() == ActivityFactory.getActivityFactory().getHomeActivity())
            return;

        Intent mainActivityIntent = new Intent(activity, ActivityFactory.getActivityFactory().getHomeActivity());

        activity.startActivity(mainActivityIntent);
        activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
        //activity.finish();
    }


    public static void goToHomeAndClearActivityStack(Activity activity) {
        if (activity == null || activity.getClass() == ActivityFactory.getActivityFactory().getHomeActivity())
            return;

        Intent mainActivityIntent = new Intent(activity, ActivityFactory.getActivityFactory().getHomeActivity());
        mainActivityIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        activity.startActivity(mainActivityIntent);
        activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }

    public static void goToActivity(Activity currentActivity, Class<?> cls, AnimationType animationType) {
        goToActivity(currentActivity, cls, animationType, false, null);
    }

    public static void goToActivity(Activity currentActivity, Class<?> cls, AnimationType animationType,
                                    boolean forgetCurrentActivity) {
        goToActivity(currentActivity, cls, animationType, forgetCurrentActivity, null);
    }

    public static void goToActivity(Activity currentActivity, Class<?> cls, AnimationType animationType,
                                    boolean forgetCurrentActivity, Bundle bundle) {
        if (currentActivity == null || currentActivity.getClass() == cls) {
            return;
        }

        Intent mainActivityIntent = new Intent(currentActivity, cls);
        if (bundle != null) {
            mainActivityIntent.putExtras(bundle);
        }
        currentActivity.startActivity(mainActivityIntent);

        setTransition(currentActivity, animationType);

        if (forgetCurrentActivity) {
            currentActivity.finish();
        }
    }

    public static void goToActivityForResult(Activity currentActivity, Class<?> cls, AnimationType animationType,
                                    Bundle bundle, int requestCode) {
        if (currentActivity == null || currentActivity.getClass() == cls) {
            return;
        }

        Intent mainActivityIntent = new Intent(currentActivity, cls);
        if (bundle != null) {
            mainActivityIntent.putExtras(bundle);
        }

        setTransition(currentActivity, animationType);

        currentActivity.startActivityForResult(mainActivityIntent, requestCode);
    }

    public static void goToActivityClearingCurrentStack(Activity currentActivity, Class<?> cls, AnimationType animationType) {
        Intent intent = new Intent(currentActivity, cls);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        setTransition(currentActivity, animationType);

        currentActivity.startActivity(intent);
    }

    public static void goToFragment(Activity currentActivity, EHomeFragmentTag fragmentTag) {
        if (currentActivity != null) {
            ((IFragmentSwitcher)currentActivity).switchFragment(fragmentTag);
        }
    }

    public static void startSetupActivity(Activity activity) {
        SetupManager.getInstance().saveCurrentWiFiNetworkId();

        goToActivity(activity, ActivityFactory.getActivityFactory().getSetupActivity(), NavigationHelper.AnimationType.SlideToLeft);
    }

    public static void goToHomeAndReconnectToInitialWiFi(Activity activity) {
        SpeakersDiscoveryManager.getInstance().tryStartDiscovery();
        SetupManager.getInstance().cleanNetRemote();
        SetupManager.getInstance().tryReconnectToInitialWiFiNetwork();

        goToHomeAndClearActivityStack(activity);
    }

    public static void goToVolumeActivity(Activity activity) {
        ScreenshootManager.prepareScreenshootBackground(activity);
        goToActivity(activity, ActivityFactory.getActivityFactory().getVolumeActivity(), AnimationType.SlideDown);
    }

    public static void openLink(Activity currentActivity, String url, AnimationType animationType) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        currentActivity.startActivity(browserIntent);

        setTransition(currentActivity, animationType);
    }

    public static void openWebViewActivityForURL(Activity currentActivity, AnimationType animationType, String url) {
        Intent intent = new Intent(currentActivity, EmbeddedWebViewActivity.class);
        intent.putExtra(EmbeddedWebViewActivity.EMBEDDED_URL, url);
        setTransition(currentActivity, animationType);
        currentActivity.startActivity(intent);
    }

    private static void setTransition(Activity currentActivity, AnimationType animationType) {
        switch (animationType) {

            case Normal:
                break;
            case SlideToLeft:
                currentActivity.overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                break;
            case SlideToRight:
                currentActivity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
                break;
            case SlideUp:
                currentActivity.overridePendingTransition(R.anim.slide_in_up, R.anim.stay_animation);
                break;
            case SlideDown:
                currentActivity.overridePendingTransition(R.anim.slide_in_down, R.anim.stay_animation);
                break;
        }
    }
}
