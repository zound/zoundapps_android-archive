package com.apps_lib.multiroom;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.widget.TextView;

import com.apps_lib.multiroom.volume.VolumePopupManager;
import com.frontier_silicon.components.connection.ConnectionStateUtil;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by lsuhov on 12/04/16.
 */
public class UEActivityBase extends AppCompatActivity {
    private boolean mAttachedToVolumePopup = false;
    private boolean mShowVolumePopup;
    private VolumePopupManager volumePopupManager = new VolumePopupManager();

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (isTablet()) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);// Allow any orientation for tablets
        }

        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onStart() {
        super.onStart();

        updateConnectionStateIfNeeded();
    }

    private void updateConnectionStateIfNeeded() {
        MainApplication application = (MainApplication)getApplication();

        if (application.getAppWasInBackground()) {
            application.setAppWasInBackground(false);

            //URBEAR-1368: No wi-fi connection available displayed even if there is a wi-fi connection available
            ConnectionStateUtil.getInstance().updateState();
        }
    }

    public  boolean isTablet() {

        DisplayMetrics displayMetrics = new DisplayMetrics();
        Display display = getWindowManager().getDefaultDisplay();
        display.getMetrics(displayMetrics);

        int heightPixels = displayMetrics.heightPixels;
        int widthPixels = displayMetrics.widthPixels;

        // includes window decorations (statusbar bar/menu bar)
        if (Build.VERSION.SDK_INT >= 15 && Build.VERSION.SDK_INT < 17) {
            try {
                widthPixels = (Integer) Display.class.getMethod("getRawWidth").invoke(display);
                heightPixels = (Integer) Display.class.getMethod("getRawHeight").invoke(display);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (Build.VERSION.SDK_INT >= 17) {
            try {
                Point realSize = new Point();
                Display.class.getMethod("getRealSize", Point.class).invoke(display, realSize);
                widthPixels = realSize.x;
                heightPixels = realSize.y;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        float dpHeight =  heightPixels / displayMetrics.density;
        float dpWidth = widthPixels / displayMetrics.density;

        @SuppressWarnings("SuspiciousNameCombination")
        float smallestWidth = dpHeight;
        if (dpWidth < dpHeight)
            smallestWidth = dpWidth;

        return smallestWidth >= 600;

    }

    protected void setupAppBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("");
        }
    }

    protected void enableUpNavigation() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public void setTitle(int resId) {
        TextView textView = (TextView) findViewById(R.id.titleTextView);
        textView.setText(resId);
    }

    @Override
    public void setTitle(CharSequence title) {
        TextView textView = (TextView) findViewById(R.id.titleTextView);
        textView.setText(title);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavigationHelper.goToHome(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }

    @Override
    protected void onPause() {
        if (mAttachedToVolumePopup) {
            volumePopupManager.disposeDialog();

        }
        super.onPause();
    }

    public void attachActivityToVolumePopup() {
        attachActivityToVolumePopup(true);
    }

    public void attachActivityToVolumePopup(boolean showVolumePopup) {
        mAttachedToVolumePopup = true;
        volumePopupManager.attachActivity(this);
        mShowVolumePopup = showVolumePopup;
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (mAttachedToVolumePopup) {
            boolean handled = volumePopupManager.dispatchKeyEvent(event, mShowVolumePopup);
            return handled || super.dispatchKeyEvent(event);
        } else {
            return super.dispatchKeyEvent(event);
        }
    }
}