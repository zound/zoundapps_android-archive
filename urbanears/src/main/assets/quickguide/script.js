function openPageNamed(name) {
     WebApiInterface.openPageNamed(name);
}

$(document).ready(function() {
	replaceCharactersWithImages();    
});

function replaceCharactersWithImages() {
	//replace the "Mickey Mouse character"
	$("p,h2,h1").html(function (_, html) {
     return html
     .replace(/☁/g,"<img src'../images/cloud.png' srcset='../images/cloud.png 1x, ../images/cloud@2x.png 2x, ../images/cloud@3x.png 3x'>")
     .replace(/①|➀/g,"<img src'../images/oval_1.png' srcset='../images/oval_1.png 1x, ../images/oval_1@2x.png 2x, ../images/oval_1@3x.png 3x'>")
     .replace(/②|➁/g,"<img src'../images/oval_2.png' srcset='../images/oval_2.png 1x, ../images/oval_2@2x.png 2x, ../images/oval_2@3x.png 3x'>")
     .replace(/③|➂/g,"<img src'../images/oval_3.png' srcset='../images/oval_3.png 1x, ../images/oval_3@2x.png 2x, ../images/oval_3@3x.png 3x'>")
     .replace(/④|➃/g,"<img src'../images/oval_4.png' srcset='../images/oval_4.png 1x, ../images/oval_4@2x.png 2x, ../images/oval_4@3x.png 3x'>")
     .replace(/⑤|➄/g,"<img src'../images/oval_5.png' srcset='../images/oval_5.png 1x, ../images/oval_5@2x.png 2x, ../images/oval_5@3x.png 3x'>")
     .replace(/⑥|➅/g,"<img src'../images/oval_6.png' srcset='../images/oval_6.png 1x, ../images/oval_6@2x.png 2x, ../images/oval_6@3x.png 3x'>")
     .replace(/⑦|➆/g,"<img src'../images/oval_7.png' srcset='../images/oval_7.png 1x, ../images/oval_7@2x.png 2x, ../images/oval_7@3x.png 3x'>")
     .replace(/⑧|➇/g,"<img src'../images/oval_8.png' srcset='../images/oval_8.png 1x, ../images/oval_8@2x.png 2x, ../images/oval_8@3x.png 3x'>")
     .replace(/⑨|➈/g,"<img src'../images/oval_9.png' srcset='../images/oval_9.png 1x, ../images/oval_9@2x.png 2x, ../images/oval_9@3x.png 3x'>")
     .replace(/\\"/g,"\"")
	});
}