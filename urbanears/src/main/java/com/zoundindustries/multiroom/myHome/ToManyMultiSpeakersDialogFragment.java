package com.zoundindustries.multiroom.myHome;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.apps_lib.multiroom.AnimatedDialogFragmentBase;
import com.apps_lib.multiroom.myHome.IToManyMultiSpeakersDialogListener;
import com.zoundindustries.multiroom.R;

/**
 * Created by nbalazs on 04/11/2016.
 */

public class ToManyMultiSpeakersDialogFragment extends AnimatedDialogFragmentBase {

    private IToManyMultiSpeakersDialogListener mListener;

    public static DialogFragment newInstance(IToManyMultiSpeakersDialogListener dialogListener) {
        ToManyMultiSpeakersDialogFragment dialogFragment = new ToManyMultiSpeakersDialogFragment();
        dialogFragment.mListener = dialogListener;
        return dialogFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_to_many_multi_speakers, container, false);

        (view.findViewById(R.id.buttonGotIt)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onDialogFinished();
                dismiss();
            }
        });

        Window window = getDialog().getWindow();
        if (window != null) {
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }

        return view;
    }
}
