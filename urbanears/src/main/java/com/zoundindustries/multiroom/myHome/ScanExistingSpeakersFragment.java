package com.zoundindustries.multiroom.myHome;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.apps_lib.multiroom.IUEChildFragment;
import com.apps_lib.multiroom.factory.EHomeFragmentTag;
import com.zoundindustries.multiroom.R;
import com.zoundindustries.multiroom.databinding.FragmentScanExistingSpeakersBinding;

public class ScanExistingSpeakersFragment extends Fragment implements IUEChildFragment {

    private FragmentScanExistingSpeakersBinding mBinding;

    private ScanExistingSpeakersViewModel mViewModel;

    public ScanExistingSpeakersFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_scan_existing_speakers, container, false);

        return mBinding.getRoot();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setupControls();
    }

    private void setupControls() {
        if (mViewModel == null) {
            mViewModel = new ScanExistingSpeakersViewModel(getActivity());
        }

        mBinding.setScanExistingSpeakersVM(mViewModel);
    }

    @Override
    public void onResume() {
        super.onResume();

        mViewModel.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();

        mViewModel.onPause();
    }

    @Override
    public void onDestroyView() {
        if (mViewModel != null) {
            mViewModel.dispose();
            mViewModel = null;
        }

        super.onDestroyView();
    }

    @Override
    public EHomeFragmentTag getStaticTag() {
        return EHomeFragmentTag.ScanExistingSpeakers;
    }
}
