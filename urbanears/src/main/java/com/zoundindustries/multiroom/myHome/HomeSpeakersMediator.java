package com.zoundindustries.multiroom.myHome;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;

import com.apps_lib.multiroom.myHome.HomeSpeakersBuilder;
import com.apps_lib.multiroom.myHome.SwitchSpeakerBetweenSoloMultiEvent;
import com.apps_lib.multiroom.myHome.speakers.SpeakerModel;
import com.apps_lib.multiroom.myHome.speakers.SpeakersMediator;
import com.apps_lib.multiroom.persistence.PersistenceMgr;
import com.apps_lib.multiroom.setup.update.CheckForUpdateRunnable;
import com.apps_lib.multiroom.setup.update.ICheckForUpdateListener;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.components.common.CommonPreferences;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CountDownLatch;

/**
 * Created by lsuhov on 12/06/16.
 */
public class HomeSpeakersMediator extends SpeakersMediator {

    public void init(Activity activity, RecyclerView recyclerView) {
        mAdapter = new HomeSpeakersAdapter(activity);
        mSpeakersBuilder = new HomeSpeakersBuilder();
        EventBus.getDefault().register(this);
        super.init(activity, recyclerView);
    }

    @Subscribe
    public void onSwapHomeSpeakerEvent(SwitchSpeakerBetweenSoloMultiEvent switchSpeakerBetweenSoloMultiEvent) {
        List<SpeakerModel> homeList = mSpeakersBuilder.switchSpeakerBetweenSoloMultiAndReturnFakeList(switchSpeakerBetweenSoloMultiEvent.mSpeakerModel, switchSpeakerBetweenSoloMultiEvent.mIsChecked);
        mAdapter.swapItems(homeList);
    }


    public void dispose() {
        super.dispose();

        EventBus.getDefault().unregister(this);
    }
}
