package com.zoundindustries.multiroom.myHome;

import android.app.Activity;
import android.databinding.ObservableBoolean;
import android.view.View;

import com.apps_lib.multiroom.NavigationHelper;
import com.apps_lib.multiroom.factory.EHomeFragmentTag;
import com.apps_lib.multiroom.myHome.IDrawerActivity;
import com.apps_lib.multiroom.myHome.SpeakersDiscoveryManager;
import com.frontier_silicon.NetRemoteLib.Discovery.scanner.IDiscoveryScanner;
import com.frontier_silicon.NetRemoteLib.Discovery.scanner.IScanListener;
import com.frontier_silicon.components.NetRemoteManager;
import com.frontier_silicon.components.connection.ConnectionState;
import com.frontier_silicon.components.connection.ConnectionStateUtil;
import com.frontier_silicon.components.connection.IConnectionStateListener;
import com.frontier_silicon.components.multiroom.IMultiroomGroupingListener;
import com.frontier_silicon.components.multiroom.MultiroomGroupManager;
import com.frontier_silicon.loggerlib.FsLogger;
import com.frontier_silicon.loggerlib.LogLevel;

/**
 * Created by viftime on 08/03/16.
 */
public class ScanExistingSpeakersViewModel implements IScanListener, IConnectionStateListener,
        IMultiroomGroupingListener {

    public ObservableBoolean isScanning;

    private Activity mActivity;
    private Boolean mRadioFound = false;

    public ScanExistingSpeakersViewModel(Activity activity) {
        mActivity = activity;

        isScanning = new ObservableBoolean(true);
    }

    void onResume() {
        NetRemoteManager.getInstance().addScanListener(this);
        MultiroomGroupManager.getInstance().addListener(this);

        SpeakersDiscoveryManager.getInstance().tryStartDiscovery();

        ConnectionStateUtil.getInstance().addListener(this, true);
    }

    void onPause() {
        NetRemoteManager.getInstance().removeScanListener(this);
        MultiroomGroupManager.getInstance().removeListener(this);

        ConnectionStateUtil.getInstance().removeListener(this);
    }

    @Override
    public void onScanError(IDiscoveryScanner.ScanError error) {
        FsLogger.log("ScanExistingSpeakersViewModel: SCAN ERROR " + error, LogLevel.Error);

        isScanning.set(false);
    }

    @Override
    public void onScanStart() {
        FsLogger.log("ScanExistingSpeakersViewModel: SCAN STARTED ", LogLevel.Info);

        isScanning.set(true);
    }


    @Override
    public void onStateUpdate(final ConnectionState newState) {
        FsLogger.log("ScanExistingSpeakersViewModel: >>>>>>>> STATE UPDATED: " + newState);

        if (!isDisposed()) {
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (!isDisposed()) {
                        switch (newState) {
                            case NO_WIFI_OR_ETHERNET:
                                NavigationHelper.goToFragment(mActivity, EHomeFragmentTag.NoWiFiFragment);
                                notifyRadioAvailability(false);
                                break;
                            case CONNECTED_RADIO_AP_MODE:
                                break;
                            case SEARCHING_RADIOS_NO_SPEAKER_FOUND_YET:
                                isScanning.set(true);
                                notifyRadioAvailability(false);
                                break;
                            case SEARCHING_RADIOS_SPEAKER_FOUND:
                                break;
                            case NO_RADIOS_FOUND_AFTER_10_SECONDS:
                                setNoSpeakersFound();
                                notifyRadioAvailability(false);
                                break;
                            case NOT_CONNECTED_TO_RADIO:
                                notifyRadioAvailability(true);
                                break;
                            case CONNECTED_TO_RADIO:
                                notifyRadioAvailability(true);
                                break;
                            case DISCONNECTED:
                            case INVALID_SESSION:
                                SpeakersDiscoveryManager.getInstance().rescan();
                                break;
                        }
                    }
                }
            });
        }
    }

    @Override
    public void onGroupingUpdate() {
        if (MultiroomGroupManager.getInstance().getFlatGroupDeviceList().size() != 0) {
            onRadioFoundImpl();
        }
    }

    private void setNoSpeakersFound() {
        isScanning.set(false);
    }

    private void onRadioFoundImpl() {
        synchronized (mRadioFound) {
            if (!mRadioFound) {
                mRadioFound = true;
                if (!isDisposed()) {
                    mActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (!isDisposed()) {
                                openMyHomeFragment();
                            }
                        }
                    });
                }
            }
        }
    }

    private void openMyHomeFragment() {
        if (!isDisposed()) {
            NavigationHelper.goToFragment(mActivity, EHomeFragmentTag.HomeFragment);
        }
    }

    public void onSetupNewSpeakerClicked(@SuppressWarnings("UnusedParameters") View view) {
        NavigationHelper.startSetupActivity(mActivity);
    }

    public void onRefreshClicked(@SuppressWarnings("UnusedParameters") View view) {
        NetRemoteManager.getInstance().rescan();
    }

    public void dispose() {
        mActivity = null;
    }

    private boolean isDisposed() {
        return mActivity == null || mActivity.isFinishing();
    }

    private void notifyRadioAvailability(final boolean isAvailable) {
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ((IDrawerActivity)mActivity).refreshNavigationDrawer(isAvailable);
            }
        });
    }
}
