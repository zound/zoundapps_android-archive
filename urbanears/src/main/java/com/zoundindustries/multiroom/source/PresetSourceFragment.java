package com.zoundindustries.multiroom.source;

import android.databinding.DataBindingUtil;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;

import com.apps_lib.multiroom.presets.IPresetAnimationFinishedListener;
import com.apps_lib.multiroom.presets.IPresetAnimationHandler;
import com.apps_lib.multiroom.source.IRotationHandler;
import com.zoundindustries.multiroom.R;
import com.zoundindustries.multiroom.anim.Rotate3dAnimation;
import com.zoundindustries.multiroom.databinding.FragmentSourcePresetBinding;


/**
 * Created by lsuhov on 11/05/16.
 */
public class PresetSourceFragment extends SourceFragment implements IPresetAnimationHandler, IRotationHandler {


    public static final String PRESET_INDEX_KEY = "preset_index";

    private int mPresetIndex = 0;

    private FragmentSourcePresetBinding mBinding;

    private float mFadeOutAlpha = SourceFragment.LAYOUT_ALPHA_0;

    private PresetSourceViewModel mViewModel;

    private boolean mRightToLeftAnimationIsRunning = false;
    private boolean mLeftToRightAnimationIsRunning = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_source_preset, container, false);

        mViewModel = new PresetSourceViewModel();
        mBinding.setViewModel(mViewModel);

        View rootView = mBinding.getRoot();
        rootView.setEnabled(false);
        rootView.setClickable(false);

        return mBinding.getRoot();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Bundle bundle = getArguments();
        if (bundle != null) {
            mPresetIndex = bundle.getInt(PRESET_INDEX_KEY);
        }

        mBinding.getViewModel().init(this.getActivity(), mPresetIndex, this, this);

//        activateHiddenFeatures();
    }

    private void activateHiddenFeatures() {
        mBinding.addPresetButton.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                mBinding.getViewModel().onAddLongPress();
                return false;
            }
        });
    }

    @Override
    public void onDestroyView() {
        if (mBinding != null) {
            mBinding.getViewModel().dispose();
            mBinding = null;
        }

        super.onDestroyView();
    }


    @Override
    public void startAnimationForPresetPlaylistDelete(IPresetAnimationFinishedListener presetAnimationFinishedListener) {
        if (getActivity() == null) {
            return;
        }

        animatePresets(ContextCompat.getDrawable(getContext(), R.drawable.ic_preset_failed), getResources().getString(R.string.playlist_deleted), presetAnimationFinishedListener);
        mBinding.deletePresetButton.setVisibility(View.GONE);
    }

    @Override
    public void startAnimationForPresetRadioStationDelete(IPresetAnimationFinishedListener presetAnimationFinishedListener) {
        if (getActivity() == null) {
            return;
        }

        animatePresets(ContextCompat.getDrawable(getContext(), R.drawable.ic_preset_failed), getResources().getString(R.string.radio_station_deleted), presetAnimationFinishedListener);
        mBinding.deletePresetButton.setVisibility(View.GONE);
    }

    @Override
    public void startAnimationForPresetPlaylistAdded(IPresetAnimationFinishedListener presetAnimationFinishedListener, boolean succeeded) {
        if (getActivity() == null) {
            return;
        }

        if (succeeded) {
            animatePresets(ContextCompat.getDrawable(getContext(), R.drawable.ic_preset_succeeded), getResources().getString(R.string.playlist_added), presetAnimationFinishedListener);
        } else {
            animatePresets(ContextCompat.getDrawable(getContext(), R.drawable.ic_preset_failed), getResources().getString(R.string.adding_playlist_failed), presetAnimationFinishedListener);
        }
    }

    @Override
    public void startAnimationForPresetRadioStationAdded(IPresetAnimationFinishedListener presetAnimationFinishedListener, boolean succeeded) {
        if (getActivity() == null) {
            return;
        }

        if (succeeded) {
            animatePresets(ContextCompat.getDrawable(getContext(), R.drawable.ic_preset_succeeded), getResources().getString(R.string.radio_station_added), presetAnimationFinishedListener);
        } else {
            animatePresets(ContextCompat.getDrawable(getContext(), R.drawable.ic_preset_failed), getResources().getString(R.string.adding_a_radio_station_failed), presetAnimationFinishedListener);
        }
    }

    @Override
    public void startAnimationForPlayingEmptyPreset(IPresetAnimationFinishedListener presetAnimationFinishedListener) {
        if (getActivity() == null) {
            return;
        }

        animatePresets(ContextCompat.getDrawable(getContext(), R.drawable.ic_preset_failed), getResources().getString(R.string.preset_is_empty), presetAnimationFinishedListener);
    }

    @Override
    public void startAnimationForUndo(IPresetAnimationFinishedListener presetAnimationFinishedListener, boolean succeeded) {
        if (getActivity() == null) {
            return;
        }

        if (succeeded) {
            animatePresets(ContextCompat.getDrawable(getContext(), R.drawable.ic_preset_succeeded), getResources().getString(R.string.preset_anim_undo_redo_succeeded), presetAnimationFinishedListener);
        } else {
            animatePresets(ContextCompat.getDrawable(getContext(), R.drawable.ic_preset_failed), getResources().getString(R.string.preset_anim_undo_redo_failed), presetAnimationFinishedListener);
        }
    }

    @Override
    public void startAnimationForRedo(IPresetAnimationFinishedListener presetAnimationFinishedListener, boolean succeeded) {
        if (getActivity() == null) {
            return;
        }
        if (succeeded) {
            animatePresets(ContextCompat.getDrawable(getContext(), R.drawable.ic_preset_succeeded), getResources().getString(R.string.preset_anim_undo_redo_succeeded), presetAnimationFinishedListener);
        } else {
            animatePresets(ContextCompat.getDrawable(getContext(), R.drawable.ic_preset_failed), getResources().getString(R.string.preset_anim_undo_redo_failed), presetAnimationFinishedListener);
        }
    }

    @Override
    public void showUndoButton() {
        Animation animationOut = AnimationUtils.loadAnimation(getContext(), R.anim.preset_undo_redo_fade_out);
        Animation animationIn = AnimationUtils.loadAnimation(getContext(), R.anim.preset_undo_redo_fade_in);

        animationIn.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                mBinding.undoAddPresetButton.setClickable(false);
                mBinding.undoAddPresetButton.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mBinding.undoAddPresetButton.setClickable(true);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        animationOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                mBinding.addPresetButton.setClickable(false);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mBinding.addPresetButton.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        mBinding.addPresetButton.startAnimation(animationOut);
        mBinding.undoAddPresetButton.startAnimation(animationIn);
    }

    @Override
    public void hideUndoButton() {
        Animation animationIn = AnimationUtils.loadAnimation(getContext(), R.anim.preset_undo_redo_fade_in);
        Animation animationOut = AnimationUtils.loadAnimation(getContext(), R.anim.preset_undo_redo_fade_out);

        animationIn.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                mBinding.addPresetButton.setClickable(false);
                mBinding.addPresetButton.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mBinding.addPresetButton.setClickable(true);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        animationOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                mBinding.undoAddPresetButton.setClickable(false);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mBinding.undoAddPresetButton.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        mBinding.addPresetButton.startAnimation(animationIn);
        mBinding.undoAddPresetButton.startAnimation(animationOut);
    }

    @Override
    public void showRedoButton() {
        Animation animationIn = AnimationUtils.loadAnimation(getContext(), R.anim.preset_undo_redo_fade_in);
        Animation animationOut = AnimationUtils.loadAnimation(getContext(), R.anim.preset_undo_redo_fade_out);

        animationIn.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                mBinding.redoDeletePresetButton.setClickable(false);
                mBinding.redoDeletePresetButton.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mBinding.redoDeletePresetButton.setClickable(true);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        animationOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                mBinding.deletePresetButton.setClickable(false);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mBinding.deletePresetButton.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        mBinding.redoDeletePresetButton.startAnimation(animationIn);
        mBinding.deletePresetButton.startAnimation(animationOut);
    }

    @Override
    public void hideRedoButton() {
        if (mBinding == null) {
            return;
        }
        
        Animation animationIn = AnimationUtils.loadAnimation(getContext(), R.anim.preset_undo_redo_fade_in);
        Animation animationOut = AnimationUtils.loadAnimation(getContext(), R.anim.preset_undo_redo_fade_out);

        animationIn.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                mBinding.deletePresetButton.setClickable(false);
                mBinding.deletePresetButton.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mBinding.deletePresetButton.setClickable(true);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        animationOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                mBinding.redoDeletePresetButton.setClickable(false);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mBinding.redoDeletePresetButton.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        mBinding.deletePresetButton.startAnimation(animationIn);
        mBinding.redoDeletePresetButton.startAnimation(animationOut);
    }

    private void animatePresets(Drawable icon, String text, final IPresetAnimationFinishedListener finishedListener) {

        if (mBinding == null) {
            return;
        }

        mBinding.artworkAnimationViewIcon.setImageDrawable(icon);
        mBinding.artworkAnimationViewTitle.setText(text);
        Animation fadeInAnimationTo70 = AnimationUtils.loadAnimation(getContext(), R.anim.fade_in_to_70);
        Animation fadeInAnimationTo100 = AnimationUtils.loadAnimation(getContext(), R.anim.fade_in_to_100);
        Animation scaleDownTo1 = AnimationUtils.loadAnimation(getContext(), R.anim.scale_down_to_1);

        AnimationSet animationSet70 = new AnimationSet(false);
        animationSet70.addAnimation(fadeInAnimationTo70);

        AnimationSet animationSet100 = new AnimationSet(false);
        animationSet100.addAnimation(fadeInAnimationTo100);
        animationSet100.addAnimation(scaleDownTo1);

        animationSet70.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                Animation fadeOutAnimationFrom70 = AnimationUtils.loadAnimation(getContext(), R.anim.fade_out_from_70);
                Animation fadeOutSlowerAnimationFrom100 = AnimationUtils.loadAnimation(getContext(), R.anim.fade_out_from_100_slower);
                Animation scaleDownAnimationTo0 = AnimationUtils.loadAnimation(getContext(), R.anim.scale_down_to_0);

                AnimationSet animationSet100 = new AnimationSet(false);
                animationSet100.addAnimation(fadeOutSlowerAnimationFrom100);
                animationSet100.addAnimation(scaleDownAnimationTo0);

                animationSet100.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        if (finishedListener != null) {
                            finishedListener.onPresetAnimationFinished();
                        }
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                mBinding.artworkAnimationViewBackground.startAnimation(fadeOutAnimationFrom70);
                mBinding.artworkAnimationViewForeground.startAnimation(animationSet100);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        mBinding.artworkAnimationViewBackground.startAnimation(animationSet70);
        mBinding.artworkAnimationViewForeground.startAnimation(animationSet100);
    }

    @Override
    public void setFragmentAlphaForOffset(float currentOffset, EFragmentPosition fragmentPosition) {
        View rootView = getView();

        if (rootView != null) {
            View buttonsLayout = rootView.findViewById(R.id.buttonsLayout);
            float layoutAlpha;
            float buttonsLayoutAlpha;
            float alpha = LAYOUT_ALPHA_100 - mFadeOutAlpha;

            if (fragmentPosition == EFragmentPosition.PRIMARY) {
                layoutAlpha = 1f - (currentOffset * alpha);
//                buttonsLayoutAlpha = 1f - (currentOffset * LAYOUT_ALPHA_100);
            } else {
                layoutAlpha = currentOffset * alpha + mFadeOutAlpha;
//                buttonsLayoutAlpha = currentOffset * LAYOUT_ALPHA_100;
            }

            buttonsLayoutAlpha = generateAlphaValueForButtons(currentOffset, fragmentPosition);

            rootView.setAlpha(layoutAlpha);
            buttonsLayout.setAlpha(buttonsLayoutAlpha);

            if (buttonsLayoutAlpha < LAYOUT_ALPHA_75) {
                setViewClickable(false);
            } else {
                setViewClickable(true);
            }

            changeViewVisibilityIfNeeded(buttonsLayout);

            rootView.invalidate();
        }
    }

    private float generateAlphaValueForButtons(float currentOffset, EFragmentPosition fragmentPosition) {
        float alpha = 0.0f;

        if (fragmentPosition == EFragmentPosition.SECONDARY) {
            if (currentOffset < 0.9f) {
                alpha = 0.0f;
            } else {
                alpha = (10.0f * currentOffset) - 9.0f;
            }
        } else if (fragmentPosition == EFragmentPosition.PRIMARY) {
            if (currentOffset < 0.1f) {
                alpha = (-10.0f * currentOffset) + 1.0f;
            } else {
                alpha = 0.0f;
            }
        }

        return alpha;
    }

    @Override
    public void setFragmentAlphaForPosition(EFragmentPosition fragmentPosition) {
        View rootView = getView();

        if (rootView != null) {
            View buttonsLayout = rootView.findViewById(R.id.buttonsLayout);

            switch (fragmentPosition) {
                case PRIMARY:
                    rootView.setAlpha(LAYOUT_ALPHA_100);
                    buttonsLayout.setAlpha(LAYOUT_ALPHA_100);
                    setViewClickable(true);
                    break;
                case SECONDARY:
                    rootView.setAlpha(mFadeOutAlpha);
                    buttonsLayout.setAlpha(LAYOUT_ALPHA_0);
                    setViewClickable(false);
                    break;
                default:
                    break;
            }

            changeViewVisibilityIfNeeded(buttonsLayout);
        }
    }

    @Override
    public final void setFadeOutAlpha(float fadeOutAlpha, boolean adjustAlphaIfNeeded) {
        mFadeOutAlpha = fadeOutAlpha;

        if (adjustAlphaIfNeeded) {
            View rootView = getView();

            if (rootView != null) {
                float startAlpha = rootView.getAlpha();
                float alphaDifference = startAlpha - mFadeOutAlpha;

                if (Math.abs(alphaDifference) > 0) {
                    final int steps = 5;

                    for (int i = 0; i < steps; i++) {
                        float nextAlpha = alphaDifference / steps * (steps - i - 1);
                        if (alphaDifference > 0) {
                            rootView.setAlpha(nextAlpha);
                        } else {
                            rootView.setAlpha(alphaDifference + nextAlpha);
                        }
                        rootView.invalidate();
                    }
                }

                if (mFadeOutAlpha < LAYOUT_ALPHA_75) {
                    setViewClickable(false);
                }

                rootView.setAlpha(mFadeOutAlpha);

                changeViewVisibilityIfNeeded(rootView.findViewById(R.id.buttonsLayout));

                rootView.invalidate();
            }
        }
    }

    @Override
    public void onOffsetChanged(float offset, int offsetPixel, boolean leftToRight, boolean rightToLeft) {
        mBinding.buttonsLayout.setTranslationX(offsetPixel);
    }

    @Override
    public final ESourceFragmentType getFragmentType() {
        return ESourceFragmentType.PRESET_SOURCE_FRAGMENT;
    }

    @Override
    public void setViewClickable(boolean clickable) {
        View rootView = getView();
        if (rootView != null) {
            rootView.setEnabled(clickable);
            rootView.setClickable(clickable);
        }
    }

    @Override
    public void startLeftToRightRotation(final Drawable drawable) {

        if (mRightToLeftAnimationIsRunning) {
            return;
        }

        mLeftToRightAnimationIsRunning = true;

        final float centerX = mBinding.artworkImageView.getWidth() / 2.0f;
        final float centerY = mBinding.artworkImageView.getHeight() / 2.0f;

        final Animation rotateOut = new Rotate3dAnimation(0, 90, centerX, centerY, 0, true);
        rotateOut.setDuration(375);
        rotateOut.setFillAfter(true);
        rotateOut.setInterpolator(new LinearInterpolator());

        final Animation mirror = new Rotate3dAnimation(90, -90, centerX, centerY, 0, true);
        mirror.setDuration(0);
        mirror.setFillAfter(true);
        mirror.setInterpolator(new LinearInterpolator());

        final Animation rotateIn = new Rotate3dAnimation(-90, 0, centerX, centerY, 0, true);
        rotateIn.setDuration(375);
        rotateIn.setFillAfter(true);
        rotateIn.setInterpolator(new LinearInterpolator());

        rotateOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                // Nothing to do here
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mViewModel.artwork.set(drawable);
                rotateIn.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        mLeftToRightAnimationIsRunning = false;
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                mBinding.artworkImageView.startAnimation(rotateIn);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                // Nothing to do here
            }
        });

        mBinding.artworkImageView.startAnimation(rotateOut);
    }

    @Override
    public void startRightToLeftRotation(final Drawable drawable) {

        if (mLeftToRightAnimationIsRunning) {
            return;
        }

        mRightToLeftAnimationIsRunning = true;

        final float centerX = mBinding.artworkImageView.getWidth() / 2.0f;
        final float centerY = mBinding.artworkImageView.getHeight() / 2.0f;

        final Animation rotateOut = new Rotate3dAnimation(0, -90, centerX, centerY, 0, true);
        rotateOut.setDuration(375);
        rotateOut.setFillAfter(true);
        rotateOut.setInterpolator(new LinearInterpolator());

        final Animation mirror = new Rotate3dAnimation(-90, 90, centerX, centerY, 0, true);
        mirror.setDuration(0);
        mirror.setFillAfter(true);
        mirror.setInterpolator(new LinearInterpolator());

        final Animation rotateIn = new Rotate3dAnimation(90, 0, centerX, centerY, 0, true);
        rotateIn.setDuration(375);
        rotateIn.setFillAfter(true);
        rotateIn.setInterpolator(new LinearInterpolator());

        rotateOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                // Nothing to do here
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mViewModel.artwork.set(drawable);
                rotateIn.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        mRightToLeftAnimationIsRunning = false;
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                mBinding.artworkImageView.startAnimation(rotateIn);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                // Nothing to do here
            }
        });

        mBinding.artworkImageView.startAnimation(rotateOut);
    }

    @Override
    protected final void setScaleToOffset(float currentOffset, EFragmentPosition fragmentPosition) {
        View rootView = getView();

        if (rootView != null) {
            View viewToScale = rootView.findViewById(R.id.artworkContainer);
            if (viewToScale != null) {
                float layoutScale;
                float scale = LAYOUT_SCALE_100 - LAYOUT_SCALE_90;

                if (fragmentPosition == EFragmentPosition.PRIMARY) {
                    layoutScale = 1f - (currentOffset * scale);
                } else {
                    layoutScale = currentOffset * scale + LAYOUT_SCALE_90;
                }

                viewToScale.setScaleX(layoutScale);
                viewToScale.setScaleY(layoutScale);

                viewToScale.invalidate();
            }
        }
    }
}
