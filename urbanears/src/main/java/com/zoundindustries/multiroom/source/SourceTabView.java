package com.zoundindustries.multiroom.source;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.apps_lib.multiroom.R;
import com.apps_lib.multiroom.source.SourceTab;
import com.apps_lib.multiroom.source.SourcesRegistry;
import com.apps_lib.multiroom.widgets.TabSelectedEvent;

import org.greenrobot.eventbus.EventBus;

public class SourceTabView extends RelativeLayout {

    enum State {
        INACTIVE, INACTIVE_PRESSED, ACTIVE, ACTIVE_PRESSED
    }

    private ImageView imageView;
    private TextView textView;
    private View usedView;
    private int position;

    public SourceTabView(Context context) {
        super(context);

        View.inflate(context, R.layout.item_tab_source, this);
        this.imageView = (ImageView) findViewById(R.id.image);
        this.textView = (TextView) findViewById(R.id.tabName);

        addClickListener();
    }

    private void addClickListener() {
        this.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(new TabSelectedEvent(position));
            }
        });
    }

    public void setTab(SourceTab tab, int resourceID, int position) {
        this.position = position;

        if (tab != SourceTab.PresetTab) {
            this.imageView.setImageResource(resourceID);
            this.imageView.setVisibility(VISIBLE);
            this.textView.setVisibility(GONE);

            usedView = imageView;
        } else {
            this.textView.setText(String.valueOf(SourcesRegistry.getInstance().getVirtualPosition(position) + 1));
            this.textView.setVisibility(VISIBLE);
            this.imageView.setVisibility(GONE);

            usedView = textView;
        }

        switchState(State.INACTIVE);
    }

    public void switchState(State state) {
        switch (state) {
            case ACTIVE:
                usedView.setAlpha(1f);
                break;
            case INACTIVE:
                usedView.setAlpha(0.3f);
                break;
            case ACTIVE_PRESSED:
                usedView.setAlpha(0.6f);
                break;
            case INACTIVE_PRESSED:
                usedView.setAlpha(0.6f);
                break;
        }
    }

    public void dispose() {
        this.setOnClickListener(null);
    }
}
