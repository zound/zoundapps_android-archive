package com.zoundindustries.multiroom.source;

/**
 * Created by nbalazs on 04/04/2017.
 */
interface IActiveStatusProvider {
    boolean isActive();
}
