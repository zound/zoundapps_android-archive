package com.zoundindustries.multiroom.source;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;

import com.apps_lib.multiroom.source.SourceTab;
import com.apps_lib.multiroom.source.SourcesRegistry;

/**
 * Created by lsuhov on 15/05/16.
 */
class SourceTabsAdapter extends PagerAdapter {

    private SourceTabView mCurrentPrimaryItem;

    @Override
    public int getCount() {
        return SourcesRegistry.getInstance().getTotalPageNumbers();
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        int virtualPosition = SourcesRegistry.getInstance().getVirtualPosition(position);
        int resourceID = SourcesRegistry.getInstance().getResourceIdBasedOnVirtualPosition(virtualPosition);
        SourceTab sourceTab = SourcesRegistry.getInstance().getSourceTabBasedOnVirtualPosition(virtualPosition);

        SourceTabView sourceTabView = new SourceTabView(container.getContext());
        sourceTabView.setTab(sourceTab, resourceID, position);
        container.addView(sourceTabView);

        return sourceTabView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        SourceTabView sourceTabView = (SourceTabView)object;
        sourceTabView.dispose();

        container.removeView(container);
    }

    @Override
    public void setPrimaryItem(ViewGroup container, int position, Object object) {
        SourceTabView sourceTabView = (SourceTabView)object;

        if (sourceTabView != mCurrentPrimaryItem) {
            if (mCurrentPrimaryItem != null) {
                mCurrentPrimaryItem.switchState(SourceTabView.State.INACTIVE);
            }

            sourceTabView.switchState(SourceTabView.State.ACTIVE);
            mCurrentPrimaryItem = sourceTabView;
        }
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    /**
     * Returns the proportional width of a given page as a percentage of the
     * ViewPager's measured width from (0.f-1.f]
     *
     * @param position The position of the page requested
     * @return Proportional width for the given page position
     */
    @Override
    public float getPageWidth(int position) {
        return 1f;
    }
}
