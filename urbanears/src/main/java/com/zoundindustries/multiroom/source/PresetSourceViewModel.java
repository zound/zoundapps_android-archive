package com.zoundindustries.multiroom.source;

import android.app.Activity;
import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.Observable;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;
import android.webkit.URLUtil;

import com.apps_lib.multiroom.NavigationHelper;
import com.apps_lib.multiroom.R;
import com.apps_lib.multiroom.connection.ConnectionManager;
import com.apps_lib.multiroom.factory.ActivityFactory;
import com.apps_lib.multiroom.nowPlaying.NowPlayingDataModel;
import com.apps_lib.multiroom.nowPlaying.NowPlayingManager;
import com.apps_lib.multiroom.presets.IPresetAnimationFinishedListener;
import com.apps_lib.multiroom.presets.IPresetAnimationHandler;
import com.apps_lib.multiroom.presets.PresetItemModel;
import com.apps_lib.multiroom.presets.PresetSubType;
import com.apps_lib.multiroom.presets.PresetTypeNames;
import com.apps_lib.multiroom.presets.PresetsManager;
import com.apps_lib.multiroom.presets.spotify.AuthenticationScope;
import com.apps_lib.multiroom.presets.spotify.ISpotifyRefreshAuthorizationListener;
import com.apps_lib.multiroom.presets.spotify.SpotifyAuthenticator;
import com.apps_lib.multiroom.presets.spotify.SpotifyManager;
import com.apps_lib.multiroom.setup.presets.ChoosePresetsTypeActivity;
import com.apps_lib.multiroom.setup.update.UpdateActivity;
import com.apps_lib.multiroom.source.IPresetStatus;
import com.apps_lib.multiroom.source.IRotationHandler;
import com.apps_lib.multiroom.source.SourcesManager;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.frontier_silicon.NetRemoteLib.Node.NodeInfo;
import com.frontier_silicon.NetRemoteLib.Node.NodeNavPresetUploadArtworkUrl;
import com.frontier_silicon.NetRemoteLib.Node.NodeNavPresetUploadBlob;
import com.frontier_silicon.NetRemoteLib.Node.NodeNavPresetUploadName;
import com.frontier_silicon.NetRemoteLib.Node.NodeNavPresetUploadType;
import com.frontier_silicon.NetRemoteLib.Node.NodeNavPresetUploadUpload;
import com.frontier_silicon.NetRemoteLib.Node.NodePlayAddPresetStatus;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysCapsValidModes;
import com.frontier_silicon.NetRemoteLib.Radio.ErrorResponse;
import com.frontier_silicon.NetRemoteLib.Radio.INodeNotificationCallback;
import com.frontier_silicon.NetRemoteLib.Radio.ISetNodesCallback;
import com.frontier_silicon.NetRemoteLib.Radio.NodeResponse;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.components.NetRemoteManager;
import com.frontier_silicon.components.common.RadioNodeUtil;
import com.frontier_silicon.loggerlib.FsLogger;
import com.frontier_silicon.loggerlib.LogLevel;
import com.zoundindustries.multiroom.presets.spotify.AddingPresetFailedFragment;

import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import static com.apps_lib.multiroom.presets.PresetSubType.Empty;
import static com.apps_lib.multiroom.presets.PresetSubType.InternetRadio;
import static com.apps_lib.multiroom.presets.PresetSubType.SpotifyAlbum;
import static com.apps_lib.multiroom.presets.PresetSubType.SpotifyArtist;
import static com.apps_lib.multiroom.presets.PresetSubType.SpotifyPlaylist;

public class PresetSourceViewModel extends BaseObservable implements INodeNotificationCallback {

    private Activity mActivity;
    private Context mApplicationContext;
    private PresetItemModel mPresetItemModel;
    private OnPropertyChangedCallback mPropertyChangedCallback;
    private IPresetAnimationHandler mPresetAnimationHandler;
    private IRotationHandler mRotationHandler;
    private DialogFragment fragment;

    public ObservableField<String> name = new ObservableField<>();
    public ObservableField<String> type = new ObservableField<>();
    public ObservableBoolean isSpotify = new ObservableBoolean(false);
    public ObservableField<Drawable> artwork = new ObservableField<>();
    public ObservableBoolean isPresetable = new ObservableBoolean(false);
    public ObservableBoolean isBuffering = new ObservableBoolean(false);

    public ObservableBoolean isPlayButtonVisible = new ObservableBoolean(false);
    public ObservableBoolean isPauseButtonVisible = new ObservableBoolean(false);
    public ObservableBoolean isStopButtonVisible = new ObservableBoolean(false);

    public ObservableBoolean showSpotifyWarning = new ObservableBoolean(false);

    public ObservableBoolean presetIsEmpty = new ObservableBoolean(false);

    private PresetSourceModelCopy mTheUndoCopy = null;
    private PresetSourceModelCopy mTheRedoCopy = null;
    private boolean mUndoButtonPressed = false;
    private boolean mRedoButtonPressed = false;
    private boolean mDeleteButtonPressed = false;
    private boolean mRotationIsInverted = false;
    private int mCountOfPendingAnimations = 0;
    private Timer mUndoTimer;
    private Timer mRedoTimer;

    private class PresetSourceModelCopy {
        String mBlob;
        String mPresetName;
        String mPresetType;
        String mArtworkUrl;
        int mPresetIndex;

        PresetSubType mPresetSubType;

        boolean mIsPlayButtonVisible;
        boolean mIsPauseButtonVisible;
        boolean mIsStopButtonVisible;

        int mIndexOfCurrentlyPlayingPreset;

        Drawable mArtwork;

        PresetSourceModelCopy(PresetSourceViewModel model) {
            if (model.mPresetItemModel == null) {
                return;
            }

            mBlob = model.mPresetItemModel.blob;
            mPresetName = model.mPresetItemModel.presetName.get();
            mPresetType = model.mPresetItemModel.presetType.get();
            mArtworkUrl = model.mPresetItemModel.artworkUrl.get();
            mPresetIndex = model.mPresetItemModel.presetIndex;

            mPresetSubType = model.mPresetItemModel.presetSubType.get();

            mIsPlayButtonVisible = model.isPlayButtonVisible.get();
            mIsPauseButtonVisible = model.isPauseButtonVisible.get();
            mIsStopButtonVisible = model.isStopButtonVisible.get();

            mIndexOfCurrentlyPlayingPreset = NowPlayingManager.getInstance().getNowPlayingDataModel().indexOfCurrentlyPlayingPreset.get();

            mArtwork = model.artwork.get();
        }

        boolean undoRedoIsSupported() {
            return mBlob != null && mBlob.length() > 0;
        }
    }

    @Override
    public void onNodeUpdated(NodeInfo node) {
        if (node instanceof NodePlayAddPresetStatus && mPresetItemModel != null && mPresetItemModel.presetIndex == PresetsManager.getInstance().getCurrentTabIndex()) {
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {

                }
            });
        }
    }

    public void init(Activity activity, int mPresetIndex, IPresetAnimationHandler animationHandler, IRotationHandler rotationHandler) {
        mActivity = activity;
        mApplicationContext = activity.getApplicationContext();
        mPresetAnimationHandler = animationHandler;
        mRotationHandler = rotationHandler;
        mPresetItemModel = SourcesManager.getInstance().getPresetAtPosition(mPresetIndex);

        checkSpotify();

        updateAll();

        addListenersToPresetItemModel();

        registerForNodeNotificationCallback();
    }

    private void registerForNodeNotificationCallback() {
        Radio radio = ConnectionManager.getInstance().getSelectedRadio();
        if (radio != null) {
            radio.addNotificationListener(this);
        }
    }

    private void removeNodeNotificationCallbackListener() {
        Radio radio = ConnectionManager.getInstance().getSelectedRadio();
        if (radio != null) {
            radio.removeNotificationListener(this);
        }
    }

    private void updateAll() {
        updateName();
        enableOrDisableDeleteButton();
        retrieveArtwork(false);
        updatePresetType();
        updateIsPresetable();
        updateSpeakerIsBuffering();
        updateControlButtonsVisibility();
    }

    private void updateSpeakerIsBuffering() {
        isBuffering.set(NowPlayingManager.getInstance().getNowPlayingDataModel().indexOfCurrentlyPlayingPreset.get() == mPresetItemModel.presetIndex
                && NowPlayingManager.getInstance().getNowPlayingDataModel().isBuffering());
    }

    private void updateIsPresetable() {
        NowPlayingDataModel nowPlayingDataModel = NowPlayingManager.getInstance().getNowPlayingDataModel();
        isPresetable.set(nowPlayingDataModel.addToPresetIsAllowed());
    }

    private void updateName() {
        String presetName = mPresetItemModel.presetName.get();
        if (TextUtils.isEmpty(presetName)) {
            name.set(mActivity.getString(R.string.empty));
        } else {
            name.set(presetName);
            setPlayButtonVisible();
        }
    }

    private void enableOrDisableDeleteButton() {
        String presetName = mPresetItemModel.presetName.get();
        if (TextUtils.isEmpty(presetName)) {
            presetIsEmpty.set(true);
        } else {
            presetIsEmpty.set(false);
        }
    }

    private void updatePresetType() {
        String presetType = mPresetItemModel.presetType.get();
        if (!TextUtils.isEmpty(presetType) && presetType.contentEquals(PresetTypeNames.Spotify)) {
            isSpotify.set(true);
        } else {
            isSpotify.set(false);
            updatePresetSubType();
        }
    }

    private void updatePresetSubType() {
        if (mPresetItemModel.presetSubType == null) {
            return;
        }

        type.set(PresetSubType.getFriendlyNameFromSubType(
                mPresetItemModel.presetSubType.get(), mActivity));
    }

    private void addListenersToPresetItemModel() {
        if (mPresetItemModel == null || mActivity == null) {
            return;
        }

        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mPropertyChangedCallback = new OnPropertyChangedCallback() {
                    @Override
                    public void onPropertyChanged(Observable observable, int i) {
                        if (observable.equals(mPresetItemModel.presetName)) {
                            updateName();
                            updateControlButtonsVisibility();
                        } else if (observable.equals(mPresetItemModel.presetType)) {
                            updatePresetType();
                            checkSpotify();
                        } else if (observable.equals(mPresetItemModel.presetSubType)) {
                            updatePresetSubType();
                        } else if (observable.equals(mPresetItemModel.artworkUrl)) {
                            retrieveArtworkAsync(true);
                            if (((SourcesActivity) mActivity).isPaused()) {
                                enableOrDisableDeleteButton();
                                updateControlButtonsVisibility();
                                updateIsPresetable();
                            }
                        } else if (observable.equals(NowPlayingManager.getInstance().getNowPlayingDataModel().indexOfCurrentlyPlayingPreset) ||
                                observable.equals(NowPlayingManager.getInstance().getNowPlayingDataModel().playStatus)) {
                            updateSpeakerIsBuffering();
                            updateControlButtonsVisibility();
                        } else if (observable.equals(NowPlayingManager.getInstance().getNowPlayingDataModel().playCaps)) {
                            updateIsPresetable();
                        }
                    }
                };
            }
        });


        mPresetItemModel.presetName.addOnPropertyChangedCallback(mPropertyChangedCallback);
        mPresetItemModel.presetType.addOnPropertyChangedCallback(mPropertyChangedCallback);
        mPresetItemModel.presetSubType.addOnPropertyChangedCallback(mPropertyChangedCallback);
        mPresetItemModel.artworkUrl.addOnPropertyChangedCallback(mPropertyChangedCallback);
        mPresetItemModel.isLoading.addOnPropertyChangedCallback(mPropertyChangedCallback);

        NowPlayingDataModel nowPlayingDataModel = NowPlayingManager.getInstance().getNowPlayingDataModel();
        nowPlayingDataModel.indexOfCurrentlyPlayingPreset.addOnPropertyChangedCallback(mPropertyChangedCallback);
        nowPlayingDataModel.playStatus.addOnPropertyChangedCallback(mPropertyChangedCallback);
        nowPlayingDataModel.playCaps.addOnPropertyChangedCallback(mPropertyChangedCallback);
    }

    private void retrieveArtworkAsync(final boolean enableAnimation) {
        if (mActivity != null) {
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (mActivity != null) {
                        retrieveArtwork(enableAnimation);
                    }
                }
            });
        }
    }

    private void retrieveArtwork(boolean enableAnimation) {
        String url = mPresetItemModel.artworkUrl.get();
        if (URLUtil.isValidUrl(url)) {
            retrieveArtworkFromUrl(url, enableAnimation);
            showSpotifyWarning.set(false);
        } else {
            setDrawable(enableAnimation, getPlaceholderDrawable());
            mPresetItemModel.artworkUrl.set("");
        }
    }

    private void retrieveArtworkFromUrl(final String artworkUrl, final boolean enableRotation) {

        FsLogger.log("Retrieving :" + artworkUrl + " hash: " + PresetSourceViewModel.this.hashCode(), LogLevel.Info);

        Glide.with(mApplicationContext)
                .load(artworkUrl)
                .fallback(getPlaceholderDrawable())
                .into(new SimpleTarget<GlideDrawable>() {
                    @Override
                    public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
                        FsLogger.log("Retrieved :" + artworkUrl + " hash: " + PresetSourceViewModel.this.hashCode(), LogLevel.Info);
                        if (mActivity == null || mActivity.isFinishing()) {
                            FsLogger.log("Problem with activity for :" + artworkUrl, LogLevel.Info);
                            return;
                        }

                        if (artwork.get() != null && resource != null) {
                            //noinspection ConstantConditions
                            if (!artwork.get().getConstantState().equals(resource.getConstantState())) {
                                setDrawable(enableRotation, resource);
                            }
                        } else {
                            setDrawable(enableRotation, resource);
                        }
                    }

                    @Override
                    public void onLoadFailed(Exception e, Drawable errorDrawable) {
                        if (mActivity == null || mActivity.isFinishing()) {
                            return;
                        }
                        setDrawable(enableRotation, getPlaceholderDrawable());
                        mPresetItemModel.artworkUrl.set("");
                    }
                });
    }

    private void setDrawable(boolean enableAnimation, Drawable drawable) {
        if (enableAnimation && mPresetItemModel.presetIndex == PresetsManager.getInstance().getCurrentTabIndex()) {
            if (mRotationIsInverted) {
                mRotationIsInverted = false;
                mRotationHandler.startRightToLeftRotation(drawable);
            } else {
                mRotationHandler.startLeftToRightRotation(drawable);
            }
            if (mDeleteButtonPressed && mTheRedoCopy != null) {
                if (mTheRedoCopy.mPresetSubType == SpotifyAlbum || mTheRedoCopy.mPresetSubType == SpotifyPlaylist || mTheRedoCopy.mPresetSubType == SpotifyArtist) {
                    mPresetAnimationHandler.startAnimationForPresetPlaylistDelete(null);
                }
                if (mTheRedoCopy.mPresetSubType == InternetRadio) {
                    mPresetAnimationHandler.startAnimationForPresetRadioStationDelete(null);
                }
                mDeleteButtonPressed = false;
            } else //noinspection StatementWithEmptyBody
                if (mRedoButtonPressed || mUndoButtonPressed) {
                // Do nothing here. Check if upload completed or not
            } else {
                NodeSysCapsValidModes.Mode currentMode = NowPlayingManager.getInstance().getNowPlayingDataModel().currentMode.get();
                startPresetAnimation(currentMode, true);
            }
        } else {
            artwork.set(drawable);
        }
    }

    private boolean isSpotifyAlbum() {
        return mPresetItemModel != null && mPresetItemModel.presetSubType.get() == SpotifyAlbum;
    }

    @SuppressWarnings("unused")
    public void onAddPresetClicked(View view) {
        mCountOfPendingAnimations++;
        tryAddingCurrentlyPlayingSourceToPreset();
    }

    void onAddLongPress() {
        if (mPresetItemModel.presetIndex == 0) {
            NavigationHelper.goToActivity(mActivity, ChoosePresetsTypeActivity.class,
                    NavigationHelper.AnimationType.SlideToLeft);
        }
        if (mPresetItemModel.presetIndex == 1) {
            NavigationHelper.goToActivity(mActivity, UpdateActivity.class,
                    NavigationHelper.AnimationType.SlideToLeft);
        }
    }

    @SuppressWarnings("unused")
    public void onPlayClicked(View view) {
        NowPlayingManager.getInstance().getNowPlayingDataModel().setAlbumCover(null);
        if (TextUtils.isEmpty(mPresetItemModel.presetName.get())) {
            mPresetAnimationHandler.startAnimationForPlayingEmptyPreset(null);
        } else {
            if (NowPlayingManager.getInstance().getNowPlayingDataModel().indexOfCurrentlyPlayingPreset.get() == mPresetItemModel.presetIndex) {
                NowPlayingManager.getInstance().play();
            } else {
                if (mPresetItemModel.presetSubType.get() != InternetRadio && mPresetItemModel.presetSubType.get() != Empty) {
                    checkSpotify();
                }
                play();
            }
        }
    }

    private void checkSpotify() {
        String presetType = mPresetItemModel.presetType.get();

        if (!TextUtils.isEmpty(presetType) && presetType.contentEquals(PresetTypeNames.Spotify)) {
            SpotifyAuthenticator spotifyAuthenticator = SpotifyManager.getInstance().getSpotifyAuthenticator();

            if (isSpotifyLinked()) {
                if (spotifyAuthenticator.isSpotifyAccountExpired()) {
                    spotifyAuthenticator.refreshToken(new ISpotifyRefreshAuthorizationListener() {
                        @Override
                        public void onAccessTokenRefreshed(boolean result) {
                            if (isSpotifyAlbum()) {
                                showSpotifyWarning.set(!result);
                            }
                        }
                    });
                }
            } else {
                showSpotifyWarning.set(true);
            }
        }
    }

    private void play() {
        SourcesManager.getInstance().selectPresetAsync(mPresetItemModel.presetIndex, new RadioNodeUtil.INodeSetResultListener() {
            @Override
            public void onNodeSetResult(boolean success) {
                // Ignore this
            }
        });
    }

    private Bundle getExtraForSportify() {
        Bundle bundle = new Bundle();
        bundle.putInt("SCOPE", AuthenticationScope.AUTHENTICATION_AT_THE_END.ordinal());
        return bundle;
    }

    @SuppressWarnings("unused")
    public void logInToSpotify(View view) {
        NavigationHelper.goToActivity(mActivity, ActivityFactory.getActivityFactory().getSpotifyAuthenticationActivity(), NavigationHelper.AnimationType.SlideToLeft, false, getExtraForSportify());
    }

    @SuppressWarnings("unused")
    public void onPauseClicked(View view) {
        NowPlayingManager.getInstance().pause();
    }

    @SuppressWarnings("unused")
    public void onDeletePreset(View view) {
        if (!TextUtils.isEmpty(mPresetItemModel.presetName.get()) &&
                !TextUtils.isEmpty(mPresetItemModel.presetType.get())) {
            mCountOfPendingAnimations++;
            mDeleteButtonPressed = true;
            deletePreset();
        }
    }

    private void tryAddingCurrentlyPlayingSourceToPreset() {
        if (isPresetable.get()) {
            addCurrentlyPlayingSourceToPreset(mPresetItemModel);
        } else {
            showInfoDialog();
        }
    }

    private void deletePreset() {
        mPresetItemModel.presetSubType.set(Empty);
        SourcesManager.getInstance().deletePresetAtPositionAsyncWithListener(mPresetItemModel.presetIndex, new RadioNodeUtil.INodeSetResultListener() {
            @Override
            public void onNodeSetResult(final boolean success) {
                if (mActivity == null) {
                    return;
                }
                
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (success) {
                            enableRedoButtonIfPossible();
                            String type = mPresetItemModel.presetType.get();
                            if (type.equals("Spotify")) {
                                mPresetAnimationHandler.startAnimationForPresetPlaylistDelete(null);
                            } else if (type.equals("IR")) {
                                mPresetAnimationHandler.startAnimationForPresetRadioStationDelete(null);
                            }
                        }
                    }
                });
            }
        });
        showSpotifyWarning.set(false);
    }


    private void restorePresetAsync(final PresetSourceModelCopy copy) {
        AsyncTask restoreTask = new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] params) {
                restorePreset(copy);
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                PresetSourceViewModel.this.mPresetItemModel.blob = copy.mBlob;
                PresetSourceViewModel.this.mPresetItemModel.presetName.set(copy.mPresetName);
                PresetSourceViewModel.this.mPresetItemModel.presetType.set(copy.mPresetType);
                PresetSourceViewModel.this.mPresetItemModel.artworkUrl.set(copy.mArtworkUrl);
                PresetSourceViewModel.this.mPresetItemModel.presetSubType.set(copy.mPresetSubType);

                NowPlayingManager.getInstance().getNowPlayingDataModel().indexOfCurrentlyPlayingPreset.set(copy.mIndexOfCurrentlyPlayingPreset);
            }
        };

        restoreTask.execute();
    }

    private void restorePreset(final PresetSourceModelCopy copy) {
        if (copy == null) {
            return;
        }

        NodeNavPresetUploadBlob nodeUploadBlob = new NodeNavPresetUploadBlob(copy.mBlob);
        NodeNavPresetUploadName nodeNavPresetUploadName = new NodeNavPresetUploadName(copy.mPresetName);
        NodeNavPresetUploadType nodeNavPresetUploadType = new NodeNavPresetUploadType(copy.mPresetType);
        NodeNavPresetUploadArtworkUrl nodeNavPresetUploadArtworkUrl = new NodeNavPresetUploadArtworkUrl(copy.mArtworkUrl);
        NodeNavPresetUploadUpload nodeNavPresetUploadUpload = new NodeNavPresetUploadUpload((long) copy.mPresetIndex);

        NodeInfo[] nodes = new NodeInfo[]{nodeUploadBlob, nodeNavPresetUploadName,
                nodeNavPresetUploadType, nodeNavPresetUploadArtworkUrl, nodeNavPresetUploadUpload};

        String lastSelectedDeviceSN = ConnectionManager.getInstance().getLastSelectedDeviceSN();
        Radio radio = NetRemoteManager.getInstance().getRadioFromSN(lastSelectedDeviceSN);

        setNodesInfo(radio, nodes, true, copy);
    }

    private void setNodesInfo(Radio radio, NodeInfo[] nodes, boolean blocking, final PresetSourceModelCopy copy) {
        try {
            radio.setNodes(nodes, blocking, new ISetNodesCallback() {
                @Override
                public void onResult(Map<Class, NodeResponse> nodeResponses) {
                    boolean success = true;
                    for (NodeResponse nodeResp : nodeResponses.values()) {
                        if (nodeResp.mType == NodeResponse.NodeResponseType.NODE) {
                            FsLogger.log("SetNodesSuccess: " + nodeResp.mNodeInfo.getName() + " value= " + nodeResp.mNodeInfo.toString());
                            PresetSourceViewModel.this.isPlayButtonVisible.set(copy.mIsPlayButtonVisible);
                            PresetSourceViewModel.this.isPauseButtonVisible.set(copy.mIsPauseButtonVisible);
                            PresetSourceViewModel.this.isStopButtonVisible.set(copy.mIsStopButtonVisible);
                        } else if (nodeResp.mError.getErrorCode() == ErrorResponse.ErrorCode.NetworkProblem ||
                                nodeResp.mError.getErrorCode() == ErrorResponse.ErrorCode.Error ||
                                nodeResp.mError.getErrorCode() == ErrorResponse.ErrorCode.NetworkTimeout) {
                            FsLogger.log("SetNodesError: " + nodeResp.mError.toString(), LogLevel.Error);
                            success = false;
                        }
                    }

                    if (mActivity == null) {
                        return;
                    }

                    if (copy.mArtwork.getConstantState() == getPlaceholderDrawable().getConstantState()) {
                        return;
                    }

                    // Show animation for user
                    startUndoRedoAnimation(success);
                    if (success) {
                        mActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mRotationHandler.startRightToLeftRotation(copy.mArtwork);
                            }
                        });
                    }
                }
            });
        } catch (NullPointerException e) {
            FsLogger.log("setNodesInfo: " + e, LogLevel.Error);
        }
    }

    private void startUndoRedoAnimation(final boolean success) {
        if (mCountOfPendingAnimations == 0) {
            return;
        }
        mCountOfPendingAnimations--;

        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mUndoButtonPressed) {
                    mPresetAnimationHandler.startAnimationForUndo(new IPresetAnimationFinishedListener() {
                        @Override
                        public void onPresetAnimationFinished() {
                            mUndoButtonPressed = false;
                            mRotationIsInverted = true;
                        }
                    }, success);
                    retrieveArtworkAsync(true);
                }
                if (mRedoButtonPressed) {
                    mPresetAnimationHandler.startAnimationForRedo(new IPresetAnimationFinishedListener() {
                        @Override
                        public void onPresetAnimationFinished() {
                            mRedoButtonPressed = false;
                            mRotationIsInverted = true;
                        }
                    }, success);
                }
            }
        });
    }

    private void addCurrentlyPlayingSourceToPreset(final PresetItemModel presetItemModel) {
        final NodeSysCapsValidModes.Mode currentMode = NowPlayingManager.getInstance().getNowPlayingDataModel().currentMode.get();

        SourcesManager.getInstance().setPresetStatusListener(new IPresetStatus() {
            @Override
            public void onPresetResponse(final boolean success) {
                if (mActivity == null) {
                    return;
                }

                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (mActivity == null) {
                            SourcesManager.getInstance().setPresetStatusListener(null);
                            return;
                        }
                        if (!success) {
                            startPresetAnimation(currentMode, false);
                        }

                        if (success) {
                            enableUndoButtonIfPossible();
                            presetIsEmpty.set(false);
                        }

                        SourcesManager.getInstance().setPresetStatusListener(null);
                    }
                });
            }
        });

        SourcesManager.getInstance().addCurrentlyPlayingSourceToPreset(presetItemModel.presetIndex, new RadioNodeUtil.INodeSetResultListener() {

            @Override
            public void onNodeSetResult(boolean success) {
                if (success) {
                    if (mActivity == null) {
                        return;
                    }

                    mActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            enableUndoButtonIfPossible();
                            if (!isSpotifyLinked() && currentMode == NodeSysCapsValidModes.Mode.Spotify) {
                                setDrawable(true, getPlaceholderDrawable());
                                mPresetItemModel.artworkUrl.set("");
                            }
                        }
                    });
                } else {
                    startPresetAnimation(currentMode, false);
                }

                if (mActivity == null) {
                    return;
                }

                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (!isSpotifyLinked() && currentMode == NodeSysCapsValidModes.Mode.Spotify) {
                            showSpotifyWarning.set(true);
                        }
                    }
                });
            }
        });
    }

    private void startPresetAnimation(final NodeSysCapsValidModes.Mode currentMode, boolean success) {
        if (mCountOfPendingAnimations == 0) {
            return;
        }

        mCountOfPendingAnimations--;

        if (currentMode.compareTo(NodeSysCapsValidModes.Mode.IR) == 0) {
            mPresetAnimationHandler.startAnimationForPresetRadioStationAdded(null, success);
        } else if (currentMode.compareTo(NodeSysCapsValidModes.Mode.Spotify) == 0) {
            mPresetAnimationHandler.startAnimationForPresetPlaylistAdded(null, success);
        }
    }

    private void showInfoDialog() {
        if (fragment != null && fragment.isVisible()) {
            return;
        }

        fragment = AddingPresetFailedFragment.newInstance();
        FragmentManager fragmentManager = ((FragmentActivity) mActivity).getSupportFragmentManager();
        fragment.show(fragmentManager, "dialog");
    }

    private void updateControlButtonsVisibility() {
        NowPlayingDataModel nowPlayingDataModel = NowPlayingManager.getInstance().getNowPlayingDataModel();
        int indexOfCurrentlySelectedPreset = NowPlayingManager.getInstance().getNowPlayingDataModel().indexOfCurrentlyPlayingPreset.get();

        hidePlayPauseStopButtons();

        if (nowPlayingDataModel.isPlaying()) {
            if (indexOfCurrentlySelectedPreset == mPresetItemModel.presetIndex) {
                if (nowPlayingDataModel.currentMode.get() == NodeSysCapsValidModes.Mode.IR) {
                    setStopButtonVisible();
                } else if (nowPlayingDataModel.currentMode.get() == NodeSysCapsValidModes.Mode.Spotify) {
                    setPauseButtonVisible();
                }
            } else {
                setPlayButtonVisible();
            }
        } else {
            setPlayButtonVisible();
        }
    }

    private void setPauseButtonVisible() {
        isPlayButtonVisible.set(false);
        isPauseButtonVisible.set(true);
        isStopButtonVisible.set(false);
    }

    private void setPlayButtonVisible() {
        isPlayButtonVisible.set(true);
        isPauseButtonVisible.set(false);
        isStopButtonVisible.set(false);
    }

    private void setStopButtonVisible() {
        isPlayButtonVisible.set(false);
        isPauseButtonVisible.set(false);
        isStopButtonVisible.set(true);
    }

    private void hidePlayPauseStopButtons() {
        isPlayButtonVisible.set(false);
        isPauseButtonVisible.set(false);
        isStopButtonVisible.set(false);
    }

    private Drawable getPlaceholderDrawable() {
        return ContextCompat.getDrawable(mActivity, R.drawable.ic_no_artwork);
    }

    private boolean isSpotifyLinked() {
        SpotifyManager spotifyManager = SpotifyManager.getInstance();

        return spotifyManager.getSpotifyAuthenticator().isSpotifyLinked();
    }

    private void enableUndoButtonIfPossible() {
        hideRedoButtonIfNeeded();

        if (mTheUndoCopy != null && mTheUndoCopy.mBlob.compareTo(mPresetItemModel.blob) != 0) {
            mTheUndoCopy = new PresetSourceModelCopy(this);
        } else if (mTheUndoCopy == null) {
            mTheUndoCopy = new PresetSourceModelCopy(this);
        }

        if (!undoRedoIsSupported()) {
            return;
        }

        mPresetAnimationHandler.showUndoButton();

        mUndoTimer = new Timer();
        mUndoTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (mActivity == null) {
                    return;
                }
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mPresetAnimationHandler.hideUndoButton();
                        mUndoTimer = null;
                    }
                });
            }
        }, 5250);
    }

    private boolean undoRedoIsSupported() {
        boolean isSupported = true;
        NowPlayingDataModel nowPlayingDataModel = NowPlayingManager.getInstance().getNowPlayingDataModel();
        // Do not show undo button if there was nothing on that preset before this
        if (mTheUndoCopy != null && !mTheUndoCopy.undoRedoIsSupported()) {
            isSupported = false;
            // if the preset name is the same with current play name
        } else if (nowPlayingDataModel.playInfoName.get().equals(mPresetItemModel.presetName.get()) ||
                nowPlayingDataModel.spotifyPlaylistName.get().equals(mPresetItemModel.presetName.get())) {
            isSupported = false;
            // if is IR and is stopped
        } else if ((nowPlayingDataModel.currentMode.get() == NodeSysCapsValidModes.Mode.IR) && nowPlayingDataModel.isStopped()) {
            isSupported = false;
        }

        return isSupported;
    }

    private void hideRedoButtonIfNeeded() {
        if (mRedoTimer != null) {
            mRedoTimer.cancel();
            mRedoTimer = null;

            mPresetAnimationHandler.hideRedoButton();
            mTheRedoCopy = null;
        }
    }

    private void enableRedoButtonIfPossible() {
        hideUndoButtonIfNeeded();

        if (mTheRedoCopy != null && mTheRedoCopy.mBlob.compareTo(mPresetItemModel.blob) != 0) {
            mTheRedoCopy = new PresetSourceModelCopy(this);
        } else if (mTheRedoCopy == null) {
            mTheRedoCopy = new PresetSourceModelCopy(this);
        }

        // Do not show Redo button if there was nothing on that preset before this
        if (!mTheRedoCopy.undoRedoIsSupported()) {
            return;
        }

        mPresetAnimationHandler.showRedoButton();

        mRedoTimer = new Timer();
        mRedoTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (mActivity == null) {
                    return;
                }
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mPresetAnimationHandler.hideRedoButton();
                        enableOrDisableDeleteButton();
                        mRedoTimer = null;
                    }
                });
            }
        }, 5250);
    }

    private void hideUndoButtonIfNeeded() {
        if (mUndoTimer != null) {
            mUndoTimer.cancel();
            mUndoTimer.purge();
            mUndoTimer = null;

            mPresetAnimationHandler.hideUndoButton();
            mTheUndoCopy = null;
        }
    }

    public void onUndoButtonPressed(@SuppressWarnings("UnusedParameters") View view) {
        mCountOfPendingAnimations++;
        mUndoButtonPressed = true;

        if (mUndoTimer != null) {
            mUndoTimer.cancel();
            mUndoTimer = null;
        }
        mPresetAnimationHandler.hideUndoButton();

        if (mTheUndoCopy != null) {
            restorePresetAsync(mTheUndoCopy);
            mTheUndoCopy = null;
        }
    }

    public void onRedoButtonPressed(@SuppressWarnings("UnusedParameters") View view) {
        mCountOfPendingAnimations++;
        mRedoButtonPressed = true;

        if (mRedoTimer != null) {
            mRedoTimer.cancel();
            mRedoTimer = null;
        }
        mPresetAnimationHandler.hideRedoButton();

        if (!isSpotifyLinked() && mTheRedoCopy.mPresetType.equals(PresetTypeNames.Spotify) ) {
            showSpotifyWarning.set(true);
        }

        if (mTheRedoCopy != null) {
            restorePresetAsync(mTheRedoCopy);
            mTheRedoCopy = null;
        }
    }

    public void dispose() {
        mApplicationContext = null;
        mActivity = null;

        mPresetItemModel.presetName.removeOnPropertyChangedCallback(mPropertyChangedCallback);
        mPresetItemModel.presetType.removeOnPropertyChangedCallback(mPropertyChangedCallback);
        mPresetItemModel.presetSubType.removeOnPropertyChangedCallback(mPropertyChangedCallback);
        mPresetItemModel.artworkUrl.removeOnPropertyChangedCallback(mPropertyChangedCallback);
        mPresetItemModel.isLoading.removeOnPropertyChangedCallback(mPropertyChangedCallback);

        NowPlayingDataModel nowPlayingDataModel = NowPlayingManager.getInstance().getNowPlayingDataModel();
        nowPlayingDataModel.indexOfCurrentlyPlayingPreset.removeOnPropertyChangedCallback(mPropertyChangedCallback);
        nowPlayingDataModel.playStatus.removeOnPropertyChangedCallback(mPropertyChangedCallback);
        nowPlayingDataModel.playCaps.removeOnPropertyChangedCallback(mPropertyChangedCallback);

        mPresetItemModel = null;
        mPropertyChangedCallback = null;

        removeNodeNotificationCallbackListener();
    }
}
