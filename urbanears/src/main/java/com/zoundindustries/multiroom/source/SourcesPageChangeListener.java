package com.zoundindustries.multiroom.source;

import android.support.v4.view.ViewPager;
import android.widget.TextView;

import com.apps_lib.multiroom.R;
import com.apps_lib.multiroom.source.SourceTab;
import com.apps_lib.multiroom.source.SourcesRegistry;

/**
 * Created by nbalazs on 21/01/2017.
 */
class SourcesPageChangeListener implements ViewPager.OnPageChangeListener{

    private SourcesActivity mActivity;
    private TextView mTitleTextView;

    SourcesPageChangeListener(SourcesActivity activity, TextView titleTextView) {
        mActivity = activity;
        mTitleTextView = titleTextView;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onPageSelected(int position) {
        if (mActivity.isFinishing()) {
            return;
        }

        int strCode = -1;
        int virtualPosition = SourcesRegistry.getInstance().getVirtualPosition(position);
        SourceTab sourceTab = SourcesRegistry.getInstance().getSourceTabBasedOnVirtualPosition(virtualPosition);

        switch (sourceTab) {
            case PresetTab:
                strCode = R.string.carousel_preset_caps;
                break;
            case AuxinTab:
                strCode = R.string.carousel_aux_caps;
                break;
            case BluetoothTab:
                strCode = R.string.carousel_bluetooth_caps;
                break;
            case CloudTab:
                strCode = R.string.carousel_cloud_caps;
                break;
        }

        mTitleTextView.setText(strCode);
    }
}
