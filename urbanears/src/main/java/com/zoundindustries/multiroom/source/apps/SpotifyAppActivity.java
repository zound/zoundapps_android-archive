package com.zoundindustries.multiroom.source.apps;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.apps_lib.multiroom.NavigationHelper;
import com.apps_lib.multiroom.UEActivityBase;
import com.apps_lib.multiroom.connection.ConnectionManager;
import com.apps_lib.multiroom.presets.spotify.SpotifyManager;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.components.common.ExternalAppsOpener;
import com.zoundindustries.multiroom.R;
import com.zoundindustries.multiroom.databinding.ActivityAppsSpotifyBinding;

/**
 * Created by lsuhov on 17/05/16.
 */
public class SpotifyAppActivity extends UEActivityBase {

    ActivityAppsSpotifyBinding mBinding;
    private SpotifyManager mSpotifyManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_apps_spotify);

        setupAppBar();
        enableUpNavigation();
        setTitle(getString(R.string.empty_string));
    }

    @Override
    protected void onResume() {
        super.onResume();
        attachActivityToVolumePopup();
        setupControls();
    }

    private void setupControls() {
        mSpotifyManager = SpotifyManager.getInstance();
        if (mSpotifyManager.checkIfSpotifyAppIsInstalled(this)) {
            mBinding.buttonOpenSpotify.setVisibility(View.VISIBLE);
            mBinding.buttonGetSpotify.setVisibility(View.GONE);
        } else {
            mBinding.buttonOpenSpotify.setVisibility(View.GONE);
            mBinding.buttonGetSpotify.setVisibility(View.VISIBLE);
        }

        mBinding.buttonOpenSpotify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ExternalAppsOpener.openSpotify(SpotifyAppActivity.this);
                SpotifyAppActivity.this.finish();
            }
        });

        mBinding.buttonGetSpotify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ExternalAppsOpener.openSpotifyInStore(SpotifyAppActivity.this);
            }
        });

        mBinding.buttonLearnMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavigationHelper.openWebViewActivityForURL(SpotifyAppActivity.this,
                        NavigationHelper.AnimationType.Normal, getString(R.string.carousel_spotify_learn_more_link));
            }
        });

        mBinding.buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        setupSpotifyPresentationText();
    }

    private void setupSpotifyPresentationText() {
        String selectedSpeakerName = "";
        Radio selectedRadio = ConnectionManager.getInstance().getSelectedRadio();
        if (selectedRadio != null) {
            selectedSpeakerName = selectedRadio.getFriendlyName();
        }

        mBinding.textViewSpotifyPresentation.setText(getString(R.string.carousel_spotify_presentation, selectedSpeakerName));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
