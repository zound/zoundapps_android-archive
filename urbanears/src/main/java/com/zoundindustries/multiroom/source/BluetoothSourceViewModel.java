package com.zoundindustries.multiroom.source;

import android.app.Activity;
import android.content.Intent;
import android.databinding.Observable;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.os.AsyncTask;
import android.view.View;

import com.apps_lib.multiroom.connection.ConnectionManager;
import com.frontier_silicon.NetRemoteLib.Node.BaseBluetoothDiscoverableState;
import com.frontier_silicon.NetRemoteLib.Node.NodeBluetoothConnectedDevices;
import com.frontier_silicon.NetRemoteLib.Node.NodeBluetoothConnectedDevicesListVersion;
import com.frontier_silicon.NetRemoteLib.Node.NodeBluetoothDiscoverableState;
import com.frontier_silicon.NetRemoteLib.Node.NodeInfo;
import com.frontier_silicon.NetRemoteLib.Node.NodeListItem;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysCapsValidModes;
import com.frontier_silicon.NetRemoteLib.Radio.INodeNotificationCallback;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.components.NetRemoteManager;
import com.frontier_silicon.components.common.RadioNodeUtil;
import com.frontier_silicon.components.common.TaskHelper;
import com.frontier_silicon.components.common.nodeCommunication.IListNodeListener;

import java.util.List;
import java.util.concurrent.CountDownLatch;

/**
 * Created by lsuhov on 12/05/16.
 */
public class BluetoothSourceViewModel extends SourceViewModelBase {

    public ObservableField<String> connectedDeviceText = new ObservableField<>("");
    public ObservableBoolean isConnectedDeviceAvailable = new ObservableBoolean(false);
    public ObservableBoolean isParingModeEnabled = new ObservableBoolean(false);
    public ObservableBoolean pairingModeButtonEnabled = new ObservableBoolean(true);

    private Activity mActivity;
    private INodeNotificationCallback mNotificationCallback;
    private CheckBTConnectionTask mCheckBTConnectionTask;

    private IActiveStatusProvider mActiveStatusProvider;

    BluetoothSourceViewModel(Activity activity, IActiveStatusProvider activeStatusProvider) {
        super(NodeSysCapsValidModes.Mode.Bluetooth);

        mActiveStatusProvider = activeStatusProvider;
        mActivity = activity;
        createNotificationCallback();

        connectedDeviceText.addOnPropertyChangedCallback(new OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable observable, int i) {
                if (connectedDeviceText.get().length() > 0) {
                    isConnectedDeviceAvailable.set(true);
                } else {
                    isConnectedDeviceAvailable.set(false);
                }
            }
        });
    }

    private void createNotificationCallback() {
        mNotificationCallback = new INodeNotificationCallback() {
            @Override
            public void onNodeUpdated(NodeInfo node) {
                if (node instanceof NodeBluetoothDiscoverableState || node instanceof NodeBluetoothConnectedDevicesListVersion) {
                    runCheckBTStateTask();
                }
            }
        };
    }


    @Override
    public void reattachNodeListeners() {
        Radio radio = ConnectionManager.getInstance().getSelectedRadio();

        if (radio != null && mNotificationCallback != null) {
            radio.removeNotificationListener(mNotificationCallback);
            radio.addNotificationListener(mNotificationCallback);
        }

        super.reattachNodeListeners();
    }

    @Override
    public Radio getActionableRadio() {
        return ConnectionManager.getInstance().getSelectedRadio();
    }

    public void onResume() {
        if (mActivity == null || mActivity.isFinishing()) {
            return;
        }

        runCheckBTStateTask();
    }

    @SuppressWarnings("unused")
    public void onEnterPairingModeClicked(View v) {
        if (!mActiveStatusProvider.isActive()) {
            return;
        }
        Radio radio = NetRemoteManager.getInstance().getCurrentRadio();
        if (radio == null) {
            return;
        }

        RadioNodeUtil.setNodeToRadioAsync(radio, new NodeBluetoothDiscoverableState(NodeBluetoothDiscoverableState.Ord.DISCOVERABLE),
                new RadioNodeUtil.INodeSetResultListener() {
                    @Override
                    public void onNodeSetResult(boolean success) {
                        enablePairingMode(success);
                    }
                });
    }

    @SuppressWarnings("unused")
    public void onBluetoothSettingsClicked(View v) {
        Intent intentOpenBluetoothSettings = new Intent();
        intentOpenBluetoothSettings.setAction(android.provider.Settings.ACTION_BLUETOOTH_SETTINGS);
        mActivity.startActivity(intentOpenBluetoothSettings);
    }

    private void runCheckBTStateTask() {
        if (mCheckBTConnectionTask != null) {
            mCheckBTConnectionTask.cancel(true);
            mCheckBTConnectionTask = null;
        }

        Radio selectedRadio = ConnectionManager.getInstance().getSelectedRadio();

        mCheckBTConnectionTask = new CheckBTConnectionTask(selectedRadio);
        TaskHelper.execute(mCheckBTConnectionTask);
    }

    private void updateConnectedDevice(String connectedDeviceName, boolean enablePairingMode) {
        if (connectedDeviceName.length() > 0) {
            isConnectedDeviceAvailable.set(true);
        }

        connectedDeviceText.set(connectedDeviceName);
        enablePairingMode(enablePairingMode);
    }

    private void enablePairingMode(boolean enable) {
        pairingModeButtonEnabled.set(!enable);
        isParingModeEnabled.set(enable);
    }

    public void dispose() {
        mActivity = null;

        Radio selectedRadio = ConnectionManager.getInstance().getSelectedRadio();
        if (selectedRadio != null) {
            selectedRadio.removeNotificationListener(mNotificationCallback);
            mNotificationCallback = null;
        }

        if (mCheckBTConnectionTask != null) {
            mCheckBTConnectionTask.cancel(true);
            mCheckBTConnectionTask = null;
        }

        super.dispose();
    }

    private class CheckBTConnectionTask extends AsyncTask<Void, Void, Void> {

        private Radio mRadio;
        private CountDownLatch mCountDownLatch;
        private String mConnectedRadioName = "";
        private boolean mEnablePairingMode = false;

        CheckBTConnectionTask(Radio radio) {
            mRadio = radio;
            mCountDownLatch = new CountDownLatch(2);
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                checkPairingMode();
                retrieveConnectedDevices();
                mCountDownLatch.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (mEnablePairingMode && mConnectedRadioName.length() > 0) {
                mEnablePairingMode = false;
            }

            BluetoothSourceViewModel.this.updateConnectedDevice(mConnectedRadioName, mEnablePairingMode);

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }

        private void checkPairingMode() {
            if (mRadio == null) {
                enablePairingMode(false);
                return;
            }

            RadioNodeUtil.getNodeFromRadioAsync(mRadio, NodeBluetoothDiscoverableState.class, new RadioNodeUtil.INodeResultListener() {
                @Override
                public void onNodeResult(NodeInfo node) {
                    if (mActivity == null) {
                        return;
                    }

                    NodeBluetoothDiscoverableState stateNode = (NodeBluetoothDiscoverableState) node;

                    mEnablePairingMode = stateNode != null && stateNode.getValueEnum() == BaseBluetoothDiscoverableState.Ord.DISCOVERABLE;

                    mCountDownLatch.countDown();
                }
            });
        }

        private void retrieveConnectedDevices() {
            if (mRadio == null) {
                return;
            }
            RadioNodeUtil.getListNodeItems(mRadio, NodeBluetoothConnectedDevices.class, false, new IListNodeListener() {
                @Override
                public void onListNodeResult(List connectedDevicesList, boolean isListComplete) {
                    if (connectedDevicesList != null) {
                        for (Object obj : connectedDevicesList) {
                            NodeBluetoothConnectedDevices.ListItem deviceListItem = (NodeBluetoothConnectedDevices.ListItem) obj;
                            if (deviceListItem.getDeviceState() == NodeListItem.DeviceState.Connected) {
                                mConnectedRadioName = deviceListItem.getDeviceName();
                            }
                        }
                    }

                    mCountDownLatch.countDown();
                }
            });
        }
    }

    @Override
    public void onActivateClicked(View view) {
        if (!mActiveStatusProvider.isActive()) {
            return;
        }
        super.onActivateClicked(view);
    }
}
