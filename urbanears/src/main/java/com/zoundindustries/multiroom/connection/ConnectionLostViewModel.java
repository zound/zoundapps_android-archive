package com.zoundindustries.multiroom.connection;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.databinding.BaseObservable;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.View;

import com.apps_lib.multiroom.NavigationHelper;
import com.apps_lib.multiroom.R;
import com.apps_lib.multiroom.connection.ConnectionManager;
import com.apps_lib.multiroom.connection.IConnectToSpeakerListener;
import com.apps_lib.multiroom.factory.ActivityFactory;
import com.apps_lib.multiroom.factory.EHomeFragmentTag;
import com.apps_lib.multiroom.speakerImages.ESpeakerImageSizeType;
import com.apps_lib.multiroom.speakerImages.ISpeakerImageIdListener;
import com.apps_lib.multiroom.speakerImages.SpeakerManager;
import com.frontier_silicon.NetRemoteLib.Discovery.IRadioDiscoveryListener;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.components.NetRemoteManager;
import com.frontier_silicon.components.common.DialogsHolder;
import com.frontier_silicon.components.connection.ConnectionState;
import com.frontier_silicon.components.connection.ConnectionStateUtil;
import com.frontier_silicon.components.multiroom.MultiroomDeviceModel;
import com.frontier_silicon.components.multiroom.MultiroomGroupManager;

/**
 * Created by lsuhov on 09/05/16.
 */
public class ConnectionLostViewModel extends BaseObservable implements
        IConnectToSpeakerListener {

    private IRadioDiscoveryListener mRadioDiscoveryListener;
    private Activity mActivity;

    public ObservableField<String> disconnectedMessage = new ObservableField<>();
    public ObservableBoolean connecting = new ObservableBoolean(false);
    public ObservableField<BitmapDrawable> speakerImage = new ObservableField<>();
    public ObservableBoolean reconnectIsVisible = new ObservableBoolean(true);

    public void init(Activity activity) {
        mActivity = activity;

        attachToDiscoveryServiceAndCheckForDisconnectedDevice();
        //  ConnectionStateUtil.getInstance().addListener(this, true);

        Radio radio = getRadioDevice();

        updateSpeakerImage(radio);
        setDisconnectMessage(radio);
        updateReconnectButtonVisibility(radio);
    }

    private void updateReconnectButtonVisibility(Radio radio) {
        reconnectIsVisible.set(radio != null && !radio.isMissing());
    }

    private void updateSpeakerImage(Radio radio) {

        final Resources resources = mActivity.getResources();

        if (radio != null) {
            SpeakerManager.getInstance().retrieveImageIdForRadio(radio, ESpeakerImageSizeType.Large, new ISpeakerImageIdListener() {
                @Override
                public void onImageIdRetrieved(int imageId) {
                    if (mActivity == null) {
                        return;
                    }
                    setSpeakerImage(resources, imageId);
                }
            });
        } else {
            setSpeakerImage(resources, SpeakerManager.getInstance().getImageIdForSSID("", ESpeakerImageSizeType.Large));
        }
    }

    private void setSpeakerImage(final Resources resources, final int imageId) {
        Bitmap speakerImageBitmap = BitmapFactory.decodeResource(resources, imageId);
        BitmapDrawable bitmapDrawable = new BitmapDrawable(resources, speakerImageBitmap);
        speakerImage.set(bitmapDrawable);
    }

    public void clear() {
        detachFromDiscoveryService();
        //  ConnectionStateUtil.getInstance().removeListener(this);
    }

    private void setDisconnectMessage(Radio radio) {

        if (radio != null) {
            int stringId;
            String disconnectedSpeakerName = radio.getFriendlyName();
            ConnectionState connectionState = ConnectionStateUtil.getInstance().getConnectionState();

            if (connectionState == ConnectionState.INVALID_SESSION) {
                stringId = R.string.msg_session_stolen;
            } else {
                stringId = R.string.msg_disconnected;
            }

            disconnectedMessage.set(mActivity.getString(stringId, disconnectedSpeakerName));

        } else {
            disconnectedMessage.set(mActivity.getResources().getString(R.string.others_speaker_disconnected));
        }
    }

    private Radio getRadioDevice() {
        String lastSelectedDeviceSN = ConnectionManager.getInstance().getLastSelectedDeviceSN();
        Radio radio = NetRemoteManager.getInstance().getRadioFromSN(lastSelectedDeviceSN);
        if (radio == null) {
            Bundle extras = mActivity.getIntent().getExtras();
            if (extras != null) {
                String radioSN = extras.getString("LAST_RADIO_SN", "");
                radio = NetRemoteManager.getInstance().getRadioFromSN(radioSN);
            }
        }
        return radio;
    }

    void attachToDiscoveryServiceAndCheckForDisconnectedDevice() {
        detachFromDiscoveryService();

        mRadioDiscoveryListener = new IRadioDiscoveryListener() {
            @Override
            public void onRadioFound(Radio radio) {
                Radio selectedRadio = getRadioDevice();
                updateReconnectButtonVisibility(selectedRadio);
            }

            @Override
            public void onRadioLost(Radio radio) {
                checkIfLostRadioMatchesTheDisconnectedRadio(radio);
            }

            @Override
            public void onRadioUpdated(Radio radio) {
            }
        };

        NetRemoteManager.getInstance().addRadioDiscoveryListener(mRadioDiscoveryListener);
    }

    void detachFromDiscoveryService() {
        if (mRadioDiscoveryListener != null) {
            NetRemoteManager.getInstance().removeRadioDiscoveryListener(mRadioDiscoveryListener);
            mRadioDiscoveryListener = null;
        }
    }

    private void checkIfLostRadioMatchesTheDisconnectedRadio(Radio lostRadio) {
        Radio disconnectedRadio = getRadioDevice();
        if (disconnectedRadio == null || lostRadio.equals(disconnectedRadio)) {

            reconnectIsVisible.set(false);
        }
    }

    public void onGoToHomeClicked(View view) {
        NavigationHelper.goToFragment(mActivity, EHomeFragmentTag.ScanExistingSpeakers);
    }

    public void onReconnectClicked(View view) {
        Radio radio = getRadioDevice();
        if (radio == null) {
            showUnsuccessfulReconnectDialogError();
            return;
        }

        MultiroomDeviceModel deviceModel = MultiroomGroupManager.getInstance().getDeviceByUdn(radio.getUDN());
        if (deviceModel == null) {
            showUnsuccessfulReconnectDialogError();
        } else {
            boolean startedConnection = ConnectionManager.getInstance().connect(deviceModel, false, this);
            if (startedConnection) {
                connecting.set(true);
            }
        }
    }

    @Override
    public void onConnectedToServerOrRadio(boolean connectedOk, Radio selectedRadio) {
        if (!connectedOk) {
            if (selectedRadio != null) {
                showUnsuccessfulReconnectDialogError(selectedRadio.getFriendlyName());
            } else {
                showUnsuccessfulReconnectDialogError();
            }
            connecting.set(false);

        } else {
            // remove this from onStateUpdate because we should wait to get presets first and after show the SourcesActivity
            goToSourcesActivity();

        }
    }

//    @Override
//    public void onStateUpdate(ConnectionState newState) {
//        switch (newState) {
//            case CONNECTED_TO_RADIO:
//
//               goToSourcesActivity();
//                break;
//
//            default:
//                break;
//        }
//    }

    private void goToSourcesActivity() {
        Bundle bundle = new Bundle();
        bundle.putBoolean(ActivityFactory.SPEAKER_JUST_CONNECTED, true);

        NavigationHelper.goToActivity(mActivity, ActivityFactory.getActivityFactory().getSourcesActivity(),
                NavigationHelper.AnimationType.SlideToLeft, false, bundle);
    }

    private void showUnsuccessfulReconnectDialogError(String radioName) {
        if (mActivity == null || mActivity.isFinishing()) {
            return;
        }

        showUnsuccessfulReconnectDialogImpl(mActivity.getString(R.string.cannot_reconnect_to_radio, radioName));
    }

    private void showUnsuccessfulReconnectDialogError() {
        if (mActivity == null || mActivity.isFinishing()) {
            return;
        }

        showUnsuccessfulReconnectDialogImpl(mActivity.getString(R.string.cannot_reconnect_to_radio_error));
    }

    private void showUnsuccessfulReconnectDialogImpl(final String message) {
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String mDialogKey = "unsuccessful_reconnect_dialog";
                AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
                builder.setMessage(message);
                builder.setNeutralButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        onGoToHomeClicked(null);
                    }
                });

                AlertDialog alertDialog = builder.create();
                alertDialog.show();

                DialogsHolder.addDialogToCollection(mDialogKey, alertDialog);
            }
        });
    }

    public void dispose() {
        mActivity = null;
        detachFromDiscoveryService();
    }
}
