package com.zoundindustries.multiroom.presets.spotify;

import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;

import com.apps_lib.multiroom.NavigationHelper;
import com.apps_lib.multiroom.R;
import com.zoundindustries.multiroom.help.PresentationActivity;


/**
 * Created by nbalazs on 12/10/2016.
 */

public class FailedToSavePresetViewModel {

    private AddingPresetFailedFragment mDialog;

    FailedToSavePresetViewModel(AddingPresetFailedFragment dialog) {
        mDialog = dialog;
    }

    @SuppressWarnings("unused")
    public void onGotItClicked(View view) {
        mDialog.dismiss();
    }

    @SuppressWarnings("unused")
    public void onReadMoreClicked(View view) {
        mDialog.dismiss();
        Resources res = mDialog.getActivity().getResources();
        final String array[] = new String[]{res.getString(R.string.help_speaker_knobs), res.getString(R.string.help_presets), res.getString(R.string.help_solo_multi)};

        Bundle bundle = new Bundle();
        bundle.putString("PAGE_TO_LOAD", array[1]);
        NavigationHelper.goToActivity(mDialog.getActivity(), PresentationActivity.class, NavigationHelper.AnimationType.SlideToLeft, false, bundle);
    }
}
