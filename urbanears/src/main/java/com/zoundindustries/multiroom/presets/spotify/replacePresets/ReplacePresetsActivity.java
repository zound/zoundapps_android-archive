package com.zoundindustries.multiroom.presets.spotify.replacePresets;

import android.annotation.SuppressLint;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.apps_lib.multiroom.NavigationHelper;
import com.apps_lib.multiroom.UEActivityBase;
import com.apps_lib.multiroom.presets.PresetItemModel;
import com.apps_lib.multiroom.source.IPresetStatus;
import com.apps_lib.multiroom.source.SourcesManager;
import com.frontier_silicon.components.common.RadioNodeUtil;
import com.frontier_silicon.components.connection.ConnectionState;
import com.frontier_silicon.components.connection.ConnectionStateUtil;
import com.frontier_silicon.components.connection.IConnectionStateListener;
import com.zoundindustries.multiroom.R;
import com.zoundindustries.multiroom.databinding.ActivityReplacePresetsBinding;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lsuhov on 05/07/16.
 */
public class ReplacePresetsActivity extends UEActivityBase implements IConnectionStateListener {

    private ActivityReplacePresetsBinding mBinding;

    private List<PresetItemModel> mPresetsList;
    private ReplacePresetsListAdapter mAdapter;
    private ProgressBar mProgressBar;
    private View mBackgroundView = null;
    private View mForegroundView = null;
    private int mReplacedPresetPosition = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_replace_presets);

        setupAppBar();
        enableUpNavigation();

        setTitle(R.string.adding_preset_caps);
        attachActivityToVolumePopup();
        setupControls();
    }

    @Override
    protected void onResume() {
        super.onResume();

        ConnectionStateUtil.getInstance().addListener(this, true);

        SourcesManager.getInstance().setPresetStatusListener(new IPresetStatus() {
            @Override
            public void onPresetResponse(boolean success) {
                mAdapter.presetWasReplacedAt(mReplacedPresetPosition);
                onPresetReplaced(success);
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();

        SourcesManager.getInstance().setPresetStatusListener(null);
        ConnectionStateUtil.getInstance().removeListener(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mAdapter.onDestroy();
    }

    private void setupControls() {
        mPresetsList = new ArrayList<>();
        mAdapter = new ReplacePresetsListAdapter(this, R.layout.list_item_setup_preset, mPresetsList);

        addHeaderAndFooterView();
        mBinding.presetsListView.setAdapter(mAdapter);
        mBinding.presetsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                if (view != null) {
                    mReplacedPresetPosition = position - 1;
                    mBackgroundView = view.findViewById(R.id.artworkAnimationViewBackground);
                    mForegroundView = view.findViewById(R.id.artworkAnimationViewForeground);
                    saveCurrentlyPlayingToPosition(position - 1, view);  // -1 comes from HeaderView
                }
            }
        });

        retrievePresets();
    }

    public void addHeaderAndFooterView() {
        @SuppressLint("InflateParams") View listHeaderView = LayoutInflater.from(this).inflate(R.layout.list_item_header, null);
        @SuppressLint("InflateParams") View listFooterView = LayoutInflater.from(this).inflate(R.layout.list_item_footer, null);
        mBinding.presetsListView.addHeaderView(listHeaderView, null, false);
        mBinding.presetsListView.addFooterView(listFooterView, null, false);
    }

    private void saveCurrentlyPlayingToPosition(final int position, View view) {
        showLoadingSpinner(view);
        addCurrentlyPlayingSourceToPreset(position);
    }

    private void addCurrentlyPlayingSourceToPreset(final int position) {
        SourcesManager.getInstance().addCurrentlyPlayingSourceToPreset(position,
                new RadioNodeUtil.INodeSetResultListener() {
                    @Override
                    public void onNodeSetResult(boolean success) {
                        hideLoadingSpinner();
                        if (!success) {
                            onPresetReplaced(false);
                        } else {
                            mAdapter.presetWasReplacedAt(position);

                        }
                    }
                });
    }

    private void retrievePresets() {
        List<PresetItemModel> newList = SourcesManager.getInstance().mPresetItemModels;

        mPresetsList.clear();
        mPresetsList.addAll(newList);

        mAdapter.notifyDataSetChanged();
    }

    private void showLoadingSpinner(View view) {
        mProgressBar = (ProgressBar) view.findViewById(R.id.progressActivating);
        mProgressBar.setVisibility(View.VISIBLE);
    }

    private void hideLoadingSpinner() {
        if (mProgressBar != null) {
            mProgressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStateUpdate(ConnectionState newState) {
        switch (newState) {
            case DISCONNECTED:
            case INVALID_SESSION:
            case NOT_CONNECTED_TO_RADIO:
            case NO_WIFI_OR_ETHERNET:
                NavigationHelper.goToHome(this);
                break;
        }
    }

    private void animateModifiedPreset(final View backgroundView, final View foregroundView) {

        Animation fadeInAnimationTo70 = AnimationUtils.loadAnimation(ReplacePresetsActivity.this, R.anim.fade_in_to_70);
        Animation fadeInAnimationTo100 = AnimationUtils.loadAnimation(ReplacePresetsActivity.this, R.anim.fade_in_to_100);
        Animation scaleDownTo1 = AnimationUtils.loadAnimation(ReplacePresetsActivity.this, R.anim.scale_down_to_1);

        AnimationSet animationSet70 = new AnimationSet(false);
        animationSet70.addAnimation(fadeInAnimationTo70);

        AnimationSet animationSet100 = new AnimationSet(false);
        animationSet100.addAnimation(fadeInAnimationTo100);
        animationSet100.addAnimation(scaleDownTo1);

        animationSet70.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                Animation fadeOutAnimationFrom70 = AnimationUtils.loadAnimation(ReplacePresetsActivity.this, R.anim.fade_out_from_70);
                Animation fadeOutSlowerAnimationFrom100 = AnimationUtils.loadAnimation(ReplacePresetsActivity.this, R.anim.fade_out_from_100_slower);
                Animation scaleDownAnimationTo0 = AnimationUtils.loadAnimation(ReplacePresetsActivity.this, R.anim.scale_down_to_0);

                AnimationSet animationSet100 = new AnimationSet(false);
                animationSet100.addAnimation(fadeOutSlowerAnimationFrom100);
                animationSet100.addAnimation(scaleDownAnimationTo0);

                backgroundView.startAnimation(fadeOutAnimationFrom70);
                foregroundView.startAnimation(animationSet100);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        backgroundView.startAnimation(animationSet70);
        foregroundView.startAnimation(animationSet100);
    }

    private void onPresetReplaced(boolean replaced) {
        int imageID = R.drawable.ic_preset_failed;

        if (replaced) {
            retrievePresets();
            imageID = R.drawable.ic_preset_succeeded;
        }

        ImageView imageView = (ImageView) mForegroundView;

        if (imageView != null) {
            imageView.setImageDrawable(ContextCompat.getDrawable(ReplacePresetsActivity.this, imageID));
        }

        animateModifiedPreset(mBackgroundView, mForegroundView);
    }
}
