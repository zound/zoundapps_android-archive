package com.zoundindustries.multiroom;

import android.app.Activity;
import android.util.DisplayMetrics;

/**
 * Created by cvladu on 27/11/2017.
 */

public class GuiUtilsUrbanears {

    public static int dpToPx(int dp, Activity activity) {
        float value =0;
        if (activity != null) {
            DisplayMetrics displayMetrics = activity.getResources().getDisplayMetrics();
            float density = displayMetrics.density;
            value = dp * density;
        }

        return Math.round(value);
    }
}
