package com.zoundindustries.multiroom.factory;

import com.apps_lib.multiroom.factory.ActivityCreator;
import com.zoundindustries.multiroom.myHome.HomeActivity;
import com.zoundindustries.multiroom.about.AboutScreenActivity;
import com.zoundindustries.multiroom.castTos.CastTosIntroductionActivity;
import com.zoundindustries.multiroom.settings.solo.speaker.SettingsSpeakerActivity;
import com.zoundindustries.multiroom.setup.normalSetup.FinalSetupActivity;
import com.zoundindustries.multiroom.setup.normalSetup.ScanNewSpeakersActivity;
import com.zoundindustries.multiroom.setup.presets.PresetsAddingFailedActivity;
import com.zoundindustries.multiroom.setup.presets.PresetsListActivity;
import com.zoundindustries.multiroom.setup.presets.SetPresetsActivity;
import com.zoundindustries.multiroom.setup.presets.SpotifyAuthenticationActivity;
import com.zoundindustries.multiroom.setup.presets.SpotifyLinkingSuccessActivity;
import com.zoundindustries.multiroom.source.SourcesActivity;
import com.zoundindustries.multiroom.volume.VolumeActivity;

/**
 * Created by nbalazs on 10/05/2017.
 */

public class UrbanearsActivityCreator extends ActivityCreator {

    @Override
    public final Class getHomeActivity() {
        return HomeActivity.class;
    }

    @Override
    public final Class getSetupActivity() {
        return ScanNewSpeakersActivity.class;
    }

    @Override
    public final Class getVolumeActivity() {
        return VolumeActivity.class;
    }

    public Class getSourcesActivity() { return SourcesActivity.class; }

    @Override
    public Class getSettingsActivity() {
        return SettingsSpeakerActivity.class;
    }

    @Override
    public Class getAboutScreenActivity() {
        return AboutScreenActivity.class;
    }

    @Override
    public Class getCastTosActivity() { return CastTosIntroductionActivity.class; }

    @Override
    public Class getPresetAddingFailedActivity() {
        return PresetsAddingFailedActivity.class;
    }

    @Override
    public Class getSetPresetsActivity() {
        return SetPresetsActivity.class;
    }

    @Override
    public Class getFinalSetupActivity() {
        return FinalSetupActivity.class;
    }

    @Override
    public Class getPresetsListActivity() { return PresetsListActivity.class; }

    @Override
    public Class getSpotifyLinkingSuccessActivity() {
        return SpotifyLinkingSuccessActivity.class;
    }

    @Override
    public Class getSpotifyAuthenticationActivity() {
        return SpotifyAuthenticationActivity.class;
    }
}
