package com.zoundindustries.multiroom.speakerImages;

import com.apps_lib.multiroom.speakerImages.ESpeakerModelType;
import com.apps_lib.multiroom.speakerImages.SpeakerTypeDecoder;

/**
 * Created by nbalazs on 09/05/2017.
 */

public class UrbanearsSpeakerTypeDecoder extends SpeakerTypeDecoder {

    public UrbanearsSpeakerTypeDecoder() {}

    @Override
    public ESpeakerModelType getSpeakerModelFromSSID(String ssid) {
        return getUrbanearsSpeakerModel(ssid, "");
    }

    @Override
    public ESpeakerModelType getSpeakerModelFromModelName(String modelName, String softwareVersion) {
        ESpeakerModelType speakerModelType = getUrbanearsSpeakerModel(modelName, softwareVersion);

        if (speakerModelType == ESpeakerModelType.Unknown) {
            speakerModelType = getMarshallSpeakerModel(modelName);
        }

        return  speakerModelType;
    }

    public static boolean isUrbanearsSpeaker(String modelName) {

        if (getUrbanearsSpeakerModel(modelName, "") == ESpeakerModelType.Unknown) {
            return false;
        }

        return true;
    }
}
