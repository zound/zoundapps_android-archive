package com.zoundindustries.multiroom.castTos;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.apps_lib.multiroom.NavigationHelper;
import com.apps_lib.multiroom.UEActivityBase;
import com.frontier_silicon.components.common.CommonPreferences;
import com.zoundindustries.multiroom.R;
import com.zoundindustries.multiroom.databinding.ActivityCastTermsAndConditionsBinding;

/**
 * Created by lsuhov on 30/05/16.
 */
public class CastTermsAndConditionsActivity extends UEActivityBase {

    private ActivityCastTermsAndConditionsBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_cast_terms_and_conditions);

        setupAppBar();
        enableUpNavigation();
        setTitle(R.string.terms_conditions_caps);

        setupControls();
    }

    private void setupControls() {
        setupGoogleLinks();
        setupUrbanearsLinks();

        setupAcceptButton();
    }

    private void setupUrbanearsLinks() {
        String ueLicenseAgreement = getString(R.string.end_user_license_agreement);
        String ueLicenseAgreementDescription = getString(R.string.urbanears_license_agreement_description, ueLicenseAgreement);

        int startIndex = ueLicenseAgreementDescription.lastIndexOf(ueLicenseAgreement);
        int endIndex = startIndex + ueLicenseAgreement.length();

        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(ueLicenseAgreementDescription);
        spannableStringBuilder.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                NavigationHelper.openWebViewActivityForURL(CastTermsAndConditionsActivity.this, NavigationHelper.AnimationType.Normal, getString(R.string.link_connected_eula));
            }
        }, startIndex, endIndex, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        mBinding.urbanearsAgreementDescription.setMovementMethod(LinkMovementMethod.getInstance());
        mBinding.urbanearsAgreementDescription.setText(spannableStringBuilder, TextView.BufferType.SPANNABLE);
    }

    private void setupAcceptButton() {
        mBinding.buttonAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonPreferences.getInstance().setCastTosAcceptance(true);
                NavigationHelper.goToHome(CastTermsAndConditionsActivity.this);
                finish();
            }
        });
    }

    private void setupGoogleLinks() {
        String googleTosText = getString(R.string.google_terms_of_service);
        String googlePrivacyText = getString(R.string.google_privacy_policy);
        String googleTosDescription = getString(R.string.google_tos_and_privacy_description, googleTosText, googlePrivacyText);

        int startTosIndex = googleTosDescription.indexOf(googleTosText);
        int endTosIndex = startTosIndex + googleTosText.length();

        int startPrivacyIndex = googleTosDescription.lastIndexOf(googlePrivacyText);
        int endPrivacyIndex = startPrivacyIndex + googlePrivacyText.length();

        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(googleTosDescription);
        if (startPrivacyIndex <= endPrivacyIndex && startPrivacyIndex >= 0) {
            spannableStringBuilder.setSpan(new ClickableSpan() {
                @Override
                public void onClick(View widget) {
                    NavigationHelper.openWebViewActivityForURL(CastTermsAndConditionsActivity.this, NavigationHelper.AnimationType.Normal, getString(R.string.google_tos_link));
                }
            }, startTosIndex, endTosIndex, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

            spannableStringBuilder.setSpan(new ClickableSpan() {
                @Override
                public void onClick(View widget) {
                    NavigationHelper.openWebViewActivityForURL(CastTermsAndConditionsActivity.this, NavigationHelper.AnimationType.Normal, getString(R.string.google_privacy_link));
                }
            }, startPrivacyIndex, endPrivacyIndex, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }

        mBinding.googleCastTosDescriptionTextView.setMovementMethod(LinkMovementMethod.getInstance());
        mBinding.googleCastTosDescriptionTextView.setText(spannableStringBuilder, TextView.BufferType.SPANNABLE);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
