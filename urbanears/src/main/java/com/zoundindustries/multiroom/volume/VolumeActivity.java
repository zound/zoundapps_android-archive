package com.zoundindustries.multiroom.volume;

import android.databinding.DataBindingUtil;
import android.databinding.Observable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CompoundButton;

import com.apps_lib.multiroom.UEActivityBase;
import com.apps_lib.multiroom.util.ScreenshootManager;
import com.apps_lib.multiroom.volume.VolumeSpeakersMediator;
import com.apps_lib.multiroom.volume.VolumesManager;
import com.frontier_silicon.components.connection.ConnectionState;
import com.frontier_silicon.components.connection.ConnectionStateUtil;
import com.frontier_silicon.components.connection.IConnectionStateListener;
import com.zoundindustries.multiroom.R;
import com.zoundindustries.multiroom.databinding.ActivityVolumeBinding;

/**
 * Created by lsuhov on 21/06/16.
 */
public class VolumeActivity extends UEActivityBase implements IConnectionStateListener {

    private ActivityVolumeBinding mBinding;
    private VolumeSpeakersMediator mVolumeSpeakersMediator;
    private Observable.OnPropertyChangedCallback mPropertyChangedCallback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_volume);

        setupControls();
    }

    private void setupControls() {
        Drawable backgroundDrawable = ScreenshootManager.getLastScreenshoot();
        if (backgroundDrawable == null) {
            backgroundDrawable = ContextCompat.getDrawable(this, R.drawable.bg_volume_window);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            mBinding.rootView.setBackground(backgroundDrawable);
        } else {
            //noinspection deprecation
            mBinding.rootView.setBackgroundDrawable(backgroundDrawable);
        }

        mBinding.upButton.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        mBinding.muteAllToggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()

        {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
                VolumesManager.getInstance().toggleMuteAll(checked);
            }
        });

        mPropertyChangedCallback = new Observable.OnPropertyChangedCallback()

        {
            @Override
            public void onPropertyChanged(Observable observable, int i) {
                if (observable == VolumesManager.getInstance().allSpeakersMuted) {
                    updateMuteAllButton();
                }
            }
        }

        ;

        updateMuteAllButton();

        attachActivityToVolumePopup();
    }

    private void updateMuteAllButton() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mBinding.muteAllToggleButton.setChecked(VolumesManager.getInstance().allSpeakersMuted.get());
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();

        mVolumeSpeakersMediator = new VolumeSpeakersMediator();
        mVolumeSpeakersMediator.init(this, mBinding.listVolume);
    }

    @Override
    public void onResume() {
        super.onResume();

        mVolumeSpeakersMediator.attachToDiscovery();

        ConnectionStateUtil.getInstance().addListener(this, true);

        VolumesManager.getInstance().startVolumeTimer();
        VolumesManager.getInstance().allSpeakersMuted.addOnPropertyChangedCallback(mPropertyChangedCallback);
    }

    @Override
    public void onPause() {
        super.onPause();

        mVolumeSpeakersMediator.detachFromDiscovery();

        ConnectionStateUtil.getInstance().removeListener(this);

        VolumesManager.getInstance().stopVolumeTimer();
        VolumesManager.getInstance().allSpeakersMuted.removeOnPropertyChangedCallback(mPropertyChangedCallback);
    }

    @Override
    public void onStop() {
        super.onStop();

        mVolumeSpeakersMediator.dispose();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        overridePendingTransition(0, R.anim.slide_out_up);
    }

    @Override
    public void onStateUpdate(final ConnectionState newState) {
        if (isFinishing()) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (isFinishing()) {
                        return;
                    }

                    switch (newState) {
                        case NO_WIFI_OR_ETHERNET:
                            onBackPressed();
                            break;
                    }
                }
            });
        }
    }
}
