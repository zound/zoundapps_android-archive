package com.zoundindustries.multiroom.setup.normalSetup;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.databinding.DataBindingUtil;
import android.net.DhcpInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import com.apps_lib.multiroom.setup.normalSetup.AccessPointModel;
import com.apps_lib.multiroom.setup.normalSetup.ConfigureFriendlyNameActivity;
import com.apps_lib.multiroom.setup.normalSetup.ConnectToHeadlessAPHelper;
import com.apps_lib.multiroom.setup.normalSetup.IConnectToHeadlessAPListener;
import com.apps_lib.multiroom.setup.normalSetup.INewSpeakersFoundListener;
import com.apps_lib.multiroom.setup.normalSetup.ISetupChildFragment;
import com.apps_lib.multiroom.setup.normalSetup.SetupManager;
import com.frontier_silicon.NetRemoteLib.AccessPointUtil;
import com.frontier_silicon.components.NetRemoteManager;
import com.frontier_silicon.components.common.DialogsHolder;
import com.frontier_silicon.components.common.GuiUtils;
import com.frontier_silicon.components.connection.ConnectionState;
import com.frontier_silicon.components.connection.ConnectionStateUtil;
import com.frontier_silicon.components.connection.IConnectionStateListener;
import com.frontier_silicon.loggerlib.FsLogger;
import com.frontier_silicon.loggerlib.LogLevel;
import com.apps_lib.multiroom.NavigationHelper;
import com.apps_lib.multiroom.myHome.SpeakersDiscoveryManager;
import com.zoundindustries.multiroom.R;
import com.zoundindustries.multiroom.databinding.FragmentScanNewSpeakersBinding;
import com.zoundindustries.multiroom.myHome.HomeActivity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class ScanNewSpeakersFragment extends Fragment implements IConnectionStateListener, ISetupChildFragment {

    public static final String FRAGMENT_TAG = "TAG_SCAN_NEW_SPEAKER_FRAGMENT";
    protected FragmentScanNewSpeakersBinding mBinding;

    protected List<AccessPointModel> mAccessPointsList = new ArrayList<>();
    protected ArrayAdapter<AccessPointModel> mAdapter;

    protected Timer mProgressTimer;
    protected int mCurrentTimeProgressOfRescan = -1;
    protected int NUM_SECONDS_AP_RESCAN = 5;
    protected BroadcastReceiver mWiFiScanReceiver;

    protected Context mAppContext;
    protected IConnectToHeadlessAPListener mConnectToAPListener;
    protected AccessPointModel mSelectedSpeakerAP;
    protected int mNumberOfConnectionAttempts = 0;
    protected SetupManager mSetupManager;
    protected ConnectToHeadlessAPHelper mConnectHelper;
    protected boolean mRefreshButtonWasPressed = false;

    public ScanNewSpeakersFragment() {
        mSetupManager = SetupManager.getInstance();
        mConnectHelper = ConnectToHeadlessAPHelper.getInstance();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_scan_new_speakers, container, false);

        return mBinding.getRoot();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setupControls();

        mAppContext = getActivity().getApplicationContext();
    }

    @Override
    public void onResume() {
        super.onResume();

        mSetupManager.closeRadioConnection();

        registerNetworkListeners();

        if (mConnectToAPListener == null) {
            mConnectToAPListener = new ConnectToAPListener();
        }
        mConnectHelper.setConnectToHeadlessAPListener(mConnectToAPListener);

        startScanForNewSpeakers();

        ConnectionStateUtil.getInstance().addListener(this, false);
    }

    @Override
    public void onPause() {
        super.onPause();

        unregisterNetworkListeners();
        mConnectHelper.removeConnectToHeadlessAPListener();

        stopScanForNewSpeakers();

        ConnectionStateUtil.getInstance().removeListener(this);

        mSelectedSpeakerAP = null;
    }

    private void startScanForNewSpeakers() {
        rescanWiFiAccessPoints();
    }

    private void stopScanForNewSpeakers() {
        cleanScanTimer();
    }

    protected void getAvailableWiFiAccessPoints() {
        List<AccessPointModel> devicesList = SetupManager.getInstance().getCurrentAccessPoints(mAppContext);

        GuiUtils.replaceContentsOfList(mAccessPointsList, devicesList);
        Collections.sort(mAccessPointsList, new AccessPointModel.NameComparator());

        int numDevicesFound = devicesList.size();

        cleanScanTimer();
        updateScanningControls(false);

        mAdapter.notifyDataSetChanged();

        mBinding.textFoundNumberOfSpeakers.setText(getResources().getString(R.string.setup_android_found_speakers, numDevicesFound));

        if (mRefreshButtonWasPressed) {
            mBinding.listNewSpeakers.setSelectionAfterHeaderView();
            mRefreshButtonWasPressed = false;
        }

        notifyParentActivityOnNewSpeakersFound(devicesList);
    }

    private void notifyParentActivityOnNewSpeakersFound(List<AccessPointModel> devicesList) {
        Activity parentActivity = getActivity();
        if (parentActivity != null) {
            try {
                ((INewSpeakersFoundListener) parentActivity).onNewSpeakersFound(devicesList);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void updateScanningControls(boolean forceShowScanning) {
        boolean mScanning = (mProgressTimer != null || forceShowScanning);

        mBinding.searchingForSpeakersLayout.setVisibility(mScanning ? View.VISIBLE : View.GONE);

        mBinding.layoutSpeakersFoundText.setVisibility(mScanning ? View.GONE : View.VISIBLE);
        mBinding.listNewSpeakers.setVisibility(mScanning ? View.GONE : View.VISIBLE);

        if (!mScanning) {
            getActivity().setTitle(R.string.select_a_speaker_caps);
        }
    }

    protected void rescanWiFiAccessPoints() {
        if (!AccessPointUtil.isWifiEnabled()) {
            showNoWiFiDialog();
        } else if (!AccessPointUtil.checkLocationIfAndroid6OrMore(getContext())) {
            showLocationDialog();
        } else {
            cleanScanTimer();

            if (mProgressTimer == null) {
                mProgressTimer = new Timer();
                mProgressTimer.schedule(new ScanWiFiTimerTask(), 0, 1000);

                updateScanningControls(false);

                mAdapter.clear();
                SetupManager.getInstance().startWiFiAccessPointScan();
            }
        }
    }

    private void showNoWiFiDialog() {
        String dialogKey = "hui_no_wifi_dialog";
        if (DialogsHolder.isShowingOrActivityIsFinishing(dialogKey, getActivity())) {
            return;
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.no_wifi);
        builder.setMessage(R.string.offer_enable_wifi);
        builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                WifiManager wifiManager = (WifiManager) mAppContext.getSystemService(Context.WIFI_SERVICE);
                wifiManager.setWifiEnabled(true);
            }
        });
        builder.setNegativeButton(getString(R.string.go_to_settings), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startActivity(new Intent(Settings.ACTION_SETTINGS));
            }
        });

        AlertDialog dlg = builder.create();
        DialogsHolder.addDialogToCollection(dialogKey, dlg);

        dlg.show();
    }

    private void showLocationDialog() {
        String dialogKey = "hui_no_location_dialog";

        if (DialogsHolder.isShowingOrActivityIsFinishing(dialogKey, getActivity())) {
            return;
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.no_location_title);
        builder.setMessage(R.string.no_location_message);

        builder.setPositiveButton(getString(R.string.go_to_settings), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
            }
        });
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(getActivity(), HomeActivity.class);
                startActivity(intent);
            }
        });

        AlertDialog dlg = builder.create();
        DialogsHolder.addDialogToCollection(dialogKey, dlg);

        dlg.show();
    }

    @Override
    public void refreshContent() {
        rescanWiFiAccessPoints();
        mRefreshButtonWasPressed = true;
    }

    class ScanWiFiTimerTask extends TimerTask {

        @Override
        public void run() {
            Activity activity = getActivity();
            if (activity == null) {
                cleanScanTimer();
                return;
            }

            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (mCurrentTimeProgressOfRescan < NUM_SECONDS_AP_RESCAN) {
                        mCurrentTimeProgressOfRescan++;
                    } else {
                        cleanScanTimer();
                        getAvailableWiFiAccessPoints();
                    }
                }
            });
        }
    }

    private void cleanScanTimer() {
        if (mProgressTimer != null) {
            mProgressTimer.cancel();
            mProgressTimer.purge();
        }

        mProgressTimer = null;
        mCurrentTimeProgressOfRescan = -1;
    }

    private void registerNetworkListeners() {
        unregisterNetworkListeners();

        mWiFiScanReceiver = new WifiScanReceiver();
        mAppContext.registerReceiver(mWiFiScanReceiver, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
    }

    private void unregisterNetworkListeners() {
        if (mWiFiScanReceiver != null) {
            mAppContext.unregisterReceiver(mWiFiScanReceiver);
            mWiFiScanReceiver = null;
        }
    }

    private class WifiScanReceiver extends BroadcastReceiver {
        public void onReceive(Context c, Intent intent) {
            getAvailableWiFiAccessPoints();
        }
    }

    private class ConnectToAPListener implements IConnectToHeadlessAPListener {

        @Override
        public void onConnectToApFailed() {
            Activity activity = getActivity();
            if (activity != null) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        FsLogger.log(">>>>>>>> onConnectToApFailed", LogLevel.Error);
                        mSetupManager.resetConnectedToHeadlessAP();

                        mAdapter.notifyDataSetChanged();

                        showCannotConnectDialog();
                    }
                });
            }
        }
    }

    @Override
    public void onStateUpdate(final ConnectionState newState) {
        FsLogger.log(">>>>>>>> STATE UPDATED: " + newState);

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                switch (newState) {
                    case NO_WIFI_OR_ETHERNET:
                        SetupManager.getInstance().closeRadioConnection();
                        mNumberOfConnectionAttempts = 0;
                        break;

                    case CONNECTED_TO_RADIO:
                        onRadioConnected();
                        break;

                    case DISCONNECTED:
                        if (mNumberOfConnectionAttempts < 3) {
                            FsLogger.log("SETUP trying again to reconnect", LogLevel.Error);
                            connectToGatewayRadioDelayed();
                        } else {
                            mSetupManager.resetConnectedToHeadlessAP();

                            if (mSelectedSpeakerAP != null) {
                                CharSequence text = getActivity().getString(R.string.error_connecting_to_radio);
                                GuiUtils.showToast(text, getContext());
                            }
                        }
                        break;

                    default:
                        mConnectHelper.cleanConnectToAPTimer();
                        connectToGatewayRadio();
                        break;
                }

                // refresh list
                mAdapter.notifyDataSetChanged();
            }
        });
    }

    private void onRadioConnected() {
        if (NetRemoteManager.getInstance().checkConnection()) {
            mSetupManager.setConnectingToRadio(false);
            mSetupManager.scanAvailableWiFiNetworks(null);

            if (mSelectedSpeakerAP != null) {
                openNextActivity();
            } else {
                mAdapter.notifyDataSetChanged();
            }
        }
    }

    protected void connectToGatewayRadio() {
        if (AccessPointUtil.isConnectedToHeadlessAP(mAppContext.getResources().getStringArray(R.array.suggested_devices_name_parts_array))) {
            mNumberOfConnectionAttempts++;

            WifiManager wifiManager = (WifiManager) mAppContext.getSystemService(Context.WIFI_SERVICE);
            DhcpInfo dhcpInfo = wifiManager.getDhcpInfo();
            FsLogger.log(">>> GATEWAY: " + AccessPointUtil.integerIPv4ToHumanReadableIPv4(dhcpInfo.serverAddress));

            mSetupManager.connectToMicroAPRadio(AccessPointUtil.integerIPv4ToHumanReadableIPv4(dhcpInfo.serverAddress));
        } else {
            FsLogger.log("SETUP not connecting to speaker", LogLevel.Info);
            mSetupManager.resetConnectedToHeadlessAP();
        }
    }

    private void connectToGatewayRadioDelayed() {
        (new Handler()).postDelayed(new Runnable() {
            @Override
            public void run() {
                connectToGatewayRadio();
            }
        }, 2000);
    }

    protected void openNextActivity() {
        Activity parentActivity = getActivity();
        if (parentActivity != null) {
            NavigationHelper.goToActivity(parentActivity, ConfigureFriendlyNameActivity.class, NavigationHelper.AnimationType.SlideToLeft);
        }
    }

    protected void showCannotConnectDialog() {
        String dialogKey = "hui_cannot_connect";
        Activity parentActivity = getActivity();

        if (parentActivity == null || DialogsHolder.isShowingOrActivityIsFinishing(dialogKey, parentActivity))
            return;

        AlertDialog.Builder builder = new AlertDialog.Builder(parentActivity);
        builder.setTitle(R.string.setup_cannot_connect_title);
        builder.setMessage(getString(R.string.setup_android_cannot_connect_description));
        builder.setPositiveButton(android.R.string.ok, null);

        AlertDialog dialog = builder.create();
        DialogsHolder.addDialogToCollection(dialogKey, dialog);

        dialog.show();
    }

    protected void connectToSelectedAP(AdapterView<?> parent, int position) {
        connectToAP(parent, position);
    }

    protected void setupControls() {

        mBinding.listNewSpeakers.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                SpeakersDiscoveryManager.getInstance().stopDiscovery();
                connectToSelectedAP(parent, position);
            }
        });
        View listHeaderView = LayoutInflater.from(getActivity()).inflate(com.apps_lib.multiroom.R.layout.list_item_header, null);
        View listFooterView = LayoutInflater.from(getActivity()).inflate(com.apps_lib.multiroom.R.layout.list_item_footer, null);
        mBinding.listNewSpeakers.addHeaderView(listHeaderView, null, false);
        mBinding.listNewSpeakers.addFooterView(listFooterView, null, false);
        mAdapter = new AccessPointsArrayAdapter(getActivity(), R.layout.list_item_new_speaker, mAccessPointsList);
        mBinding.listNewSpeakers.setAdapter(mAdapter);
    }

    protected void connectToAP(AdapterView<?> parent, int position) {
        AccessPointModel apModel = (AccessPointModel) parent.getItemAtPosition(position);

        if (apModel == null) {
            return;
        }

        mSelectedSpeakerAP = apModel;

        if (SetupManager.getInstance().isConnectedToGivenAP(apModel.getScanResult())) {
            if (NetRemoteManager.getInstance().checkConnection()) {
                openNextActivity();
            } else {
                mNumberOfConnectionAttempts = 0;
                connectToGatewayRadio();
            }
        } else {
            mNumberOfConnectionAttempts = 0;
            SetupManager.getInstance().setSelectedAccessPoint(apModel);
            ConnectToHeadlessAPHelper.getInstance().tryConnectToAP(mAppContext, apModel);
            mAdapter.notifyDataSetChanged();
        }
    }
}
