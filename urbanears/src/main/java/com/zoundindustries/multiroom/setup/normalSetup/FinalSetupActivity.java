package com.zoundindustries.multiroom.setup.normalSetup;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;

import com.apps_lib.multiroom.NavigationHelper;
import com.apps_lib.multiroom.UEActivityBase;
import com.zoundindustries.multiroom.R;
import com.zoundindustries.multiroom.databinding.ActivitySetupFinalBinding;

/**
 * Created by lsuhov on 04/06/16.
 */
public class FinalSetupActivity extends UEActivityBase {

    private ActivitySetupFinalBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_setup_final);

        setupAppBar();
        setTitle(R.string.all_done);

        setupControls();
    }

    private void setupControls() {
        mBinding.buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavigationHelper.goToHomeAndClearActivityStack(FinalSetupActivity.this);
            }
        });
    }

    @Override
    public void onBackPressed() {

    }
}
