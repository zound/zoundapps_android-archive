package com.zoundindustries.multiroom.setup.normalSetup;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zoundindustries.multiroom.R;
import com.apps_lib.multiroom.util.DisposableTimerTask;
import com.zoundindustries.multiroom.databinding.FragmentNoNewSpeakerFound3Binding;

import java.util.Timer;

/**
 * Created by cvladu on 04/08/16.
 */

public class NoNewSpeakerFound3Fragment extends NoNewSpeakerFoundFragmentBase {

    public static final String FRAGMENT_TAG = "TAG_NO_NEW_SPEAKER_FOUND_3_FRAGMENT";
    private FragmentNoNewSpeakerFound3Binding mBinding;
    private Timer mTimer;
    private DisposableTimerTask mTimerTask;
    private int flag = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_no_new_speaker_found_3, container, false);

        return mBinding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        startTimer();
    }

    private void startTimer() {
        mTimer = new Timer();
        mTimerTask = new ChangeImageResourceTimerTask();
        mTimer.schedule(mTimerTask, 1000, 1000);

    }

    private void stopTimerTask() {
        if (mTimer != null) {
            mTimer.cancel();
            mTimer.purge();
            mTimer = null;
        }
        if (mTimerTask != null) {
            mTimerTask.cancel();
            mTimerTask.dispose();
            mTimerTask = null;
        }
    }

    @Override
    protected void setupControls() {
        mBinding.buttonYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                scanNewSpeaker();
            }
        });
        mBinding.buttonNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToNextPage();
            }
        });
    }

    class ChangeImageResourceTimerTask extends DisposableTimerTask {

        @Override
        public void run() {
            if (mIsDisposed) {
                cancel();
                return;
            }
            if (getActivity() != null) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (getActivity() == null) {
                            return;
                        }

                        if (flag == 0) {
                            mBinding.imagePresets.setImageResource(R.drawable.ic_prests_knob_light_up);
                            flag = 1;
                        } else {
                            mBinding.imagePresets.setImageResource(R.drawable.ic_presets_knob_unselected);
                            flag = 0;
                        }
                    }
                });

            }
        }
    }

    @Override
    public void onPause() {
        stopTimerTask();
        super.onPause();
    }
}
