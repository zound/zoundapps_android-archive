package com.zoundindustries.multiroom.setup.normalSetup;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.apps_lib.multiroom.setup.normalSetup.INoNewSpeakerFoundListener;

/**
 * Created by lsuhov on 10/04/16.
 */
public abstract class NoNewSpeakerFoundFragmentBase extends Fragment {

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setupControls();
    }

    protected abstract void setupControls();

    protected void cancelSetup() {
        INoNewSpeakerFoundListener listener = (INoNewSpeakerFoundListener) getActivity();
        listener.cancelSetup();
    }

    protected void goToNextPage() {
        INoNewSpeakerFoundListener listener = (INoNewSpeakerFoundListener) getActivity();
        listener.goToNextPage();
    }

    protected void scanNewSpeaker() {
        INoNewSpeakerFoundListener listener = (INoNewSpeakerFoundListener) getActivity();
        listener.scanNewSpeakers();
    }

}
