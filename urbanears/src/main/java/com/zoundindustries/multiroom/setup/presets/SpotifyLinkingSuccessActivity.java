package com.zoundindustries.multiroom.setup.presets;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;

import com.apps_lib.multiroom.NavigationHelper;
import com.zoundindustries.multiroom.R;
import com.apps_lib.multiroom.UEActivityBase;
import com.apps_lib.multiroom.factory.ActivityFactory;
import com.apps_lib.multiroom.presets.spotify.SpotifyManager;
import com.zoundindustries.multiroom.databinding.ActivitySetupSpotifyLinkingSuccessBinding;

/**
 * Created by lsuhov on 22/04/16.
 */
public class SpotifyLinkingSuccessActivity extends UEActivityBase {

    private ActivitySetupSpotifyLinkingSuccessBinding mBinding;
    private boolean mIsOpenedFromNormalApp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_setup_spotify_linking_success);

        setupAppBar();

        setTitle(R.string.all_done);

        extractBundleInfo();

        setupControls();
    }

    private void extractBundleInfo() {
        Bundle bundle = getIntent().getExtras();
        if (bundle == null) {
            return;
        }

        mIsOpenedFromNormalApp = bundle.getBoolean(NavigationHelper.EXTERNAL_SPOTIFY_LINKING);
    }

    private void setupControls() {
        String accountName = SpotifyManager.getInstance().getAccountName();

        mBinding.accountNameTextView.setText(accountName);

        mBinding.buttonContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mIsOpenedFromNormalApp) {
                    NavigationHelper.goToHome(SpotifyLinkingSuccessActivity.this);
                } else {
                    NavigationHelper.goToActivity(SpotifyLinkingSuccessActivity.this, ActivityFactory.getActivityFactory().getSetPresetsActivity(),
                            NavigationHelper.AnimationType.SlideToLeft);
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
    }
}
