package com.zoundindustries.multiroom.setup.normalSetup;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.apps_lib.multiroom.NavigationHelper;
import com.apps_lib.multiroom.UEActivityBase;
import com.apps_lib.multiroom.factory.FragmentFactory;
import com.apps_lib.multiroom.setup.normalSetup.AccessPointModel;
import com.apps_lib.multiroom.setup.normalSetup.INewSpeakersFoundListener;
import com.apps_lib.multiroom.setup.normalSetup.INoNewSpeakerFoundListener;
import com.apps_lib.multiroom.setup.normalSetup.ISetupChildFragment;
import com.apps_lib.multiroom.setup.normalSetup.SetupManager;
import com.apps_lib.multiroom.util.PermissionsUtil;
import com.frontier_silicon.loggerlib.FsLogger;
import com.zoundindustries.multiroom.R;

import java.util.List;

/**
 * Created by nbalazs on 15/06/2017.
 */

public class ScanNewSpeakersActivity extends UEActivityBase implements INewSpeakersFoundListener,
        INoNewSpeakerFoundListener {

    private ViewPager mViewPager;
    private View mScanSpeakersFragment;
    private volatile boolean mScanIsRunning = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_scan_new_speakers);
        if (!shouldShowConnectionLostMessage()) {
            loadNewSpeakerFragment();
        }

        setupAppBar();
        enableUpNavigation();

        initViewPager();

        PermissionsUtil.requestLocationPermission(this, true);

        showConnectionLostMessageIfNecessary();
    }

    public void loadNewSpeakerFragment() {
        Class theFragmentClass = FragmentFactory.getInstanece().getScanNewSpeakerFragment();
        try {
            Fragment fragment = (Fragment) theFragmentClass.newInstance();
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.layoutFragmentScanning, fragment);
            transaction.commit();
            if (getSupportActionBar() != null) {
                getSupportActionBar().show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showConnectionLostMessageIfNecessary() {
        Bundle bundle = getIntent().getExtras();
        FsLogger.log("SETUP: checking if bundle was added");

        if (bundle == null)
            return;

        FsLogger.log("SETUP: bundle was added");

        if (bundle.containsKey(NavigationHelper.CONNECTION_LOST_ARG)) {
            ConnectionLostDuringSetupFragment fragment = new ConnectionLostDuringSetupFragment();
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.layoutFragmentScanning, fragment);
            transaction.commit();
            if (getSupportActionBar() != null) {
                getSupportActionBar().hide();
            }
        }
    }

    private boolean shouldShowConnectionLostMessage() {
        Bundle bundle = getIntent().getExtras();

        if (bundle == null) {
            return false;
        } else {
            return true;
        }
    }

    private void initViewPager() {
        NoSpeakerFoundPagerAdapter mNoSpeakerFoundPagerAdapter = new NoSpeakerFoundPagerAdapter(getSupportFragmentManager());
        mViewPager = (ViewPager) findViewById(R.id.viewPagerNoSpeakerFound);
        mViewPager.setAdapter(mNoSpeakerFoundPagerAdapter);
        mViewPager.setEnabled(false);

        mScanSpeakersFragment = findViewById(R.id.layoutFragmentScanning);
    }

    @Override
    public void onNewSpeakersFound(List<AccessPointModel> deviceList) {
        mScanIsRunning = false;
        if (deviceList.isEmpty()) {
            showNoNewSpeakersFoundView();
        } else {
            showScanningNewSpeakersFragment();
        }
        hideShowToolbarImageView(false);
    }

    @Override
    public void goToNextPage() {
        int indexOfPage = mViewPager.getCurrentItem();

        if (indexOfPage == 0) {
            mViewPager.setCurrentItem(1, true);
        } else if (indexOfPage == 1) {
            mViewPager.setCurrentItem(2, true);
        } else if (indexOfPage == 2) {
            mViewPager.setCurrentItem(3, true);
        } else if (indexOfPage == 3) {
            mViewPager.setCurrentItem(2, true);
        }
    }

    @Override
    public void cancelSetup() {
        NavigationHelper.goToHome(this);
    }

    @Override
    public void scanNewSpeakers() {
        showScanningNewSpeakersFragment();
        hideShowToolbarImageView(true);
        scanForNewSpeakers();
    }

    private void showScanningNewSpeakersFragment() {
        mScanSpeakersFragment.setVisibility(View.VISIBLE);
        mViewPager.setVisibility(View.GONE);
        invalidateOptionsMenu();
    }

    private void showNoNewSpeakersFoundView() {
        mViewPager.setCurrentItem(0, false);
        mScanSpeakersFragment.setVisibility(View.GONE);
        mViewPager.setVisibility(View.VISIBLE);
        invalidateOptionsMenu();
        setTitle(com.apps_lib.multiroom.R.string.setup_guide_title);

    }

    private void hideShowToolbarImageView(boolean isShowing) {
        Toolbar toolbar = (Toolbar) findViewById(com.apps_lib.multiroom.R.id.toolbar);
        ImageView img = (ImageView) toolbar.findViewById(com.apps_lib.multiroom.R.id.titleImageView);
        TextView textView = (TextView) toolbar.findViewById(com.apps_lib.multiroom.R.id.titleTextView);
        if (isShowing) {
            textView.setVisibility(View.GONE);
            img.setVisibility(View.VISIBLE);
        } else {
            img.setVisibility(View.GONE);
            textView.setVisibility(View.VISIBLE);
        }
    }

    private void scanForNewSpeakers() {
        mScanIsRunning = true;
        invalidateOptionsMenu();
        FragmentManager fragmentManager = getSupportFragmentManager();
        ISetupChildFragment childFrag = (ISetupChildFragment) fragmentManager.findFragmentById(R.id.layoutFragmentScanning);
        childFrag.refreshContent();
        hideShowToolbarImageView(true);
    }

    private class NoSpeakerFoundPagerAdapter extends FragmentPagerAdapter {

        private static final int NUM_FRAGMENTS = 4;

        private static final int NO_SPEAKER_FOUND_HELP_SCREEN_1_IDX = 0;
        private static final int NO_SPEAKER_FOUND_HELP_SCREEN_2_IDX = 1;
        private static final int NO_SPEAKER_FOUND_HELP_SCREEN_3_IDX = 2;
        private static final int NO_SPEAKER_FOUND_HELP_SCREEN_4_IDX = 3;

        private NoSpeakerFoundPagerAdapter(FragmentManager supportFragmentManager) {
            super(supportFragmentManager);
        }

        @Override
        public Fragment getItem(int position) {
            if (position == NO_SPEAKER_FOUND_HELP_SCREEN_1_IDX) {
                return getNoSpakerFoundFragment(position);
            }

            if (position == NO_SPEAKER_FOUND_HELP_SCREEN_2_IDX) {
                return getNoSpakerFoundFragment(position);
            }

            if (position == NO_SPEAKER_FOUND_HELP_SCREEN_3_IDX) {
                return getNoSpakerFoundFragment(position);
            }

            if (position == NO_SPEAKER_FOUND_HELP_SCREEN_4_IDX) {
                return getNoSpakerFoundFragment(position);
            }

            return null;
        }

        private Fragment getNoSpakerFoundFragment(int position) {
            Class theFragmentClass = FragmentFactory.getInstanece().getNoSpeakerFoundFragmentClass(position);
            Fragment fragment = null;
            try {

                fragment = (Fragment) theFragmentClass.newInstance();

            }catch (Exception e) {
                e.printStackTrace();
            }

            return fragment;
        }

        @Override
        public int getCount() {
            return NUM_FRAGMENTS;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(com.apps_lib.multiroom.R.menu.menu_refresh, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (mViewPager.getVisibility() == View.VISIBLE || mScanIsRunning) {
            menu.findItem(com.apps_lib.multiroom.R.id.action_rescan).setVisible(false);
        } else {
            menu.findItem(com.apps_lib.multiroom.R.id.action_rescan).setVisible(true);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int i = item.getItemId();
        if (i == android.R.id.home) {
            NavigationHelper.goToHomeAndReconnectToInitialWiFi(this);
            return true;
        } else if (i == com.apps_lib.multiroom.R.id.action_rescan) {
            scanForNewSpeakers();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        NavigationHelper.goToHomeAndReconnectToInitialWiFi(this);
        overridePendingTransition(com.apps_lib.multiroom.R.anim.slide_in_right, com.apps_lib.multiroom.R.anim.slide_out_right);
    }

    private String getRadioName() {
        String radioFriendlyName;
        String radioName;
        if (SetupManager.getInstance().getCurrentRadio() != null) {
            radioFriendlyName = SetupManager.getInstance().getCurrentRadio().getFriendlyName();
            radioName = getRadioNameAndMac(radioFriendlyName) + " " + getRadioColor(radioFriendlyName);
        } else {
            radioName = getString(com.apps_lib.multiroom.R.string.starting_selected_audio_device);
        }

        return radioName;
    }

    private String getRadioNameAndMac(String radioFullName) {
        String [] radioName = radioFullName.split("_");
        int length = radioName.length;
        return radioName[0] + " " +radioName[length-1];
    }

    private String getRadioColor(String radioFullName) {
        String [] radioName = radioFullName.split("_");
        int length = radioName.length;
        if (length == 3) {
            return radioName[1];
        } else {
            return radioName[1] + " " + radioName[2];
        }
    }
}