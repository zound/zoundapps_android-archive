package com.zoundindustries.multiroom.nowPlaying;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;

import com.apps_lib.multiroom.NavigationHelper;
import com.apps_lib.multiroom.connection.ConnectionManager;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.components.NetRemoteManager;
import com.frontier_silicon.components.connection.ConnectionState;
import com.frontier_silicon.components.connection.ConnectionStateUtil;
import com.frontier_silicon.components.connection.IConnectionStateListener;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.zoundindustries.multiroom.R;
import com.zoundindustries.multiroom.databinding.ActivityNowPlayingBinding;
import com.zoundindustries.multiroom.source.SourcesActivity;

/**
 * Created by lsuhov on 25/05/16.
 */
public class NowPlayingFragment extends Fragment implements IConnectionStateListener {

    private ActivityNowPlayingBinding mBinding;
    private NowPlayingViewModel mViewModel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.inflate(inflater, R.layout.activity_now_playing, container, false);

        Radio radio = NetRemoteManager.getInstance().getCurrentRadio();

        mViewModel = new NowPlayingViewModel();
        mViewModel.init(getActivity(), radio, mBinding);

        mBinding.setViewModel(mViewModel);

        setTitle();

        setupControls();

        View rootView = mBinding.getRoot();

        rootView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });


        mBinding.nowPlayingContent.setEnabled(true);

        return mBinding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        ConnectionStateUtil.getInstance().addListener(this, true);
        mViewModel.updateSeekBarPositionForSpotify();
        mViewModel.setPaused(false);
        mViewModel.updateProgressTimer();
    }

    @Override
    public void onPause() {
        super.onPause();

        ConnectionStateUtil.getInstance().removeListener(this);
        mBinding.getViewModel().setPaused(true);
        mBinding.getViewModel().stopProgressTimer();
    }

    @Override
    public void onDestroy() {
        mBinding.getViewModel().dispose();
        mBinding.setViewModel(null);
        mBinding.buttonDown.setOnClickListener(null);
        mBinding.seekBarPlayPosition.setOnSeekBarChangeListener(null);

        super.onDestroy();
    }

    private void setupControls() {
        mBinding.buttonDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((SourcesActivity) getActivity()).mBinding.slidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
            }
        });

        mBinding.seekBarPlayPosition.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                mViewModel.setIsUpdatedByUser(true);
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                mViewModel.setIsUpdatedByUser(false);
                mBinding.getViewModel().seekPlaybackToPos(seekBar.getProgress());
            }
        });

        mBinding.buttonVolume.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavigationHelper.goToVolumeActivity(getActivity());
            }
        });

        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        };

        mBinding.sourceLayout.setOnClickListener(onClickListener);
        mBinding.sourceName.setOnClickListener(onClickListener);
        mBinding.sourceName.setClickable(true);
        mBinding.imageSource.setOnClickListener(onClickListener);
    }

    private void setTitle() {
        mBinding.titleTextView.setText(ConnectionManager.getInstance().getNameOfSelectedSpeakerUpperCase());
    }

//    @Override
//    public void onBackPressed() {
//        super.onBackPressed();
//
//        overridePendingTransition(0, R.anim.slide_out_down);
//    }

    @Override
    public void onStateUpdate(ConnectionState newState) {
        switch (newState) {
            case DISCONNECTED:
            case INVALID_SESSION:
            case NOT_CONNECTED_TO_RADIO:
            case NO_WIFI_OR_ETHERNET:
                NavigationHelper.goToHome(getActivity());
                getActivity().finish();
                break;
        }
    }
}
