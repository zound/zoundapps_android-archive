package com.zoundindustries.multiroom.nowPlaying;

import android.app.Activity;
import android.databinding.Observable;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.apps_lib.multiroom.NavigationHelper;
import com.apps_lib.multiroom.R;
import com.apps_lib.multiroom.connection.ConnectionManager;
import com.apps_lib.multiroom.nowPlaying.AlbumCoverDownloader;
import com.apps_lib.multiroom.nowPlaying.IAlbumCoverListener;
import com.apps_lib.multiroom.nowPlaying.NowPlayingManager;
import com.apps_lib.multiroom.presets.PresetItemModel;
import com.apps_lib.multiroom.source.SourcesManager;
import com.apps_lib.multiroom.util.BlurBuilder;
import com.apps_lib.multiroom.util.DisposableTimerTask;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysCapsValidModes;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.components.NetRemoteManager;
import com.frontier_silicon.components.common.ExternalAppsOpener;
import com.frontier_silicon.components.common.GuiUtils;
import com.frontier_silicon.loggerlib.FsLogger;
import com.frontier_silicon.loggerlib.LogLevel;
import com.zoundindustries.multiroom.browse_ir.BrowseIRActivity;
import com.zoundindustries.multiroom.databinding.ActivityNowPlayingBinding;
import com.zoundindustries.multiroom.presets.spotify.replacePresets.ReplacePresetsActivity;

import java.util.Timer;

/**
 * Created by lsuhov on 25/05/16.
 * <p>
 * Extending NowPlayingFooterViewModel because all its functionality is needed
 */
public class NowPlayingViewModel extends NowPlayingFooterViewModel implements IAlbumCoverListener {

    private static final long PROGRESS_UPDATE_TIME_MS = 1000;
    private boolean mNextPreviousButtonClicked = false;

    private Observable.OnPropertyChangedCallback mNowPlayingPropertyChangedCallback;
    private Timer mProgressTimer;
    private ProgressTimerTask mProgressTimerTask;
    private BitmapDrawable mNoArtworkDrawable;

    private volatile boolean mIsUpdatable = true;
    private volatile boolean mIsUpdatedByUser = false;

    public ObservableField<String> sourceName = new ObservableField<>();
    public ObservableField<String> castingAppName = new ObservableField<>();
    public ObservableBoolean castingAppNameIsVisible = new ObservableBoolean(false);
    public ObservableBoolean auxLayoutIsVisible = new ObservableBoolean(false);
    public ObservableField<Drawable> artwork = new ObservableField<>();
    public ObservableField<Drawable> blurredBackground = new ObservableField<>();
    public ObservableBoolean browseStationsIsVisible = new ObservableBoolean(false);
    public ObservableBoolean addAsPresetIsVisible = new ObservableBoolean(false);
    public ObservableField<String> friendlyDuration = new ObservableField<>();
    public ObservableField<String> friendlyElapsedTime = new ObservableField<>();
    public ObservableInt elapsedTime = new ObservableInt();
    public ObservableBoolean isElapsedTimeVisible = new ObservableBoolean(false);
    public ObservableBoolean isRepeatVisible = new ObservableBoolean(false);
    public ObservableBoolean isRepeatChecked = new ObservableBoolean(false);
    public ObservableBoolean isShuffleVisible = new ObservableBoolean(false);
    public ObservableBoolean isShuffleChecked = new ObservableBoolean(false);
    public ObservableBoolean isPrevButtonVisible = new ObservableBoolean(false);
    public ObservableBoolean isNextButtonVisible = new ObservableBoolean(false);
    public ObservableBoolean isSeekBarBarVisible = new ObservableBoolean(false);
    public ObservableBoolean isProgressBarVisible = new ObservableBoolean(false);
    public ObservableBoolean isSourceNameVisible = new ObservableBoolean(false);
    public ObservableBoolean isInMultiMode = new ObservableBoolean(false);

    private Handler mImageDownloaderTask = null;

    private ActivityNowPlayingBinding mBinding;

    public void init(Activity activity, Radio radio, ActivityNowPlayingBinding binding) {
        super.init(radio, activity);

        mBinding = binding;

        Bitmap bitmapNoArtwork = BitmapFactory.decodeResource(activity.getResources(), R.drawable.ic_no_artwork);
        mNoArtworkDrawable = new BitmapDrawable(activity.getResources(), bitmapNoArtwork);

        NodeSysCapsValidModes.Mode currentMode = NowPlayingManager.getInstance().getNowPlayingDataModel().currentMode.get();
        if (currentMode == NodeSysCapsValidModes.Mode.IR || currentMode == NodeSysCapsValidModes.Mode.Spotify || currentMode == NodeSysCapsValidModes.Mode.Bluetooth) {
            Bitmap theAlbumCover = NowPlayingManager.getInstance().getNowPlayingDataModel().getAlbumCover();
            if (theAlbumCover != null) {
                Bitmap theBlurredAlbumCover = BlurBuilder.getInstance().blurImage(0.2f, theAlbumCover, 15);
                BitmapDrawable theAlbumCoverDrawable = new BitmapDrawable(mActivity.getResources(), theAlbumCover);
                BitmapDrawable theAlbumCoverBlurredDrawable = new BitmapDrawable(mActivity.getResources(), theBlurredAlbumCover);
                artwork.set(theAlbumCoverDrawable);
                blurredBackground.set(theAlbumCoverBlurredDrawable);
            } else {
                Bitmap theBlurredAlbumCover = BlurBuilder.getInstance().blurImage(0.2f, bitmapNoArtwork, 15);
                artwork.set(mNoArtworkDrawable);
                blurredBackground.set(new BitmapDrawable(mActivity.getResources(), theBlurredAlbumCover));
            }
        } else if (currentMode == NodeSysCapsValidModes.Mode.Standby
                || currentMode == NodeSysCapsValidModes.Mode.Audsync
                || currentMode == NodeSysCapsValidModes.Mode.None
                || currentMode == NodeSysCapsValidModes.Mode.UnknownMode) {
            Bitmap theBlurredAlbumCover = BlurBuilder.getInstance().blurImage(0.2f, bitmapNoArtwork, 15);
            artwork.set(mNoArtworkDrawable);
            blurredBackground.set(new BitmapDrawable(mActivity.getResources(), theBlurredAlbumCover));
        }

        isInMultiMode.set(ConnectionManager.getInstance().isInMultiMode());
    }

    @Override
    protected void addListeners() {
        super.addListeners();

        mNowPlayingPropertyChangedCallback = new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable observable, int i) {
                if (mIsUpdatable) {
                    if (observable == mNowPlayingDataModel.playInfoText ||
                            observable == mNowPlayingDataModel.album ||
                            observable == mNowPlayingDataModel.artist) {
                        updateAlbumCover();
                        updateProgressTimer();
                    } else if (observable == mNowPlayingDataModel.duration) {
                        updateDuration();
                    } else if (observable == mNowPlayingDataModel.playPosition) {
                        updateElapsedTime();
                    } else if (observable == mNowPlayingDataModel.isRepeat) {
                        updateRepeatButton();
                    } else if (observable == mNowPlayingDataModel.isShuffle) {
                        updateShuffleButton();
                    } else if (observable == mNowPlayingDataModel.playCaps) {
                        updatePreviousButton();
                        updateNextButton();
                        updateSeekBarVisibility();
                        updateProgressBarVisibility();
                    } else if (observable == mNowPlayingDataModel.spotifyPlaylistName) {
                        updateSpotifyPlaylistName();
                    }
                }
            }
        };

        mNowPlayingDataModel.playInfoText.addOnPropertyChangedCallback(mNowPlayingPropertyChangedCallback);
        mNowPlayingDataModel.album.addOnPropertyChangedCallback(mNowPlayingPropertyChangedCallback);
        mNowPlayingDataModel.artist.addOnPropertyChangedCallback(mNowPlayingPropertyChangedCallback);
        mNowPlayingDataModel.playStatus.addOnPropertyChangedCallback(mNowPlayingPropertyChangedCallback);
        mNowPlayingDataModel.duration.addOnPropertyChangedCallback(mNowPlayingPropertyChangedCallback);
        mNowPlayingDataModel.isRepeat.addOnPropertyChangedCallback(mNowPlayingPropertyChangedCallback);
        mNowPlayingDataModel.isShuffle.addOnPropertyChangedCallback(mNowPlayingPropertyChangedCallback);
        mNowPlayingDataModel.playCaps.addOnPropertyChangedCallback(mNowPlayingPropertyChangedCallback);
        mNowPlayingDataModel.playPosition.addOnPropertyChangedCallback(mNowPlayingPropertyChangedCallback);
        mNowPlayingDataModel.spotifyPlaylistName.addOnPropertyChangedCallback(mNowPlayingPropertyChangedCallback);
        mNowPlayingDataModel.currentMode.addOnPropertyChangedCallback(mNowPlayingPropertyChangedCallback);
    }

    private void updateSpotifyPlaylistName() {
        if (!mIsUpdatable) {
            return;
        }

        NodeSysCapsValidModes.Mode mode = mNowPlayingDataModel.currentMode.get();
        if (mode == null) {
            sourceName.set("");
        } else if (mode == NodeSysCapsValidModes.Mode.Spotify && !TextUtils.isEmpty(mNowPlayingDataModel.spotifyPlaylistName.get())) {
            sourceName.set(mNowPlayingDataModel.spotifyPlaylistName.get());
        }
    }

    void updateSeekBarPositionForSpotify() {
        NodeSysCapsValidModes.Mode currentMode = NowPlayingManager.getInstance().getNowPlayingDataModel().currentMode.get();

        if (currentMode == NodeSysCapsValidModes.Mode.Spotify) {
            NowPlayingManager.getInstance().updatePlayPositionAsync();
        }
    }

    private void updateElapsedTime() {
        if (!mIsUpdatable) {
            return;
        }

        long elapsedTimeValue = mNowPlayingDataModel.playPosition.get();
        friendlyElapsedTime.set(GuiUtils.getMilisecAsTimeString(elapsedTimeValue));
        updateProgressBar(elapsedTimeValue);
        updateProgressBarVisibility();
    }

    private void updateProgressBar(long elapsedTimeValue) {
        if (!mIsUpdatedByUser && mIsUpdatable) {
            elapsedTime.set(getProgressPercent(elapsedTimeValue, mNowPlayingDataModel.duration.get()));
        }
    }

    private int getProgressPercent(long position, long duration) {
        float posNorm = 0;

        if (duration > 0) {
            posNorm = (float) position / (float) duration;
        }

        return (int) (posNorm * 100);
    }

    private void updateDuration() {
        long durationValue = mNowPlayingDataModel.duration.get();

        friendlyDuration.set(GuiUtils.getMilisecAsTimeString(durationValue));
        isElapsedTimeVisible.set(durationValue > 0);
    }

    @Override
    protected void updateAllObservables() {
        if (!mIsUpdatable) {
            return;
        }

        super.updateAllObservables();

        updateDuration();
        updateElapsedTime();
        updateAlbumCover();
        updateRepeatButton();
        updateShuffleButton();
        updatePreviousButton();
        updateNextButton();
        updateSeekBarVisibility();
        updateProgressBarVisibility();
        updateSpotifyPlaylistName();
        updateSourceNameVisibility();
        updateSeekBarPositionForSpotify();
        updateAddAsPresetIsVisible();
    }

    private void updateSourceNameVisibility() {
        isSourceNameVisible.set(mNowPlayingDataModel.currentMode.get() == NodeSysCapsValidModes.Mode.Spotify);
    }

    void setIsUpdatedByUser(boolean isUpdatedByUser) {
        mIsUpdatedByUser = isUpdatedByUser;
    }

    private void updateSeekBarVisibility() {
        isSeekBarBarVisible.set(mNowPlayingDataModel.isSeekBarAvailable() && isElapsedTimeVisible.get());
    }

    private void updateProgressBarVisibility() {
        isProgressBarVisible.set(!mNowPlayingDataModel.isSeekBarAvailable() && isElapsedTimeVisible.get());
    }

    private void updateNextButton() {
        isNextButtonVisible.set(mNowPlayingDataModel.isNextButtonAvailable());
    }

    private void updatePreviousButton() {
        isPrevButtonVisible.set(mNowPlayingDataModel.isPrevButtonAvailable());
    }

    private void updateRepeatButton() {
        isRepeatVisible.set(mNowPlayingDataModel.isRepeatAvailable());
        isRepeatChecked.set(mNowPlayingDataModel.isRepeat.get());
    }

    private void updateShuffleButton() {
        isShuffleVisible.set(mNowPlayingDataModel.isShuffleAvailable());
        isShuffleChecked.set(mNowPlayingDataModel.isShuffle.get());
    }

    @Override
    protected void updateSelectedSource() {
        if (!mIsUpdatable) {
            return;
        }

        super.updateSelectedSource();
        updateSourceName();
        updateLayoutVisibility();
        updateBrowseStationsVisibility();
        updateAddAsPresetIsVisible();
        updateCastingAppName();
        updateSourceNameVisibility();
    }

    private void updateCastingAppName() {
        if (!mIsUpdatable) {
            return;
        }

        NodeSysCapsValidModes.Mode mode = mNowPlayingDataModel.currentMode.get();
        if (mode != null && mode != NodeSysCapsValidModes.Mode.Cast && mode != NodeSysCapsValidModes.Mode.UnknownMode) {
            if (mode == NodeSysCapsValidModes.Mode.Spotify) {
                isSmallSpotifyLogoVisible.set(true);
                castingAppName.set(null);
                castingAppNameIsVisible.set(false);
            } else {
                castingAppName.set(getModeForUEApp(mode));
                castingAppNameIsVisible.set(true);
                isSmallSpotifyLogoVisible.set(false);
            }
        } else {
            castingAppName.set(mNowPlayingDataModel.castingApp.get());
            castingAppNameIsVisible.set(true);
            isSmallSpotifyLogoVisible.set(false);
        }
    }

    private String getModeForUEApp(NodeSysCapsValidModes.Mode mode) {
        String displayMode;
        switch (mode) {
            case AuxIn:
                displayMode = mActivity.getString(R.string.carousel_aux_caps);
                break;
            default:
                //noinspection deprecation
                displayMode = mode.toString();
        }

        return displayMode;
    }

    private void updateAddAsPresetIsVisible() {
        if (!mIsUpdatable) {
            return;
        }

        addAsPresetIsVisible.set((mNowPlayingDataModel.isInternetRadioMode() ||
                mNowPlayingDataModel.isSpotifyMode()) && sourceName.get() != null && sourceName.get().length() > 0 &&
                ((firstLine.get() != null && firstLine.get().length() > 0) || (secondLine.get() != null && secondLine.get().length() > 0)));
    }

    private void updateBrowseStationsVisibility() {
        if (!mIsUpdatable) {
            return;
        }

        browseStationsIsVisible.set(mNowPlayingDataModel.isInternetRadioMode());
    }

    private void updateLayoutVisibility() {
        if (!mIsUpdatable) {
            return;
        }

        auxLayoutIsVisible.set(mNowPlayingDataModel.isAuxInMode());
    }

    private void updateSourceName() {
        if (!mIsUpdatable) {
            return;
        }

        int positionOfPreset = mNowPlayingDataModel.indexOfCurrentlyPlayingPreset.get();
        if (positionOfPreset >= 0) {
            PresetItemModel presetItemModel = SourcesManager.getInstance().getPresetAtPosition(positionOfPreset);

            if (presetItemModel != null) {
                String presetName = presetItemModel.presetName.get();

                if (!TextUtils.isEmpty(presetName)) {
                    sourceName.set(presetName);
                    return;
                }
            }
        }

        NodeSysCapsValidModes.Mode mode = mNowPlayingDataModel.currentMode.get();
        if (mode == null) {
            sourceName.set("");
            return;
        }

        if (mode == NodeSysCapsValidModes.Mode.Spotify && !TextUtils.isEmpty(mNowPlayingDataModel.album.get())) {
            sourceName.set(mNowPlayingDataModel.album.get());
            isSourceNameVisible.set(true);
            return;
        }

        int stringId = -1;
        switch (mode) {
            case AirPlay:
                stringId = R.string.airplay;
                break;
            case AuxIn:
                stringId = R.string.carousel_aux_caps;
                break;
            case Bluetooth:
                stringId = R.string.bluetooth;
                break;
            case IR:
                stringId = R.string.setup_internet_radio;
                break;
            case Spotify:
                break;
            case Cast:
                stringId = R.string.common_chromecast_built_in;
                break;
        }

        String source = stringId != -1 ? mActivity.getString(stringId) : "";
        sourceName.set(source);
    }

    private void updateAlbumCover() {
        if (!mIsUpdatable) {
            return;
        }

        if (mImageDownloaderTask == null) {
            mImageDownloaderTask = new Handler(Looper.getMainLooper());
            mImageDownloaderTask.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Radio radio = NetRemoteManager.getInstance().getCurrentRadio();
                    if (canGetAlbumCover() && radio != null) {
                        AlbumCoverDownloader coverDownloader = new AlbumCoverDownloader();
                        coverDownloader.getAlbumArtwork(radio, NowPlayingViewModel.this, mActivity.getApplicationContext());
                    }
                    mImageDownloaderTask = null;
                }
            }, 100);
        }
    }

    private boolean canGetAlbumCover() {
        if (mNowPlayingDataModel == null) {
            return false;
        }
        
        boolean result = !(mNowPlayingDataModel.isError() ||
                mNowPlayingDataModel.currentMode.get() == NodeSysCapsValidModes.Mode.AuxIn);

        result &= (mNowPlayingDataModel.isPlaying() || mNowPlayingDataModel.isPaused() ||
                mNowPlayingDataModel.isBuffering() || mNowPlayingDataModel.isStopped());

        return result;
    }

    @Override
    public void onAlbumCoverUpdate(Bitmap albumCover) {
        if (mActivity == null || !mIsUpdatable) {
            return;
        }

        BitmapDrawable drawable = (BitmapDrawable) artwork.get();
        Bitmap previousBitmap = drawable != null ? drawable.getBitmap() : null;

        if (albumCover == null) {
            if (drawable != mNoArtworkDrawable) {
                setImageAnimatedOnMainThread(mBinding.artworkImageView, artwork, mNoArtworkDrawable);
                setBackgroundBlurredArtwork(mNoArtworkDrawable.getBitmap());
                FsLogger.log("setting empty artwork", LogLevel.Error);
            } else {
                FsLogger.log("empty artwork already set", LogLevel.Error);
            }
        } else if (previousBitmap != albumCover) {
            BitmapDrawable newDrawable = new BitmapDrawable(mActivity.getResources(), albumCover);
            setImageAnimatedOnMainThread(mBinding.artworkImageView, artwork, newDrawable);
            setBackgroundBlurredArtwork(albumCover);
            FsLogger.log("setting new artwork", LogLevel.Error);
        }
    }

    private void setBackgroundBlurredArtwork(Bitmap albumCover) {
        boolean useBlurredArtwork = (albumCover != null);

        if (useBlurredArtwork) {
            Bitmap blurAlbumArt = BlurBuilder.getInstance().blurImage(0.2f, albumCover, 15);

            if (blurAlbumArt != null && mActivity != null) {

                final BitmapDrawable bgBlured = new BitmapDrawable(mActivity.getResources(), blurAlbumArt);

                setImageAnimatedOnMainThread(mBinding.blurredBackground, blurredBackground, bgBlured);
            } else {
                setImageAnimatedOnMainThread(mBinding.blurredBackground, blurredBackground, null);
            }

        } else {
            setImageAnimatedOnMainThread(mBinding.blurredBackground, blurredBackground, null);
        }
    }

    private void setImageAnimatedOnMainThread(final ImageView imageView, final ObservableField<Drawable> imageField, final BitmapDrawable newBitmapDrawable) {
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                setImageAnimated(imageView, imageField, newBitmapDrawable);
            }
        });
    }

    private void setImageAnimated(final ImageView imageView, final ObservableField<Drawable> imageField, final BitmapDrawable newBitmapDrawable) {
        // New drawable must be a not null object
        if (newBitmapDrawable == null) {
            return;
        }

        // If the oldBitmap is null or it's equals the new one, do not update it
        BitmapDrawable oldBitmapDrawable = (BitmapDrawable) imageField.get();
        if (oldBitmapDrawable != null) {
            Bitmap oldBitmap = oldBitmapDrawable.getBitmap();
            if (oldBitmap != null && oldBitmap.sameAs(newBitmapDrawable.getBitmap())) {
                return;
            }
        }

        // Everything is fine, change the drawables with animations
        final Animation anim_out = AnimationUtils.loadAnimation(mActivity, android.R.anim.fade_out);
        final Animation anim_in = AnimationUtils.loadAnimation(mActivity, android.R.anim.fade_in);
        anim_out.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                imageField.set(newBitmapDrawable);
                anim_in.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                    }
                });
                imageView.startAnimation(anim_in);
            }
        });
        imageView.startAnimation(anim_out);
    }

    void updateProgressTimer() {
        if (mActivityIsPaused) {
            return;
        }

        if (isProgressTimerVisible()) {
            startProgressTimer();
        } else {
            stopProgressTimer();
        }
    }

    private synchronized void startProgressTimer() {

        if (mProgressTimer != null)
            return; // Already started

        stopProgressTimer();

        mProgressTimer = new Timer();
        mProgressTimerTask = new ProgressTimerTask();
        mProgressTimer.schedule(mProgressTimerTask, PROGRESS_UPDATE_TIME_MS, PROGRESS_UPDATE_TIME_MS);
    }

    void stopProgressTimer() {
        if (mProgressTimer != null) {
            mProgressTimer.cancel();
            mProgressTimer.purge();
            mProgressTimer = null;
        }
        if (mProgressTimerTask != null) {
            mProgressTimerTask.cancel();
            mProgressTimerTask.dispose();
            mProgressTimerTask = null;
        }
    }

    void seekPlaybackToPos(int progress) {
        if (!mIsUpdatable) {
            return;
        }

        stopProgressTimer();
        long seekPos = getPositionMilisec(progress, mNowPlayingDataModel.duration.get());
        NowPlayingManager.getInstance().seek(seekPos);
        mNowPlayingDataModel.playPosition.set(seekPos);
        startProgressTimer();
    }

    private long getPositionMilisec(int percent, long duration) {
        float pos = ((float) percent * (float) duration) / 100.0f;
        return (long) pos;
    }

    private class ProgressTimerTask extends DisposableTimerTask {
        private static final int MAX_FAKE_UPDATE = 3;
        private int mNumFakeUpdateIncrement = 0;

        @Override
        public void run() {
            if (mIsDisposed) {
                cancel();
                return;
            }

            if (mNextPreviousButtonClicked) {
                updatePositionFake(0);

                NowPlayingManager.getInstance().updatePlayPositionAsync();
                mNumFakeUpdateIncrement = 0;
                FsLogger.log("Next or Prev button was pressed", LogLevel.Info);

            } else if (canIncrementFakePosition()) {
                showNextFakeTime();
            } else {
                showNextFakeTime();

                NowPlayingManager.getInstance().updatePlayPositionAsync();
                mNumFakeUpdateIncrement = 0;
                FsLogger.log("reset", LogLevel.Info);
            }
        }

        private void showNextFakeTime() {
            mNumFakeUpdateIncrement++;
            if (mNowPlayingDataModel.isPlaying() && playPositionIsSmallerThanDuration()) {
                updatePositionFake(PROGRESS_UPDATE_TIME_MS * mNumFakeUpdateIncrement);
            }
            mNextPreviousButtonClicked = false;
        }

        private boolean canIncrementFakePosition() {
            return mNumFakeUpdateIncrement < MAX_FAKE_UPDATE;
        }

    }

    private void updatePositionFake(long incrementMs) {
        if (!mIsUpdatable) {
            return;
        }

        long position = mNowPlayingDataModel.playPosition.get() + incrementMs;
        friendlyElapsedTime.set(GuiUtils.getMilisecAsTimeString(position));
        updateProgressBar(position);
        FsLogger.log(friendlyElapsedTime.get(), LogLevel.Error);
    }

    private boolean playPositionIsSmallerThanDuration() {
        return mNowPlayingDataModel.playPosition.get() < mNowPlayingDataModel.duration.get();
    }

    private boolean isProgressTimerVisible() {
        return isElapsedTimeVisible.get();
    }

    @SuppressWarnings("all")
    public void onBrowseStationsClicked(View view) {
        NavigationHelper.goToActivity(mActivity, BrowseIRActivity.class,
                NavigationHelper.AnimationType.SlideUp);
    }

    @SuppressWarnings("all")
    public void onAddAsPresetClicked(View view) {
        NavigationHelper.goToActivity(mActivity, ReplacePresetsActivity.class,
                NavigationHelper.AnimationType.SlideToLeft);
    }

    @SuppressWarnings("all")
    public void onRepeatClicked(View view) {
        boolean isChecked = mNowPlayingDataModel.isRepeat.get();
        NowPlayingManager.getInstance().setRepeat(!isChecked);
    }

    @SuppressWarnings("all")
    public void onShuffleClicked(View view) {
        boolean isChecked = mNowPlayingDataModel.isShuffle.get();
        NowPlayingManager.getInstance().setShuffle(!isChecked);
    }

    @SuppressWarnings("all")
    public void onPreviousClicked(View view) {
        mNextPreviousButtonClicked = true;
        NowPlayingManager.getInstance().previous();
    }

    @SuppressWarnings("all")
    public void onNextClicked(View view) {
        mNextPreviousButtonClicked = true;
        NowPlayingManager.getInstance().next();
    }

    @SuppressWarnings("all")
    public void onSpotifyLogoClicked(View view) {
        ExternalAppsOpener.openSpotify(mActivity);
    }


    void setUpdatable(boolean updatable) {
        mIsUpdatable = updatable;

        if (mNowPlayingDataModel == null) {
            return;
        }

        if (updatable) {
            updateAllObservables();
        }
    }

    @Override
    public void dispose() {
        mNowPlayingDataModel.playStatus.removeOnPropertyChangedCallback(mNowPlayingPropertyChangedCallback);
        mNowPlayingDataModel.playInfoText.removeOnPropertyChangedCallback(mNowPlayingPropertyChangedCallback);
        mNowPlayingDataModel.album.removeOnPropertyChangedCallback(mNowPlayingPropertyChangedCallback);
        mNowPlayingDataModel.artist.removeOnPropertyChangedCallback(mNowPlayingPropertyChangedCallback);
        mNowPlayingDataModel.duration.removeOnPropertyChangedCallback(mNowPlayingPropertyChangedCallback);
        mNowPlayingDataModel.isRepeat.removeOnPropertyChangedCallback(mNowPlayingPropertyChangedCallback);
        mNowPlayingDataModel.isShuffle.removeOnPropertyChangedCallback(mNowPlayingPropertyChangedCallback);
        mNowPlayingDataModel.playCaps.removeOnPropertyChangedCallback(mNowPlayingPropertyChangedCallback);
        mNowPlayingDataModel.playPosition.removeOnPropertyChangedCallback(mNowPlayingPropertyChangedCallback);
        mNowPlayingDataModel.spotifyPlaylistName.removeOnPropertyChangedCallback(mNowPlayingPropertyChangedCallback);
        mNowPlayingDataModel.currentMode.removeOnPropertyChangedCallback(mNowPlayingPropertyChangedCallback);

        mNowPlayingPropertyChangedCallback = null;
        mBinding = null;

        stopProgressTimer();

        super.dispose();
    }
}
