package com.zoundindustries.multiroom.browse_ir;

import android.app.Activity;
import android.databinding.ObservableBoolean;

import com.apps_lib.multiroom.browse_ir.BrowseIRAdapter;
import com.apps_lib.multiroom.browse_ir.events.NavigationResultEvent;
import com.apps_lib.multiroom.browse_ir.events.ShowLoadingProgressEvent;
import com.apps_lib.multiroom.browse_ir.model.BrowseIRManager;
import com.apps_lib.multiroom.browse_ir.model.EIRNavigationResult;
import com.apps_lib.multiroom.browse_ir.model.IIRNavigationListener;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

/**
 * Created by lsuhov on 28/06/16.
 */

public class BrowseIRViewModel implements IIRNavigationListener {

    private Activity mActivity;
    private BrowseIRAdapter mAdapter;

    public ObservableBoolean showLoadingProgress = new ObservableBoolean(false);
    public ObservableBoolean showList = new ObservableBoolean(false);
    public ObservableBoolean showError = new ObservableBoolean(false);
    public ObservableBoolean backButtonIsEnabled = new ObservableBoolean(false);

    private volatile boolean mIsSearching = false;

    public BrowseIRViewModel(Activity activity) {
        mActivity = activity;
    }

    public void onStart(BrowseIRAdapter adapter) {
        mAdapter = adapter;

        EventBus.getDefault().register(this);

        BrowseIRManager.getInstance().navigateToCurrentList(this);
    }

    public void onStop() {
        EventBus.getDefault().unregister(this);
    }

    public boolean tryNavigateUp() {
        if (BrowseIRManager.getInstance().getNavDepth() > 0) {
            BrowseIRManager.getInstance().navigateUp(this);
            return true;
        }
        return false;
    }

    public void onSearchInitiated() {
        showList.set(false);
    }

    public void onSearchCancelled() {
        showList.set(true);
    }

    public void search(String searchText) {
        mIsSearching = true;
        BrowseIRManager.getInstance().search(searchText, this);
    }

    public void dispose() {
        mActivity = null;
    }

    @Override
    public void onNavigationCompleted(final EIRNavigationResult result) {
        if (mActivity != null && !mActivity.isFinishing()) {
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    onNavigationImpl(result);
                }
            });
        }
        mIsSearching = false;
    }

    private void onNavigationImpl(EIRNavigationResult result) {
        showLoadingProgress.set(false);
        backButtonIsEnabled.set(true);

        if (result == EIRNavigationResult.Success) {
            showList.set(true);
            showError.set(false);
            mAdapter.notifyDataSetChanged();
        } else {
            showList.set(false);
            showError.set(true);
        }
    }

    @Subscribe
    public void onShowLoadingProgress(ShowLoadingProgressEvent event) {
        if (mActivity != null && !mActivity.isFinishing()) {
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showLoadingScreen();
                }
            });
        }
    }

    @Subscribe
    public void onNavigatioResult(final NavigationResultEvent event) {
        if (mActivity != null && !mActivity.isFinishing()) {
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    onNavigationImpl(event.result);
                }
            });
        }
    }

    private void showLoadingScreen() {
        showLoadingProgress.set(true);
        backButtonIsEnabled.set(false);
        showList.set(false);
        showError.set(false);
    }

    public boolean isSearchRunning() {
        return mIsSearching;
    }
}
