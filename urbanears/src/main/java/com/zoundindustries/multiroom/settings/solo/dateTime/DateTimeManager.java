package com.zoundindustries.multiroom.settings.solo.dateTime;

import android.os.AsyncTask;

import com.frontier_silicon.NetRemoteLib.Node.BaseSysCapsUtcSettingsList;
import com.frontier_silicon.NetRemoteLib.Node.BaseSysClockTimeZone;
import com.frontier_silicon.NetRemoteLib.Node.NodeInfo;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysCapsUtcSettingsList;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.components.common.RadioNodeUtil;
import com.frontier_silicon.components.common.TaskHelper;
import com.frontier_silicon.components.common.nodeCommunication.IListNodeListener;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

/**
 * Created by nbalazs on 12/12/2016.
 */

public class DateTimeManager {
    private static DateTimeManager INSTANCE;
    private List<BaseSysCapsUtcSettingsList.ListItem> mLastUtcSettingsList;
    private BaseSysClockTimeZone mTimeZone;
    private Radio mTargetRadio;


    public static DateTimeManager getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new DateTimeManager();
        }
        return INSTANCE;
    }

    private DateTimeManager() {
        mLastUtcSettingsList = null;
    }

    public void loadTimeZoneSettingsForRadio(final Radio radio, final ILoadTimeZoneListener timeZoneListener) {
        mTargetRadio = radio;
        GetTimeZoneSettingsTask task = new GetTimeZoneSettingsTask(radio, timeZoneListener);
        TaskHelper.execute(task);
    }

    String[] getUtcSettingsList() {
        ArrayList<String> arrayList =  new ArrayList<>();
        if (mLastUtcSettingsList != null) {
            for (BaseSysCapsUtcSettingsList.ListItem listItem : mLastUtcSettingsList) {
                arrayList.add(listItem.getRegion());
            }
        }
        return arrayList.toArray(new String[arrayList.size()]);
    }

    String getTimeZoneRegionValueForIndex(int index) {
        String timezone = null;

        try {
            timezone = mLastUtcSettingsList != null ? mLastUtcSettingsList.get(index).getRegion() : null;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return timezone;
    }

    int getTimeZoneIndex() {
        int returnValue = 0;
        if (mLastUtcSettingsList != null) {
            for (int i = 0; i < mLastUtcSettingsList.size(); i++) {
                BaseSysCapsUtcSettingsList.ListItem listItem = mLastUtcSettingsList.get(i);
                if (listItem.getRegion().compareTo(mTimeZone.getValue()) == 0) {
                    returnValue = i;
                    break;
                }
            }
        }
        return returnValue;
    }

    void setTimeZone(String region, final ISetTimeZoneListener setTimeZoneListener) {
        BaseSysClockTimeZone baseSysClockTimeZone = new BaseSysClockTimeZone(region);
        RadioNodeUtil.setNodeToRadioAsync(mTargetRadio, baseSysClockTimeZone, new RadioNodeUtil.INodeSetResultListener() {
            @Override
            public void onNodeSetResult(boolean success) {
                setTimeZoneListener.onSetFinished(success);
            }
        });
    }

    private class GetTimeZoneSettingsTask extends AsyncTask<Void, Void, Void> {

        private Radio mRadio;
        private ILoadTimeZoneListener mTimeZoneListener;

        private GetTimeZoneSettingsTask(Radio radio, ILoadTimeZoneListener timeZoneListener) {
            mRadio = radio;
            mTimeZoneListener = timeZoneListener;
        }

        @Override
        protected Void doInBackground(Void... params) {
            final CountDownLatch countDownLatch = new CountDownLatch(2);
            RadioNodeUtil.getListNodeItems(mRadio, NodeSysCapsUtcSettingsList.class, false, new IListNodeListener() {
                @Override
                public void onListNodeResult(List resultList, boolean isListComplete) {
                    mLastUtcSettingsList = (List<BaseSysCapsUtcSettingsList.ListItem>) resultList;
                    countDownLatch.countDown();
                }
            });
            RadioNodeUtil.getNodeFromRadioAsync(mRadio, BaseSysClockTimeZone.class, new RadioNodeUtil.INodeResultListener() {
                @Override
                public void onNodeResult(NodeInfo node) {
                    mTimeZone = (BaseSysClockTimeZone) node;
                    countDownLatch.countDown();
                }
            });

            try {
                countDownLatch.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            mTimeZoneListener.onTimeZoneSettingsReceived();
        }
    }
}
