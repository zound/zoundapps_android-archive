package com.zoundindustries.multiroom.settings.solo.speakerList;

import android.content.Context;
import android.content.res.Resources;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.view.View;

import com.apps_lib.multiroom.myHome.speakers.SpeakerNameManager;
import com.apps_lib.multiroom.settings.solo.speakerList.RadioSelectedEvent;
import com.apps_lib.multiroom.speakerImages.ESpeakerImageSizeType;
import com.apps_lib.multiroom.speakerImages.ISpeakerImageIdListener;
import com.apps_lib.multiroom.speakerImages.SpeakerManager;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.zoundindustries.multiroom.R;
import com.zoundindustries.multiroom.speakerImages.UrbanearsSpeakerTypeDecoder;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by lsuhov on 06/06/16.
 */
public class SettingsSpeakersItemViewModel extends BaseObservable {

    private Context mContext;
    private Radio mRadio;

    public ObservableField<BitmapDrawable> speakerImage = new ObservableField<>();
    public ObservableBoolean isUrbanearsSpeaker = new ObservableBoolean(true);

    public SettingsSpeakersItemViewModel(Context context, Radio radio) {
        mContext = context;
        mRadio = radio;

        updateSpeakerImage();
        isUrbanearsSpeaker();
    }

    private void isUrbanearsSpeaker() {
        if (!UrbanearsSpeakerTypeDecoder.isUrbanearsSpeaker(mRadio.getModelName())) {
            isUrbanearsSpeaker.set(false);
        }
    }

    private void updateSpeakerImage() {
        SpeakerManager.getInstance().retrieveImageIdForRadio(mRadio, ESpeakerImageSizeType.Volume, new ISpeakerImageIdListener() {
            @Override
            public void onImageIdRetrieved(int imageId) {
                if (mContext == null) {
                    return;
                }
                Resources resources = mContext.getResources();

                Bitmap speakerImageBitmap = BitmapFactory.decodeResource(resources, imageId);
                BitmapDrawable bitmapDrawable = new BitmapDrawable(resources, speakerImageBitmap);

                speakerImage.set(bitmapDrawable);
            }
        });
    }

    @Bindable
    public String getSpeakerName() {
        if (mRadio != null) {
            SpeakerNameManager speakerNameManager = SpeakerNameManager.getInstance();
            String friendlyName = speakerNameManager.getNameForRadio(mRadio);
            if (friendlyName == null) {
                speakerNameManager.updateNameForRadio(mRadio);
                friendlyName = speakerNameManager.getNameForRadio(mRadio);
            }

            return friendlyName;
        }

        return mContext.getResources().getString(R.string.streaming_quality);
    }

    @SuppressWarnings("unused")
    public void onListItemClicked(View view) {
        if (isUrbanearsSpeaker.get())
            EventBus.getDefault().post(new RadioSelectedEvent(mRadio));
    }
}
