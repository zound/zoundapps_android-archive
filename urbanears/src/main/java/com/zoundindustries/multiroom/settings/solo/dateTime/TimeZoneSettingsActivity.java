package com.zoundindustries.multiroom.settings.solo.dateTime;

import android.annotation.SuppressLint;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import com.apps_lib.multiroom.UEActivityBase;
import com.zoundindustries.multiroom.R;
import com.zoundindustries.multiroom.databinding.ActivitySettingsTimeZoneBinding;

/**
 * Created by nbalazs on 12/12/2016.
 */
public class TimeZoneSettingsActivity extends UEActivityBase {

    private ActivitySettingsTimeZoneBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        View rootView = LayoutInflater.from(this).inflate(R.layout.activity_settings_time_zone, null);
        mBinding = DataBindingUtil.bind(rootView);
        setContentView(rootView);

        setupAppBar();
        enableUpNavigation();
        setTitle(R.string.time_zone_caps);

        setup();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressLint("InflateParams")
    private void setup() {
        // Add header / footer view
        LayoutInflater inflater = LayoutInflater.from(this);
        View headerView = inflater.inflate(R.layout.list_item_header_no_margin, null);
        View footerView = inflater.inflate(R.layout.list_item_footer_no_margin, null);
        mBinding.timeZoneListView.addHeaderView(headerView);
        mBinding.timeZoneListView.addFooterView(footerView);

        // Init the list
        final String[] timeZones = DateTimeManager.getInstance().getUtcSettingsList();
        final int selectedTimeZoneIndex = DateTimeManager.getInstance().getTimeZoneIndex();
        final TimeZoneListAdapter timeZoneListAdapter = new TimeZoneListAdapter(this, timeZones, selectedTimeZoneIndex);
        mBinding.timeZoneListView.setAdapter(timeZoneListAdapter);
        mBinding.timeZoneListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (0 < position && position < timeZones.length + 1) {
                    String selectedTimeZoneValue = timeZoneListAdapter.getSelectedItem(position - 1);
                    if (selectedTimeZoneValue != null) {
                        DateTimeManager.getInstance().setTimeZone(selectedTimeZoneValue, new ISetTimeZoneListener() {
                            @Override
                            public void onSetFinished(boolean success) {
                                if (success) {
                                    onBackPressed();
                                } else {
                                    Toast.makeText(TimeZoneSettingsActivity.this, getResources().getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                    }
                }
            }
        });
        mBinding.timeZoneListView.setSelection(selectedTimeZoneIndex);

        // Search
        mBinding.searchTimeZoneTextView.addTextChangedListener(new TextWatcher() {
            private int scrollPosition = selectedTimeZoneIndex;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                scrollPosition = after == 0 ? selectedTimeZoneIndex : 0;
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String text = mBinding.searchTimeZoneTextView.getText().toString().toLowerCase();
                timeZoneListAdapter.filter(text);
                TimeZoneSettingsActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        View listView = TimeZoneSettingsActivity.this.findViewById(R.id.timeZoneListView);
                        if (listView != null) {
                            int selectedItemCount = timeZoneListAdapter.getSelectedItemsCount();
                            if (selectedItemCount > 0){
                                listView.setVisibility(View.VISIBLE);
                            } else {
                                listView.setVisibility(View.GONE);
                            }
                        }
                    }
                });
            }

            @Override
            public void afterTextChanged(Editable s) {
                mBinding.timeZoneListView.setSelection(scrollPosition);

            }
        });
    }
}
