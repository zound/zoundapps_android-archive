package com.zoundindustries.multiroom.settings.multi;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.apps_lib.multiroom.NavigationHelper;
import com.apps_lib.multiroom.UEActivityBase;
import com.apps_lib.multiroom.widgets.DividerItemDecoration;
import com.zoundindustries.multiroom.R;
import com.zoundindustries.multiroom.databinding.ActivityMultiSpeakersMenuBinding;
import com.zoundindustries.multiroom.databinding.ListItemSettingsActivitySoloMultiBinding;
import com.zoundindustries.multiroom.settings.multi.streamingQuality.StreamingQualityActivity;

/**
 * Created by nbalazs on 09/06/2017.
 */

public class MultiSpeakersActivity extends UEActivityBase {

    private ActivityMultiSpeakersMenuBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_multi_speakers_menu);

        setupAppBar();

        enableUpNavigation();

//        setTitle(R.string.settings_multi_speakers_title);

        setTitle(R.string.settings_menu_title);

        setup();
    }

    private void setup() {
        final String [] menuContent = new String[1];
        menuContent[0] = getResources().getString(R.string.streaming_quality);
        DividerItemDecoration itemDecoration = new DividerItemDecoration(this, LinearLayoutManager.VERTICAL);

        mBinding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        mBinding.recyclerView.addItemDecoration(itemDecoration);

        mBinding.recyclerView.setAdapter(new RecyclerView.Adapter() {

            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                LayoutInflater inflater = LayoutInflater.from(MultiSpeakersActivity.this);
                ListItemSettingsActivitySoloMultiBinding theLayoutBinding = DataBindingUtil.inflate(inflater, R.layout.list_item_settings_activity_solo_multi, parent, true);
                return new MultiSpeakersActivity.MultiSpeakersViewHolder(theLayoutBinding);
            }

            @Override
            public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
                MultiSpeakersActivity.MultiSpeakersViewHolder viewHolder = (MultiSpeakersActivity.MultiSpeakersViewHolder) holder;
                viewHolder.mBinding.titleTextView.setText(menuContent[position]);
                viewHolder.mBinding.containerLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        switch (holder.getAdapterPosition()){
//                            case 0: // Stereo Pairing
//                                break;
                            case 0: // Multi Streaming Quality
                                NavigationHelper.goToActivity(MultiSpeakersActivity.this, StreamingQualityActivity.class, NavigationHelper.AnimationType.SlideToLeft);
                                break;
                        }
                    }
                });
            }

            @Override
            public int getItemCount() {
                return menuContent.length;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private class MultiSpeakersViewHolder extends RecyclerView.ViewHolder{

        private ListItemSettingsActivitySoloMultiBinding mBinding;

        MultiSpeakersViewHolder(ListItemSettingsActivitySoloMultiBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }
    }
}