package com.zoundindustries.multiroom.settings.solo.dateTime;

/**
 * Created by nbalazs on 13/12/2016.
 */

public interface ISetTimeZoneListener {
    void onSetFinished(boolean success);
}
