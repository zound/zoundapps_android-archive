package com.zoundindustries.multiroom.settings.solo.speakerList;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.util.SortedList;
import android.support.v7.widget.LinearLayoutManager;
import android.view.MenuItem;

import com.apps_lib.multiroom.NavigationHelper;
import com.apps_lib.multiroom.UEActivityBase;
import com.apps_lib.multiroom.factory.ActivityFactory;
import com.apps_lib.multiroom.myHome.SpeakersDiscoveryManager;
import com.apps_lib.multiroom.settings.solo.speakerList.RadioSelectedEvent;
import com.apps_lib.multiroom.widgets.DividerItemDecoration;
import com.frontier_silicon.NetRemoteLib.Discovery.IRadioDiscoveryListener;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.components.NetRemoteManager;
import com.zoundindustries.multiroom.R;
import com.zoundindustries.multiroom.databinding.ActivitySettingsSpeakerListBinding;
import com.zoundindustries.multiroom.speakerImages.UrbanearsSpeakerTypeDecoder;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lsuhov on 06/06/16.
 */
public class SettingsSpeakerListActivity extends UEActivityBase implements IRadioDiscoveryListener {

    private ActivitySettingsSpeakerListBinding mBinding;
    private SettingsSpeakersAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_settings_speaker_list);

        setupAppBar();
        enableUpNavigation();
        setTitle(R.string.solo_speakers_activity_title);

        setupControls();
    }

    private void setupControls() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        DividerItemDecoration itemDecoration = new DividerItemDecoration(this, LinearLayoutManager.VERTICAL);

        mAdapter = new SettingsSpeakersAdapter(this);

        mBinding.listOfSpeakers.setHasFixedSize(true);
        mBinding.listOfSpeakers.setLayoutManager(layoutManager);
        mBinding.listOfSpeakers.addItemDecoration(itemDecoration);
        mBinding.listOfSpeakers.setAdapter(mAdapter);
    }

    @Override
    protected void onResume() {

        NetRemoteManager.getInstance().addRadioDiscoveryListener(this);
        SpeakersDiscoveryManager.getInstance().tryStartDiscovery();
        updateAdapter();

        EventBus.getDefault().register(this);

        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();

        NetRemoteManager.getInstance().removeRadioDiscoveryListener(this);
        EventBus.getDefault().unregister(this);
    }

    private void updateAdapter() {
        List<Radio> radios = NetRemoteManager.getInstance().getRadios();

        mAdapter.mSortedRadios.beginBatchedUpdates();
        mAdapter.mSortedRadios.clear();
        addOnlyUrbanearsSpeakers(radios);
        mAdapter.mSortedRadios.endBatchedUpdates();
    }

    private void addOnlyUrbanearsSpeakers(List<Radio> radios) {
        List<Radio> finalList = new ArrayList<>();
        for (Radio radio : radios) {
            if (UrbanearsSpeakerTypeDecoder.isUrbanearsSpeaker(radio.getModelName())) {
                finalList.add(radio);
            }
        }

        mAdapter.mSortedRadios.addAll(finalList);
    }


    @Override
    public void onRadioFound(final Radio radio) {
        if (!isFinishing()) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mAdapter.mSortedRadios.add(radio);
                }
            });
        }
    }


    @Override
    public void onRadioLost(final Radio radio) {
        if (!isFinishing()) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mAdapter.mSortedRadios.remove(radio);
                }
            });
        }
    }

    @Override
    public void onRadioUpdated(final Radio radio) {
        if (!isFinishing()) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    final int position = mAdapter.mSortedRadios.indexOf(radio);
                    if (position == SortedList.INVALID_POSITION) {
                        return;
                    }
                    mAdapter.mSortedRadios.updateItemAt(position, radio);
                }
            });
        }
    }

    @Subscribe
    @SuppressWarnings("unused")
    public void onSpeakerClicked(RadioSelectedEvent event) {
        if (event.radio != null) {
            Bundle bundle = new Bundle();
            bundle.putString(NavigationHelper.RADIO_UDN_ARG, event.radio.getUDN());

            NavigationHelper.goToActivity(this, ActivityFactory.getActivityFactory().getSettingsActivity(), NavigationHelper.AnimationType.SlideToLeft,
                    false, bundle);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
