package com.zoundindustries.multiroom.settings.solo.googleCast;

/**
 * Created by lsuhov on 08/06/16.
 */
public enum GoogleCastSettingType {
    ShareUsageData,
    LearnToCast,
    CastApps,
    CastPrivacy,
    ToS,
    OpenSourceLicences
}
