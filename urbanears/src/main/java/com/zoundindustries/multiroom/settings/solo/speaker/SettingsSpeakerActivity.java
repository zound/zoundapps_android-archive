package com.zoundindustries.multiroom.settings.solo.speaker;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.apps_lib.multiroom.NavigationHelper;
import com.apps_lib.multiroom.UEActivityBase;
import com.apps_lib.multiroom.settings.RadioFromBundleExtractor;
import com.zoundindustries.multiroom.settings.solo.SettingsViewModel;
import com.zoundindustries.multiroom.settings.solo.ChangeFriendlyNameActivity;
import com.zoundindustries.multiroom.settings.solo.dateTime.DateTimeManager;
import com.zoundindustries.multiroom.settings.solo.dateTime.ILoadTimeZoneListener;
import com.zoundindustries.multiroom.settings.solo.dateTime.TimeZoneSettingsActivity;
import com.zoundindustries.multiroom.settings.solo.equalizer.EqualizerActivity;
import com.apps_lib.multiroom.settings.solo.equalizer.EqualizerManager;
import com.zoundindustries.multiroom.settings.solo.googleCast.GoogleCastManager;
import com.zoundindustries.multiroom.settings.solo.googleCast.GoogleCastSettingsActivity;
import com.apps_lib.multiroom.settings.solo.speaker.IRetrieveTaskListener;
import com.apps_lib.multiroom.settings.solo.speaker.SpeakerSettingModel;
import com.apps_lib.multiroom.settings.solo.speaker.SpeakerSettingType;
import com.apps_lib.multiroom.speakerImages.ESpeakerImageSizeType;
import com.apps_lib.multiroom.speakerImages.ISpeakerImageIdListener;
import com.apps_lib.multiroom.speakerImages.SpeakerManager;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.zoundindustries.multiroom.R;
import com.zoundindustries.multiroom.databinding.ActivitySettingsSpeakerBinding;
import com.zoundindustries.multiroom.settings.solo.aboutSpeaker.AboutSpeakerActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lsuhov on 07/06/16.
 */
public class SettingsSpeakerActivity extends UEActivityBase {

    private ActivitySettingsSpeakerBinding mBinding;
    private Radio mRadio;
    private SettingsViewModel mSettingsViewModel;
    private Activity mActivity = this;
    ProgressBar progressBar = null;
    ImageView imageView = null;
    private boolean handleDispose = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_settings_speaker);

        setupAppBar();
        enableUpNavigation();

        mRadio = RadioFromBundleExtractor.extractRadio(getIntent().getExtras(), SettingsSpeakerActivity.class);

        mSettingsViewModel = new SettingsViewModel(this, mRadio);

        if (mSettingsViewModel.startHomeActivityIfNeeded()) {
            return;
        }

        setupControls();
    }

    @Override
    protected void onResume() {
        super.onResume();
        handleDispose = false;
        mSettingsViewModel.startListeningToDiscovery();

        updateFriendlyName();
    }

    @Override
    protected void onPause() {
        super.onPause();

        mSettingsViewModel.stopListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (handleDispose) {
            dispose();
        }
    }

    private void setupControls() {
        if (mSettingsViewModel.startHomeActivityIfNeeded()) {
            return;
        }

        List<SpeakerSettingModel> speakerSettingTypeList = getSpeakerSettings();
        final SettingsSpeakerAdapter adapter = new SettingsSpeakerAdapter(this,
                R.layout.list_item_settings_speaker, speakerSettingTypeList);
        View listHeaderView = LayoutInflater.from(this).inflate(R.layout.list_item_header, null);
        View listFooterView = LayoutInflater.from(this).inflate(R.layout.list_item_footer, null);
        mBinding.settingsListView.addHeaderView(listHeaderView, null, false);
        mBinding.settingsListView.addFooterView(listFooterView, null, false);
        mBinding.settingsListView.setAdapter(adapter);
        mBinding.settingsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                SpeakerSettingModel model = adapter.getItem(position - 1); // "-1" to synchronize ListView position with the adapter position
                onSpeakerSettingClicked(model, view);

            }
        });

        updateSpeakerImage();
    }

    private void updateSpeakerImage() {
        SpeakerManager.getInstance().retrieveImageIdForRadio(mRadio, ESpeakerImageSizeType.Large, new ISpeakerImageIdListener() {
            @Override
            public void onImageIdRetrieved(int imageId) {
                if (isFinishing()) {
                    return;
                }

                mBinding.speakerImageView.setImageResource(imageId);
            }
        });
    }

    private void updateFriendlyName() {
        if (mSettingsViewModel.startHomeActivityIfNeeded()) {
            return;
        }
        if (mRadio != null) {
            setTitle(mRadio.getFriendlyName().toUpperCase());
        }
    }

    private List<SpeakerSettingModel> getSpeakerSettings() {
        ArrayList<SpeakerSettingModel> speakerSettingTypeList = new ArrayList<>();

        SpeakerSettingModel aboutSetting = new SpeakerSettingModel();
        aboutSetting.speakerSettingType = SpeakerSettingType.About;
        aboutSetting.name = getString(R.string.about_this_speaker);
        speakerSettingTypeList.add(aboutSetting);

        SpeakerSettingModel equalizerSetting = new SpeakerSettingModel();
        equalizerSetting.speakerSettingType = SpeakerSettingType.Equalizer;
        equalizerSetting.name = getString(R.string.adjust_equalizer);
        speakerSettingTypeList.add(equalizerSetting);

        SpeakerSettingModel changeNameSetting = new SpeakerSettingModel();
        changeNameSetting.speakerSettingType = SpeakerSettingType.ChangeName;
        changeNameSetting.name = getString(R.string.change_name);
        speakerSettingTypeList.add(changeNameSetting);

        SpeakerSettingModel timeZoneSettings = new SpeakerSettingModel();
        timeZoneSettings.speakerSettingType = SpeakerSettingType.TimeZone;
        timeZoneSettings.name = getString(R.string.time_zone);
        speakerSettingTypeList.add(timeZoneSettings);

        SpeakerSettingModel castSetting = new SpeakerSettingModel();
        castSetting.speakerSettingType = SpeakerSettingType.GoogleCast;
        castSetting.name = getString(R.string.chromecast_built_in_settings);
        speakerSettingTypeList.add(castSetting);

        return speakerSettingTypeList;
    }

    private void onSpeakerSettingClicked(SpeakerSettingModel model, View view) {
        if (model == null || model.speakerSettingType == null) {
            return;
        }

        switch (model.speakerSettingType) {
            case About:
                openSubSettingActivity(AboutSpeakerActivity.class);
                break;
            case ChangeName:
                openSubSettingActivity(ChangeFriendlyNameActivity.class);
                break;
            case Equalizer:
                retrieveDataAndOpenEqualizerActivity(view);
                break;
            case TimeZone:
                retrieveDataAndOpenDateTimeSettingsActivity(view);
                break;
            case GoogleCast:
                retrieveDataAndOpenGoogleCastActivity(view);
                break;
            default:
                break;
        }
    }

    private void retrieveDataAndOpenEqualizerActivity(View view) {
        if (mSettingsViewModel.startHomeActivityIfNeeded()) {
            return;
        }

        handleDispose = true;
        initButtons(view);
        EqualizerManager equalizerManager = EqualizerManager.getInstance();
        equalizerManager.init(mRadio, mActivity);
        equalizerManager.startEqualizerTask(new IRetrieveTaskListener() {
            @Override
            public void onFinish() {
                openSubSettingActivity(EqualizerActivity.class);
            }
        });
    }

    private void retrieveDataAndOpenDateTimeSettingsActivity(View view) {
        if (mSettingsViewModel.startHomeActivityIfNeeded()) {
            return;
        }

        handleDispose = true;
        initButtons(view);
        DateTimeManager.getInstance().loadTimeZoneSettingsForRadio(mRadio, new ILoadTimeZoneListener() {
            @Override
            public void onTimeZoneSettingsReceived() {
                NavigationHelper.goToActivity(SettingsSpeakerActivity.this, TimeZoneSettingsActivity.class, NavigationHelper.AnimationType.SlideToLeft, false);
            }
        });
    }

    private void retrieveDataAndOpenGoogleCastActivity(View view) {
        if (mSettingsViewModel.startHomeActivityIfNeeded()) {
            return;
        }

        handleDispose = true;
        initButtons(view);
        GoogleCastManager googleCastManager = new GoogleCastManager(mRadio, mActivity);
        googleCastManager.startSettingsRetrievalTask(new IRetrieveTaskListener() {
            @Override
            public void onFinish() {
                openSubSettingActivity(GoogleCastSettingsActivity.class);
            }
        });

    }

    private void initButtons(View view) {
        progressBar = (ProgressBar) view.findViewById(R.id.progressActivating);
        imageView = (ImageView) view.findViewById(R.id.imageViewChevron);
        imageView.setVisibility(View.INVISIBLE);
        progressBar.setVisibility(View.VISIBLE);
    }

    private void dispose() {
        if (imageView != null && progressBar != null) {
            imageView.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
        }
    }

    private void openSubSettingActivity(Class subSettingActivityClass) {
        if (mSettingsViewModel.startHomeActivityIfNeeded()) {
            return;
        }
        Bundle bundle = new Bundle();
        bundle.putString(NavigationHelper.RADIO_UDN_ARG, mRadio.getUDN());

        NavigationHelper.goToActivity(this, subSettingActivityClass, NavigationHelper.AnimationType.SlideToLeft,
                false, bundle);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
