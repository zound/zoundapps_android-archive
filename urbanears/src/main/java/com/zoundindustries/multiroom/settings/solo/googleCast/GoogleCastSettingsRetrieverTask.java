package com.zoundindustries.multiroom.settings.solo.googleCast;

import android.content.Context;

import com.frontier_silicon.NetRemoteLib.Node.BaseCastUsageReport;
import com.frontier_silicon.NetRemoteLib.Node.NodeCastUsageReport;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.components.common.BaseShowProgressDialogTask;
import com.zoundindustries.multiroom.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lsuhov on 18/07/16.
 */

class GoogleCastSettingsRetrieverTask extends BaseShowProgressDialogTask<List<GoogleCastSettingModel>> {

    private Radio mRadio;
    private IGoogleCastSettingsListener mListener;

    GoogleCastSettingsRetrieverTask(Radio radio, Context context, IGoogleCastSettingsListener listener) {
        mRadio = radio;
        mContext = context;
        mListener = listener;
    }

    @Override
    protected void onPreExecute() {
        initTimeoutTimer();
    }

    @Override
    protected List<GoogleCastSettingModel> doInBackground(Integer... arg0) {
        return createGoogleCastSettingModels();
    }

    private List<GoogleCastSettingModel> createGoogleCastSettingModels() {
        List<GoogleCastSettingModel> googleCastSettingModels = new ArrayList<>();

        NodeCastUsageReport nodeCastUsageReport = (NodeCastUsageReport) mRadio.getNodeSyncGetter(NodeCastUsageReport.class).get();

        boolean isChecked = (nodeCastUsageReport != null &&
                nodeCastUsageReport.getValueEnum() == BaseCastUsageReport.Ord.ACTIVE);

        GoogleCastSettingModel shareSetting = new GoogleCastSettingModel(GoogleCastSettingType.ShareUsageData,
                SettingType.CheckBox, mContext.getString(R.string.share_usage_data), isChecked);
        googleCastSettingModels.add(shareSetting);

        GoogleCastSettingModel learnToCastSetting = new GoogleCastSettingModel(GoogleCastSettingType.LearnToCast,
                SettingType.Link, mContext.getString(R.string.learn_to_cast), mContext.getString(R.string.google_learn_to_cast_link));
        googleCastSettingModels.add(learnToCastSetting);

        GoogleCastSettingModel castAppsSetting = new GoogleCastSettingModel(GoogleCastSettingType.CastApps,
                SettingType.Link, mContext.getString(R.string.google_cast_apps), mContext.getString(R.string.google_cast_apps_link));
        googleCastSettingModels.add(castAppsSetting);

        GoogleCastSettingModel castPrivacySetting = new GoogleCastSettingModel(GoogleCastSettingType.CastPrivacy,
                SettingType.Link, mContext.getString(R.string.google_cast_privacy), mContext.getString(R.string.google_privacy_link));
        googleCastSettingModels.add(castPrivacySetting);

        GoogleCastSettingModel tosSetting = new GoogleCastSettingModel(GoogleCastSettingType.ToS,
                SettingType.Link, mContext.getString(R.string.google_terms_of_service), mContext.getString(R.string.google_tos_link));
        googleCastSettingModels.add(tosSetting);

        GoogleCastSettingModel openSourceLicenceSetting = new GoogleCastSettingModel(GoogleCastSettingType.OpenSourceLicences,
                SettingType.Link, mContext.getString(R.string.open_source_licences), mContext.getString(R.string.google_cast_open_source_licences_link));
        googleCastSettingModels.add(openSourceLicenceSetting);

        return googleCastSettingModels;
    }

    @Override
    protected void onPostExecute(List<GoogleCastSettingModel> result) {
        super.onPostExecute(result);

        if (mListener != null) {
            mListener.onGoogleCastSettingsRetrieved(result);
            mListener = null;
        }

        mRadio = null;

        dispose();
    }
}
