package com.zoundindustries.multiroom.settings.solo.aboutSpeaker;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.MenuItem;
import android.view.View;

import com.apps_lib.multiroom.UEActivityBase;
import com.apps_lib.multiroom.persistence.PersistenceMgr;
import com.apps_lib.multiroom.settings.RadioFromBundleExtractor;
import com.apps_lib.multiroom.util.DisposableTimerTask;
import com.frontier_silicon.NetRemoteLib.Node.NodeCastVersion;
import com.frontier_silicon.NetRemoteLib.Node.NodeInfo;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysInfoVersion;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysNetWlanConnectedSSID;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysNetWlanMacAddress;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysNetWlanRssi;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.components.common.RadioNodeUtil;
import com.zoundindustries.multiroom.R;
import com.zoundindustries.multiroom.databinding.ActivitySettingsAboutSpeakerBinding;
import com.zoundindustries.multiroom.settings.solo.SettingsViewModel;

import java.util.Map;
import java.util.Timer;

/**
 * Created by lsuhov on 08/06/16.
 */
public class AboutSpeakerActivity extends UEActivityBase {

    private Radio mRadio;
    private ActivitySettingsAboutSpeakerBinding mBinding;
    private String mWifiNetworkName;
    private Timer mTimer;
    private DisposableTimerTask mTimerTask;
    private SettingsViewModel mSettingsViewModel;
    private boolean mActivityWasCreated;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_settings_about_speaker);

        setupAppBar();
        enableUpNavigation();
        setTitle(R.string.about_this_speaker_caps);

        mRadio = RadioFromBundleExtractor.extractRadio(getIntent().getExtras(), AboutSpeakerActivity.class);

        mSettingsViewModel = new SettingsViewModel(this, mRadio);

        if (mSettingsViewModel.startHomeActivityIfNeeded()) {
            return;
        }

        setupControls();
        mActivityWasCreated = true;

        mBinding.buttonUpdate.setVisibility(PersistenceMgr.getInstance().checkIfSpeakerIsUpdatable(mRadio.getSN()) ? View.VISIBLE : View.GONE);
    }

    @Override
    protected void onResume() {
        super.onResume();

        mSettingsViewModel.startListeningToDiscovery();

        if (mActivityWasCreated) {
            //not
            mActivityWasCreated = false;
            return;
        }
        startStrengthTimer();
    }

    @Override
    protected void onPause() {
        mSettingsViewModel.stopListening();
        stopStrengthTimer();

        super.onPause();
    }

    private void setupControls() {
        if (mSettingsViewModel.startHomeActivityIfNeeded()) {
            return;
        }
        setSpeakerName();

        RadioNodeUtil.getNodesFromRadioAsync(mRadio, new Class[]{NodeSysInfoVersion.class, NodeCastVersion.class,
                        NodeSysNetWlanConnectedSSID.class, NodeSysNetWlanRssi.class, NodeSysNetWlanMacAddress.class},
                false, new RadioNodeUtil.INodesResultListener() {
                    @Override
                    public void onNodesResult(Map<Class, NodeInfo> nodes) {
                        NodeSysNetWlanConnectedSSID ssidNode = (NodeSysNetWlanConnectedSSID) nodes.get(NodeSysNetWlanConnectedSSID.class);
                        if (ssidNode != null) {
                            mWifiNetworkName = RadioNodeUtil.getHumanReadableSSID(ssidNode.getValue());
                        }
                        NodeSysNetWlanRssi signalStrengthNode = (NodeSysNetWlanRssi) nodes.get(NodeSysNetWlanRssi.class);
                        if (signalStrengthNode != null) {
                            showWifiWithStrength(signalStrengthNode.getValue());
                        }

                        startStrengthTimer();

                        setIPAddress();
                        setMACAddress((NodeSysNetWlanMacAddress) nodes.get(NodeSysNetWlanMacAddress.class));
                        setModelName();
                        setSoftwareVersion((NodeSysInfoVersion) nodes.get(NodeSysInfoVersion.class));
                        setGoogleCastVersion((NodeCastVersion) nodes.get(NodeCastVersion.class));
                    }
                });

        setUpdateButton();
    }

    void setSpeakerName() {
        Spannable speakerName = new SpannableString(getResources().getString(R.string.speaker_name_with_value, mRadio.getFriendlyName()));
        speakerName.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.secondaryText)), 6, speakerName.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mBinding.speakerNameTextView.setText(speakerName);
    }

    private void setGoogleCastVersion(NodeCastVersion nodeCastVersion) {
        if (nodeCastVersion != null) {
            Spannable googleCastVersion = new SpannableString(getString(R.string.chromecast_built_in_version_with_value, nodeCastVersion.getValue()));
            googleCastVersion.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.secondaryText)), 29, googleCastVersion.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            mBinding.googleCastVersionTextView.setText(googleCastVersion);
            mBinding.googleCastVersionTextView.setVisibility(View.VISIBLE);
        } else {
            mBinding.googleCastVersionTextView.setVisibility(View.GONE);
        }
    }

    private void setModelName() {
        if (mSettingsViewModel.startHomeActivityIfNeeded()) {
            return;
        }

        Spannable modelName = new SpannableString(getString(R.string.model_name_with_value, getModelNameWithoutUnderscore(mRadio.getModelName())));
        modelName.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.secondaryText)), 7, modelName.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mBinding.speakerModelTextView.setText(modelName);
    }

    private String getModelNameWithoutUnderscore(String modelName) {
        String speakerModel = "";

        if (modelName.contains("_")) {
            String[] array = modelName.split("_");
            int length = array.length;
            if (length == 3 && array[1].equalsIgnoreCase("Gold") && array[2].equalsIgnoreCase("Fish")) {
                speakerModel += array[0] + " " + array[1] + array[2].toLowerCase();
            } else {
                for (int i = 0; i < length; i++) {
                    speakerModel += array[i] + " ";
                }
            }

        } else {
            speakerModel = modelName;
        }

        return speakerModel;
    }

    private void setMACAddress(NodeSysNetWlanMacAddress nodeSysNetWlanMacAddress) {
        if (nodeSysNetWlanMacAddress == null) {
            return;
        }

        Spannable speakerMAC = new SpannableString(getString(R.string.mac_address_with_value, nodeSysNetWlanMacAddress.getValue()));
        speakerMAC.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.secondaryText)), 5, speakerMAC.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mBinding.speakerMACTextView.setText(speakerMAC);
    }

    private void setIPAddress() {
        Spannable speakerIP = new SpannableString(getString(R.string.ip_address_with_value, mRadio.getIpAddress()));
        speakerIP.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.secondaryText)), 4, speakerIP.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mBinding.speakerIpTextView.setText(speakerIP);
    }

    private void setSoftwareVersion(NodeSysInfoVersion nodeSysInfoVersion) {
        if (nodeSysInfoVersion == null) {
            return;
        }

        Spannable swVersion = new SpannableString(getString(R.string.software_version_with_value, nodeSysInfoVersion.getValue()));
        swVersion.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.secondaryText)), 18, swVersion.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mBinding.softwareVersionTextView.setText(swVersion);
    }

    private void setUpdateButton() {
        mBinding.buttonUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SpeakerUpdateNoteFragment speakerUpdateNoteFragment = SpeakerUpdateNoteFragment.newInstance(mSettingsViewModel);
                FragmentManager fragmentManager = getSupportFragmentManager();
                speakerUpdateNoteFragment.show(fragmentManager, "OldUpdateWarningDialogFragment");
            }
        });
    }

    private void startStrengthTimer() {
        stopStrengthTimer();

        mTimerTask = new WiFiStrengthTimerTask();
        mTimer = new Timer();
        mTimer.schedule(mTimerTask, 3000, 3000);
    }

    private void stopStrengthTimer() {
        if (mTimer != null) {
            mTimer.cancel();
            mTimer.purge();
            mTimer = null;
        }
        if (mTimerTask != null) {
            mTimerTask.cancel();
            mTimerTask.dispose();
            mTimerTask = null;
        }
    }

    private class WiFiStrengthTimerTask extends DisposableTimerTask {

        @Override
        public void run() {
            if (mIsDisposed) {
                cancel();
                return;
            }

            if (!AboutSpeakerActivity.this.isFinishing()) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        getWifiStrengthAndShowIt();
                    }
                });
            }
        }
    }

    private void getWifiStrengthAndShowIt() {
        if (!isFinishing()) {
            if (mSettingsViewModel.startHomeActivityIfNeeded()) {
                return;
            }
            RadioNodeUtil.getNodeFromRadioAsync(mRadio, NodeSysNetWlanRssi.class, new RadioNodeUtil.INodeResultListener() {
                @Override
                public void onNodeResult(NodeInfo node) {
                    NodeSysNetWlanRssi signalStrengthNode = (NodeSysNetWlanRssi) node;
                    if (signalStrengthNode != null) {
                        showWifiWithStrength(signalStrengthNode.getValue());
                    }
                }
            });
        }
    }

    private void showWifiWithStrength(long rawWiFiStrength) {
        Spannable wifiWithStrength = new SpannableString(getString(R.string.wifi_network_name_with_value, mWifiNetworkName, rawWiFiStrength));
        wifiWithStrength.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.secondaryText)), 14, wifiWithStrength.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mBinding.wifiNetworkNameTextView.setText(wifiWithStrength);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
