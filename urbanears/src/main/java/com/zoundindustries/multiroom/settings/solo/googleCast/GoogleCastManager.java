package com.zoundindustries.multiroom.settings.solo.googleCast;

import android.app.Activity;

import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.components.common.TaskHelper;
import com.apps_lib.multiroom.settings.solo.speaker.IRetrieveTaskListener;

import java.util.List;

/**
 * Created by cvladu on 25/11/16.
 */

public class GoogleCastManager {
    private Radio mRadio;
    private Activity mActivity;
    public static List<GoogleCastSettingModel> mGoogleCastSettingsModels;

    public GoogleCastManager(Radio radio, Activity activity) {
        mRadio = radio;
        mActivity = activity;
    }
    public void startSettingsRetrievalTask(final IRetrieveTaskListener listener) {
        GoogleCastSettingsRetrieverTask task = new GoogleCastSettingsRetrieverTask(mRadio, mActivity, new IGoogleCastSettingsListener() {
            @Override
            public void onGoogleCastSettingsRetrieved(List<GoogleCastSettingModel> googleCastSettingModels) {
                mGoogleCastSettingsModels = googleCastSettingModels;

                listener.onFinish();
            }
        });

        TaskHelper.execute(task);
    }

}
