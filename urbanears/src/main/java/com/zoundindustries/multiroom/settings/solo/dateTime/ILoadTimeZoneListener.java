package com.zoundindustries.multiroom.settings.solo.dateTime;

/**
 * Created by nbalazs on 12/12/2016.
 */

public interface ILoadTimeZoneListener {
    void onTimeZoneSettingsReceived();
}
