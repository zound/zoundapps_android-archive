package com.zoundindustries.multiroom.settings.solo.equalizer;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.MenuItem;

import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.apps_lib.multiroom.UEActivityBase;
import com.apps_lib.multiroom.settings.RadioFromBundleExtractor;
import com.zoundindustries.multiroom.R;
import com.zoundindustries.multiroom.databinding.ActivitySettingsEqualizerBinding;
import com.zoundindustries.multiroom.settings.solo.SettingsViewModel;

/**
 * Created by lsuhov on 19/07/16.
 */

public class EqualizerActivity extends UEActivityBase {

    private ActivitySettingsEqualizerBinding mBinding;
    private Radio mRadio;
    private EqualizerViewModel mViewModel;
    private SettingsViewModel mSettingsViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mRadio = RadioFromBundleExtractor.extractRadio(getIntent().getExtras(), EqualizerActivity.class);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_settings_equalizer);

        mSettingsViewModel = new SettingsViewModel(this, mRadio);

        if (mSettingsViewModel.startHomeActivityIfNeeded()) {
            return;
        }

        mViewModel = new EqualizerViewModel(this, mRadio);

        mBinding.setViewModel(mViewModel);

        setupAppBar();
        enableUpNavigation();
        setTitle(R.string.equalizer_caps);

        setupControls();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mSettingsViewModel.stopListening();
    }

    @Override
    protected void onResume() {
        if (mViewModel != null) {
            mViewModel.onResume();
        }
        mSettingsViewModel.startListeningToDiscovery();

        super.onResume();
    }

    @Override
    protected void onDestroy() {
        if (mViewModel != null) {
            mViewModel = null;
        }
        mRadio = null;

        mBinding.bassSeekBar.setOnSeekBarChangeListener(null);
        mBinding.trebleSeekBar.setOnSeekBarChangeListener(null);

        super.onDestroy();
    }

    private void setupControls() {
        mBinding.bassSeekBar.setOnSeekBarChangeListener(mViewModel);
        mBinding.trebleSeekBar.setOnSeekBarChangeListener(mViewModel);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
