package com.zoundindustries.multiroom;

import android.os.Bundle;

import com.apps_lib.multiroom.NavigationHelper;
import com.apps_lib.multiroom.UEActivityBase;
import com.apps_lib.multiroom.factory.ActivityFactory;
import com.frontier_silicon.components.common.CommonPreferences;


public class SplashscreenActivity extends UEActivityBase {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void onResume() {
        super.onResume();

        if (CommonPreferences.getInstance().getCastTosAcceptance()) {
            NavigationHelper.goToHome(this);
        } else {
            openCastTosActivity();
        }

        //remove from backstack
        this.finish();
    }

    private void openCastTosActivity() {
        NavigationHelper.goToActivity(this, ActivityFactory.getActivityFactory().getCastTosActivity(),
                NavigationHelper.AnimationType.SlideToLeft);
    }
}
