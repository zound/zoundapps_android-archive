package com.zoundindustries.multiroom;

import com.bugsee.library.Bugsee;

import java.util.HashMap;

/**
 * Created by nbalazs on 02/05/2017.
 */

public class UrbanearsApplicationFlavor extends UrbanearsApplication{


    @Override
    public void onCreate() {
        initBugsee();
        super.onCreate();

    }

    private void initBugsee() {
        HashMap<String, Object> options = new HashMap<>();
        options.put(Bugsee.Option.MaxRecordingTime, 60);
        options.put(Bugsee.Option.VideoEnabled, true);
        options.put(Bugsee.Option.MonitorNetwork, true);
        options.put(Bugsee.Option.UseSdCard, false);
        options.put(Bugsee.Option.ExtendedVideoMode, false);
        options.put(Bugsee.Option.ShakeToTrigger, false);
        Bugsee.launch(this, "165aa0b2-caa6-4b32-8eb8-b0991eb7e1ae", options);
    }
}
