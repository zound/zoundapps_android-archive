package com.zoundindustries.marshall;

import com.bugsee.library.Bugsee;

import java.util.HashMap;

/**
 * Created by nbalazs on 03/05/2017.
 */

public class MarshallApplicationFlavor extends MarshallApplication {


    @Override
    public void onCreate() {
        initBugsee();
        super.onCreate();
    }

    private void initBugsee() {
        HashMap<String, Object> options = new HashMap<>();
        options.put(Bugsee.Option.MaxRecordingTime, 60);
        options.put(Bugsee.Option.VideoEnabled, true);
        options.put(Bugsee.Option.MonitorNetwork, true);
        options.put(Bugsee.Option.UseSdCard, false);
        options.put(Bugsee.Option.ExtendedVideoMode, false);
        options.put(Bugsee.Option.ShakeToTrigger, false);
        Bugsee.launch(this, "4cc0b519-c122-4277-924e-5322d52c2955", options);
    }
}
