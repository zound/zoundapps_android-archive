package com.zoundindustries.marshall.connection;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.apps_lib.multiroom.IUEChildFragment;
import com.apps_lib.multiroom.NavigationHelper;
import com.apps_lib.multiroom.factory.EHomeFragmentTag;
import com.frontier_silicon.components.connection.ConnectionState;
import com.frontier_silicon.components.connection.ConnectionStateUtil;
import com.frontier_silicon.components.connection.IConnectionStateListener;
import com.zoundindustries.marshall.R;
import com.zoundindustries.marshall.databinding.FragmentNoWifiBinding;

/**
 * Created by lsuhov on 27/04/16.
 */
public class NoWiFiFragment extends Fragment implements IUEChildFragment, IConnectionStateListener {

    private FragmentNoWifiBinding mBinding;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_no_wifi, container, false);

        return mBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mBinding.goToWiFiSettingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openWiFiSettings();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        if (ConnectionStateUtil.getInstance().getConnectionState() != ConnectionState.NO_WIFI_OR_ETHERNET) {
            goToHomeFragment();
        }

        ConnectionStateUtil.getInstance().addListener(this, true);
    }

    @Override
    public void onPause() {
        super.onPause();

        ConnectionStateUtil.getInstance().removeListener(this);
    }

    private void goToHomeFragment() {
        NavigationHelper.goToFragment(getActivity(), EHomeFragmentTag.ScanExistingSpeakers);
    }

    private void openWiFiSettings() {
        Activity parentActivity = getActivity();
        if (parentActivity != null) {
            parentActivity.startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
        }
    }

    @Override
    public EHomeFragmentTag getStaticTag() {
        return EHomeFragmentTag.NoWiFiFragment;
    }

    @Override
    public void onStateUpdate(ConnectionState newState) {
        if (newState != ConnectionState.NO_WIFI_OR_ETHERNET) {
            goToHomeFragment();
        }
    }
}
