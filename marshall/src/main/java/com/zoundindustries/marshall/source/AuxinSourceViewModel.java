package com.zoundindustries.marshall.source;

import android.view.View;

import com.apps_lib.multiroom.connection.ConnectionManager;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysCapsValidModes;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;

/**
 * Created by lsuhov on 12/05/16.
 */
public class AuxinSourceViewModel extends SourceViewModelBase {

    private IActiveStatusProvider mActiveStatusProvider;

    public AuxinSourceViewModel(IActiveStatusProvider activeStatusProvider) {
        super(NodeSysCapsValidModes.Mode.AuxIn);
        mActiveStatusProvider = activeStatusProvider;
    }

    @Override
    public Radio getActionableRadio() {
        return ConnectionManager.getInstance().getSelectedRadio();
    }

    @Override
    public void onActivateClicked(View view) {
        if (!mActiveStatusProvider.isActive()) {
            return;
        }
        super.onActivateClicked(view);
    }
}
