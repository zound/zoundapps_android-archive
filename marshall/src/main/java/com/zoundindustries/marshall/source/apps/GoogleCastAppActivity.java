package com.zoundindustries.marshall.source.apps;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.apps_lib.multiroom.NavigationHelper;
import com.apps_lib.multiroom.UEActivityBase;
import com.frontier_silicon.components.common.ExternalAppsOpener;
import com.zoundindustries.marshall.R;
import com.zoundindustries.marshall.databinding.ActivityAppsGoogleCastBinding;

/**
 * Created by lsuhov on 18/05/16.
 */
public class GoogleCastAppActivity extends UEActivityBase {

    ActivityAppsGoogleCastBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_apps_google_cast);

        setupAppBar();

        setTitle("");

        setupControls();
        attachActivityToVolumePopup();
    }

    private void setupControls() {
        mBinding.buttonFindCastApps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavigationHelper.openWebViewActivityForURL(GoogleCastAppActivity.this, NavigationHelper.AnimationType.Normal, getString(R.string.google_cast_apps_link));
            }
        });

        mBinding.firstAppLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ExternalAppsOpener.openDeezer(GoogleCastAppActivity.this);
            }
        });

        mBinding.secondAppLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ExternalAppsOpener.openPandora(GoogleCastAppActivity.this);
            }
        });

        mBinding.thirdAppLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ExternalAppsOpener.openTidal(GoogleCastAppActivity.this);
            }
        });

        mBinding.buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              onBackPressed();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
