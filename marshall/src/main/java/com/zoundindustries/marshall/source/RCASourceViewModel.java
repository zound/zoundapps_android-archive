package com.zoundindustries.marshall.source;

import android.view.View;

import com.apps_lib.multiroom.connection.ConnectionManager;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysCapsValidModes;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;

/**
 * Created by nbalazs on 18/05/2017.
 */

public class RCASourceViewModel extends SourceViewModelBase {

    private IActiveStatusProvider mActiveStatusProvider;

    public RCASourceViewModel(IActiveStatusProvider activeStatusProvider) {
        super(NodeSysCapsValidModes.Mode.RCA);
        mActiveStatusProvider = activeStatusProvider;
    }

    @Override
    public Radio getActionableRadio() {
        return ConnectionManager.getInstance().getSelectedRadio();
    }

    @Override
    public void onActivateClicked(View view) {
        if (!mActiveStatusProvider.isActive()) {
            return;
        }
        super.onActivateClicked(view);
    }
}
