package com.zoundindustries.marshall.source;

import android.app.Activity;
import android.databinding.Observable;
import android.graphics.Bitmap;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;

import com.apps_lib.multiroom.nowPlaying.AlbumCoverDownloader;
import com.apps_lib.multiroom.nowPlaying.IAlbumCoverListener;
import com.apps_lib.multiroom.nowPlaying.NowPlayingDataModel;
import com.apps_lib.multiroom.nowPlaying.NowPlayingManager;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysCapsValidModes;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.components.NetRemoteManager;
import com.zoundindustries.marshall.GuiUtilsMarshall;
import com.zoundindustries.marshall.databinding.ActivitySourcesBinding;

/**
 * Created by nbalazs on 09/03/2017.
 */

class SourcesViewModel implements IAlbumCoverListener {

    private Activity mActivity;
    private Observable.OnPropertyChangedCallback mNowPlayingPropertyChangedCallback;
    private NowPlayingDataModel nowPlayingDataModel = NowPlayingManager.getInstance().getNowPlayingDataModel();
    private NodeSysCapsValidModes.Mode lastKnownMode = nowPlayingDataModel.currentMode.get();
    private ActivitySourcesBinding mBinding;
    SourcesViewModel() {}

    public void init(Activity activity, ActivitySourcesBinding binding) {
        mActivity = activity;
        mBinding = binding;
        addListeners();
        updateAlbumCover();
        hideNowPlayingFooterIfNeeded(false);
    }


    private void addListeners() {
        mNowPlayingPropertyChangedCallback = new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable sender, int propertyId) {
                if (sender == nowPlayingDataModel.playInfoText ||
                        sender == nowPlayingDataModel.album ||
                        sender == nowPlayingDataModel.artist ||
                        sender == nowPlayingDataModel.playStatus) {
                    updateAlbumCover();
                } else if (sender == nowPlayingDataModel.currentMode) {
                    if (mActivity == null) {
                        return;
                    }

                    mActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            hideNowPlayingFooterIfNeeded(true);
                        }
                    });

                }
            }
        };

        nowPlayingDataModel.playInfoText.addOnPropertyChangedCallback(mNowPlayingPropertyChangedCallback);
        nowPlayingDataModel.album.addOnPropertyChangedCallback(mNowPlayingPropertyChangedCallback);
        nowPlayingDataModel.artist.addOnPropertyChangedCallback(mNowPlayingPropertyChangedCallback);
        nowPlayingDataModel.playStatus.addOnPropertyChangedCallback(mNowPlayingPropertyChangedCallback);
        nowPlayingDataModel.currentMode.addOnPropertyChangedCallback(mNowPlayingPropertyChangedCallback);
    }

    private void updateAlbumCover() {
        Radio radio = NetRemoteManager.getInstance().getCurrentRadio();
        if (radio != null && canGetAlbumCover()) {
            AlbumCoverDownloader coverDownloader = new AlbumCoverDownloader();
            coverDownloader.getAlbumArtwork(radio, this, mActivity.getApplicationContext());
        }
    }

    private boolean canGetAlbumCover() {
        NowPlayingDataModel nowPlayingDataModel = NowPlayingManager.getInstance().getNowPlayingDataModel();
        boolean result = !(nowPlayingDataModel.isError() ||
                nowPlayingDataModel.currentMode.get() == NodeSysCapsValidModes.Mode.AuxIn);

        result &= (nowPlayingDataModel.isPlaying() || nowPlayingDataModel.isPaused() ||
                nowPlayingDataModel.isBuffering() || nowPlayingDataModel.isStopped());

        return result;
    }

    private void hideNowPlayingFooterIfNeeded(Boolean animated) {
        if (mBinding == null) {
            return;
        }

        if (nowPlayingDataModel.currentMode.get() == NodeSysCapsValidModes.Mode.Standby) {
            lastKnownMode = nowPlayingDataModel.currentMode.get();
            if (animated) {
                collapse(mBinding.footerFragmentContainer);
                mBinding.slidingLayout.setPanelHeight(0);
            } else {
                mBinding.footerFragmentContainer.setVisibility(View.INVISIBLE);
                mBinding.slidingLayout.setPanelHeight(0);
            }

        } else if (lastKnownMode ==  NodeSysCapsValidModes.Mode.Standby) {
            lastKnownMode = nowPlayingDataModel.currentMode.get();
            if (animated) {
                expand(mBinding.footerFragmentContainer);
                mBinding.slidingLayout.setPanelHeight(GuiUtilsMarshall.dpToPx(50, mActivity));
            } else {
                mBinding.footerFragmentContainer.setVisibility(View.VISIBLE);
                mBinding.slidingLayout.setPanelHeight(GuiUtilsMarshall.dpToPx(50, mActivity));
            }
        }
    }

    private void expand(final View v) {
        v.measure(ViewPager.LayoutParams.MATCH_PARENT, ViewPager.LayoutParams.WRAP_CONTENT);
        final int targetHeight = v.getMeasuredHeight();

        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        v.getLayoutParams().height = 1;
        v.setVisibility(View.VISIBLE);
        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1
                        ? ViewPager.LayoutParams.WRAP_CONTENT
                        : (int) (targetHeight * interpolatedTime);
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms

        a.setDuration((int) (targetHeight / v.getContext().getResources().getDisplayMetrics().density * 2.5));
        v.startAnimation(a);

    }

    private void collapse(final View v) {
        final int initialHeight = v.getMeasuredHeight();

        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (interpolatedTime == 1) {
                    v.setVisibility(View.GONE);
                } else {
                    v.getLayoutParams().height = initialHeight - (int) (initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        a.setDuration((int) (initialHeight / v.getContext().getResources().getDisplayMetrics().density * 2.5));
        v.startAnimation(a);
    }

    @Override
    public void onAlbumCoverUpdate(Bitmap albumCover) {
        // Nothing to do here, just let it go
    }

    public void dispose() {
        mActivity = null;
        mBinding = null;
        removeListeners();
    }

    private void removeListeners() {
        final NowPlayingDataModel nowPlayingDataModel = NowPlayingManager.getInstance().getNowPlayingDataModel();

        nowPlayingDataModel.playInfoText.removeOnPropertyChangedCallback(mNowPlayingPropertyChangedCallback);
        nowPlayingDataModel.album.removeOnPropertyChangedCallback(mNowPlayingPropertyChangedCallback);
        nowPlayingDataModel.artist.removeOnPropertyChangedCallback(mNowPlayingPropertyChangedCallback);
        nowPlayingDataModel.playStatus.removeOnPropertyChangedCallback(mNowPlayingPropertyChangedCallback);
        nowPlayingDataModel.currentMode.removeOnPropertyChangedCallback(mNowPlayingPropertyChangedCallback);

        mNowPlayingPropertyChangedCallback = null;
    }

}
