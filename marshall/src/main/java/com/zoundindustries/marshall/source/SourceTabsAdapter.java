package com.zoundindustries.marshall.source;


import android.animation.ArgbEvaluator;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.apps_lib.multiroom.source.SourcesRegistry;
import com.zoundindustries.marshall.R;

class SourceTabsAdapter extends PagerAdapter implements ViewPager.OnPageChangeListener {
    private Context mContext;
    private SparseArray<View> sparseArray = new SparseArray<>();
    private static float PRIMARY_ITEM_SCALE = 0.8f;
    @SuppressWarnings("FieldCanBeLocal")
    private static float SECONDARY_ITEM_SCALE = 0.5f;
    private ArgbEvaluator mArgbEvaluator;
    private static Integer[] colors = {Color.argb(0, 255, 255, 255), Color.argb(255, 173, 145, 92)};

    SourceTabsAdapter(Context context) {
        mContext = context;
        mArgbEvaluator = new ArgbEvaluator();
    }

    @Override
    public int getCount() {
        return SourcesRegistry.getInstance().getTotalPageNumbers();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        int virtualPosition = SourcesRegistry.getInstance().getVirtualPosition(position);

        View view = LayoutInflater.from(mContext).inflate(R.layout.tabs_layout, container, false);
        int id = SourcesRegistry.getInstance().getResourceIdBasedOnVirtualPosition(virtualPosition);

        try {
            ((ImageView) view).setImageResource(id);
        }catch (Resources.NotFoundException e) {
            e.printStackTrace();
        }

        sparseArray.put(virtualPosition, view);
        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        int virtualPosition = SourcesRegistry.getInstance().getVirtualPosition(position);
        sparseArray.delete(virtualPosition);
        container.removeView((View) object);
    }

    /**
     * Returns the proportional width of a given page as a percentage of the
     * ViewPager's measured width from (0.f-1.f]
     *
     * @param position The position of the page requested
     * @return Proportional width for the given page position
     */
    @Override
    public float getPageWidth(int position) {
        return 1f;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        int virtualPosition = SourcesRegistry.getInstance().getVirtualPosition(position);

        View current = sparseArray.get(virtualPosition);
        adjustViewToOffset(current, positionOffset, true);

        View previouse = sparseArray.get(virtualPosition + 1 == SourcesRegistry.getInstance().getPageCount() ? 0 : virtualPosition + 1);
        adjustViewToOffset(previouse, positionOffset, false);
    }

    @Override
    public void onPageSelected(int position) {
        int virtualPosition = SourcesRegistry.getInstance().getVirtualPosition(position);
        View view = sparseArray.get(virtualPosition);
        if (view != null) {
            view.setScaleX(PRIMARY_ITEM_SCALE);
            view.setScaleY(PRIMARY_ITEM_SCALE);
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {
        // Nothing to do here
    }

    private void adjustViewToOffset(View view, float positionOffset, boolean isPrimary) {
        if (view != null) {
            float scale;

            if (isPrimary) {
                scale = PRIMARY_ITEM_SCALE - positionOffset;
            } else {
                scale = positionOffset <= PRIMARY_ITEM_SCALE ? positionOffset : PRIMARY_ITEM_SCALE;
            }

            if (scale > SECONDARY_ITEM_SCALE) {
                view.setScaleX(scale);
                view.setScaleY(scale);
            } else {
                view.setScaleX(SECONDARY_ITEM_SCALE);
                view.setScaleY(SECONDARY_ITEM_SCALE);
            }
            changeColorFilter(view, positionOffset, isPrimary);
        }
    }

    private void changeColorFilter(View view, float positionOffset, boolean isPrimary) {
        if (view != null) {
            int color;
            if (isPrimary) {
                color = (Integer) mArgbEvaluator.evaluate(positionOffset, colors[1], colors[0]);
            } else {
                color = (Integer) mArgbEvaluator.evaluate(positionOffset, colors[0], colors[1]);
            }
            ImageView imageView = (ImageView) view;
            imageView.getDrawable().clearColorFilter();
            imageView.getDrawable().setColorFilter(color, PorterDuff.Mode.SRC_ATOP);
        }
    }

}