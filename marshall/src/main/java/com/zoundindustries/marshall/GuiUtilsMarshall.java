package com.zoundindustries.marshall;

import android.app.Activity;
import android.util.DisplayMetrics;
import android.widget.ImageView;

/**
 * Created by cvladu on 14/07/2017.
 */

public class GuiUtilsMarshall {

    public static void rescaleImagesBasedOnScreenDensity(Activity activity, ImageView knobLabel, ImageView blinkingLedAnimated) {
        DisplayMetrics metrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);

        float widthPixels = metrics.widthPixels;
        float heightPixels = metrics.heightPixels;
        float scaleFactor = metrics.density;

        float widthDp = widthPixels / scaleFactor;
        float heightDp = heightPixels / scaleFactor;

        if (widthDp > 400 && heightDp > 700) {
            blinkingLedAnimated.getLayoutParams().height = Math.round(280 * scaleFactor);
            blinkingLedAnimated.getLayoutParams().width = Math.round(280 * scaleFactor);
            knobLabel.getLayoutParams().height = Math.round(330 * scaleFactor);
            knobLabel.getLayoutParams().width = Math.round(330 * scaleFactor);
        } else if (widthDp < 400 && heightDp < 600 && scaleFactor <= 2) {
            blinkingLedAnimated.getLayoutParams().height = Math.round(200 * scaleFactor);
            blinkingLedAnimated.getLayoutParams().width = Math.round(200 * scaleFactor);
            knobLabel.getLayoutParams().height = Math.round(240 * scaleFactor);
            knobLabel.getLayoutParams().width = Math.round(240 * scaleFactor);
        }
    }

    public static int dpToPx(int dp, Activity activity) {
        float value =0;
        if (activity != null) {
            DisplayMetrics displayMetrics = activity.getResources().getDisplayMetrics();
            float density = displayMetrics.density;
            value = dp * density;
        }
        
        return Math.round(value);
    }
}
