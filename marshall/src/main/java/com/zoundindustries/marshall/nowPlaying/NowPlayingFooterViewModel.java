package com.zoundindustries.marshall.nowPlaying;

import android.app.Activity;
import android.databinding.Observable;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.View;

import com.apps_lib.multiroom.connection.ConnectionManager;
import com.apps_lib.multiroom.nowPlaying.NowPlayingDataModel;
import com.apps_lib.multiroom.nowPlaying.NowPlayingManager;
import com.frontier_silicon.NetRemoteLib.Node.NodeMultiroomGroupState;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysCapsValidModes;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.zoundindustries.marshall.R;

/**
 * Created by lsuhov on 24/05/16.
 */
public class NowPlayingFooterViewModel {

    protected Activity mActivity;
    NowPlayingDataModel mNowPlayingDataModel;
    boolean mActivityIsPaused = false;
    protected Radio mRadio;
    private Observable.OnPropertyChangedCallback mPropertyChangedCallback;

    public ObservableBoolean footerIsVisible = new ObservableBoolean(false);
    public ObservableBoolean presetIsSelected = new ObservableBoolean(false);
    public ObservableField<String> presetNumber = new ObservableField<>();
    public ObservableBoolean sourceIsSelected = new ObservableBoolean(false);
    public ObservableField<Drawable> sourceImage = new ObservableField<>();
    public ObservableBoolean isPlaying = new ObservableBoolean(false);
    public ObservableBoolean isBuffering = new ObservableBoolean();
    public ObservableBoolean playPauseButtonsLayoutVisible = new ObservableBoolean(false);
    public ObservableField<String> firstLine = new ObservableField<>();
    public ObservableField<String> secondLine = new ObservableField<>();
    public ObservableBoolean isSmallSpotifyLogoVisible = new ObservableBoolean(false);
    public ObservableField<String> nowPlayingContent = new ObservableField<>();
    public ObservableField<String> multiOrSpeakerName = new ObservableField<>();
    public ObservableBoolean isPlayButtonVisible = new ObservableBoolean(false);
    public ObservableBoolean isPauseButtonVisible = new ObservableBoolean(false);
    public ObservableBoolean isStopButtonVisible = new ObservableBoolean(false);

    public void init(Radio radio, Activity activity) {
        mActivity = activity;
        mRadio = radio;
        mNowPlayingDataModel = NowPlayingManager.getInstance().getNowPlayingDataModel();

        addListeners();

        updateAllObservables();
    }

    protected void addListeners() {
        mPropertyChangedCallback = new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable observable, int i) {
                if (observable == mNowPlayingDataModel.indexOfCurrentlyPlayingPreset ||
                        observable == mNowPlayingDataModel.currentMode ||
                        observable == mNowPlayingDataModel.playCaps) {
                    updateAllObservables();
                } else if (observable == mNowPlayingDataModel.playStatus) {
                    updateFooterIsVisible();
                    updateIsBuffering();
                    updateControlButtonsVisibility();
                } else if (observable == mNowPlayingDataModel.playInfoName) {
                    updateFirstLine();
                } else if (observable == mNowPlayingDataModel.playInfoText ||
                        observable == mNowPlayingDataModel.album ||
                        observable == mNowPlayingDataModel.artist) {
                    updateSecondLine();
                    updateNowPlayingContent();
                } else if (observable == mNowPlayingDataModel.multiState) {
                    updateSpeakerName(mNowPlayingDataModel.multiState.get());
                }
            }
        };

        mNowPlayingDataModel.indexOfCurrentlyPlayingPreset.addOnPropertyChangedCallback(mPropertyChangedCallback);
        mNowPlayingDataModel.currentMode.addOnPropertyChangedCallback(mPropertyChangedCallback);
        mNowPlayingDataModel.playCaps.addOnPropertyChangedCallback(mPropertyChangedCallback);
        mNowPlayingDataModel.playStatus.addOnPropertyChangedCallback(mPropertyChangedCallback);
        mNowPlayingDataModel.playInfoName.addOnPropertyChangedCallback(mPropertyChangedCallback);
        mNowPlayingDataModel.playInfoText.addOnPropertyChangedCallback(mPropertyChangedCallback);
        mNowPlayingDataModel.album.addOnPropertyChangedCallback(mPropertyChangedCallback);
        mNowPlayingDataModel.artist.addOnPropertyChangedCallback(mPropertyChangedCallback);
        mNowPlayingDataModel.multiState.addOnPropertyChangedCallback(mPropertyChangedCallback);
    }

    protected void updateAllObservables() {
        updateSelectedSource();
        updateFooterIsVisible();
        updateFirstLine();
        updateSecondLine();
        updatePlayPauseButtonsLayoutVisibility();
        updateNowPlayingContent();
        updateMultiOrSpeakerName();
        updateControlButtonsVisibility();
    }

    private void updateIsBuffering() {
        isBuffering.set(mNowPlayingDataModel.isBuffering());
    }

    private void updatePlayPauseButtonsLayoutVisibility() {
        NodeSysCapsValidModes.Mode mode = mNowPlayingDataModel.currentMode.get();
        playPauseButtonsLayoutVisible.set(mode != NodeSysCapsValidModes.Mode.AuxIn && modeIsSelected(mode));
    }

    private void updateSecondLine() {
        String infoText;
        NodeSysCapsValidModes.Mode mode = mNowPlayingDataModel.currentMode.get();
        if (mode == NodeSysCapsValidModes.Mode.AuxIn) {
            infoText = mActivity.getResources().getString(R.string.carousel_aux_audio_active);
        } else if (mode == NodeSysCapsValidModes.Mode.Bluetooth) {
            infoText = mNowPlayingDataModel.artist.get();
        } else {
            if (!TextUtils.isEmpty(mNowPlayingDataModel.artist.get())) {
                infoText = mNowPlayingDataModel.artist.get();
            } else {
                infoText = mNowPlayingDataModel.playInfoText.get();
            }
        }

        secondLine.set(infoText);
    }

    private void updateFirstLine() {
        String infoName;
        NodeSysCapsValidModes.Mode mode = mNowPlayingDataModel.currentMode.get();
        if (mode == NodeSysCapsValidModes.Mode.AuxIn) {
            infoName = mActivity.getString(R.string.carousel_aux_caps);
        } else {
            infoName = mNowPlayingDataModel.playInfoName.get();
        }

        firstLine.set(infoName);
    }

    private void updateNowPlayingContent() {
        NodeSysCapsValidModes.Mode mode = mNowPlayingDataModel.currentMode.get();
        String infoText = "";

        if (mode == NodeSysCapsValidModes.Mode.AuxIn) {
            infoText = mActivity.getResources().getString(R.string.carousel_aux_audio_active);
        } else if (mode == NodeSysCapsValidModes.Mode.Bluetooth) {
            infoText = mNowPlayingDataModel.artist.get();
        } else {
            if (!TextUtils.isEmpty(mNowPlayingDataModel.playInfoName.get())) {
                infoText = mNowPlayingDataModel.playInfoName.get();
                if (!TextUtils.isEmpty(mNowPlayingDataModel.artist.get())) {
                    infoText += " - ";
                }
            }

            if (!TextUtils.isEmpty(mNowPlayingDataModel.artist.get())) {
                infoText += mNowPlayingDataModel.artist.get();
            } else {
                infoText += mNowPlayingDataModel.playInfoText.get();
            }
        }

        nowPlayingContent.set(infoText);
    }

    private void updateMultiOrSpeakerName() {
        multiOrSpeakerName.set(ConnectionManager.getInstance().isInMultiMode() ? mActivity.getResources().getString(R.string.multi_caps) : mRadio != null ? mRadio.getFriendlyName().toUpperCase() : "");
    }

    private void updateSpeakerName(NodeMultiroomGroupState.Ord multiState) {
        if(multiState == NodeMultiroomGroupState.Ord.NO_GROUP) {
            multiOrSpeakerName.set(mRadio != null ? mRadio.getFriendlyName().toUpperCase() : "");
        } else {
            multiOrSpeakerName.set(mActivity.getResources().getString(R.string.multi_caps));
        }
    }

    private void updateFooterIsVisible() {
        footerIsVisible.set(!mNowPlayingDataModel.isIdle() || modeIsSelected(mNowPlayingDataModel.currentMode.get()));
    }

    private boolean modeIsSelected(NodeSysCapsValidModes.Mode mode) {
        return mode != null && mode != NodeSysCapsValidModes.Mode.UnknownMode &&
                mode != NodeSysCapsValidModes.Mode.None;
    }

    protected void updateSelectedSource() {
        NodeSysCapsValidModes.Mode currentMode = mNowPlayingDataModel.currentMode.get();

        presetIsSelected.set(mNowPlayingDataModel.indexOfCurrentlyPlayingPreset.get() >= 0 &&
                (currentMode == NodeSysCapsValidModes.Mode.Spotify || currentMode == NodeSysCapsValidModes.Mode.IR));

        sourceIsSelected.set(!presetIsSelected.get());

        if (presetIsSelected.get()) {
            presetNumber.set(String.valueOf(mNowPlayingDataModel.indexOfCurrentlyPlayingPreset.get() + 1));
        }
        updateSourceImage(presetIsSelected.get());

        updateSpotifyLogo();
    }

    private void updateSpotifyLogo() {
        NodeSysCapsValidModes.Mode mode = mNowPlayingDataModel.currentMode.get();
        if (mode != null) {
            isSmallSpotifyLogoVisible.set(mode == NodeSysCapsValidModes.Mode.Spotify || (mode == NodeSysCapsValidModes.Mode.Cast && mNowPlayingDataModel.castingApp.get() != null && mNowPlayingDataModel.castingApp.get().equals("Spotify")));
        }
    }

    private void updateSourceImage(boolean presetIsSelected) {
        NodeSysCapsValidModes.Mode mode = mNowPlayingDataModel.currentMode.get();
        if (mode == null) {
            return;
        }
        int imageResource = -1;

        switch (mode) {
            case Spotify:
                if (presetIsSelected) {
                    imageResource = R.drawable.ic_spotify_small;
                    break;
                }
            case IR:
                if (presetIsSelected) {
                    break;
                }
            case AirPlay:
            case Cast:
                imageResource = R.drawable.ic_now_playing_wifi;
                break;
            case Bluetooth:
                imageResource = R.drawable.ic_now_playing_bt;
                break;
            case AuxIn:
                imageResource = R.drawable.ic_now_playing_aux;
                break;
            case RCA:
                imageResource = R.drawable.ic_now_playing_rca;
                break;
        }

        BitmapDrawable bitmapDrawable = null;
        if (imageResource != -1) {
            Bitmap bitmap = BitmapFactory.decodeResource(mActivity.getResources(), imageResource);
            bitmapDrawable = new BitmapDrawable(mActivity.getResources(), bitmap);
        }

        sourceImage.set(bitmapDrawable);
    }

    @SuppressWarnings("unused")
    public void onPlayClicked(View view) {
        mNowPlayingDataModel.setAlbumCover(null);
        NowPlayingManager.getInstance().play();
    }

    @SuppressWarnings("unused")
    public void onPauseClicked(View view) {
        NowPlayingManager.getInstance().pause();
    }

    public void dispose() {
        mActivity = null;
        mRadio = null;

        mNowPlayingDataModel.indexOfCurrentlyPlayingPreset.removeOnPropertyChangedCallback(mPropertyChangedCallback);
        mNowPlayingDataModel.currentMode.removeOnPropertyChangedCallback(mPropertyChangedCallback);
        mNowPlayingDataModel.playCaps.removeOnPropertyChangedCallback(mPropertyChangedCallback);
        mNowPlayingDataModel.playStatus.removeOnPropertyChangedCallback(mPropertyChangedCallback);
        mNowPlayingDataModel.playInfoName.removeOnPropertyChangedCallback(mPropertyChangedCallback);
        mNowPlayingDataModel.playInfoText.removeOnPropertyChangedCallback(mPropertyChangedCallback);
        mNowPlayingDataModel.album.removeOnPropertyChangedCallback(mPropertyChangedCallback);
        mNowPlayingDataModel.artist.removeOnPropertyChangedCallback(mPropertyChangedCallback);
        mNowPlayingDataModel.multiState.removeOnPropertyChangedCallback(mPropertyChangedCallback);

        mNowPlayingDataModel = null;
        mPropertyChangedCallback = null;
    }

    void setPaused(boolean activityIsPaused) {
        mActivityIsPaused = activityIsPaused;
    }

    private void updateControlButtonsVisibility() {
        NodeSysCapsValidModes.Mode currentMode = mNowPlayingDataModel.currentMode.get();

        if (mNowPlayingDataModel.isPlaying()) {
            if (currentMode == NodeSysCapsValidModes.Mode.IR) {
                setStopButtonVisible();
            } else if (currentMode != NodeSysCapsValidModes.Mode.AuxIn && currentMode != NodeSysCapsValidModes.Mode.RCA && currentMode != NodeSysCapsValidModes.Mode.Cast) {
                setPauseButtonVisible();
            } else {
                hidePlayPauseStopButtons();
            }
        } else {
            if (currentMode != NodeSysCapsValidModes.Mode.AuxIn && currentMode != NodeSysCapsValidModes.Mode.RCA && currentMode != NodeSysCapsValidModes.Mode.Cast && currentMode != NodeSysCapsValidModes.Mode.Standby) {
                setPlayButtonVisible();
            }
        }
    }

    private void setPlayButtonVisible() {
        isPlayButtonVisible.set(true);
        isPauseButtonVisible.set(false);
        isStopButtonVisible.set(false);
    }

    private void setStopButtonVisible() {
        isPlayButtonVisible.set(false);
        isPauseButtonVisible.set(false);
        isStopButtonVisible.set(true);
    }

    private void setPauseButtonVisible() {
        isPlayButtonVisible.set(false);
        isPauseButtonVisible.set(true);
        isStopButtonVisible.set(false);
    }

    protected void hidePlayPauseStopButtons() {
        isPlayButtonVisible.set(false);
        isPauseButtonVisible.set(false);
        isStopButtonVisible.set(false);
    }
}
