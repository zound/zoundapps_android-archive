package com.zoundindustries.marshall.factory;

import com.apps_lib.multiroom.factory.ActivityCreator;
import com.zoundindustries.marshall.about.AboutScreenActivity;
import com.zoundindustries.marshall.castTos.CastTosIntroductionActivity;
import com.zoundindustries.marshall.myHome.HomeActivity;
import com.zoundindustries.marshall.setup.normalSetup.FinalSetupActivity;
import com.zoundindustries.marshall.setup.normalSetup.ScanNewSpeakersActivity;
import com.zoundindustries.marshall.setup.presets.PresetsAddingFailedActivity;
import com.zoundindustries.marshall.setup.presets.PresetsListActivity;
import com.zoundindustries.marshall.setup.presets.SetPresetsActivity;
import com.zoundindustries.marshall.setup.presets.SpotifyAuthenticationActivity;
import com.zoundindustries.marshall.setup.presets.SpotifyLinkingSuccessActivity;
import com.zoundindustries.marshall.source.SourcesActivity;
import com.zoundindustries.marshall.settings.solo.speaker.SettingsSpeakerActivity;
import com.zoundindustries.marshall.volume.VolumeActivity;

/**
 * Created by nbalazs on 10/05/2017.
 */

public class MarshallActivityCreator extends ActivityCreator {

    // TODO: Reimplement ScanNewSpeakers and Volume activity and changed the imports to point to the marshall package

    @Override
    public final Class getHomeActivity() {
        return HomeActivity.class;
    }

    @Override
    public final Class getSetupActivity() { return ScanNewSpeakersActivity.class; }

    @Override
    public final Class getVolumeActivity() {
        return VolumeActivity.class;
    }

    @Override
    public Class getSourcesActivity() {
        return SourcesActivity.class;
    }

    @Override
    public Class getSettingsActivity() {
        return SettingsSpeakerActivity.class;
    }

    @Override
    public Class getAboutScreenActivity() {
        return AboutScreenActivity.class;
    }

    @Override
    public Class getCastTosActivity() { return CastTosIntroductionActivity.class; }

    @Override
    public Class getPresetAddingFailedActivity() {
        return PresetsAddingFailedActivity.class;
    }

    @Override
    public Class getSetPresetsActivity() {
        return SetPresetsActivity.class;
    }

    @Override
    public Class getFinalSetupActivity() {
        return FinalSetupActivity.class;
    }

    @Override
    public Class getPresetsListActivity() { return PresetsListActivity.class; }

    @Override
    public Class getSpotifyLinkingSuccessActivity() {
        return SpotifyLinkingSuccessActivity.class;
    }

    @Override
    public Class getSpotifyAuthenticationActivity() {
        return SpotifyAuthenticationActivity.class;
    }


}
