package com.zoundindustries.marshall.help;

import android.content.res.Resources;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import com.apps_lib.multiroom.NavigationHelper;
import com.apps_lib.multiroom.UEActivityBase;
import com.zoundindustries.marshall.R;
import com.zoundindustries.marshall.databinding.ActivityHelpBinding;

/**
 * Created by nbalazs on 31/10/2016.
 */
public class HelpActivity extends UEActivityBase{

    private ActivityHelpBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        View view = LayoutInflater.from(this).inflate(R.layout.activity_help, null);
        mBinding = DataBindingUtil.bind(view);
        setContentView(view);

        setupAppBar();
        enableUpNavigation();
        setTitle(R.string.hamburger_help);

        setupControls();
    }

    private void setupControls() {
        Resources res = getResources();
        String array[] = new String[] {res.getString(R.string.help_quick_guide), res.getString(R.string.help_online_manual), res.getString(R.string.help_contact)};
        ArrayAdapter<String> listAdapter = new HelpScreenListAdapter(this, R.layout.list_item_help_screen, array);
        addHeaderViewAndFooterView();
        mBinding.helpListView.setAdapter(listAdapter);
        mBinding.helpListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 1:
                        NavigationHelper.goToActivity(HelpActivity.this, QuickGuideActivity.class, NavigationHelper.AnimationType.SlideToLeft, false);
                        break;
                    case 2:
                        NavigationHelper.goToActivity(HelpActivity.this, OnlineManualActivity.class, NavigationHelper.AnimationType.SlideToLeft, false);
                        break;
                    case 3:
                        NavigationHelper.goToActivity(HelpActivity.this, ContactActivity.class, NavigationHelper.AnimationType.SlideToLeft, false);
                        break;
                    default:
                        break;
                }
            }
        });
    }

    public void addHeaderViewAndFooterView() {
        View listHeaderView = LayoutInflater.from(this).inflate(R.layout.list_item_header, null);
        View listFooterView = LayoutInflater.from(this).inflate(R.layout.list_item_footer, null);
        mBinding.helpListView.addHeaderView(listHeaderView, null, false);
        mBinding.helpListView.addFooterView(listFooterView, null, false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
