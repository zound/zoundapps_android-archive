package com.zoundindustries.marshall.help;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.apps_lib.multiroom.R;

/**
 * Created by nbalazs on 01/11/2016.
 */

public class HelpScreenListAdapter extends ArrayAdapter<String> {

    public HelpScreenListAdapter(Context context, int resource, String[] array) {
        super(context, resource, array);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;

        if (view == null) {
            view = LayoutInflater.from(getContext()).inflate(R.layout.list_item_help_screen, null);
            TextView titleView = (TextView)view.findViewById(R.id.titleTextView);
            titleView.setText(getItem(position));
        }

        return view;
    }
}
