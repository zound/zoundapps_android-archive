package com.zoundindustries.marshall.myHome;

/**
 * Created by nbalazs on 09/01/2018.
 */

public interface IOldUpdateListener {
    void onDialogFinished();
}
