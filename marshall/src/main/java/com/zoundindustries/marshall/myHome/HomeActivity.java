package com.zoundindustries.marshall.myHome;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.apps_lib.multiroom.IUEChildFragment;
import com.apps_lib.multiroom.NavigationHelper;
import com.apps_lib.multiroom.UEActivityBase;
import com.apps_lib.multiroom.factory.ActivityFactory;
import com.apps_lib.multiroom.factory.EHomeFragmentTag;
import com.apps_lib.multiroom.factory.FragmentFactory;
import com.apps_lib.multiroom.myHome.IDrawerActivity;
import com.apps_lib.multiroom.myHome.IFragmentSwitcher;
import com.apps_lib.multiroom.myHome.NavDrawerItem;
import com.apps_lib.multiroom.myHome.NavDrawerListAdapter;
import com.apps_lib.multiroom.setup.normalSetup.SetupManager;
import com.frontier_silicon.NetRemoteLib.AccessPointUtil;
import com.frontier_silicon.NetRemoteLib.Discovery.IRadioDiscoveryListener;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.components.NetRemoteManager;
import com.frontier_silicon.components.connection.ConnectionStateUtil;
import com.frontier_silicon.components.multiroom.MultiroomGroupManager;
import com.zoundindustries.marshall.R;
import com.zoundindustries.marshall.connection.ConnectionLostFragment;
import com.zoundindustries.marshall.help.HelpActivity;
import com.zoundindustries.marshall.settings.SettingsSoloMultiSwitchActivity;
import com.zoundindustries.marshall.settings.multi.MultiSpeakersActivity;

import java.math.BigInteger;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.ByteOrder;
import java.util.ArrayList;

public class HomeActivity extends UEActivityBase implements IFragmentSwitcher, IDrawerActivity, IRadioDiscoveryListener {

    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private ArrayList<NavDrawerItem> mNavDrawerItems = new ArrayList<>();
    private final int FRAGMENT_CONTAINER_ID = R.id.layoutContentFragment;
    private boolean mIsSpeakerSettingsDisplayed = true;
    private Boolean exit = false;
    private String mSpeakerIPAddress;

    private boolean mIsFirstRun = true;

    private static final String STATE_NAVIGATION_DRAWER_SETTINGS_ENABLED = "settings_enabled";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_home);

        setupControls();

        checkIfOnBoardingNeeded(getIntent());
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        // Save the user's current game state
        savedInstanceState.putBoolean(STATE_NAVIGATION_DRAWER_SETTINGS_ENABLED, mIsSpeakerSettingsDisplayed);

        // Always call the superclass so it can save the view hierarchy state
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        mIsSpeakerSettingsDisplayed = savedInstanceState.getBoolean(STATE_NAVIGATION_DRAWER_SETTINGS_ENABLED, true);
    }

    @Override
    protected void onStart() {
        super.onStart();

        Bundle extras = getIntent().getExtras();
        if (extras != null && extras.getBoolean("DISCONNECTED", false)) {
            getSupportFragmentManager().beginTransaction()
                    .add(FRAGMENT_CONTAINER_ID, new ConnectionLostFragment())
                    .commit();
        } else {
            addMainFragment(extras);
        }
    }

    @Override
    public void onResume() {
        if (currentNetworkIsAP()) {
            NavigationHelper.startSetupActivity(this);
        }

        initNavigationDrawer(mIsSpeakerSettingsDisplayed);


        super.onResume();

        hideToolbar();
    }

    @Override
    protected void onPause() {
        super.onPause();
        NetRemoteManager.getInstance().addRadioDiscoveryListener(this);
    }

    private void initNavigationDrawer(boolean displaySpeakerSettings) {
        mIsSpeakerSettingsDisplayed = displaySpeakerSettings && AccessPointUtil.isConnectedToWiFiOrEthernet();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setupAppBar();
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar,
                R.string.hamburger_open_desc, R.string.hamburger_close_desc);

        mDrawerToggle.setDrawerIndicatorEnabled(false);
        Drawable drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.ic_hamburger_menu, this.getTheme());
        mDrawerToggle.setHomeAsUpIndicator(drawable);
        mDrawerToggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDrawerLayout.isDrawerVisible(GravityCompat.START)) {
                    mDrawerLayout.closeDrawer(GravityCompat.START);
                } else {
                    mDrawerLayout.openDrawer(GravityCompat.START);
                }
            }
        });

        mDrawerLayout.addDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();

        mNavDrawerItems.clear();


        mNavDrawerItems.add(new NavDrawerItem(getString(R.string.hamburger_menu_setup), R.drawable.hamburger_setup_icon));
        if (mIsSpeakerSettingsDisplayed) {
            mNavDrawerItems.add(new NavDrawerItem(getString(R.string.hamburger_menu_settings), R.drawable.hamburger_settings_icon));
        }
        mNavDrawerItems.add(new NavDrawerItem(getString(R.string.hamburger_menu_help), R.drawable.hamburger_help_icon));
        mNavDrawerItems.add(new NavDrawerItem(getString(R.string.hamburger_menu_shop), R.drawable.hamburger_shop_icon));
        mNavDrawerItems.add(new NavDrawerItem(getString(R.string.hamburger_menu_about), R.drawable.hamburger_about_icon));

        ListView mDrawerList = (ListView) findViewById(R.id.navList);
        NavDrawerListAdapter navDrawerListAdapter = new NavDrawerListAdapter(this, mNavDrawerItems);

        mDrawerList.setAdapter(navDrawerListAdapter);
        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                onNavDrawerListItemClicked(position);
            }
        });
    }

    private void addMainFragment(Bundle savedInstanceState) {
        // Check that the activity is using the layout version with
        // the fragment_container FrameLayout
        if (findViewById(R.id.layoutContentFragment) == null)
            return;

        // However, if we're being restored from a previous state,
        // then we don't need to do anything and should return or else
        // we could end up with overlapping fragments.
        if (savedInstanceState != null)
            return;

        hideAllActionButtons();

        EHomeFragmentTag fragmentTag = getTagOfNeededFragment();

        Fragment fragment = FragmentFactory.getFragment(fragmentTag);

        adjustHeaderContent(fragmentTag);

        if (fragment != null) {

            // In case this activity was started with special instructions from an
            // Intent, pass the Intent's extras to the fragment as arguments
            fragment.setArguments(getIntent().getExtras());

            getSupportFragmentManager().beginTransaction()
                    .replace(FRAGMENT_CONTAINER_ID, fragment)
                    .commitAllowingStateLoss();
        }
    }

    private void adjustHeaderContent(EHomeFragmentTag fragmentTag) {
        if (fragmentTag.equals(EHomeFragmentTag.ConnectionLostFragment) || fragmentTag.equals(EHomeFragmentTag.NoWiFiFragment)
                || fragmentTag.equals(EHomeFragmentTag.ScanExistingSpeakers)) {
            hideToolbar();
        } else {
            showToolbar();
        }
    }

    private void hideToolbar() {
        if (getSupportActionBar() != null) {
            if (mIsFirstRun) {
                mIsFirstRun = false;
                getSupportActionBar().hide();
                mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            }
        }
    }

    private void showToolbar() {
        if (getSupportActionBar() != null && !getSupportActionBar().isShowing()) {
            getSupportActionBar().show();
            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        }
    }

    @Override
    public void switchFragment(EHomeFragmentTag fragmentTag) {

        if (fragmentTag.equals(EHomeFragmentTag.NoWiFiFragment)) {
            initNavigationDrawer(false);
        }

        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment currentFragment = fragmentManager.findFragmentById(FRAGMENT_CONTAINER_ID);

        if (currentFragment != null) {
            IUEChildFragment childFragment = (IUEChildFragment) currentFragment;
            EHomeFragmentTag tagOfCurrentFragment = childFragment.getStaticTag();

            if (tagOfCurrentFragment.equals(fragmentTag)) {
                return;
            }
        }

        HomeActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                hideAllActionButtons();
            }
        });

        Fragment newFragment = FragmentFactory.getFragment(fragmentTag);

        adjustHeaderContent(fragmentTag);

        FragmentTransaction transaction = fragmentManager.beginTransaction();

        // Replace whatever is in the fragment_container view with this fragment,
        // and add the transaction to the back stack so the user can navigate back
        transaction.replace(FRAGMENT_CONTAINER_ID, newFragment);
        //transaction.addToBackStack(null);

        // Commit the transaction
        transaction.commit();
    }

    private void hideAllActionButtons() {
        View refreshButton = findViewById(R.id.buttonRefresh);
        refreshButton.setVisibility(View.GONE);

        View volumeButton = findViewById(R.id.buttonVolume);
        volumeButton.setVisibility(View.GONE);
    }

    private EHomeFragmentTag getTagOfNeededFragment() {
        switch (ConnectionStateUtil.getInstance().getConnectionState()) {
            case NO_WIFI_OR_ETHERNET:
                return EHomeFragmentTag.NoWiFiFragment;
            case DISCONNECTED:
            case INVALID_SESSION:
                return EHomeFragmentTag.ConnectionLostFragment;
            default:
                if (canGoToHome()) {
                    return EHomeFragmentTag.HomeFragment;
                }
                return EHomeFragmentTag.ScanExistingSpeakers;
        }
    }

    private boolean canGoToHome() {
        return NetRemoteManager.getInstance().getRadios().size() != 0 &&
                MultiroomGroupManager.getInstance().getFlatGroupDeviceList().size() != 0;
    }

    private void setupControls() {
        ImageView closeNavigation = (ImageView) findViewById(R.id.close_drawer);
        if (closeNavigation != null) {
            closeNavigation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mDrawerLayout.closeDrawer(GravityCompat.START);
                }
            });
        }
    }

    @Override
    protected void onDestroy() {
        mDrawerLayout.removeDrawerListener(mDrawerToggle);
        mDrawerLayout = null;
        mDrawerToggle = null;

        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            closeAppWhenDoubleTapBack();
        }
    }

    private void closeAppWhenDoubleTapBack() {
        if (exit) {
            finish();
        } else {
            Toast.makeText(this, getString(R.string.press_back_again), Toast.LENGTH_SHORT).show();
            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 3 * 1000);
        }
    }

    @Override
    public void refreshNavigationDrawer(boolean displaySpeakerSettings) {
        initNavigationDrawer(displaySpeakerSettings);
    }

    private void onNavDrawerListItemClicked(int position) {
        if (mIsSpeakerSettingsDisplayed) {
            switch (position) {
                case 0:
                    openSetupNewSpeakerActivity();
                    break;
                case 1:
                    openSettingsActivity();
                    break;
                case 2:
                    openHelpActivity();
                    break;
                case 3:
                    openWebShop();
                    break;
                case 4:
                    openAboutActivity();
                    break;
            }
        } else {
            switch (position) {
                case 0:
                    openSetupNewSpeakerActivity();
                    break;
                case 1:
                    openHelpActivity();
                    break;
                case 2:
                    openWebShop();
                    break;
                case 3:
                    openAboutActivity();
                    break;
            }
        }

        mDrawerLayout.closeDrawer(GravityCompat.START);
    }

    private void openSettingsActivity() {
        NavigationHelper.goToActivity(this, MultiSpeakersActivity.class, NavigationHelper.AnimationType.SlideToLeft);
    }

    private void openWebShop() {
        NavigationHelper.openWebViewActivityForURL(this, NavigationHelper.AnimationType.Normal, getResources().getString(R.string.link_marshall_shop));
    }

    private void openSetupNewSpeakerActivity() {
        NavigationHelper.startSetupActivity(this);
    }

    private void openAboutActivity() {
        NavigationHelper.goToActivity(this, ActivityFactory.getActivityFactory().getAboutScreenActivity(), NavigationHelper.AnimationType.SlideToLeft);
    }

    private void openHelpActivity() {
        NavigationHelper.goToActivity(this, HelpActivity.class, NavigationHelper.AnimationType.SlideToLeft);
    }

    private boolean currentNetworkIsAP() {
        String ipAddressString;

        int ipAddress = AccessPointUtil.getWifiManager().getDhcpInfo().gateway;

        // Convert little-endian to big-endianif needed
        if (ByteOrder.nativeOrder().equals(ByteOrder.LITTLE_ENDIAN)) {
            ipAddress = Integer.reverseBytes(ipAddress);
        }

        byte[] ipByteArray = BigInteger.valueOf(ipAddress).toByteArray();

        try {
            ipAddressString = InetAddress.getByAddress(ipByteArray).getHostAddress();
        } catch (UnknownHostException ex) {
            ipAddressString = null;
        }

        return ipAddressString != null && ipAddressString.equals(NetRemoteManager.RADIO_HUI_DEFAULT_ADDRESS);
    }

    @Override
    public void onRadioFound(Radio radio) {
        if (mSpeakerIPAddress != null && mSpeakerIPAddress.length() > 0 && radio.getIpAddress().equals(mSpeakerIPAddress)) {
//            NetRemoteManager.getInstance().connectToRadio(radio, true, true, true);
            onRadioMatch(radio);
            NetRemoteManager.getInstance().removeRadioDiscoveryListener(this);
        }
    }

    @Override
    public void onRadioLost(Radio radio) {

    }

    @Override
    public void onRadioUpdated(Radio radio) {

    }

    private void onRadioMatch(Radio radio) {
        if (radio != null) {
            SetupManager.getInstance().setRadioForPresets(radio);
            NavigationHelper.goToActivity(this, ActivityFactory.getActivityFactory().getSetPresetsActivity(), NavigationHelper.AnimationType.SlideToLeft, true);
            Toast.makeText(this, "Radio: " + radio.getFriendlyName(), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Radio: NOT FOUND", Toast.LENGTH_SHORT).show();
        }
    }

    private void checkIfOnBoardingNeeded(Intent intent) {
        mSpeakerIPAddress = intent.getStringExtra("ip_address");
        if (mSpeakerIPAddress != null && mSpeakerIPAddress.length() > 0) {
            NetRemoteManager.getInstance().addRadioDiscoveryListener(this);
            NetRemoteManager.getInstance().startBackgroundRadioScan(false);
            Toast.makeText(this, "Registered for radio updates for ip:" + mSpeakerIPAddress, Toast.LENGTH_SHORT).show();
        }
    }
}
