package com.zoundindustries.marshall.myHome;

import android.app.Activity;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;

import com.apps_lib.multiroom.myHome.HomeSpeakersBuilder;
import com.apps_lib.multiroom.myHome.SwitchSpeakerBetweenSoloMultiEvent;
import com.apps_lib.multiroom.myHome.speakers.SpeakerModel;
import com.apps_lib.multiroom.myHome.speakers.SpeakerType;
import com.apps_lib.multiroom.myHome.speakers.SpeakersMediator;
import com.frontier_silicon.NetRemoteLib.Node.NodeInfo;
import com.frontier_silicon.NetRemoteLib.Node.NodeMiscNvsData;
import com.frontier_silicon.NetRemoteLib.Radio.IGetNodeCallback;
import com.frontier_silicon.NetRemoteLib.Radio.NodeErrorResponse;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.components.multiroom.MultiroomGroupManager;
import com.frontier_silicon.components.multiroom.MultiroomItemModel;
import com.zoundindustries.marshall.speakerImages.MarshallSpeakerTypeDecoder;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

/**
 * Created by lsuhov on 12/06/16.
 */
public class HomeSpeakersMediator extends SpeakersMediator {

    public static List<Radio> mNotConfiguredRadios = new ArrayList<>();

    private IConnectToRadioListener mIRadioListUpdateListener = null;

    public static String mSpeakerIP = null;

    interface IConnectToRadioListener {
        void onConnectToRadioNeeded(SpeakerModel speakerModel);
    }

    void setCnnectToRadioListener(IConnectToRadioListener listener) {
        mIRadioListUpdateListener = listener;
    }

    public void init(Activity activity, RecyclerView recyclerView) {
        mAdapter = new HomeSpeakersAdapter(activity);
        mSpeakersBuilder = new HomeSpeakersBuilder();
        EventBus.getDefault().register(this);
        super.init(activity, recyclerView);
    }

    public void checkOnboardingForSpeakers() {
        checkOnboarding();
    }

    @Subscribe
    public void onSwapHomeSpeakerEvent(SwitchSpeakerBetweenSoloMultiEvent switchSpeakerBetweenSoloMultiEvent) {
        List<SpeakerModel> homeList = mSpeakersBuilder.switchSpeakerBetweenSoloMultiAndReturnFakeList(switchSpeakerBetweenSoloMultiEvent.mSpeakerModel, switchSpeakerBetweenSoloMultiEvent.mIsChecked);

        mAdapter.swapItems(homeList);

        checkOnboarding();
    }

    public void dispose() {
        super.dispose();

        EventBus.getDefault().unregister(this);
    }

    private void checkOnboarding() {
        if (mActivity == null) {
            return;
        }

        List<MultiroomItemModel> multiroomItemModels = MultiroomGroupManager.getInstance().getFlatGroupDeviceList();
        final List<SpeakerModel> homeSpeakers = mSpeakersBuilder.buildHomeSpeakersFromMultiroomModels(multiroomItemModels, mActivity.getApplicationContext());

        CheckOnBoardingTask task = new CheckOnBoardingTask(homeSpeakers);
        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private class CheckOnBoardingTask extends AsyncTask<Void, Void, Void> {

        private List<SpeakerModel> mSpeakerModels;
        private CountDownLatch mCountDownLatch;

        CheckOnBoardingTask(List<SpeakerModel> speakerModels) {
            mNotConfiguredRadios.clear();

            mSpeakerModels = new ArrayList<>(speakerModels);
            mCountDownLatch = new CountDownLatch(speakerModels.size());
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            for (SpeakerModel speakerModel : mSpeakerModels) {
                if (speakerModel.deviceModel != null) {
                    final Radio radio = speakerModel.deviceModel.mRadio;
                    if (!MarshallSpeakerTypeDecoder.isMarshallSpeaker(radio.getModelName())) {
                        mCountDownLatch.countDown();
                        continue;
                    }
                    radio.getNode(NodeMiscNvsData.class, false, new IGetNodeCallback() {
                        @Override
                        public void getNodeResult(NodeInfo node) {
                            NodeMiscNvsData data = (NodeMiscNvsData) node;
                            String str = data.getValue();
                            try {
                                JSONObject jsonObject = new JSONObject(str);
                                if (jsonObject.has("s")) {
                                    String value = jsonObject.getString("s");
                                    if (!value.equals("1")) {
                                        mNotConfiguredRadios.add(radio);
                                    }
                                } else {
                                    mNotConfiguredRadios.add(radio);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                mNotConfiguredRadios.add(radio);
                            }
                            mCountDownLatch.countDown();
                        }

                        @Override
                        public void getNodeError(Class nodeType, NodeErrorResponse error) {
                            mCountDownLatch.countDown();
                        }
                    });
                } else {
                    mCountDownLatch.countDown();
                }
            }

            try {
                mCountDownLatch.await();
            } catch (Exception e) {
                e.printStackTrace();
            }

//            if (mNotConfiguredRadios != null && mNotConfiguredRadios.size() == 0 && mSpeakerIP != null && mSpeakerIP.length() > 0) {
//                for (SpeakerModel speakerModel : mSpeakerModels) {
//                    if (speakerModel.deviceModel.mRadio.getIpAddress().equals(mSpeakerIP)) {
//                        mIRadioListUpdateListener.onConnectToRadioNeeded(speakerModel);
//                        break;
//                    }
//                }
//            }

            if (mSpeakerIP != null && mSpeakerIP.length() > 0) {
                for (SpeakerModel speakerModel : mSpeakerModels) {
                    if (speakerModel.type == SpeakerType.Speaker && speakerModel.deviceModel.mRadio.getIpAddress().equals(mSpeakerIP)) {
                        mIRadioListUpdateListener.onConnectToRadioNeeded(speakerModel);
                        break;
                    }
                }
            }

            // Ignore the speaker in the next
            mSpeakerIP = null;

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
//            if (mNotConfiguredRadios.size() > 0) {
//                NavigationHelper.startSetupActivity(mActivity);
//            }
        }
    }
}
