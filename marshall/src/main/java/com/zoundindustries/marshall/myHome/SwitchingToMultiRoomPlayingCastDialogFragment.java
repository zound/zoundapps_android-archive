package com.zoundindustries.marshall.myHome;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.apps_lib.multiroom.AnimatedDialogFragmentBase;
import com.apps_lib.multiroom.myHome.ISwitchCastToMultiroomListener;
import com.zoundindustries.marshall.R;

/**
 * Created by nbalazs on 28/11/2016.
 */

public class SwitchingToMultiRoomPlayingCastDialogFragment extends AnimatedDialogFragmentBase {

    private ISwitchCastToMultiroomListener mListener;
    private boolean mShouldDismiss = false;

    public static DialogFragment newInstance(final ISwitchCastToMultiroomListener listener) {
        SwitchingToMultiRoomPlayingCastDialogFragment dialogFragment = new SwitchingToMultiRoomPlayingCastDialogFragment();
        dialogFragment.mListener = listener;
        return dialogFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_switching_to_multi_during_cast, container, false);

        Window window = getDialog().getWindow();
        if (window != null) {
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }

        setupControls(view);

        return view;
    }

    private void setupControls(View view) {
        (view.findViewById(R.id.buttonContinue)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onResult(true);
                dismiss();
            }
        });

        (view.findViewById(R.id.buttonLearnMore)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onResult(false);
                dismiss();
            }
        });
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);

        mListener = null;
    }

    @Override
    public void onStart() {
        super.onStart();

        if (mShouldDismiss) {
            dismiss();
        }
    }

    @Override
    public void onStop() {
        super.onStop();

        mShouldDismiss = true;
    }
}
