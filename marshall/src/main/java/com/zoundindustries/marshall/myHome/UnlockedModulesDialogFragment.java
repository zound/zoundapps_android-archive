package com.zoundindustries.marshall.myHome;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.apps_lib.multiroom.AnimatedDialogFragmentBase;
import com.apps_lib.multiroom.myHome.IUnlockedSpeakersDialogListener;
import com.apps_lib.multiroom.myHome.speakers.SpeakerModel;
import com.apps_lib.multiroom.persistence.PersistenceMgr;
import com.zoundindustries.marshall.R;
import com.zoundindustries.marshall.databinding.FragmentUnlockedSpeakersBinding;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by cvladu on 07/11/2017.
 */

public class UnlockedModulesDialogFragment extends AnimatedDialogFragmentBase {
    private SpeakerModel mSpeakerModel;
    private IUnlockedSpeakersDialogListener mListener;
    private FragmentUnlockedSpeakersBinding mBinding;

    public static UnlockedModulesDialogFragment newInstance(SpeakerModel speakerModel, IUnlockedSpeakersDialogListener listener) {
        UnlockedModulesDialogFragment fragment = new UnlockedModulesDialogFragment();
        fragment.mSpeakerModel = speakerModel;
        fragment.mListener = listener;

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_unlocked_speakers, container, false);

        setTextMessage();
        handleButtons();

        Window window = getDialog().getWindow();
        if (window != null) {
            window.requestFeature(Window.FEATURE_NO_TITLE);
        }

        return mBinding.getRoot();
    }

    private void setTextMessage() {
        if (mSpeakerModel == null) {
            return;
        }

        mBinding.textMessage.setText(getString(R.string.unlocked_speakers_message, mSpeakerModel.deviceModel.mRadio.getFriendlyName(), getString(R.string.unlocked_ultimate_date)));


    }

    private void handleButtons() {
        if (mListener == null) {
            return;
        }

        mBinding.buttonGotIt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.onDialogFinished();
                setCurrentDateToPreferences();
                dismiss();
            }
        });
    }

    private void setCurrentDateToPreferences() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date currentDate = Calendar.getInstance().getTime();
        PersistenceMgr.getInstance().setCheckingDateForUnlockedSpeakers(mSpeakerModel.deviceModel.mRadio.getSN(), simpleDateFormat.format(currentDate));
    }
}