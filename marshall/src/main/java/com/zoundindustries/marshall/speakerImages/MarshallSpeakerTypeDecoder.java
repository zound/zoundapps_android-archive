package com.zoundindustries.marshall.speakerImages;

import com.apps_lib.multiroom.speakerImages.ESpeakerModelType;
import com.apps_lib.multiroom.speakerImages.SpeakerTypeDecoder;

/**
 * Created by nbalazs on 09/05/2017.
 */

public class MarshallSpeakerTypeDecoder extends SpeakerTypeDecoder {

    public MarshallSpeakerTypeDecoder() {}

    @Override
    public ESpeakerModelType getSpeakerModelFromSSID(String ssid) {
        ESpeakerModelType speakerModelType = getMarshallSpeakerModel(ssid);
        if (speakerModelType == ESpeakerModelType.Unknown) {
            speakerModelType = getUrbanearsSpeakerModel(ssid, "");
        }
        return speakerModelType;
    }

    @Override
    public ESpeakerModelType getSpeakerModelFromModelName(String modelName, String softwareVersion) {
        ESpeakerModelType speakerModelType = getMarshallSpeakerModel(modelName);
        if (speakerModelType == ESpeakerModelType.Unknown) {
            speakerModelType = getUrbanearsSpeakerModel(modelName, softwareVersion);
        }
        return speakerModelType;
    }

    public static  boolean isMarshallSpeaker(String modelName) {
        ESpeakerModelType speakerModelType = getMarshallSpeakerModel(modelName);

        if (speakerModelType == ESpeakerModelType.Unknown) {
            return false;
        }

        return true;
    }

    public static boolean isUrbanearsSpeaker(String modelName) {
        ESpeakerModelType speakerModelType = getUrbanearsSpeakerModel(modelName,"");

        if (speakerModelType == ESpeakerModelType.Unknown) {
            return  false;
        }

        return true;
    }
}
