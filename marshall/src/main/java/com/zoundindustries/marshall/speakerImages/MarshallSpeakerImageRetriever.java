package com.zoundindustries.marshall.speakerImages;

import com.apps_lib.multiroom.speakerImages.ESpeakerImageSizeType;
import com.apps_lib.multiroom.speakerImages.ESpeakerModelType;
import com.apps_lib.multiroom.speakerImages.SpeakerImageRetriever;
import com.zoundindustries.marshall.R;

/**
 * Created by nbalazs on 09/05/2017.
 */
public class MarshallSpeakerImageRetriever extends SpeakerImageRetriever {

    @Override
    protected int getImageIdForESpeakerModelType(ESpeakerModelType speakerModelType, ESpeakerImageSizeType sizeType) {
        switch (speakerModelType) {
            case BaggenConcreteGrey:
                switch (sizeType) {
                    case Home:
                        return R.drawable.ic_baggen_grey_home;
                    case Large:
                        return R.drawable.ic_baggen_grey_large;
                    case Volume:
                        return R.drawable.ic_baggen_grey_volume;
                }
            case BaggenDirtyPink:
                switch (sizeType) {
                    case Home:
                        return R.drawable.ic_baggen_pink_home;
                    case Large:
                        return R.drawable.ic_baggen_pink_large;
                    case Volume:
                        return R.drawable.ic_baggen_pink_volume;
                }
            case BaggenGoldFish:
                switch (sizeType) {
                    case Home:
                        return R.drawable.ic_baggen_orange_home;
                    case Large:
                        return R.drawable.ic_baggen_orange_large;
                    case Volume:
                        return R.drawable.ic_baggen_orange_volume;
                }
            case BaggenIndigo:
                switch (sizeType) {
                    case Home:
                        return R.drawable.ic_baggen_blue_home;
                    case Large:
                        return R.drawable.ic_baggen_blue_large;
                    case Volume:
                        return R.drawable.ic_baggen_blue_volume;
                }
            case BaggenPlantGreen:
                switch (sizeType) {
                    case Home:
                        return R.drawable.ic_baggen_green_home;
                    case Large:
                        return R.drawable.ic_baggen_green_large;
                    case Volume:
                        return R.drawable.ic_baggen_green_volume;
                }
            case BaggenVinylBlack:
                switch (sizeType) {
                    case Home:
                        return R.drawable.ic_baggen_black_home;
                    case Large:
                        return R.drawable.ic_baggen_black_large;
                    case Volume:
                        return R.drawable.ic_baggen_black_volume;
                }
            case StammenConcreteGrey:
                switch (sizeType) {
                    case Home:
                        return R.drawable.ic_stammen_grey_home;
                    case Large:
                        return R.drawable.ic_stammen_grey_large;
                    case Volume:
                        return R.drawable.ic_stammen_grey_volume;
                }
            case StammenDirtyPink:
                switch (sizeType) {
                    case Home:
                        return R.drawable.ic_stammen_pink_home;
                    case Large:
                        return R.drawable.ic_stammen_pink_large;
                    case Volume:
                        return R.drawable.ic_stammen_pink_volume;
                }
            case StammenGoldFish:
                switch (sizeType) {
                    case Home:
                        return R.drawable.ic_stammen_orange_home;
                    case Large:
                        return R.drawable.ic_stammen_orange_large;
                    case Volume:
                        return R.drawable.ic_stammen_orange_volume;
                }
            case StammenIndigo:
                switch (sizeType) {
                    case Home:
                        return R.drawable.ic_stammen_blue_home;
                    case Large:
                        return R.drawable.ic_stammen_blue_large;
                    case Volume:
                        return R.drawable.ic_stammen_blue_volume;
                }
            case StammenPlantGreen:
                switch (sizeType) {
                    case Home:
                        return R.drawable.ic_stammen_green_home;
                    case Large:
                        return R.drawable.ic_stammen_green_large;
                    case Volume:
                        return R.drawable.ic_stammen_green_volume;
                }
            case StammenVinylBlack:
                switch (sizeType) {
                    case Home:
                        return R.drawable.ic_stammen_black_home;
                    case Large:
                        return R.drawable.ic_stammen_black_large;
                    case Volume:
                        return R.drawable.ic_stammen_black_volume;
                }
            case ActonBlack:
                switch (sizeType) {
                    case Home:
                        return R.drawable.ic_acton_black_home;
                    case Large:
                        return R.drawable.ic_acton_black_large;
                    case Volume:
                        return R.drawable.ic_acton_black_volume;
                }
            case ActonCream:
                switch (sizeType) {
                    case Home:
                        return R.drawable.ic_acton_cream_home;
                    case Large:
                        return R.drawable.ic_acton_cream_large;
                    case Volume:
                        return R.drawable.ic_acton_cream_volume;
                }
            case StanmoreBlack:
                switch (sizeType) {
                    case Home:
                        return R.drawable.ic_stanmore_black_home;
                    case Large:
                        return R.drawable.ic_stanmore_black_large;
                    case Volume:
                        return R.drawable.ic_stanmore_black_volume;
                }
            case StanmoreCream:
                switch (sizeType) {
                    case Home:
                        return R.drawable.ic_stanmore_cream_home;
                    case Large:
                        return R.drawable.ic_stanmore_cream_large;
                    case Volume:
                        return R.drawable.ic_stanmore_cream_volume;
                }
            case WoburnBlack:
                switch (sizeType) {
                    case Home:
                        return R.drawable.ic_woburn_black_home;
                    case Large:
                        return R.drawable.ic_woburn_black_large;
                    case Volume:
                        return R.drawable.ic_woburn_black_volume;
                }
            case WoburnCream:
                switch (sizeType) {
                    case Home:
                        return R.drawable.ic_woburn_cream_home;
                    case Large:
                        return R.drawable.ic_woburn_cream_large;
                    case Volume:
                        return R.drawable.ic_woburn_cream_volume;
                }
            case LotsenVinylBlack:
                switch (sizeType) {
                    case Home:
                        return R.drawable.ic_lotsen_black_home;
                    case Large:
                        return R.drawable.ic_lotsen_black_large;
                    case Volume:
                        return R.drawable.ic_lotsen_black_volume;
                }
            case LotsenConcreteGrey:
                switch (sizeType) {
                    case Home:
                        return R.drawable.ic_lotsen_grey_home;
                    case Large:
                        return R.drawable.ic_lotsen_grey_large;
                    case Volume:
                        return R.drawable.ic_lotsen_grey_volume;
                }
            case LotsenDirtyPink:
                switch (sizeType) {
                    case Home:
                        return R.drawable.ic_lotsen_pink_home;
                    case Large:
                        return R.drawable.ic_lotsen_pink_large;
                    case Volume:
                        return R.drawable.ic_lotsen_pink_volume;
                }
            case LotsenGoldFish:
                switch (sizeType) {
                    case Home:
                        return R.drawable.ic_lotsen_orange_home;
                    case Large:
                        return R.drawable.ic_lotsen_orange_large;
                    case Volume:
                        return R.drawable.ic_lotsen_orange_volume;
                }
            case LotsenIndigo:
                switch (sizeType) {
                    case Home:
                        return R.drawable.ic_lotsen_indigo_home;
                    case Large:
                        return R.drawable.ic_lotsen_indigo_large;
                    case Volume:
                        return R.drawable.ic_lotsen_indigo_volume;
                }
            case LotsenPlantGreen:
                switch (sizeType) {
                    case Home:
                        return R.drawable.ic_lotsen_green_home;
                    case Large:
                        return R.drawable.ic_lotsen_green_large;
                    case Volume:
                        return R.drawable.ic_lotsen_green_volume;
                }

            case BaggenUnlocked:
                switch (sizeType) {
                    case Home:
                        return R.drawable.ic_urbanears_unlocked_home;
                    case Volume:
                        return R.drawable.ic_urbanears_unlocked_volume;
                }

            case StammenUnlocked:
                switch (sizeType) {
                    case Home:
                        return R.drawable.ic_urbanears_unlocked_home;
                    case Volume:
                        return R.drawable.ic_urbanears_unlocked_volume;
                }


            default:
                switch (sizeType) {
                    case Home:
                        return R.drawable.ic_marshall_speaker_placeholder_home;
                    case Volume:
                        return R.drawable.ic_marshall_speaker_placeholder_volume;
                }
        }

        return R.drawable.ic_marshall_speaker_placeholder_home;
    }
}
