package com.zoundindustries.marshall.settings.solo.speaker;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.apps_lib.multiroom.NavigationHelper;
import com.apps_lib.multiroom.UEActivityBase;
import com.apps_lib.multiroom.myHome.speakers.SpeakerNameManager;
import com.apps_lib.multiroom.settings.RadioFromBundleExtractor;
import com.apps_lib.multiroom.speakerImages.ESpeakerModelType;
import com.frontier_silicon.NetRemoteLib.Node.NodeInfo;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysInfoFriendlyName;
import com.frontier_silicon.NetRemoteLib.Radio.IGetNodeCallback;
import com.frontier_silicon.NetRemoteLib.Radio.NodeErrorResponse;
import com.zoundindustries.marshall.settings.solo.SettingsViewModel;
import com.apps_lib.multiroom.settings.solo.speaker.IRetrieveTaskListener;
import com.apps_lib.multiroom.settings.solo.speaker.SpeakerSettingModel;
import com.apps_lib.multiroom.settings.solo.speaker.SpeakerSettingType;
import com.apps_lib.multiroom.speakerImages.ESpeakerImageSizeType;
import com.apps_lib.multiroom.speakerImages.ISpeakerImageIdListener;
import com.apps_lib.multiroom.speakerImages.SpeakerManager;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.components.common.ExternalAppsOpener;
import com.zoundindustries.marshall.R;
import com.zoundindustries.marshall.databinding.ActivitySettingsSpeakerBinding;
import com.zoundindustries.marshall.settings.solo.aboutSpeaker.AboutSpeakerActivity;
import com.zoundindustries.marshall.settings.solo.lightIntensity.LightIntensityManager;
import com.zoundindustries.marshall.settings.solo.lightIntensity.LightsActivity;

import java.util.ArrayList;
import java.util.List;

public class SettingsSpeakerActivity extends UEActivityBase {

    private ActivitySettingsSpeakerBinding mBinding;
    private Radio mRadio;
    private SettingsViewModel mSettingsViewModel;
    private Activity mActivity = this;
    ProgressBar progressBar = null;
    ImageView imageView = null;
    private boolean handleDispose = false;
    private boolean clicked = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_settings_speaker);

        setupAppBar();
        enableUpNavigation();

        mRadio = RadioFromBundleExtractor.extractRadio(getIntent().getExtras(), SettingsSpeakerActivity.class);

        mSettingsViewModel = new SettingsViewModel(this, mRadio);

        if (mSettingsViewModel.startHomeActivityIfNeeded()) {
            return;
        }

        setupControls();

        if (mRadio != null) {
            setTitle(mRadio.getFriendlyName());
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        updateSpeakerNameIfChanged();
    }

    @Override
    protected void onResume() {
        super.onResume();
        initEvents();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mSettingsViewModel.stopListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (handleDispose) {
            dispose();
        }
    }

    private void setupControls() {
        if (mSettingsViewModel.startHomeActivityIfNeeded()) {
            return;
        }

        List<SpeakerSettingModel> speakerSettingTypeList = getSpeakerSettings();
        final SettingsSpeakerAdapter adapter = new SettingsSpeakerAdapter(this,
                R.layout.list_item_settings_speaker, speakerSettingTypeList);

        mBinding.settingsListView.setAdapter(adapter);
        mBinding.settingsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                SpeakerSettingModel model = adapter.getItem(position );
                if (!clicked) {
                    onSpeakerSettingClicked(model, view);
                    clicked = true;
                }

            }
        });

        updateSpeakerImage();
    }

    private void updateSpeakerImage() {
        SpeakerManager.getInstance().retrieveImageIdForRadio(mRadio, ESpeakerImageSizeType.Large, new ISpeakerImageIdListener() {
            @Override
            public void onImageIdRetrieved(int imageId) {
                if (isFinishing()) {
                    return;
                }

                mBinding.speakerImageView.setImageResource(imageId);
            }
        });
    }

    private List<SpeakerSettingModel> getSpeakerSettings() {
        ArrayList<SpeakerSettingModel> speakerSettingTypeList = new ArrayList<>();

        SpeakerSettingModel aboutSetting = new SpeakerSettingModel();
        aboutSetting.speakerSettingType = SpeakerSettingType.About;
        aboutSetting.name = getString(R.string.about_this_speaker);
        speakerSettingTypeList.add(aboutSetting);

        SpeakerSettingModel changeNameSetting = new SpeakerSettingModel();
        changeNameSetting.speakerSettingType = SpeakerSettingType.ChangeName;
        changeNameSetting.name = getString(R.string.change_name);
        speakerSettingTypeList.add(changeNameSetting);

        SpeakerSettingModel equalizerSetting = new SpeakerSettingModel();
        equalizerSetting.speakerSettingType = SpeakerSettingType.LedAdjuster;
        equalizerSetting.name = getString(R.string.settings_lights);
        speakerSettingTypeList.add(equalizerSetting);

//        SpeakerSettingModel soundsSettings = new SpeakerSettingModel();
//        soundsSettings.speakerSettingType = SpeakerSettingType.Sounds;
//        soundsSettings.name = getString(R.string.settings_sounds);
//        speakerSettingTypeList.add(soundsSettings);

        return speakerSettingTypeList;
    }

    private void onSpeakerSettingClicked(SpeakerSettingModel model, View view) {
        if (model == null || model.speakerSettingType == null) {
            return;
        }

        switch (model.speakerSettingType) {
            case About:
                openSubSettingActivity(AboutSpeakerActivity.class);
                break;
            case ChangeName:
                openGoogleHomeToRenameSpeaker();
                break;
            case LedAdjuster:
                retrieveDataAndOpenLightIntensityActivity(view);
                break;
//            case Sounds:
//                openSoundsActivity();
//                break;
            default:
                break;
        }
    }

    private void retrieveDataAndOpenLightIntensityActivity(View view) {
        if (mSettingsViewModel.startHomeActivityIfNeeded()) {
            return;
        }

        handleDispose = true;
        initButtons(view);
        LightIntensityManager lightIntensityManager = LightIntensityManager.getInstance();
        lightIntensityManager.init(mActivity, mRadio);
        lightIntensityManager.startLightIntensityTask(new IRetrieveTaskListener() {
            @Override
            public void onFinish() {
                openSubSettingActivity(LightsActivity.class);
            }
        });
    }


    private void initButtons(View view) {
        progressBar = (ProgressBar) view.findViewById(R.id.progressActivating);
        imageView = (ImageView) view.findViewById(R.id.imageViewChevron);
        imageView.setVisibility(View.INVISIBLE);
        progressBar.setVisibility(View.VISIBLE);
    }

    private void dispose() {
        if (imageView != null && progressBar != null) {
            imageView.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
        }
    }

//    private void openSoundsActivity() {
//        NavigationHelper.goToActivity(this, SoundsActivity.class, NavigationHelper.AnimationType.SlideToLeft, false);
//    }

    private void openSubSettingActivity(Class subSettingActivityClass) {
        if (mSettingsViewModel.startHomeActivityIfNeeded()) {
            return;
        }
        Bundle bundle = new Bundle();
        bundle.putString(NavigationHelper.RADIO_UDN_ARG, mRadio.getUDN());

        NavigationHelper.goToActivity(this, subSettingActivityClass, NavigationHelper.AnimationType.SlideToLeft,
                false, bundle);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void openGoogleHomeToRenameSpeaker() {
        ExternalAppsOpener.openGoogleHomeForSpeakerSettings(this, mRadio);
    }

    private void initEvents() {
        handleDispose = false;
        mSettingsViewModel.startListeningToDiscovery();
        clicked = false;
    }

    private void updateSpeakerNameIfChanged() {
        if (mRadio != null) {
            mRadio.getNode(NodeSysInfoFriendlyName.class, false, new IGetNodeCallback() {
                @Override
                public void getNodeResult(NodeInfo node) {
                    NodeSysInfoFriendlyName nodeSysInfoFriendlyName = (NodeSysInfoFriendlyName)node;
                    if (nodeSysInfoFriendlyName != null) {
                        ESpeakerModelType speakerModelType = SpeakerManager.getInstance().getSpeakerModelFromModelName(mRadio.getModelName());
                        String newName = SpeakerNameManager.getInstance().generateNameForDevice(speakerModelType, mRadio.getUDN(), nodeSysInfoFriendlyName.getValue());
                        setTitle(newName);
                    }
                }

                @Override
                public void getNodeError(Class nodeType, NodeErrorResponse error) {

                }
            });
        }
    }
}
