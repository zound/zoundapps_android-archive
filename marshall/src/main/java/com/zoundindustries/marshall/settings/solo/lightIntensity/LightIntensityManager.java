package com.zoundindustries.marshall.settings.solo.lightIntensity;

import android.app.Activity;

import com.apps_lib.multiroom.settings.solo.speaker.IRetrieveTaskListener;
import com.frontier_silicon.NetRemoteLib.Node.NodePlatformOEMLedIntensity;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.components.common.RadioNodeUtil;
import com.frontier_silicon.components.common.TaskHelper;

/**
 * Created by cvladu on 15/06/2017.
 */

public class LightIntensityManager {
    private static LightIntensityManager mInstance = new LightIntensityManager();
    private Activity mActivity;
    private int lastKnownStepsValue;
    private Radio mRadio;
    private static final int STEPS_NUMBER = 13;

    private LightIntensityModel lightIntensityModel = new LightIntensityModel();

    private LightIntensityManager() {
    }

    public static LightIntensityManager getInstance() {
        return mInstance;
    }

    public void init(Activity activity, Radio radio) {
        mActivity = activity;
        mRadio = radio;
    }

    public LightIntensityModel getLightIntensityModel() {
        return lightIntensityModel;
    }

    public void startLightIntensityTask(IRetrieveTaskListener listener) {
        LightIntensityRetrievalTask lightIntensityRetrievalTask = new LightIntensityRetrievalTask(mRadio, mActivity, listener);
        TaskHelper.execute(lightIntensityRetrievalTask);
    }

    public int transformValueToSteps(int value) {
        return value / (lightIntensityModel.lightIntensitySteps.get() / STEPS_NUMBER);
    }

    public void transformStepsToValueAndSetLedIntensity(int steps) {
        lastKnownStepsValue = transformValueToSteps(lightIntensityModel.lightIntensity.get());

        if (lastKnownStepsValue != steps ) {

            setLedIntensity(transformStepstoValue(steps));

            lightIntensityModel.lightIntensity.set((int) transformStepstoValue(steps));
        }
    }


    public void setLedIntensity(long value) {
        NodePlatformOEMLedIntensity nodePlatformOEMLedIntensity = new NodePlatformOEMLedIntensity(value);
        if (mRadio == null) {
            return;
        }

        RadioNodeUtil.setNodeToRadioAsync(mRadio, nodePlatformOEMLedIntensity, null);
    }

    public long transformStepstoValue(int steps) {
        double stepsD = lightIntensityModel.lightIntensitySteps.get();
        double value = stepsD / STEPS_NUMBER;

        return Math.round(value * steps);
    }

    public int getStepsNumber() {

        return STEPS_NUMBER;
    }

}
