package com.zoundindustries.marshall.settings.solo;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

import com.apps_lib.multiroom.NavigationHelper;
import com.frontier_silicon.NetRemoteLib.Discovery.IRadioDiscoveryListener;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.components.NetRemoteManager;
import com.zoundindustries.marshall.myHome.HomeActivity;


/**
 * Created by nbalazs on 28/09/2016.
 */

public class SettingsViewModel implements IRadioDiscoveryListener {

    private Activity mActivity;
    public Radio mRadio;

    public SettingsViewModel(Activity activity, Radio radio) {
        mActivity = activity;
        mRadio = radio;
    }

    public void startListeningToDiscovery() {
        NetRemoteManager.getInstance().addRadioDiscoveryListener(this);
    }

    public void stopListening() {
        NetRemoteManager.getInstance().removeRadioDiscoveryListener(this);
    }

    @Override
    public void onRadioFound(Radio radio) {
    }

    @Override
    public void onRadioLost(Radio radio) {
        if (mRadio.compareTo(radio) == 0) {
            startHomeActivity();
        }
    }

    @Override
    public void onRadioUpdated(Radio radio) {
    }

    public boolean startHomeActivityIfNeeded() {
        boolean exitActivity = false;
        if (!isRadioAlive()) {
            startHomeActivity();
            exitActivity = true;
        }
        return exitActivity;
    }

    private boolean isRadioAlive() {
        boolean isRadioAlive = mRadio != null && mRadio.isAvailable();
        Log.i("SettingsViewModel", "Radio Alive status: " + isRadioAlive);
        return isRadioAlive;
    }

    private void startHomeActivity() {
        Bundle bundle = new Bundle();
        bundle.putBoolean("DISCONNECTED", true);
        bundle.putString("LAST_RADIO_SN", mRadio != null ? mRadio.getSN() : "");
        NavigationHelper.goToActivity(mActivity, HomeActivity.class, NavigationHelper.AnimationType.SlideToLeft, true, bundle);
    }
}
