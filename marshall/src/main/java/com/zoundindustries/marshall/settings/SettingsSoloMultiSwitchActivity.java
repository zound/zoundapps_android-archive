package com.zoundindustries.marshall.settings;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.apps_lib.multiroom.NavigationHelper;
import com.apps_lib.multiroom.UEActivityBase;
import com.zoundindustries.marshall.R;
import com.zoundindustries.marshall.databinding.ActivitySettingsBinding;
import com.zoundindustries.marshall.databinding.ListItemSettingsActivitySoloMultiBinding;
import com.zoundindustries.marshall.settings.multi.MultiSpeakersActivity;
import com.zoundindustries.marshall.settings.solo.speakerList.SoloSpeakersActivity;

/**
 * Created by nbalazs on 09/06/2017.
 */

public class SettingsSoloMultiSwitchActivity extends UEActivityBase{

    private ActivitySettingsBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_settings);

        setupAppBar();

        enableUpNavigation();

        setTitle(R.string.settings_menu_title);

        setup();
    }

    private void setup() {
        final String [] menuContent = new String[2];
        menuContent[0] = getResources().getString(R.string.settings_menu_single_speakers);
        menuContent[1] = getResources().getString(R.string.settings_menu_multi_speakers);

        mBinding.recyclerView.setLayoutManager(new LinearLayoutManager(this));

        mBinding.recyclerView.setAdapter(new RecyclerView.Adapter() {

            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                LayoutInflater inflater = LayoutInflater.from(SettingsSoloMultiSwitchActivity.this);
                ListItemSettingsActivitySoloMultiBinding theLayoutBinding = DataBindingUtil.inflate(inflater, R.layout.list_item_settings_activity_solo_multi, parent, true);
                return new SettingsViewHolder(theLayoutBinding);
            }

            @Override
            public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
                SettingsViewHolder viewHolder = (SettingsViewHolder) holder;
                viewHolder.mBinding.titleTextView.setText(menuContent[position]);
                viewHolder.mBinding.containerLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        switch (holder.getAdapterPosition()){
                            case 0: // Solo Speakers
                                NavigationHelper.goToActivity(SettingsSoloMultiSwitchActivity.this, SoloSpeakersActivity.class, NavigationHelper.AnimationType.SlideToLeft);
                                break;
                            case 1: // Multi Speakers
                                NavigationHelper.goToActivity(SettingsSoloMultiSwitchActivity.this, MultiSpeakersActivity.class, NavigationHelper.AnimationType.SlideToLeft);
                                break;
                        }
                    }
                });
            }

            @Override
            public int getItemCount() {
                return menuContent.length;
            }
        });
    }

    private class SettingsViewHolder extends RecyclerView.ViewHolder{

        private ListItemSettingsActivitySoloMultiBinding mBinding;

        SettingsViewHolder(ListItemSettingsActivitySoloMultiBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
