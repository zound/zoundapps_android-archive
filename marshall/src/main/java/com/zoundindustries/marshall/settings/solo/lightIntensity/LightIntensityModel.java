package com.zoundindustries.marshall.settings.solo.lightIntensity;

import android.databinding.ObservableInt;

/**
 * Created by cvladu on 16/06/2017.
 */

public class LightIntensityModel {

    ObservableInt lightIntensity = new ObservableInt(0);
    ObservableInt lightIntensitySteps = new ObservableInt(255);
}
