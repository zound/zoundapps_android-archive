package com.zoundindustries.marshall.settings.solo.sounds;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.MenuItem;

import com.apps_lib.multiroom.UEActivityBase;
import com.zoundindustries.marshall.R;
import com.zoundindustries.marshall.databinding.ActivitySoundsBinding;

/**
 * Created by nbalazs on 03/07/2017.
 */

public class SoundsActivity extends UEActivityBase {

    private ActivitySoundsBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_sounds);

        setupAppBar();
        enableUpNavigation();
        setTitle(R.string.settings_sounds);

        setupControls();
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupControls() {

    }
}