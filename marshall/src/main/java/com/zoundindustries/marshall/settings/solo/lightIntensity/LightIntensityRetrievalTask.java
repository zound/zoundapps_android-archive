package com.zoundindustries.marshall.settings.solo.lightIntensity;

import android.app.Activity;

import com.apps_lib.multiroom.settings.solo.speaker.IRetrieveTaskListener;
import com.frontier_silicon.NetRemoteLib.Node.NodePlatformOEMLedIntensity;
import com.frontier_silicon.NetRemoteLib.Node.NodePlatformOEMLedIntensitySteps;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.components.common.BaseShowProgressDialogTask;
import com.frontier_silicon.components.common.DialogsHolder;

/**
 * Created by cvladu on 15/06/2017.
 */

public class LightIntensityRetrievalTask extends BaseShowProgressDialogTask<Boolean> {
    private Radio mRadio;
    private IRetrieveTaskListener mListener;
    NodePlatformOEMLedIntensity ledIntensity;
    NodePlatformOEMLedIntensitySteps ledIntensitySteps;

    public LightIntensityRetrievalTask(Radio radio, Activity activity, IRetrieveTaskListener listener) {
        mRadio = radio;
        mListener = listener;
        mContext = activity;

        mDialogKey = "light_intensity_retriever";
    }

    @Override
    protected void onPreExecute() {
        if (mContext != null && !DialogsHolder.isShowingOrActivityIsFinishing(mDialogKey, (Activity) mContext)) {
            initTimeoutTimer();
        }

    }

    @Override
    protected Boolean doInBackground(Integer... params) {
        if (mRadio == null) {
            return false;
        }

        ledIntensity = (NodePlatformOEMLedIntensity) mRadio.getNodeSyncGetter(NodePlatformOEMLedIntensity.class).get();
        ledIntensitySteps = (NodePlatformOEMLedIntensitySteps) mRadio.getNodeSyncGetter(NodePlatformOEMLedIntensitySteps.class).get();

        if (ledIntensity != null && ledIntensitySteps != null) {
            return true;
        }

        return true;
    }

    @Override
    protected void onPostExecute(Boolean result) {
        LightIntensityModel lightIntensityModel = LightIntensityManager.getInstance().getLightIntensityModel();
        if (mListener != null && ledIntensity != null && ledIntensitySteps != null) {
            lightIntensityModel.lightIntensity.set(ledIntensity.getValue().intValue());
            lightIntensityModel.lightIntensitySteps.set(ledIntensitySteps.getValue().intValue());
            mListener.onFinish();
        }

        super.onPostExecute(result);

        ledIntensitySteps = null;
        ledIntensity = null;

        dispose();
    }
}
