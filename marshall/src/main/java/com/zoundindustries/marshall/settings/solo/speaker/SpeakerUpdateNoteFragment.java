package com.zoundindustries.marshall.settings.solo.speaker;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Toast;

import com.apps_lib.multiroom.AnimatedDialogFragmentBase;
import com.apps_lib.multiroom.NavigationHelper;
import com.apps_lib.multiroom.setup.update.UpdateManager;
import com.zoundindustries.marshall.R;
import com.zoundindustries.marshall.databinding.FragmentSpeakerUpdateNoteBinding;
import com.zoundindustries.marshall.settings.solo.SettingsViewModel;

/**
 * Created by jedszmic on 10.01.2018.
 */

public class SpeakerUpdateNoteFragment extends AnimatedDialogFragmentBase {
    private SettingsViewModel mSpeakerModel;
    private FragmentSpeakerUpdateNoteBinding mBinding;

    public static SpeakerUpdateNoteFragment newInstance(SettingsViewModel speakerModel) {
        SpeakerUpdateNoteFragment fragment = new SpeakerUpdateNoteFragment();
        fragment.mSpeakerModel = speakerModel;

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_speaker_update_note,
                container, false);

        setTextMessage();
        handleButtons();

        Window window = getDialog().getWindow();
        if (window != null) {
            window.requestFeature(Window.FEATURE_NO_TITLE);
        }

        return mBinding.getRoot();
    }

    private void handleButtons() {
        mBinding.buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        mBinding.buttonContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UpdateManager.getInstance().updateRadio(mSpeakerModel.mRadio);
                NavigationHelper.goToHomeAndClearActivityStack(getActivity());
            }
        });
    }

    private void setTextMessage() {
        if (mSpeakerModel == null) {
            return;
        }

        mBinding.textMessage.setText(getString(R.string.speaker_update_note,
                mSpeakerModel.mRadio.getFriendlyName()));
    }
}

