package com.zoundindustries.marshall.settings.solo.aboutSpeaker;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.MenuItem;
import android.view.View;

import com.apps_lib.multiroom.UEActivityBase;
import com.apps_lib.multiroom.settings.RadioFromBundleExtractor;
import com.zoundindustries.marshall.settings.solo.SettingsViewModel;
import com.apps_lib.multiroom.util.DisposableTimerTask;
import com.frontier_silicon.NetRemoteLib.Node.NodeCastVersion;
import com.frontier_silicon.NetRemoteLib.Node.NodeInfo;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysInfoBuildVersion;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysNetWlanConnectedSSID;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysNetWlanMacAddress;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysNetWlanRssi;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.components.common.RadioNodeUtil;
import com.zoundindustries.marshall.R;
import com.zoundindustries.marshall.databinding.ActivitySettingsAboutSpeakerBinding;
import com.zoundindustries.marshall.settings.solo.speaker.SpeakerUpdateNoteFragment;

import java.util.Map;
import java.util.Timer;

/**
 * Created by lsuhov on 08/06/16.
 */
public class AboutSpeakerActivity extends UEActivityBase {

    private Radio mRadio;
    private ActivitySettingsAboutSpeakerBinding mBinding;
    private String mWifiNetworkName;
    private Timer mTimer;
    private DisposableTimerTask mTimerTask;
    private SettingsViewModel mSettingsViewModel;
    private boolean mActivityWasCreated;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_settings_about_speaker);

        setupAppBar();
        enableUpNavigation();
        setTitle(R.string.about_this_speaker_caps);

        mRadio = RadioFromBundleExtractor.extractRadio(getIntent().getExtras(), AboutSpeakerActivity.class);

        mSettingsViewModel = new SettingsViewModel(this, mRadio);

        if (mSettingsViewModel.startHomeActivityIfNeeded()) {
            return;
        }

        setupControls();
        mActivityWasCreated = true;
    }

    @Override
    protected void onResume() {
        super.onResume();

        mSettingsViewModel.startListeningToDiscovery();

        if (mActivityWasCreated) {
            //not
            mActivityWasCreated = false;
            return;
        }
        startStrengthTimer();
    }

    @Override
    protected void onPause() {
        mSettingsViewModel.stopListening();
        stopStrengthTimer();

        super.onPause();
    }

    private void setupControls() {
        if (mSettingsViewModel.startHomeActivityIfNeeded()) {
            return;
        }
        setSpeakerName();

        RadioNodeUtil.getNodesFromRadioAsync(mRadio, new Class[]{ NodeCastVersion.class,
                        NodeSysNetWlanConnectedSSID.class, NodeSysNetWlanRssi.class, NodeSysNetWlanMacAddress.class, NodeSysInfoBuildVersion.class},
                false, new RadioNodeUtil.INodesResultListener() {
                    @Override
                    public void onNodesResult(Map<Class, NodeInfo> nodes) {
                        NodeSysNetWlanConnectedSSID ssidNode = (NodeSysNetWlanConnectedSSID) nodes.get(NodeSysNetWlanConnectedSSID.class);
                        if (ssidNode != null) {
                            mWifiNetworkName = RadioNodeUtil.getHumanReadableSSID(ssidNode.getValue());
                        }
                        NodeSysNetWlanRssi signalStrengthNode = (NodeSysNetWlanRssi) nodes.get(NodeSysNetWlanRssi.class);
                        if (signalStrengthNode != null) {
                            showWifiWithStrength(signalStrengthNode.getValue());
                        }

                        startStrengthTimer();

                        setIPAddress();
                        setMACAddress((NodeSysNetWlanMacAddress)nodes.get(NodeSysNetWlanMacAddress.class));
                        setModelName();
                        setFirmwareVersion((NodeSysInfoBuildVersion)nodes.get(NodeSysInfoBuildVersion.class));
                        setGoogleCastVersion((NodeCastVersion)nodes.get(NodeCastVersion.class));
                    }
                });

        setUpdateButton();
    }

    void setSpeakerName() {
        Spannable speakerName = new SpannableString(getResources().getString(R.string.speaker_name_with_value, mRadio.getFriendlyName()));
        speakerName.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.secondaryText)), 6, speakerName.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mBinding.speakerNameTextView.setText(speakerName);
    }

    private void setUpdateButton() {
        mBinding.buttonUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SpeakerUpdateNoteFragment speakerUpdateNoteFragment = SpeakerUpdateNoteFragment.newInstance(mSettingsViewModel);
                FragmentManager fragmentManager = getSupportFragmentManager();
                speakerUpdateNoteFragment.show(fragmentManager, "OldUpdateWarningDialogFragment");
            }
        });
    }

    private void setGoogleCastVersion(NodeCastVersion nodeCastVersion) {
        if (nodeCastVersion != null) {
            Spannable googleCastVersion = new SpannableString(getString(R.string.chromecast_built_in_version_with_value, nodeCastVersion.getValue()));
            googleCastVersion.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.secondaryText)), 29, googleCastVersion.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            mBinding.googleCastVersionTextView.setText(googleCastVersion);
            mBinding.googleCastVersionTextView.setVisibility(View.VISIBLE);
        } else {
            mBinding.googleCastVersionTextView.setVisibility(View.GONE);
        }
    }

    private void setModelName() {
        if (mSettingsViewModel.startHomeActivityIfNeeded()) {
            return;
        }

        Spannable modelName = new SpannableString(getString(R.string.model_name_with_value, getModelNameWithoutUnderscore(mRadio.getModelName())));
        modelName.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.secondaryText)), 7, modelName.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mBinding.speakerModelTextView.setText(modelName);
    }

    private String getModelNameWithoutUnderscore(String modelName) {
        StringBuilder builder = new StringBuilder();
        builder.append("Marshall ");

        if(modelName.contains("_")) {
            String[] array = modelName.split("_");
            int length = array.length;
            for(int i=0;i<length;i++) {
                if (i==0) {
                    builder.append( array[i] + " Multi-Room ");
                } else {
                    builder.append( array[i]);
                }
            }
            
        } else {
            builder.delete(0, builder.length());
            builder.append(modelName);
        }

        return  builder.toString();
    }

    private void setMACAddress(NodeSysNetWlanMacAddress nodeSysNetWlanMacAddress) {
        if (nodeSysNetWlanMacAddress == null) {
            return;
        }

        Spannable speakerMAC = new SpannableString(getString(R.string.mac_address_with_value, nodeSysNetWlanMacAddress.getValue()));
        speakerMAC.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.secondaryText)), 5, speakerMAC.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mBinding.speakerMACTextView.setText(speakerMAC);
    }

    private void setIPAddress() {
        Spannable speakerIP = new SpannableString(getString(R.string.ip_address_with_value, mRadio.getIpAddress()));
        speakerIP.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.secondaryText)), 4, speakerIP.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mBinding.speakerIpTextView.setText(speakerIP);
    }

    private void setFirmwareVersion(NodeSysInfoBuildVersion nodeSysInfoBuildVersion) {
        if (nodeSysInfoBuildVersion == null) {
            return;
        }

        Spannable fwVersion = new SpannableString(getString(R.string.system_firmware_version_with_value, nodeSysInfoBuildVersion.getValue()));
        fwVersion.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.secondaryText)), 25, fwVersion.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mBinding.firmwareVersionTextView.setText(fwVersion);
    }

    private void startStrengthTimer() {
        stopStrengthTimer();

        mTimerTask = new WiFiStrengthTimerTask();
        mTimer = new Timer();
        mTimer.schedule(mTimerTask, 3000, 3000);
    }

    private void stopStrengthTimer() {
        if (mTimer != null) {
            mTimer.cancel();
            mTimer.purge();
            mTimer = null;
        }
        if (mTimerTask != null) {
            mTimerTask.cancel();
            mTimerTask.dispose();
            mTimerTask = null;
        }
    }

    private class WiFiStrengthTimerTask extends DisposableTimerTask {

        @Override
        public void run() {
            if (mIsDisposed) {
                cancel();
                return;
            }

            if (!AboutSpeakerActivity.this.isFinishing()) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        getWifiStrengthAndShowIt();
                    }
                });
            }
        }
    }

    private void getWifiStrengthAndShowIt() {
        if (!isFinishing()) {
            if (mSettingsViewModel.startHomeActivityIfNeeded()) {
                return;
            }
            RadioNodeUtil.getNodeFromRadioAsync(mRadio, NodeSysNetWlanRssi.class, new RadioNodeUtil.INodeResultListener() {
                @Override
                public void onNodeResult(NodeInfo node) {
                    NodeSysNetWlanRssi signalStrengthNode = (NodeSysNetWlanRssi)node;
                    if (signalStrengthNode != null) {
                        showWifiWithStrength(signalStrengthNode.getValue());
                    }
                }
            });
        }
    }

    private void showWifiWithStrength(long rawWiFiStrength) {
        Spannable wifiWithStrength = new SpannableString(getString(R.string.wifi_network_name_with_value, mWifiNetworkName, rawWiFiStrength));
        wifiWithStrength.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.secondaryText)), 14, wifiWithStrength.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mBinding.wifiNetworkNameTextView.setText(wifiWithStrength);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
