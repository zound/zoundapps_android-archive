package com.zoundindustries.marshall.settings.solo.speakerList;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.util.SortedList;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.util.SortedListAdapterCallback;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.apps_lib.multiroom.myHome.speakers.SpeakerNameManager;
import com.apps_lib.multiroom.speakerImages.SpeakerManager;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.loggerlib.FsLogger;
import com.zoundindustries.marshall.R;
import com.zoundindustries.marshall.databinding.ListItemSettingsSpeakersBinding;

/**
 * Created by nbalazs on 09/06/2017.
 */

class SoloSpeakersAdapter  extends RecyclerView.Adapter<SoloSpeakersAdapter.SoloSpeakerViewHolder> {

    class SoloSpeakerViewHolder extends RecyclerView.ViewHolder{
        ListItemSettingsSpeakersBinding mBinding;

        SoloSpeakerViewHolder(ListItemSettingsSpeakersBinding listItemSettingsSpeakerBinding) {
            super(listItemSettingsSpeakerBinding.getRoot());

            mBinding = listItemSettingsSpeakerBinding;
        }
    }

    private Context mContext;
    SortedList<Radio> mSortedRadios;

    SoloSpeakersAdapter(Context context) {
        mContext = context;
        initSortedList();
    }

    private void initSortedList() {
        mSortedRadios = new SortedList<>(Radio.class, new SortedListAdapterCallback<Radio>(this) {
            @Override
            public int compare(Radio o1, Radio o2) {
                int retValue = 0;
                if (o1 != o2) {

                    // 1. condition: Friendly Name
                    retValue = o1.getFriendlyName().compareToIgnoreCase(o2.getFriendlyName());

                    // 1. condition: Speaker Model
                    if (retValue == 0) {
                        String thisModelName = SpeakerManager.getInstance().getSpeakerModelFromModelName(o1.getModelName()).name();
                        String otherModelName = SpeakerManager.getInstance().getSpeakerModelFromModelName(o2.getModelName()).name();
                        retValue = thisModelName.compareToIgnoreCase(otherModelName);
                    }

                    // 3. condition: Internal Friendly Name
                    if (retValue == 0) {
                        String o1InternalName = SpeakerNameManager.getInstance().getNameForRadio(o1);
                        String o2InternalName = SpeakerNameManager.getInstance().getNameForRadio(o2);
                        if (o1InternalName != null && o2InternalName != null) {
                            retValue = o1InternalName.compareToIgnoreCase(o2InternalName);
                        }
                    }

                    // Fallback: udn -> should never happen
                    if (retValue == 0) {
                        retValue = o1.getUDN().compareToIgnoreCase(o2.getUDN());
                    }
                }
                return retValue;
            }

            @Override
            public boolean areContentsTheSame(Radio oldItem, Radio newItem) {
                //noinspection unchecked
                return oldItem.compareTo(newItem) == 0;
            }

            @Override
            public boolean areItemsTheSame(Radio item1, Radio item2) {
                return item1.equals(item2);
            }
        });
    }


    @Override
    public SoloSpeakerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ListItemSettingsSpeakersBinding listItemBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.list_item_settings_speakers,
                parent, false);

        return new SoloSpeakerViewHolder(listItemBinding);
    }

    @Override
    public void onBindViewHolder(SoloSpeakerViewHolder holder, int position) {
        if (position < mSortedRadios.size()) {
            Radio radio = mSortedRadios.get(position);
            ListItemSettingsSpeakersBinding binding = holder.mBinding;
            SettingsSpeakersItemViewModel viewModel = new SettingsSpeakersItemViewModel(mContext, radio);
            binding.setViewModel(viewModel);
        } else {
            FsLogger.log("MyHomeSpeakersAdapter: a speaker was requested from out of upper bound");
        }
    }

    @Override
    public int getItemCount() {
        return mSortedRadios.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
        super.onDetachedFromRecyclerView(recyclerView);

    }

}
