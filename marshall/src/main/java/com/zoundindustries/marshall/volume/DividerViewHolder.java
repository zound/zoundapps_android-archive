package com.zoundindustries.marshall.volume;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by nbalazs on 25/05/2017.
 */

public class DividerViewHolder extends RecyclerView.ViewHolder {
    public DividerViewHolder(View view) {
        super(view);
    }
}
