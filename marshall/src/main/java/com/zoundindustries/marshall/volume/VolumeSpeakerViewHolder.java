package com.zoundindustries.marshall.volume;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.zoundindustries.marshall.databinding.ListItemVolumeScreenSpeakerBinding;

/**
 * Created by nbalazs on 25/05/2017.
 */

public class VolumeSpeakerViewHolder extends RecyclerView.ViewHolder{

    ListItemVolumeScreenSpeakerBinding mBinding;
    boolean mIsExpanded = false;

    public VolumeSpeakerViewHolder(ListItemVolumeScreenSpeakerBinding binding) {
        super(binding.getRoot());
        mBinding = binding;
    }

    public boolean isExpanded() {
        return  mIsExpanded;
    }

    public void setExpanded(boolean isExpanded) {
        mIsExpanded = isExpanded;
    }
}
