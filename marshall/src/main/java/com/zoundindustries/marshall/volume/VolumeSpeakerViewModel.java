package com.zoundindustries.marshall.volume;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.res.Resources;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.Observable;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.databinding.ObservableFloat;
import android.databinding.ObservableInt;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.SeekBar;

import com.apps_lib.multiroom.myHome.speakers.SpeakerModel;
import com.apps_lib.multiroom.settings.solo.equalizer.EqualizerManager;
import com.apps_lib.multiroom.settings.solo.equalizer.EqualizerNodesUtil;
import com.apps_lib.multiroom.settings.solo.speaker.IRetrieveTaskListener;
import com.apps_lib.multiroom.speakerImages.ESpeakerImageSizeType;
import com.apps_lib.multiroom.speakerImages.ISpeakerImageIdListener;
import com.apps_lib.multiroom.speakerImages.SpeakerManager;
import com.apps_lib.multiroom.volume.VolumeModel;
import com.apps_lib.multiroom.volume.VolumesManager;
import com.frontier_silicon.NetRemoteLib.Node.NodeInfo;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysCapsValidModes;
import com.frontier_silicon.NetRemoteLib.Node.NodeSysMode;
import com.frontier_silicon.NetRemoteLib.Radio.INodeNotificationCallback;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.loggerlib.FsLogger;
import com.frontier_silicon.loggerlib.LogLevel;
import com.zoundindustries.marshall.R;
import com.zoundindustries.marshall.databinding.ListItemVolumeScreenSpeakerBinding;

/**
 * Created by lsuhov on 22/06/16.
 */
public class VolumeSpeakerViewModel extends BaseObservable implements SeekBar.OnSeekBarChangeListener, INodeNotificationCallback {

    private static int MAX_VIRTUAL_STEP_VALUE = 100;
    private static int CHECK_TIME_LIMIT = 1000;

    @SuppressLint("StaticFieldLeak")
    private static VolumeSpeakerViewModel theExpandedOne = null;

    private Activity mActivity;
    private SpeakerModel mSpeakerModel;
    private VolumeModel mVolumeModel;
    private Observable.OnPropertyChangedCallback mPropertyChangedCallback;
    private boolean isSeeking = false;
    private ListItemVolumeScreenSpeakerBinding mBinding;
    public ObservableFloat seekbarsOpacity = new ObservableFloat(VolumesManager.UNMUTED_OPACITY);

    public ObservableField<BitmapDrawable> speakerImage = new ObservableField<>();

    // Volume Values
    private int volumeMaxValue = 0;
    public ObservableInt volumeMaxLayoutValue = new ObservableInt(MAX_VIRTUAL_STEP_VALUE);
    public ObservableInt volumeSeekLayoutValue = new ObservableInt(0);

    // Equaliser values
    private int eqMaxValue = 0;
    public ObservableInt eqMaxLayoutStep = new ObservableInt(MAX_VIRTUAL_STEP_VALUE);
    public ObservableInt eqBassLayoutProgress = new ObservableInt(0);
    public ObservableInt eqTrebleLayoutProgress = new ObservableInt(0);

    public ObservableFloat volumeSeekbarOpacity = new ObservableFloat(VolumesManager.UNMUTED_OPACITY);
    public ObservableBoolean isEqualiserShowing = new ObservableBoolean(false);

    private long mLastVolumeChangeMoment;
    private long mLastEqValueChangeMoment;

    VolumeSpeakerViewModel(Activity activity, SpeakerModel speakerModel, ListItemVolumeScreenSpeakerBinding binding) {
        mActivity = activity;
        mSpeakerModel = speakerModel;
        mBinding = binding;

        init();
    }

    private void init() {
        if (mSpeakerModel.deviceModel == null || mSpeakerModel.deviceModel.mRadio == null) {
            FsLogger.log("Device model or Radio from VolumeSpeakerViewModel is null", LogLevel.Error);
            return;
        }

        String udn = mSpeakerModel.deviceModel.mRadio.getUDN();
        mVolumeModel = VolumesManager.getInstance().getVolumeModel(udn);

        // Disable volume settings if the device is casting
        mSpeakerModel.deviceModel.mRadio.getMode(new Radio.IRadioModeCallback() {
            @Override
            public void getCurrentMode(NodeSysCapsValidModes.Mode currentMode) {
                setVolumeProgressEnabled(currentMode);
            }
        });

        addListeners();
        updateAll();
    }

    private void addListeners() {
        mPropertyChangedCallback = new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable observable, int i) {
                if (observable == mVolumeModel.steps) {
                    updateVolumeSteps();
                } else if (observable == mVolumeModel.value) {
                    updateVolumeValue();
                } else if (observable == mVolumeModel.muted) {
                    updateMuted();
                }
            }
        };

        mVolumeModel.steps.addOnPropertyChangedCallback(mPropertyChangedCallback);
        mVolumeModel.muted.addOnPropertyChangedCallback(mPropertyChangedCallback);
        mVolumeModel.value.addOnPropertyChangedCallback(mPropertyChangedCallback);

        mSpeakerModel.deviceModel.mRadio.addNotificationListener(this);
    }

    private void updateAll() {
        updateSpeakerImage();
        requestVolumeStepsIfNeeded();
        updateVolumeSteps();
        updateVolumeValue();
        updateMuted();
        updateEqualizerValues();
    }

    private void updateMuted() {
        seekbarsOpacity.set(mVolumeModel.muted.get() ? VolumesManager.MUTED_OPACITY : VolumesManager.UNMUTED_OPACITY);
    }

    private void updateVolumeSteps() {
        volumeMaxValue = mVolumeModel.steps.get() - 1;
        volumeMaxLayoutValue.set(MAX_VIRTUAL_STEP_VALUE - MAX_VIRTUAL_STEP_VALUE % volumeMaxValue);
    }

    private void requestVolumeStepsIfNeeded() {
        if (!mVolumeModel.stepsRetrieved) {
            VolumesManager.getInstance().retrieveVolumeSteps(mSpeakerModel.deviceModel, mVolumeModel);
        }
    }

    private void updateVolumeValue() {
        if (isSeeking || !shouldUpdateVolumeLayout()) {
            return;
        }

        final int newValue = realVolumeStepToVirtual(mVolumeModel.value.get());
        if (!isTheSameVirtualVolumeValue(volumeSeekLayoutValue.get(), newValue)) {
            final ObjectAnimator animation = android.animation.ObjectAnimator.ofInt(mBinding.volumeSeekBar, "progress", newValue);
            animation.setDuration(250);
            animation.setInterpolator(new AccelerateDecelerateInterpolator());
            animation.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {

                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    volumeSeekLayoutValue.set(newValue);
                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            });
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    animation.start();
                }
            });
        }

    }

    private void updateEqualizerValues() {
        EqualizerManager.getInstance().init(mSpeakerModel.deviceModel.mRadio, mActivity);
        EqualizerManager.getInstance().startEqualizerTask(new IRetrieveTaskListener() {
            @Override
            public void onFinish() {
                setSeekbarValues();
            }
        });

    }

    private void updateSpeakerImage() {
        Radio radio = getRadio();

        SpeakerManager.getInstance().retrieveImageIdForRadio(radio, ESpeakerImageSizeType.Volume, new ISpeakerImageIdListener() {
            @Override
            public void onImageIdRetrieved(int imageId) {
                if (mActivity == null) {
                    return;
                }
                Resources resources = mActivity.getResources();

                Bitmap speakerImageBitmap = BitmapFactory.decodeResource(resources, imageId);
                BitmapDrawable bitmapDrawable = new BitmapDrawable(resources, speakerImageBitmap);

                speakerImage.set(bitmapDrawable);
            }
        });
    }

    @Bindable
    public String getRadioName() {

        Radio radio = getRadio();

        String radioName = "";
        if (radio != null) {
            radioName = radio.getFriendlyName();
        }
        return radioName;
    }

    public String getSoloMulti() {
        String soloMulti = mActivity.getResources().getString(mSpeakerModel.isInMultiMode ? R.string.multi : R.string.single);
        soloMulti = soloMulti.substring(0, 1).toUpperCase() + soloMulti.substring(1);
        return soloMulti;
    }

    private Radio getRadio() {
        if (mSpeakerModel.deviceModel != null && mSpeakerModel.deviceModel.mRadio != null) {
            return mSpeakerModel.deviceModel.mRadio;
        }
        return null;
    }

    public void dispose() {
        mVolumeModel.steps.removeOnPropertyChangedCallback(mPropertyChangedCallback);
        mVolumeModel.muted.removeOnPropertyChangedCallback(mPropertyChangedCallback);
        mVolumeModel.value.removeOnPropertyChangedCallback(mPropertyChangedCallback);

        mSpeakerModel.deviceModel.mRadio.removeNotificationListener(this);

        mPropertyChangedCallback = null;
        mVolumeModel = null;
        mActivity = null;
        mSpeakerModel = null;
        speakerImage.set(null);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
        if (!isSeeking) {
            return;
        }

        int id = seekBar.getId();

        if (id == mBinding.volumeSeekBar.getId()) {
            setVolume(i);
        }

        if (id == mBinding.bassSeekBar.getId()) {
            setBass(virtualEqStepToReal(i));
        }

        if (id == mBinding.trebleSeekBar.getId()) {
            setTreble(virtualEqStepToReal(i));
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        isSeeking = true;
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        isSeeking = false;
    }

    @SuppressWarnings({"deprecation", "UnusedParameters"})
    public void onEqButtonClicked(View view) {
        if (!isEqualiserShowing.get()) {
            EqualizerManager.getInstance().init(mSpeakerModel.deviceModel.mRadio, mActivity);

            isEqualiserShowing.set(!isEqualiserShowing.get());
            expand(mBinding.animatedLayout);
            EqualizerManager.getInstance().startEqValueChecking(new EqualizerManager.IEqualiserListener() {

                @Override
                public void onBassUpdated(long bass) {
                    if (isSeeking || !shouldUpdateEqLayout()) {
                        return;
                    }

                    final int newBassValue = realEqStepToVirtual((int) bass);
                    final ObjectAnimator animation = ObjectAnimator.ofInt(mBinding.bassSeekBar, "progress", newBassValue);
                    animation.setDuration(250);
                    animation.setInterpolator(new AccelerateDecelerateInterpolator());
                    animation.addListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {
                            eqBassLayoutProgress.set(newBassValue);
                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {

                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {

                        }
                    });
                    if (mActivity != null) {
                        mActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                animation.start();
                            }
                        });
                    }
                }

                @Override
                public void onTrebleUpdated(long treble) {
                    if (isSeeking || !shouldUpdateEqLayout()) {
                        return;
                    }

                    final int newTrebleValue = realEqStepToVirtual((int) treble);
                    final ObjectAnimator animation = ObjectAnimator.ofInt(mBinding.trebleSeekBar, "progress", newTrebleValue);
                    animation.setDuration(250);
                    animation.setInterpolator(new AccelerateDecelerateInterpolator());
                    animation.addListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {
                            eqTrebleLayoutProgress.set(newTrebleValue);
                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {

                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {

                        }
                    });
                    if (mActivity != null) {
                        mActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                animation.start();
                            }
                        });
                    }
                }
            });

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
                mBinding.buttonExpandEqualiser.setBackground(ContextCompat.getDrawable(mBinding.buttonExpandEqualiser.getContext(), R.drawable.btn_small_outlined));
            } else {
                mBinding.buttonExpandEqualiser.setBackgroundDrawable(ContextCompat.getDrawable(mBinding.buttonExpandEqualiser.getContext(), R.drawable.btn_small_outlined));
            }

            if (theExpandedOne != null) {
                theExpandedOne.onEqButtonClicked(null);
            }
            theExpandedOne = this;
        } else {
            isEqualiserShowing.set(!isEqualiserShowing.get());
            collapse(mBinding.animatedLayout);
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
                mBinding.buttonExpandEqualiser.setBackground(ContextCompat.getDrawable(mBinding.buttonExpandEqualiser.getContext(), R.drawable.btn_small_filled));
            } else {
                mBinding.buttonExpandEqualiser.setBackgroundDrawable(ContextCompat.getDrawable(mBinding.buttonExpandEqualiser.getContext(), R.drawable.btn_small_filled));
            }
            EqualizerManager.getInstance().stopEqValueChecking();
            theExpandedOne = null;
        }
    }

    private void expand(final View v) {
        v.measure(ViewPager.LayoutParams.MATCH_PARENT, ViewPager.LayoutParams.WRAP_CONTENT);
        final int targetHeight = v.getMeasuredHeight();

        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        v.getLayoutParams().height = 1;
        v.setVisibility(View.VISIBLE);
        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1
                        ? ViewPager.LayoutParams.WRAP_CONTENT
                        : (int) (targetHeight * interpolatedTime);
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        a.setDuration((int) (targetHeight / v.getContext().getResources().getDisplayMetrics().density * 1.5));
        v.startAnimation(a);
    }

    private void collapse(final View v) {
        final int initialHeight = v.getMeasuredHeight();

        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (interpolatedTime == 1) {
                    v.setVisibility(View.GONE);
                } else {
                    v.getLayoutParams().height = initialHeight - (int) (initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        a.setDuration((int) (initialHeight / v.getContext().getResources().getDisplayMetrics().density * 1.5));
        v.startAnimation(a);
    }

    private void setVolume(int progress) {
        if (!isTheSameVirtualVolumeValue(volumeSeekLayoutValue.get(), progress) && isSeeking) {
            int realVolumeValue = virtualVolumeStepToReal(progress);
            VolumesManager.getInstance().setVolumeValueToRadio(mSpeakerModel.deviceModel.mRadio, realVolumeValue);
            volumeSeekLayoutValue.set(progress);
            setMomentOfVolumeSet();
        }
    }

    private void setBass(int progress) {
        int virtualBassValue = realEqStepToVirtual(progress);
        if (!isTheSameVirtualEqValue(eqBassLayoutProgress.get(), virtualBassValue)) {
            EqualizerNodesUtil.sendBassValue(mSpeakerModel.deviceModel.mRadio, progress);
            setMomentOfEqValueSet();
        }
    }

    private void setTreble(int progress) {
        int virtualTrebleValue = realEqStepToVirtual(progress);
        if (!isTheSameVirtualEqValue(eqTrebleLayoutProgress.get(), virtualTrebleValue)) {
            EqualizerNodesUtil.sendTrebleValue(mSpeakerModel.deviceModel.mRadio, progress);
            setMomentOfEqValueSet();
        }
    }

    private void setSeekbarValues() {
        eqMaxValue = (int) EqualizerManager.getInstance().max;
        if (eqMaxValue == 0) {
            return;
        }

        eqBassLayoutProgress.set(MAX_VIRTUAL_STEP_VALUE - MAX_VIRTUAL_STEP_VALUE % eqMaxValue);
        eqTrebleLayoutProgress.set(MAX_VIRTUAL_STEP_VALUE - MAX_VIRTUAL_STEP_VALUE % eqMaxValue);
    }

    @Override
    public void onNodeUpdated(NodeInfo node) {
        if (node instanceof NodeSysMode) {
            NodeSysCapsValidModes.Mode mode = mSpeakerModel.deviceModel.mRadio.getModeSync();
            setVolumeProgressEnabled(mode);
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private void setVolumeProgressEnabled(NodeSysCapsValidModes.Mode currentMode) {
        if (currentMode == NodeSysCapsValidModes.Mode.Cast) {
            volumeSeekbarOpacity.set(VolumesManager.MUTED_OPACITY);
            mBinding.volumeSeekBar.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    return true;
                }
            });
        } else {
            volumeSeekbarOpacity.set(VolumesManager.UNMUTED_OPACITY);
            mBinding.volumeSeekBar.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    return false;
                }
            });
        }
    }

    private int realVolumeStepToVirtual(int realVolumeStep) {
        return volumeMaxValue != 0 ? (MAX_VIRTUAL_STEP_VALUE / volumeMaxValue) * realVolumeStep : 0;
    }

    private int virtualVolumeStepToReal(int virtualVolumeStep) {
        if (volumeMaxValue == 0) {
            return 0;
        }

        int virtualStepPerValue = MAX_VIRTUAL_STEP_VALUE / volumeMaxValue;

        return virtualVolumeStep / virtualStepPerValue;
    }

    private int realEqStepToVirtual(int realEqStep) {
        return eqMaxValue != 0 ? (MAX_VIRTUAL_STEP_VALUE / eqMaxValue) * realEqStep : 0;
    }

    private int virtualEqStepToReal(int virtualEqStep) {
        if (eqMaxValue == 0) {
            return 0;
        }

        int virtualStepPerValue = MAX_VIRTUAL_STEP_VALUE / eqMaxValue;

        return virtualEqStep / virtualStepPerValue;
    }

    private boolean isTheSameVirtualEqValue(int actualValue, int newValue) {
        return eqMaxValue != 0 && virtualEqStepToReal(actualValue) == virtualEqStepToReal(newValue);

    }

    private boolean isTheSameVirtualVolumeValue(int actualValue, int newValue) {
        return volumeMaxValue != 0 && virtualVolumeStepToReal(actualValue) == virtualVolumeStepToReal(newValue);
    }

    private void setMomentOfVolumeSet() {
        mLastVolumeChangeMoment = System.currentTimeMillis();
    }

    private boolean shouldUpdateVolumeLayout() {
        return (System.currentTimeMillis() - mLastVolumeChangeMoment) > CHECK_TIME_LIMIT;
    }

    private void setMomentOfEqValueSet() {
        mLastEqValueChangeMoment = System.currentTimeMillis();
    }

    private boolean shouldUpdateEqLayout() {
        return (System.currentTimeMillis() - mLastEqValueChangeMoment) > CHECK_TIME_LIMIT;
    }
}

