package com.zoundindustries.marshall.volume;

import android.support.v7.widget.RecyclerView;

import com.zoundindustries.marshall.databinding.ListItemVolumeScreenMasterBinding;


/**
 * Created by nbalazs on 25/05/2017.
 */

public class MasterVolumeViewHolder extends RecyclerView.ViewHolder {
    ListItemVolumeScreenMasterBinding mBinding;
    public MasterVolumeViewHolder(ListItemVolumeScreenMasterBinding listItemBinding) {
        super(listItemBinding.getRoot());

        mBinding = listItemBinding;
    }
}
