package com.zoundindustries.marshall.setup.update;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.apps_lib.multiroom.UEActivityBase;
import com.zoundindustries.marshall.R;
import com.zoundindustries.marshall.databinding.ActivityUpdateBinding;

/**
 * Created by nbalazs on 30/05/2017.
 */
public class UpdateActivity extends UEActivityBase{

    private ActivityUpdateBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_update);

        setupControls();
    }

    private void setupControls() {
        final UpdateAdapter updateAdapter = new UpdateAdapter(getSupportFragmentManager());
        mBinding.pager.setAdapter(updateAdapter);
        mBinding.pager.setCurrentItem(0);

        mBinding.pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == UpdateAdapter.PAGES_COUNT - 1) {
                    mBinding.layoutToHide.setVisibility(View.GONE);
                } else {
                    mBinding.layoutToHide.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        mBinding.cubeIndicator.setViewPager(mBinding.pager);

        mBinding.skipButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBinding.pager.setCurrentItem(UpdateAdapter.PAGES_COUNT - 1);
            }
        });
    }
}
