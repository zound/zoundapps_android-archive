package com.zoundindustries.marshall.setup.normalSetup;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.sahasbhop.apngview.ApngImageLoader;
import com.zoundindustries.marshall.R;
import com.zoundindustries.marshall.databinding.FragmentNoNewSpeakerFound2Binding;


/**
 * Created by lsuhov on 08/04/16.
 */
public class NoNewSpeakerFound2Fragment extends NoNewSpeakerFoundFragmentBase {

    public static final String FRAGMENT_TAG = "TAG_NO_NEW_SPEAKER_FOUND_2_FRAGMENT";

    private FragmentNoNewSpeakerFound2Binding mBinding;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_no_new_speaker_found_2, container, false);
        addAnimation();
        return mBinding.getRoot();
    }

    private void addAnimation() {
        String uri = "assets://apng/failed_boot.png";
        ApngImageLoader.getInstance().init(getContext());
        ApngImageLoader.getInstance().displayApng(uri, mBinding.multiSingleAnimated, new ApngImageLoader.ApngConfig(0,true));
    }

    @Override
    protected void setupControls() {
        mBinding.buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToNextPage();
            }
        });

        mBinding.buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelSetup();
            }
        });
    }
}
