package com.zoundindustries.marshall.setup.presets;

import android.app.AlertDialog;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.MenuItem;
import android.view.View;

import com.apps_lib.multiroom.NavigationHelper;
import com.apps_lib.multiroom.UEActivityBase;
import com.apps_lib.multiroom.factory.ActivityFactory;
import com.apps_lib.multiroom.presets.DownloadPresetsEvent;
import com.apps_lib.multiroom.presets.spotify.AuthenticationScope;
import com.apps_lib.multiroom.presets.spotify.ISpotifyAccountListener;
import com.apps_lib.multiroom.presets.spotify.ISpotifyRefreshAuthorizationListener;
import com.apps_lib.multiroom.presets.spotify.SpotifyManager;
import com.zoundindustries.marshall.R;
import com.zoundindustries.marshall.databinding.ActivitySetupSpotifyAuthenticationBinding;
import com.zoundindustries.marshall.presets.spotify.SpotifyErrorDialog;

import org.greenrobot.eventbus.EventBus;

public class SpotifyAuthenticationActivity extends UEActivityBase implements ISpotifyAccountListener,
        ISpotifyRefreshAuthorizationListener {

    private ActivitySetupSpotifyAuthenticationBinding mBinding;
    private SpotifyManager mSpotifyManager;
    private boolean mIsOpenedFromNormalApp;
    private AuthenticationScope mScope;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_setup_spotify_authentication);

        int extra = getIntent().getIntExtra("SCOPE", 0);
        mScope = AuthenticationScope.values()[extra];

        setupAppBar();
        enableUpNavigation();

        extractBundleInfo();

        setupControls();
    }

    private void extractBundleInfo() {
        Bundle bundle = getIntent().getExtras();
        if (bundle == null) {
            return;
        }

        mIsOpenedFromNormalApp = bundle.getBoolean(NavigationHelper.EXTERNAL_SPOTIFY_LINKING);
    }

    private void setupControls() {
        mSpotifyManager = SpotifyManager.getInstance();

        mBinding.buttonConnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSpotifyManager.getSpotifyAuthenticator().loginSpotify(SpotifyAuthenticationActivity.this);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        mSpotifyManager.getSpotifyAuthenticator().onSpotifyAuthenticationResponseReceived(requestCode, resultCode, intent, this);
    }

    @Override
    public void onUserAccountRetrieved(boolean result, boolean hasUserPremiumAccount) {
        if (result) {
            if (hasUserPremiumAccount) {
                Bundle bundle = null;
                if (mIsOpenedFromNormalApp) {
                    bundle = new Bundle();
                    bundle.putBoolean(NavigationHelper.EXTERNAL_SPOTIFY_LINKING, true);
                }
                if (mScope == AuthenticationScope.AUTHENTICATION_AT_THE_BEGINNING) {
                    NavigationHelper.goToActivity(this, ActivityFactory.getActivityFactory().getSpotifyLinkingSuccessActivity(),
                            NavigationHelper.AnimationType.SlideToLeft, false, bundle);
                } else {
                    finish();
                    EventBus.getDefault().post(new DownloadPresetsEvent());
                }
            } else {
                noPremiumAccountAlertDialog();
            }
        } else {
            showError();
        }
    }

    @Override
    public void onAccessTokenRefreshed(boolean result) {
        if (result) {
            mSpotifyManager.getCurrentSpotifyUserInfo(this);
        } else {
            showError();
        }
    }

    public void noPremiumAccountAlertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setPositiveButton(android.R.string.ok, null);

        if (mSpotifyManager.checkIfSpotifyAppIsInstalled(this)) {
            builder.setMessage(getString(R.string.only_premium_accout_spotify_not_installed));
        } else {
            builder.setMessage(getString(R.string.only_premium_account));
        }

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void showError() {
        DialogFragment dialogFragment = SpotifyErrorDialog.newInstance(mScope);
        dialogFragment.show(getSupportFragmentManager(), "SpotifyErrorDialog");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
