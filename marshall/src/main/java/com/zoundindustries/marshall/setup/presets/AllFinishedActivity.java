package com.zoundindustries.marshall.setup.presets;

import android.os.Bundle;
import android.view.View;

import com.apps_lib.multiroom.NavigationHelper;
import com.apps_lib.multiroom.UEActivityBase;
import com.apps_lib.multiroom.setup.normalSetup.SetupManager;
import com.frontier_silicon.NetRemoteLib.Node.NodeInfo;
import com.frontier_silicon.NetRemoteLib.Node.NodeMiscNvsData;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.zoundindustries.marshall.R;

/**
 * Created by nbalazs on 28/06/2017.
 */

public class AllFinishedActivity extends UEActivityBase {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_finished);

        setupControls();

        markOnBoardingFinished();
    }

    private void setupControls() {
        findViewById(R.id.buttonDone) .setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavigationHelper.goToHome(AllFinishedActivity.this);
            }
        });
    }

    public void markOnBoardingFinished() {
        Radio radio = SetupManager.getInstance().getRadioForPresets();
        if (radio != null) {
            NodeInfo nodeInfo = new NodeMiscNvsData("{\"s\":1}");
            radio.setNode(nodeInfo, false);
        }
    }
}