package com.zoundindustries.marshall.setup.update;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by nbalazs on 30/05/2017.
 */

class UpdateAdapter extends FragmentPagerAdapter{

    public static int PAGES_COUNT = 5;

    UpdateAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position){
            case 0:
                fragment = new UpdateFragment1();
                break;
            case 1:
                fragment = new UpdateFragment2();
                break;
            case 2:
                fragment = new UpdateFragment3();
                break;
            case 3:
                fragment = new UpdateFragment4();
                break;
            case 4:
                fragment = new UpdateFragment5();
            default:
                break;
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return PAGES_COUNT;
    }
}
