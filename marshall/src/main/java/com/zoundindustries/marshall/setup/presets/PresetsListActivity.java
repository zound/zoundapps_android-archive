package com.zoundindustries.marshall.setup.presets;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;

import com.apps_lib.multiroom.NavigationHelper;
import com.apps_lib.multiroom.UEActivityBase;
import com.apps_lib.multiroom.presets.PresetItemModel;
import com.apps_lib.multiroom.presets.PresetsManager;
import com.apps_lib.multiroom.setup.presets.PresetsListAdapter;
import com.zoundindustries.marshall.R;
import com.zoundindustries.marshall.databinding.ActivitySetupPresetsListBinding;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lsuhov on 22/04/16.
 */
public class PresetsListActivity extends UEActivityBase {

    private ActivitySetupPresetsListBinding mBinding;

    private List<PresetItemModel> mPresetsList;
    private ArrayAdapter<PresetItemModel> mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_setup_presets_list);

        setupAppBar();

        setTitle(R.string.setup_meet_your_presets);

        setupControls();
    }

    private void setupControls() {

        mPresetsList = new ArrayList<>();
        mAdapter = new PresetsListAdapter(this, R.layout.list_item_setup_preset, mPresetsList);

        mBinding.presetsListView.setAdapter(mAdapter);

        mBinding.buttonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavigationHelper.goToActivity(PresetsListActivity.this, UsingPresetsActivity.class, NavigationHelper.AnimationType.SlideToLeft);
            }
        });

        retrievePresetsFromLastConfiguredDevice();
    }

    private void retrievePresetsFromLastConfiguredDevice() {
        List<PresetItemModel> newList = PresetsManager.getInstance().setupPresetItemModels;

        mPresetsList.clear();
        mPresetsList.addAll(newList);

        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onBackPressed() {}

}
