package com.zoundindustries.marshall.setup.presets;

import android.databinding.DataBindingUtil;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;

import com.apps_lib.multiroom.NavigationHelper;
import com.apps_lib.multiroom.UEActivityBase;
import com.apps_lib.multiroom.factory.ActivityFactory;
import com.apps_lib.multiroom.presets.ESetupPresetsType;
import com.apps_lib.multiroom.presets.IPresetsRetrieverListener;
import com.apps_lib.multiroom.presets.IPresetsUploadListener;
import com.apps_lib.multiroom.presets.PresetItemModel;
import com.apps_lib.multiroom.presets.PresetsManager;
import com.apps_lib.multiroom.presets.spotify.SpotifyManager;
import com.apps_lib.multiroom.setup.normalSetup.SetupManager;
import com.frontier_silicon.NetRemoteLib.Radio.Radio;
import com.frontier_silicon.components.NetRemoteManager;
import com.frontier_silicon.components.common.RadioNodeUtil;
import com.frontier_silicon.components.common.TaskHelper;
import com.github.sahasbhop.apngview.ApngImageLoader;
import com.zoundindustries.marshall.GuiUtilsMarshall;
import com.zoundindustries.marshall.R;
import com.zoundindustries.marshall.databinding.ActivitySetupPresetsSetBinding;

import java.util.List;
import java.util.concurrent.CountDownLatch;

/**
 * Created by lsuhov on 22/04/16.
 */
public class SetPresetsActivity extends UEActivityBase implements IPresetsUploadListener {

    private ActivitySetupPresetsSetBinding mBinding;
    private AnimationSet mPresetNumberAnimationSet;
    private AnimationSet mPresetDescriptionAnimationSet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_setup_presets_set);

        setupAppBar();
        setTitle(R.string.setup_adding_presets);

        setupControls();

        startPresetsSetup();
    }

    private void startPresetsSetup() {
        if (PresetsManager.getInstance().setupPresetsType == ESetupPresetsType.Spotify) {
            mBinding.textViewRetrievingPresets.setText(getResources().getString(R.string.retrieving_recommended_playlists));
        } else {
            mBinding.textViewRetrievingPresets.setText(getResources().getString(R.string.retrieving_presets_data));
        }

        mBinding.progressActivating.setVisibility(View.VISIBLE);
        PresetsManager.getInstance().retrievePresetsForUpload(new IPresetsRetrieverListener() {
            @Override
            public void onPresetsRetrieved(List<PresetItemModel> presets) {

                Radio radio = getRadio();

                PresetsManager.getInstance().uploadPresetToRadio(presets, radio, SetPresetsActivity.this);
            }
        }, this);
    }

    private void setupControls() {
        Animation inAlpha = AnimationUtils.loadAnimation(this, R.anim.preset_alpha_in_anim);
        Animation outAlpha = AnimationUtils.loadAnimation(this, R.anim.preset_alpha_out_anim);

        Animation tNumberIn = AnimationUtils.loadAnimation(this, R.anim.preset_number_in_translate_anim);
        Animation tNameIn = AnimationUtils.loadAnimation(this, R.anim.preset_name_in_translate_anim);

        Animation tNumberOut = AnimationUtils.loadAnimation(this, R.anim.preset_number_out_translate_anim);
        Animation tNameOut = AnimationUtils.loadAnimation(this, R.anim.preset_name_out_translate_anim);

        mPresetNumberAnimationSet = new AnimationSet(true);
        mPresetNumberAnimationSet.addAnimation(inAlpha);
        mPresetNumberAnimationSet.addAnimation(tNumberIn);
        mPresetNumberAnimationSet.addAnimation(tNumberOut);
        mPresetNumberAnimationSet.addAnimation(outAlpha);

        mPresetDescriptionAnimationSet = new AnimationSet(true);
        mPresetDescriptionAnimationSet.addAnimation(inAlpha);
        mPresetDescriptionAnimationSet.addAnimation(tNameIn);
        mPresetDescriptionAnimationSet.addAnimation(tNameOut);
        mPresetDescriptionAnimationSet.addAnimation(outAlpha);
    }

    @Override
    public void onUploadingPreset(final PresetItemModel presetItemModel, final int presetIndex) {
        // Nothing to do here
    }

    @Override
    public void onComplete(final List<PresetItemModel> presetItemModelList, final boolean result) {
        if (isFinishing()) {
            return;
        }

        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (result) {
                    mBinding.progressActivating.setVisibility(View.GONE);
                    mBinding.presetsKnob.setVisibility(View.VISIBLE);

                    TaskHelper.execute(new AnimationTask(presetItemModelList));
                    addAnimation();
                } else {
                    NavigationHelper.goToActivity(SetPresetsActivity.this, ActivityFactory.getActivityFactory().getPresetAddingFailedActivity(), NavigationHelper.AnimationType.SlideToLeft);
                }
            }
        });
    }

    public void onAnimationFinished() {
        sendAccessTokenToSpeakerIfNeeded();
        NavigationHelper.goToActivity(SetPresetsActivity.this, PresetsListActivity.class, NavigationHelper.AnimationType.SlideToLeft);
    }

    private void sendAccessTokenToSpeakerIfNeeded() {
        Radio radio = getRadio();
        if (radio == null) {
            return;
        }

        SpotifyManager.getInstance().sendAccessTokenToRadioIfNeededAsync(radio, new RadioNodeUtil.INodeSetResultListener() {
            @Override
            public void onNodeSetResult(boolean success) {
            }
        });
    }

    private Radio getRadio() {
        Radio radio = SetupManager.getInstance().getFoundRadioAfterSetup();

        //needed for the add presets shortcut
        if (radio == null) {
            radio = NetRemoteManager.getInstance().getCurrentRadio();
        }

        // TODO: remove this later?
        if (radio == null) {
            radio = SetupManager.getInstance().getRadioForPresets();
        }

        return radio;
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavigationHelper.goToHomeAndClearActivityStack(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void addAnimation() {
        GuiUtilsMarshall.rescaleImagesBasedOnScreenDensity(this, mBinding.knobLabels, mBinding.presetsKnob);

        String uri = "assets://apng/presets_loading.png";
        ApngImageLoader.getInstance().init(SetPresetsActivity.this);
        ApngImageLoader.getInstance().displayApng(uri, mBinding.presetsKnob, new ApngImageLoader.ApngConfig(1, true));
        mBinding.knobLabels.setVisibility(View.VISIBLE);
        mBinding.presetsKnob.setVisibility(View.VISIBLE);
    }

    private class AnimationTask extends AsyncTask<Void, Void, Void> {

        private List<PresetItemModel> mPresetItemModelList;

        private AnimationTask(List<PresetItemModel> presetItemModelList) {
            mPresetItemModelList = presetItemModelList;
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                showClearTextsAnimation();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }


            for (int i = 0; i < mPresetItemModelList.size(); i++) {
                final int presetIndex = i;
                final PresetItemModel presetItemModel = mPresetItemModelList.get(presetIndex);
                if (!isFinishing()) {
                    SetPresetsActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (isFinishing()) {
                                return;
                            }

                            updateTextViews(presetItemModel);


                        }
                    });
                }

                try {
                    animateTextViews();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            try {
                showDoneAnimation();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            onAnimationFinished();
        }

        private void updateTextViews(PresetItemModel presetItemModel) {
            mBinding.textViewPresetNumber.setText(String.valueOf(presetItemModel.getFriendlyPresetIndex()));
            mBinding.textViewAddingPresetsDescription.setText(getString(R.string.setup_presets_adding_description_and_number,
                    presetItemModel.presetName.get(), String.valueOf(presetItemModel.getFriendlyPresetIndex())));
        }

        private void animateTextViews() throws InterruptedException {
            final CountDownLatch countDownLatch = new CountDownLatch(2);
            final TargetAnimationHandler targetAnimationHandler1 = new TargetAnimationHandler(countDownLatch, mBinding.textViewAddingPresetsDescription);
            final TargetAnimationHandler targetAnimationHandler2 = new TargetAnimationHandler(countDownLatch, mBinding.textViewPresetNumber);
            mPresetDescriptionAnimationSet.setAnimationListener(targetAnimationHandler1);
            mPresetNumberAnimationSet.setAnimationListener(targetAnimationHandler2);
            SetPresetsActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mBinding.textViewAddingPresetsDescription.startAnimation(mPresetDescriptionAnimationSet);
                    mBinding.textViewPresetNumber.startAnimation(mPresetNumberAnimationSet);
                }
            });

            countDownLatch.await();
        }

        private void showDoneAnimation() throws InterruptedException {
            final String done = SetPresetsActivity.this.getResources().getString(R.string.presets_pre_populating_done);
            final CountDownLatch countDownLatch = new CountDownLatch(1);
            final Animation doneAnimation = AnimationUtils.loadAnimation(SetPresetsActivity.this, R.anim.presets_done_anim);
            final TargetAnimationHandler targetAnimationHandler = new TargetAnimationHandler(countDownLatch, mBinding.textViewAddingPresetsDescription);
            doneAnimation.setAnimationListener(targetAnimationHandler);
            SetPresetsActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mBinding.textViewAddingPresetsDescription.setText(done);
                    mBinding.textViewAddingPresetsDescription.startAnimation(doneAnimation);
                }
            });

            countDownLatch.await();
        }

        private void showClearTextsAnimation() throws InterruptedException {
            final CountDownLatch countDownLatch = new CountDownLatch(1);
            final Animation animation = AnimationUtils.loadAnimation(SetPresetsActivity.this, R.anim.preset_alpha_out_anim);
            final TargetAnimationHandler targetAnimationHandler = new TargetAnimationHandler(countDownLatch, mBinding.textViewAddingPresetsDescription);
            animation.setAnimationListener(targetAnimationHandler);
            SetPresetsActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mBinding.textViewAddingPresetsDescription.startAnimation(animation);
                }
            });

            countDownLatch.await();
        }

    }

    private class TargetAnimationHandler implements Animation.AnimationListener {

        private CountDownLatch mCountDownLatch;
        private View mTargetView;


        TargetAnimationHandler(CountDownLatch countDownLatch, View targetView) {
            mCountDownLatch = countDownLatch;
            mTargetView = targetView;
        }

        @Override
        public void onAnimationStart(Animation animation) {
            mTargetView.setVisibility(View.VISIBLE);
        }

        @Override
        public void onAnimationEnd(Animation animation) {
            mTargetView.setVisibility(View.INVISIBLE);
            mCountDownLatch.countDown();
        }

        @Override
        public void onAnimationRepeat(Animation animation) {
            // No need to handle this
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
    }
}
