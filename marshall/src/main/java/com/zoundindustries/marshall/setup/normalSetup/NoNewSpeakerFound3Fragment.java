package com.zoundindustries.marshall.setup.normalSetup;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.sahasbhop.apngview.ApngImageLoader;
import com.zoundindustries.marshall.GuiUtilsMarshall;
import com.zoundindustries.marshall.R;
import com.zoundindustries.marshall.databinding.FragmentNoNewSpeakerFound3Binding;

/**
 * Created by cvladu on 04/08/16.
 */

public class NoNewSpeakerFound3Fragment extends NoNewSpeakerFoundFragmentBase {

    public static final String FRAGMENT_TAG = "TAG_NO_NEW_SPEAKER_FOUND_3_FRAGMENT";
    private FragmentNoNewSpeakerFound3Binding mBinding;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_no_new_speaker_found_3, container, false);
        addAnimation();
        return mBinding.getRoot();
    }

    private void addAnimation() {
        GuiUtilsMarshall.rescaleImagesBasedOnScreenDensity(getActivity(), mBinding.knobLabels, mBinding.blinkingLedsImageAnimated);

        String uri = "assets://apng/failed_setup.png";
        ApngImageLoader.getInstance().init(getContext());
        ApngImageLoader.getInstance().displayApng(uri, mBinding.blinkingLedsImageAnimated, new ApngImageLoader.ApngConfig(0, true));
    }


    @Override
    protected void setupControls() {
        mBinding.buttonYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                scanNewSpeaker();
            }
        });
        mBinding.buttonNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToNextPage();
            }
        });
    }
}



