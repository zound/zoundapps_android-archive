package com.zoundindustries.marshall.setup.normalSetup;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zoundindustries.marshall.R;
import com.zoundindustries.marshall.databinding.FragmentNoNewSpeakerFound4Binding;


/**
 * Created by cvladu on 05/08/16.
 */

public class NoNewSpeakerFound4Fragment extends NoNewSpeakerFoundFragmentBase {
   private FragmentNoNewSpeakerFound4Binding mBinding;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_no_new_speaker_found_4, container, false);

        return mBinding.getRoot();
    }

    @Override
    protected void setupControls() {

        mBinding.buttonDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cancelSetup();
            }
        });
    }
}
