package com.zoundindustries.marshall.setup.presets;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;

import com.apps_lib.multiroom.NavigationHelper;
import com.apps_lib.multiroom.UEActivityBase;
import com.apps_lib.multiroom.factory.ActivityFactory;
import com.apps_lib.multiroom.myHome.SpeakersDiscoveryManager;
import com.apps_lib.multiroom.presets.ESetupPresetsType;
import com.apps_lib.multiroom.presets.PresetsManager;
import com.apps_lib.multiroom.presets.spotify.AuthenticationScope;
import com.apps_lib.multiroom.presets.spotify.ISpotifyRefreshAuthorizationListener;
import com.apps_lib.multiroom.presets.spotify.SpotifyManager;
import com.frontier_silicon.components.common.ExternalAppsOpener;
import com.zoundindustries.marshall.R;
import com.zoundindustries.marshall.databinding.ActivitySetupPresetsChooseTypeMarshallBinding;


/**
 * Created by cvladu on 14/06/2017.
 */

public class ChoosePresetsTypeActivityMarshall extends UEActivityBase {

    private ActivitySetupPresetsChooseTypeMarshallBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_setup_presets_choose_type_marshall);

        setupAppBar();

        setTitle(R.string.adding_presets_title);

        setupControls();

        SpeakersDiscoveryManager.getInstance().tryStartDiscovery();
    }

    private void setupControls() {
        mBinding.spotifyToggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                updateStateOfSelectButton();
            }
        });

        mBinding.internetRadioToggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                updateStateOfSelectButton();
            }
        });

        mBinding.buttonSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSelectButtonClicked();
            }
        });

        mBinding.buttonSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavigationHelper.goToActivity(ChoosePresetsTypeActivityMarshall.this, AllFinishedActivity.class,
                        NavigationHelper.AnimationType.SlideToLeft);
            }
        });

        mBinding.signUpSpotify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ExternalAppsOpener.openSpotify(ChoosePresetsTypeActivityMarshall.this);
            }
        });
    }

    private void updateStateOfSelectButton() {
        mBinding.buttonSelect.setEnabled(mBinding.spotifyToggleButton.isChecked() ||
                mBinding.internetRadioToggleButton.isChecked());
    }

    private void onSelectButtonClicked() {
        decideTheTypeOfPresetsToUpload();

        goToNextActivity();
    }

    private void decideTheTypeOfPresetsToUpload() {
        if (mBinding.spotifyToggleButton.isChecked()) {
            if (mBinding.internetRadioToggleButton.isChecked()) {
                PresetsManager.getInstance().setupPresetsType = ESetupPresetsType.Mixed;
            } else {
                PresetsManager.getInstance().setupPresetsType = ESetupPresetsType.Spotify;
            }
        } else {
            PresetsManager.getInstance().setupPresetsType = ESetupPresetsType.IR;
        }
    }

    private void goToNextActivity() {
        if (isSpotifyPlaylistsSelected()) {
            if (isSpotifyAccountLinked()) {
                if (isSpotifyRefreshTokenAlive()) {
                    getPresets(); // Spotify is linked & Spotify refresh token is not expired, get the presets
                } else {
                    SpotifyManager.getInstance().getSpotifyAuthenticator().refreshToken(new ISpotifyRefreshAuthorizationListener() {
                        @Override
                        public void onAccessTokenRefreshed(boolean refreshed) {
                            if (refreshed) {
                                getPresets(); // Token refreshed, get the presets
                            } else {
                                linkToSpotifyAccount(); // Token refresh failed --> Spotify authentication needed
                            }
                        }
                    });
                }
            } else {
                linkToSpotifyAccount(); // Spotify account is not linked -- >Spotify authentication needed
            }
        } else {
            getPresets(); // No Spotify account needed at all, just get the IR presets
        }
    }

    private void linkToSpotifyAccount() {
        NavigationHelper.goToActivity(ChoosePresetsTypeActivityMarshall.this, SpotifyAuthenticationActivity.class, NavigationHelper.AnimationType.SlideToLeft, false, getExtraForSportify());
    }

    private void getPresets() {
        NavigationHelper.goToActivity(this, ActivityFactory.getActivityFactory().getSetPresetsActivity(), NavigationHelper.AnimationType.SlideToLeft);
    }

    private boolean isSpotifyPlaylistsSelected() {
        ESetupPresetsType setupPresetsType = PresetsManager.getInstance().setupPresetsType;
        return (setupPresetsType == ESetupPresetsType.Spotify || setupPresetsType == ESetupPresetsType.Mixed);
    }

    private boolean isSpotifyAccountLinked() {
        return SpotifyManager.getInstance().getSpotifyAuthenticator().isSpotifyLinked();
    }

    private boolean isSpotifyRefreshTokenAlive() {
        return !SpotifyManager.getInstance().getSpotifyAuthenticator().isSpotifyAccountExpired();
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavigationHelper.goToHomeAndClearActivityStack(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private Bundle getExtraForSportify() {
        Bundle bundle = new Bundle();
        bundle.putInt("SCOPE", AuthenticationScope.AUTHENTICATION_AT_THE_BEGINNING.ordinal());
        return bundle;
    }
}
