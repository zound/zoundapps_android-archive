package com.zoundindustries.marshall;

import android.content.Context;
import android.os.Vibrator;

/**
 * Created by nbalazs on 06/07/2017.
 */
public class VibratorService {
    private static Vibrator mVibrator;

    public static void init(Context context) {
        mVibrator = (Vibrator)context.getSystemService(Context.VIBRATOR_SERVICE);
    }

    public static void vibrate() {
        mVibrator.vibrate(50);
    }
}
