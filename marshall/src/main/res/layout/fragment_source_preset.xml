<?xml version="1.0" encoding="utf-8"?>
<layout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools">

    <data>

        <import type="android.view.View" />

        <variable
            name="viewModel"
            type="com.zoundindustries.marshall.source.PresetSourceViewModel" />
    </data>

    <android.support.percent.PercentRelativeLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:alpha="0"
        android:orientation="vertical"
        android:paddingTop="@dimen/default_margin"
        tools:alpha="1">

        <FrameLayout
            android:id="@+id/container"
            android:layout_centerHorizontal="true"
            android:adjustViewBounds="true"
            app:layout_aspectRatio="100%"
            app:layout_heightPercent="50%">

            <ImageView
                android:layout_width="match_parent"
                android:layout_height="match_parent"
                android:adjustViewBounds="true"
                android:background="@color/transparent"
                android:src="@{viewModel.artwork}"
                android:visibility="invisible"
                tools:ignore="ContentDescription"
                tools:src="@drawable/artwork_placeholder_black_empty" />

            <FrameLayout
                android:id="@+id/artworkContainer"
                android:layout_width="match_parent"
                android:layout_height="match_parent"
                android:scaleX="0.9"
                android:scaleY="0.9">

                <ImageView
                    android:id="@+id/artworkImageView"
                    android:layout_width="match_parent"
                    android:layout_height="match_parent"
                    android:adjustViewBounds="true"
                    android:src="@{viewModel.artwork}"
                    tools:ignore="ContentDescription"
                    tools:src="@drawable/artwork_placeholder_black_empty" />

                <RelativeLayout
                    android:id="@+id/spotifyAuthenticationRequiredNoteContainer"
                    android:layout_width="match_parent"
                    android:layout_height="match_parent"
                    android:visibility="@{viewModel.showSpotifyWarning ? View.VISIBLE : View.GONE}">

                    <TextView
                        android:id="@+id/spotifyAuthenticationRequiredNote"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:layout_alignParentBottom="true"
                        android:layout_marginBottom="@dimen/default_margin"
                        android:layout_marginEnd="@dimen/default_margin_small"
                        android:layout_marginLeft="@dimen/default_margin_small"
                        android:layout_marginRight="@dimen/default_margin_small"
                        android:layout_marginStart="@dimen/default_margin_small"
                        android:clickable="true"
                        android:gravity="center"
                        android:onClick="@{viewModel::logInToSpotify}"
                        android:text="@string/spotify_error_artwork" />

                </RelativeLayout>

                <ImageView
                    android:id="@+id/artworkAnimationViewBackground"
                    android:layout_width="match_parent"
                    android:layout_height="match_parent"
                    android:adjustViewBounds="true"
                    android:background="@color/preset_animation_black"
                    android:visibility="invisible"
                    tools:ignore="ContentDescription" />

                <LinearLayout
                    android:id="@+id/artworkAnimationViewForeground"
                    android:layout_width="match_parent"
                    android:layout_height="match_parent"
                    android:adjustViewBounds="true"
                    android:orientation="vertical"
                    android:visibility="invisible"
                    android:weightSum="10">

                    <RelativeLayout
                        android:layout_width="match_parent"
                        android:layout_height="0dp"
                        android:layout_weight="6">

                        <ImageView
                            android:id="@+id/artworkAnimationViewIcon"
                            android:layout_width="50dp"
                            android:layout_height="35dp"
                            android:layout_alignParentBottom="true"
                            android:layout_centerInParent="true"
                            android:layout_marginBottom="@dimen/default_margin_small"
                            android:src="@drawable/ic_preset_success_anim"
                            tools:ignore="ContentDescription" />

                    </RelativeLayout>

                    <RelativeLayout
                        android:layout_width="match_parent"
                        android:layout_height="0dp"
                        android:layout_weight="4">

                        <TextView
                            android:id="@+id/artworkAnimationViewTitle"
                            style="@style/PresetAnimTextStyle"
                            android:layout_width="200dp"
                            android:layout_height="wrap_content"
                            android:layout_alignParentTop="true"
                            android:layout_centerInParent="true"
                            android:gravity="center"
                            android:text="@string/radio_station_added" />

                    </RelativeLayout>

                </LinearLayout>

            </FrameLayout>

        </FrameLayout>

        <android.support.percent.PercentRelativeLayout
            android:id="@+id/buttonsLayout"
            android:layout_width="match_parent"
            android:layout_below="@+id/container"
            android:alpha="0"
            app:layout_heightPercent="50%"
            tools:alpha="1">

            <FrameLayout
                android:id="@+id/presetTypeLayout"
                android:layout_width="match_parent"
                app:layout_heightPercent="12%"
                app:layout_marginTopPercent="7%">

                <TextView
                    android:id="@+id/presetSourceText"
                    style="@style/PresetTypeText"
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    android:layout_gravity="center"
                    android:gravity="center"
                    android:text="@{viewModel.type}"
                    android:visibility="@{viewModel.isSpotify ? View.GONE : View.VISIBLE}"
                    tools:text="Playlist" />

                <ImageView
                    android:layout_width="67dp"
                    android:layout_height="20dp"
                    android:layout_gravity="center"
                    android:alpha="0.9"
                    android:src="@drawable/ic_spotify_with_text"
                    android:visibility="@{viewModel.isSpotify ? View.VISIBLE : View.GONE}"
                    tools:ignore="ContentDescription" />
            </FrameLayout>

            <android.support.percent.PercentRelativeLayout
                android:id="@+id/presetButtonsLayout"
                android:layout_width="match_parent"
                android:layout_below="@+id/presetTypeLayout"
                android:gravity="center"
                android:orientation="horizontal"
                app:layout_heightPercent="14%"
                app:layout_marginTopPercent="7%">

                <ImageButton
                    android:id="@+id/addPresetButton"
                    android:layout_height="match_parent"
                    android:layout_alignParentLeft="true"
                    android:layout_alignParentStart="true"
                    android:layout_centerInParent="true"
                    android:layout_gravity="center_vertical"
                    android:background="@color/transparent"
                    android:onClick="@{viewModel::onAddPresetClicked}"
                    android:src="@{viewModel.isPresetable? @drawable/ic_add_preset : @drawable/ic_info}"
                    android:visibility="visible"
                    app:layout_widthPercent="15%"
                    tools:ignore="ContentDescription"
                    tools:src="@drawable/add_preset_icon" />


                <ImageButton
                    android:id="@+id/undoAddPresetButton"
                    android:layout_height="match_parent"
                    android:layout_alignParentLeft="true"
                    android:layout_alignParentStart="true"
                    android:layout_centerInParent="true"
                    android:layout_gravity="center_vertical"
                    android:background="@color/transparent"
                    android:onClick="@{viewModel::onUndoButtonPressed}"
                    android:src="@drawable/ic_undo_preset"
                    android:visibility="gone"
                    app:layout_widthPercent="15%"
                    tools:ignore="ContentDescription" />

                <View
                    android:id="@+id/fakeViewAdd"
                    android:layout_height="match_parent"
                    android:layout_alignParentLeft="true"
                    android:layout_alignParentStart="true"
                    android:layout_centerInParent="true"
                    android:layout_gravity="center_vertical"
                    android:background="@color/transparent"
                    android:visibility="gone"
                    app:layout_widthPercent="15%" />

                <TextView
                    android:id="@+id/presetName"
                    android:layout_height="wrap_content"
                    android:layout_alignEnd="@+id/deletePresetButton"
                    android:layout_alignLeft="@+id/addPresetButton"
                    android:layout_alignRight="@+id/deletePresetButton"
                    android:layout_alignStart="@+id/addPresetButton"
                    android:layout_centerInParent="true"
                    android:layout_gravity="center_vertical"
                    android:ellipsize="end"
                    android:gravity="center"
                    android:maxLines="1"
                    android:text="@{viewModel.name}"
                    android:textAppearance="@style/TextMediumWhite"
                    app:layout_marginEndPercent="20%"
                    app:layout_marginLeftPercent="20%"
                    app:layout_marginRightPercent="20%"
                    app:layout_marginStartPercent="20%"
                    app:layout_widthPercent="60%"
                    tools:text="Preset" />

                <ImageButton
                    android:id="@+id/deletePresetButton"
                    android:layout_height="match_parent"
                    android:layout_alignParentEnd="true"
                    android:layout_alignParentRight="true"
                    android:layout_centerInParent="true"
                    android:alpha="@{viewModel.presetIsEmpty ? 0.5f : 1f}"
                    android:background="@color/transparent"
                    android:clickable="@{viewModel.presetIsEmpty ? false : true}"
                    android:onClick="@{viewModel::onDeletePreset}"
                    android:src="@drawable/ic_delete_preset"
                    app:layout_widthPercent="15%"
                    tools:ignore="ContentDescription" />

                <ImageButton
                    android:id="@+id/redoDeletePresetButton"
                    android:layout_height="match_parent"
                    android:layout_alignParentEnd="true"
                    android:layout_alignParentRight="true"
                    android:layout_centerInParent="true"
                    android:background="@color/transparent"
                    android:onClick="@{viewModel::onRedoButtonPressed}"
                    android:src="@drawable/ic_undo_preset"
                    android:visibility="gone"
                    app:layout_widthPercent="15%"
                    tools:ignore="ContentDescription" />

                <ImageButton
                    android:id="@+id/fakeViewDelete"
                    android:layout_height="match_parent"
                    android:layout_alignParentEnd="true"
                    android:layout_alignParentRight="true"
                    android:layout_centerInParent="true"
                    android:background="@color/transparent"
                    android:visibility="gone"
                    app:layout_widthPercent="15%"
                    tools:ignore="ContentDescription" />

            </android.support.percent.PercentRelativeLayout>

            <LinearLayout
                android:layout_width="match_parent"
                android:layout_height="match_parent"
                android:layout_alignParentBottom="true"
                android:layout_below="@+id/presetButtonsLayout"
                android:layout_centerHorizontal="true"
                android:orientation="horizontal">

                <View
                    android:layout_width="0dp"
                    android:layout_height="match_parent"
                    android:layout_weight="1"
                    android:clickable="false"
                    android:focusableInTouchMode="false"
                    android:longClickable="false"
                    android:visibility="invisible" />


                <FrameLayout
                    android:id="@+id/playPauseLayout"
                    android:layout_width="0dp"
                    android:layout_height="match_parent"
                    android:layout_weight="1">

                    <LinearLayout
                        android:layout_width="match_parent"
                        android:layout_height="match_parent"
                        android:gravity="center">

                        <ImageButton
                            android:id="@+id/playButton"
                            style="@style/PlayPauseButton"
                            android:layout_marginLeft="6dp"
                            android:layout_marginStart="6dp"
                            android:alpha="@{viewModel.presetIsEmpty ? 0.5f : 1f}"
                            android:clickable="@{viewModel.presetIsEmpty ? false : true}"
                            android:onClick="@{viewModel::onPlayClicked}"
                            android:src="@drawable/ic_btn_play_big"
                            android:visibility="@{viewModel.isPlayButtonVisible ? View.VISIBLE : View.GONE}"
                            tools:ignore="ContentDescription"
                            tools:visibility="visible" />

                    </LinearLayout>

                    <LinearLayout
                        android:layout_width="match_parent"
                        android:layout_height="match_parent"
                        android:gravity="center">

                        <ImageView
                            android:layout_width="wrap_content"
                            android:layout_height="wrap_content"
                            android:alpha="@{viewModel.presetIsEmpty ? 0.5f : 1f}"
                            android:src="@drawable/ic_circle"
                            android:visibility="@{viewModel.isBuffering ? View.INVISIBLE : viewModel.isPauseButtonVisible || viewModel.isPlayButtonVisible || viewModel.isStopButtonVisible ? View.VISIBLE : View.INVISIBLE}" />

                    </LinearLayout>

                    <LinearLayout
                        android:layout_width="match_parent"
                        android:layout_height="match_parent"
                        android:gravity="center">

                        <ImageButton
                            android:id="@+id/pauseButton"
                            style="@style/PlayPauseButton"
                            android:onClick="@{viewModel::onPauseClicked}"
                            android:src="@drawable/btn_pause_with_circle_two_states"
                            android:visibility="@{viewModel.isPauseButtonVisible ? View.VISIBLE : View.GONE}"
                            tools:ignore="ContentDescription"
                            tools:visibility="visible" />

                    </LinearLayout>

                    <LinearLayout
                        android:layout_width="match_parent"
                        android:layout_height="match_parent"
                        android:gravity="center">

                        <ImageButton
                            android:id="@+id/stopButton"
                            style="@style/PlayPauseButton"
                            android:onClick="@{viewModel::onPauseClicked}"
                            android:src="@drawable/btn_stop_with_circle_two_states"
                            android:visibility="@{viewModel.isStopButtonVisible ? View.VISIBLE : View.GONE}"
                            tools:ignore="ContentDescription"
                            tools:visibility="visible" />

                    </LinearLayout>

                    <LinearLayout
                        android:layout_width="match_parent"
                        android:layout_height="match_parent"
                        android:gravity="center">

                        <ProgressBar
                            style="@style/BufferingProgress"
                            android:layout_gravity="center"
                            android:layout_marginRight="2dp"
                            android:indeterminate="true"
                            android:visibility="@{viewModel.isBuffering ? View.VISIBLE : View.INVISIBLE}"
                            tools:visibility="visible" />

                    </LinearLayout>
                </FrameLayout>

                <View
                    android:layout_width="0dp"
                    android:layout_height="match_parent"
                    android:layout_weight="1"
                    android:clickable="false"
                    android:focusableInTouchMode="false"
                    android:longClickable="false"
                    android:visibility="invisible" />

            </LinearLayout>

        </android.support.percent.PercentRelativeLayout>
    </android.support.percent.PercentRelativeLayout>
</layout>
