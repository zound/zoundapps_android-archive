<?xml version="1.0" encoding="utf-8"?>
<layout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools">

    <data>

        <import type="android.view.View" />

        <variable
            name="viewModel"
            type="com.zoundindustries.marshall.nowPlaying.NowPlayingViewModel" />
    </data>

    <RelativeLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:animateLayoutChanges="true">

        <ImageView
            android:id="@+id/blurredBackground"
            android:layout_width="match_parent"
            android:layout_height="match_parent"
            android:alpha="0.4"
            android:scaleType="centerCrop"
            android:src="@{viewModel.blurredBackground}"
            tools:ignore="ContentDescription" />

        <View
            android:layout_width="match_parent"
            android:layout_height="match_parent"
            android:background="@drawable/default_background" />

        <RelativeLayout
            android:id="@+id/actionBar"
            android:layout_width="match_parent"
            android:layout_height="?android:attr/actionBarSize">

            <FrameLayout
                android:id="@+id/sourceLayout"
                android:layout_width="@dimen/icon_size_in_nowplaying_footer"
                android:layout_height="wrap_content"
                android:layout_centerVertical="true"
                android:layout_marginEnd="@dimen/default_margin"
                android:layout_marginLeft="@dimen/default_margin"
                android:layout_marginRight="@dimen/default_margin"
                android:layout_marginStart="@dimen/default_margin">

                <ImageView
                    android:id="@+id/imageSource"
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    android:layout_gravity="center"
                    android:scaleType="centerInside"
                    android:src="@{viewModel.sourceImage}"
                    android:tint="@color/m_secondaryTextColor"
                    android:visibility="@{viewModel.sourceIsSelected ? View.VISIBLE : View.GONE}"
                    tools:ignore="ContentDescription" />

                <TextView
                    android:id="@+id/preset"
                    style="@style/NowPlayingPresetIndexColor"
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    android:layout_gravity="center"
                    android:text="@{viewModel.presetNumber}"
                    android:visibility="@{viewModel.presetIsSelected ? View.VISIBLE : View.GONE}"
                    tools:text="1"
                    tools:visibility="visible" />
            </FrameLayout>

            <LinearLayout
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:layout_centerVertical="true"
                android:layout_toEndOf="@+id/sourceLayout"
                android:layout_toLeftOf="@+id/buttonVolume"
                android:layout_toRightOf="@+id/sourceLayout"
                android:layout_toStartOf="@+id/buttonVolume"
                android:gravity="center"
                tools:ignore="UseCompoundDrawables">


                <ImageView
                    android:id="@+id/spotifyLogo"
                    android:layout_width="67dp"
                    android:layout_height="20dp"
                    android:layout_gravity="center_vertical"
                    android:alpha="0.9"
                    android:onClick="@{viewModel::onSpotifyLogoClicked}"
                    android:src="@drawable/ic_spotify_with_text"
                    android:visibility="@{viewModel.isSmallSpotifyLogoVisible ? View.VISIBLE : View.GONE}"
                    tools:ignore="ContentDescription" />

                <TextView
                    style="@style/NowPlayingTitle"
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    android:text="@{viewModel.castingAppName}"
                    android:visibility="@{viewModel.castingAppNameIsVisible ? View.VISIBLE : View.GONE}" />
            </LinearLayout>

            <ImageButton
                android:id="@+id/buttonVolume"
                style="@style/ActionBarButton"
                android:layout_alignParentEnd="true"
                android:layout_alignParentRight="true"
                android:src="@drawable/ic_volume_two_states"
                tools:ignore="ContentDescription" />
        </RelativeLayout>

        <FrameLayout
            android:id="@+id/auxinLayout"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_above="@+id/buttonDown"
            android:layout_below="@+id/actionBar"
            android:visibility="@{viewModel.auxLayoutIsVisible ? View.VISIBLE : View.GONE}"
            tools:visibility="visible">

            <android.support.percent.PercentFrameLayout
                android:layout_width="match_parent"
                android:layout_height="match_parent">

                <ImageView
                    android:layout_width="match_parent"
                    android:layout_centerHorizontal="true"
                    android:src="@drawable/player_placeholder_aux"
                    app:layout_aspectRatio="100%"
                    app:layout_heightPercent="44%"
                    tools:ignore="ContentDescription" />

            </android.support.percent.PercentFrameLayout>
        </FrameLayout>

        <FrameLayout
            android:id="@+id/rcaLayout"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_above="@+id/buttonDown"
            android:layout_below="@+id/actionBar"
            android:visibility="@{viewModel.rcaLayoutIsVisible ? View.VISIBLE : View.GONE}"
            tools:visibility="visible">

            <android.support.percent.PercentFrameLayout
                android:layout_width="match_parent"
                android:layout_height="match_parent">

                <ImageView
                    android:layout_width="match_parent"
                    android:layout_centerHorizontal="true"
                    android:src="@drawable/player_placeholder_rca"
                    app:layout_aspectRatio="100%"
                    app:layout_heightPercent="44%"
                    tools:ignore="ContentDescription" />

            </android.support.percent.PercentFrameLayout>
        </FrameLayout>

        <FrameLayout
            android:id="@+id/bluetoothLayout"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_above="@+id/buttonDown"
            android:layout_below="@+id/actionBar"
            android:visibility="@{viewModel.bluetoothLayoutIsVisible ? View.VISIBLE : View.GONE}"
            tools:visibility="visible">

            <android.support.percent.PercentFrameLayout
                android:layout_width="match_parent"
                android:layout_height="match_parent">

                <ImageView
                    android:layout_width="match_parent"
                    android:layout_centerHorizontal="true"
                    android:src="@drawable/player_placeholder_bluetooth"
                    app:layout_aspectRatio="100%"
                    app:layout_heightPercent="44%"
                    tools:ignore="ContentDescription" />

            </android.support.percent.PercentFrameLayout>
        </FrameLayout>

        <android.support.percent.PercentRelativeLayout
            android:layout_width="match_parent"
            android:layout_height="match_parent"
            android:layout_above="@+id/buttonDown"
            android:layout_below="@+id/actionBar"
            android:layout_marginBottom="@dimen/default_margin"
            android:layout_marginTop="@dimen/default_margin_small"
            android:visibility="@{viewModel.auxLayoutIsVisible ? View.GONE : View.VISIBLE}">

            <ImageView
                android:id="@+id/artworkImageView"
                android:layout_alignParentTop="true"
                android:layout_centerHorizontal="true"
                android:adjustViewBounds="true"
                android:scaleType="fitCenter"
                android:src="@{viewModel.artwork}"
                android:visibility="@{viewModel.bluetoothLayoutIsVisible || viewModel.rcaLayoutIsVisible ? View.INVISIBLE : View.VISIBLE}"
                app:layout_aspectRatio="100%"
                app:layout_heightPercent="44%"
                tools:ignore="ContentDescription" />

            <TextView
                android:id="@+id/textTrackElapsed"
                style="@style/TextSmallDark"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_alignBottom="@+id/artworkImageView"
                android:layout_marginBottom="-8dp"
                android:layout_marginEnd="8dp"
                android:layout_marginRight="8dp"
                android:layout_toLeftOf="@+id/artworkImageView"
                android:layout_toStartOf="@+id/artworkImageView"
                android:text="@{viewModel.friendlyElapsedTime}"
                android:visibility="@{viewModel.isElapsedTimeVisible ? View.VISIBLE : View.GONE}"
                tools:ignore="RelativeOverlap"
                tools:text="0:42" />

            <TextView
                android:id="@+id/textTrackDuration"
                style="@style/TextSmallDark"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_alignBottom="@+id/artworkImageView"
                android:layout_marginBottom="-8dp"
                android:layout_marginLeft="8dp"
                android:layout_marginStart="8dp"
                android:layout_toEndOf="@+id/artworkImageView"
                android:layout_toRightOf="@+id/artworkImageView"
                android:text="@{viewModel.friendlyDuration}"
                android:visibility="@{viewModel.isElapsedTimeVisible ? View.VISIBLE : View.GONE}"
                tools:text="3:00"
                tools:visibility="visible" />

            <ProgressBar
                android:id="@+id/progressBarPlayPosition"
                style="@style/DefaultProgressBar"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:layout_alignEnd="@+id/artworkImageView"
                android:layout_alignLeft="@+id/artworkImageView"
                android:layout_alignRight="@+id/artworkImageView"
                android:layout_alignStart="@+id/artworkImageView"
                android:layout_below="@+id/artworkImageView"
                android:indeterminate="false"
                android:progress="@{viewModel.elapsedTime}"
                android:visibility="@{viewModel.isProgressBarVisible ? View.VISIBLE : View.GONE}"
                tools:progress="50" />

            <SeekBar
                android:id="@+id/seekBarPlayPosition"
                style="@style/NowPlayingSeekBar"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:layout_alignEnd="@+id/artworkImageView"
                android:layout_alignLeft="@+id/artworkImageView"
                android:layout_alignRight="@+id/artworkImageView"
                android:layout_alignStart="@+id/artworkImageView"
                android:layout_below="@+id/artworkImageView"
                android:progress="@{viewModel.elapsedTime}"
                android:visibility="@{viewModel.isSeekBarBarVisible ? View.VISIBLE : View.GONE}"
                tools:progress="50" />

            <LinearLayout
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:layout_below="@+id/artworkImageView"
                android:layout_marginTop="@dimen/default_margin_small"
                android:gravity="center_horizontal|top"
                android:orientation="vertical">

                <TextView
                    android:id="@+id/multiOrSpeakerName"
                    style="@style/TextNormalWhiteBold"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:layout_marginLeft="@dimen/default_margin_bigger"
                    android:layout_marginRight="@dimen/default_margin_bigger"
                    android:layout_marginTop="@dimen/default_margin_very_small"
                    android:ellipsize="marquee"
                    android:focusable="true"
                    android:focusableInTouchMode="true"
                    android:freezesText="true"
                    android:gravity="center"
                    android:lines="1"
                    android:marqueeRepeatLimit="marquee_forever"
                    android:maxLines="1"
                    android:scrollHorizontally="true"
                    android:singleLine="true"
                    android:text="@{viewModel.secondLine}"
                    tools:ignore="Deprecated"
                    tools:text="Track name" />

                <TextView
                    android:id="@+id/nowPlayingContent"
                    style="@style/TextSmallWhite"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:layout_marginLeft="@dimen/default_margin_bigger"
                    android:layout_marginRight="@dimen/default_margin_bigger"
                    android:ellipsize="marquee"
                    android:focusable="true"
                    android:focusableInTouchMode="true"
                    android:freezesText="true"
                    android:gravity="center"
                    android:lines="1"
                    android:marqueeRepeatLimit="marquee_forever"
                    android:maxLines="1"
                    android:scrollHorizontally="true"
                    android:singleLine="true"
                    android:text="@{viewModel.firstLine}"
                    tools:ignore="Deprecated"
                    tools:text="Artist name" />

                <LinearLayout
                    android:id="@+id/playShuffleButtonsLayout"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:layout_marginLeft="@dimen/default_margin"
                    android:layout_marginRight="@dimen/default_margin"
                    android:layout_marginTop="@dimen/default_margin_small"
                    android:layout_marginBottom="@dimen/default_margin_small"
                    android:baselineAligned="false"
                    android:orientation="horizontal">

                    <FrameLayout
                        android:layout_width="0dp"
                        android:layout_height="wrap_content"
                        android:layout_gravity="center_vertical"
                        android:layout_weight="1">

                        <ToggleButton
                            android:id="@+id/toggleShuffle"
                            android:layout_width="22dp"
                            android:layout_height="18dp"
                            android:background="@drawable/btn_shuffle"
                            android:checked="@{viewModel.isShuffleChecked}"
                            android:onClick="@{viewModel::onShuffleClicked}"
                            android:textOff=""
                            android:textOn=""
                            android:visibility="@{viewModel.isShuffleVisible ? View.VISIBLE : View.GONE}" />
                    </FrameLayout>

                    <FrameLayout
                        android:layout_width="0dp"
                        android:layout_height="wrap_content"
                        android:layout_gravity="center_vertical"
                        android:layout_weight="1">

                        <ImageButton
                            android:id="@+id/buttonPrev"
                            style="@style/NowPlayingPrevNextButton"
                            android:layout_gravity="center"
                            android:onClick="@{viewModel::onPreviousClicked}"
                            android:src="@drawable/ic_skip_prev_two_states"
                            android:visibility="@{viewModel.isPrevButtonVisible ? View.VISIBLE : View.GONE}"
                            tools:ignore="ContentDescription" />
                    </FrameLayout>

                    <FrameLayout
                        android:id="@+id/playButtonsLayout"
                        android:layout_width="0dp"
                        android:layout_height="wrap_content"
                        android:layout_weight="2">

                        <LinearLayout
                            android:layout_width="match_parent"
                            android:layout_height="match_parent"
                            android:gravity="center">

                            <ProgressBar
                                style="@style/BufferingProgress"
                                android:layout_width="match_parent"
                                android:layout_height="match_parent"
                                android:layout_gravity="center"
                                android:alpha="@{viewModel.isPlayPauseStopButtonEnabled ? 1.0f : 0.3f}"
                                android:indeterminate="true"
                                android:visibility="@{viewModel.isBuffering ? View.VISIBLE : View.GONE}"
                                tools:visibility="visible" />

                        </LinearLayout>

                        <LinearLayout
                            android:layout_width="match_parent"
                            android:layout_height="match_parent"
                            android:gravity="center">

                            <ImageView
                                android:layout_width="wrap_content"
                                android:layout_height="wrap_content"
                                android:src="@drawable/ic_circle"
                                android:visibility="@{viewModel.isBuffering ? View.INVISIBLE : viewModel.isPauseButtonVisible || viewModel.isPlayButtonVisible || viewModel.isStopButtonVisible ? View.VISIBLE : View.INVISIBLE}"
                                tools:visibility="invisible" />

                        </LinearLayout>

                        <LinearLayout
                            android:layout_width="match_parent"
                            android:layout_height="match_parent"
                            android:gravity="center">

                            <ImageButton
                                android:layout_marginLeft="6dp"
                                android:layout_marginStart="6dp"
                                android:id="@+id/playButton"
                                style="@style/PlayPauseButton"
                                android:alpha="@{viewModel.isPlayPauseStopButtonEnabled ? 1.0f : 0.3f}"
                                android:clickable="@{viewModel.isPlayPauseStopButtonEnabled}"
                                android:enabled="@{viewModel.isPlayPauseStopButtonEnabled}"
                                android:onClick="@{viewModel::onPlayClicked}"
                                android:src="@drawable/btn_play_with_circle_two_states"
                                android:visibility="@{viewModel.isPlayButtonVisible ? View.VISIBLE : View.GONE}"
                                tools:ignore="ContentDescription"
                                tools:visibility="visible" />

                        </LinearLayout>

                        <LinearLayout
                            android:layout_width="match_parent"
                            android:layout_height="match_parent"
                            android:gravity="center">

                            <ImageButton
                                android:id="@+id/pauseButton"
                                style="@style/PlayPauseButton"
                                android:alpha="@{viewModel.isPlayPauseStopButtonEnabled ? 1.0f : 0.3f}"
                                android:clickable="@{viewModel.isPlayPauseStopButtonEnabled}"
                                android:enabled="@{viewModel.isPlayPauseStopButtonEnabled}"
                                android:onClick="@{viewModel::onPauseClicked}"
                                android:src="@drawable/btn_pause_with_circle_two_states"
                                android:visibility="@{viewModel.isPauseButtonVisible ? View.VISIBLE : View.GONE}"
                                tools:ignore="ContentDescription"
                                tools:visibility="visible" />

                        </LinearLayout>

                        <LinearLayout
                            android:layout_width="match_parent"
                            android:layout_height="match_parent"
                            android:gravity="center">

                            <ImageButton
                                android:id="@+id/stopButton"
                                style="@style/PlayPauseButton"
                                android:alpha="@{viewModel.isPlayPauseStopButtonEnabled ? 1.0f : 0.3f}"
                                android:clickable="@{viewModel.isPlayPauseStopButtonEnabled}"
                                android:enabled="@{viewModel.isPlayPauseStopButtonEnabled}"
                                android:onClick="@{viewModel::onPauseClicked}"
                                android:src="@drawable/btn_stop_with_circle_two_states"
                                android:visibility="@{viewModel.isStopButtonVisible ? View.VISIBLE : View.GONE}"
                                tools:ignore="ContentDescription"
                                tools:visibility="visible" />

                        </LinearLayout>

                    </FrameLayout>

                    <FrameLayout
                        android:layout_width="0dp"
                        android:layout_height="wrap_content"
                        android:layout_gravity="center_vertical"
                        android:layout_weight="1">

                        <ImageButton
                            android:id="@+id/buttonNext"
                            style="@style/NowPlayingPrevNextButton"
                            android:layout_gravity="center_vertical"
                            android:onClick="@{viewModel::onNextClicked}"
                            android:src="@drawable/ic_skip_next_two_states"
                            android:visibility="@{viewModel.isNextButtonVisible ? View.VISIBLE : View.GONE}"
                            tools:ignore="ContentDescription" />
                    </FrameLayout>

                    <FrameLayout
                        android:layout_width="0dp"
                        android:layout_height="wrap_content"
                        android:layout_gravity="center_vertical"
                        android:layout_weight="1">

                        <ToggleButton
                            android:id="@+id/toggleRepeat"
                            android:layout_width="20dp"
                            android:layout_height="17dp"
                            android:layout_gravity="end"
                            android:background="@drawable/btn_repeat"
                            android:checked="@{viewModel.isRepeatChecked}"
                            android:onClick="@{viewModel::onRepeatClicked}"
                            android:textOff=""
                            android:textOn=""
                            android:visibility="@{viewModel.isRepeatVisible ? View.VISIBLE : View.GONE}" />
                    </FrameLayout>

                </LinearLayout>

                <LinearLayout
                    android:layout_width="match_parent"
                    android:layout_height="match_parent"
                    android:gravity="center"
                    android:visibility="@{viewModel.buttonsAreVisible ? View.VISIBLE : View.GONE}"
                    android:orientation="vertical">

                    <Button
                        android:id="@+id/buttonAddAsPreset"
                        style="?attr/AddToPresetButton"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:layout_marginTop="@dimen/default_margin_small"
                        android:layout_marginLeft="@dimen/default_margin_bigger"
                        android:layout_marginRight="@dimen/default_margin_bigger"
                        android:onClick="@{viewModel::onAddAsPresetClicked}"
                        android:text="@string/now_playing_add_to_preset"
                        android:singleLine="true"
                        android:textAllCaps="true"
                        android:visibility="@{viewModel.addAsPresetIsVisible ? View.VISIBLE : View.GONE}" />

                    <Button
                        android:id="@+id/buttonBrowseStations"
                        style="?attr/ButtonWithOutline"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:layout_marginLeft="@dimen/default_margin_bigger"
                        android:layout_marginRight="@dimen/default_margin_bigger"
                        android:layout_marginTop="@dimen/default_margin_small"
                        android:onClick="@{viewModel::onBrowseStationsClicked}"
                        android:text="@string/now_playing_browse_radio"
                        android:singleLine="true"
                        android:textAllCaps="true"
                        android:visibility="@{viewModel.browseStationsIsVisible ? View.VISIBLE : View.GONE}" />

                </LinearLayout>

            </LinearLayout>
        </android.support.percent.PercentRelativeLayout>

        <LinearLayout
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_alignTop="@+id/buttonDown"
            android:layout_gravity="center_horizontal"
            android:layout_marginBottom="@dimen/default_margin_small"
            android:gravity="center"
            android:orientation="horizontal">

            <TextView
                style="@style/NowPlayingFooterText"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:text="@string/multi_caps"
                android:visibility="@{viewModel.isInMultiMode ? View.VISIBLE : View.GONE}" />

            <TextView
                android:id="@+id/titleTextView"
                style="@style/NowPlayingFooterText"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_gravity="center_vertical"
                android:visibility="@{viewModel.isInMultiMode ? View.GONE : View.VISIBLE}"
                tools:text="@string/app_name" />
        </LinearLayout>

        <FrameLayout
            android:id="@+id/buttonDown"
            android:layout_width="match_parent"
            android:layout_height="40dp"
            android:layout_alignParentBottom="true">

            <ImageView
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_gravity="center_horizontal|top"
                android:layout_marginTop="@dimen/default_margin"
                android:src="@drawable/ic_down_two_states"
                tools:ignore="ContentDescription" />
        </FrameLayout>
    </RelativeLayout>

</layout>